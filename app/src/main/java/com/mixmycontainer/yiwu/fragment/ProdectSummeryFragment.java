package com.mixmycontainer.yiwu.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.Boothdetailactivity;
import com.mixmycontainer.yiwu.activity.ItemDeatailActivity;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.bus.BusFactory;
import com.mixmycontainer.yiwu.bus.events.RefreshEvent;
import com.mixmycontainer.yiwu.bus.events.RefreshSingleViewOrder;
import com.mixmycontainer.yiwu.models.BoothDetails;
import com.mixmycontainer.yiwu.models.ContainerDetails;
import com.mixmycontainer.yiwu.models.ItemDetails;
import com.mixmycontainer.yiwu.models.OrderListDetails;
import com.mixmycontainer.yiwu.realm.MyApplication;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 03-12-2016.
 */
public class ProdectSummeryFragment extends BaseFragment implements View.OnClickListener {
    RealmList<BoothDetails> boothDetailses;
    RealmList<ContainerDetails> containerDetailses;
    RealmList<ItemDetails> itemDetailses;
    private Realm mRealm;
    private MenuItem menuItem;
    private Button btNewItem, btNewBooth, btEndOrder;
    private ProgressBar progressBarSmall, progressBar;
    private TextView tvOrdernumber, tvDolerVlaue, tvRMBValue, tvTotalCBM, tvTotalWeight;

    public static ProdectSummeryFragment getnewInStance() {
        ProdectSummeryFragment fragment = new ProdectSummeryFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        mRealm = Realm.getDefaultInstance();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        menuItem = menu.add("action");
        menuItem.setTitle(getResources().getString(R.string.txt_back_to_orders));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    }

    @Override
    public int getTitle() {
        return R.string.txt_prodect_summery;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getResources().getString(R.string.txt_back_to_orders))) {
            String toPage = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ADD, "0");
            String toPageORDER_ID = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
            String prefEditBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_BOOTH_ID,"0");
            if (toPage.equals("continueOrder")) {
                SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                PREF.putString(PreferenceUtil.ORDER_ADD, "0");
                PREF.putString(PreferenceUtil.ITEM_ID, "0");
                PREF.putString(PreferenceUtil.BOOTH_ID, "0");
                PREF.putString(PreferenceUtil.ORDER_ID, toPageORDER_ID);
                PREF.apply();
                BusFactory.getBus().post(new RefreshSingleViewOrder());
                BusFactory.getBus().post(new RefreshEvent());
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            } else {
                SharedPreferences.Editor PREFe = PreferenceUtil.edit(getActivity());
                PREFe.putString(PreferenceUtil.ITEM_ID, "0");
                PREFe.putString(PreferenceUtil.BOOTH_ID, "0");
                PREFe.putString(PreferenceUtil.ORDER_ID, "0");
                PREFe.apply();
                BusFactory.getBus().post(new RefreshEvent());
                if (!prefEditBoothId.equals("0"))
                {
                    BusFactory.getBus().post(new RefreshSingleViewOrder());

                }
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prodect_summery, container, false);
        initview(view);
        inilestener();
        setEnableBackButton(false);
        getSingleOrderDetailsOffline();

        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        final String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");
        String prefItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ITEM_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    BoothDetails boothExistOrNot = mRealm.where(BoothDetails.class)
                         .equalTo("OrderId", prefOrderId).equalTo("BoothId", prefBoothId).findFirst();
                    if (boothExistOrNot != null) {
                        boothExistOrNot.setBoothStatus("0");
                        System.out.println("status setbooth end");

                    }
                    OrderListDetails orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).findFirst();
                    if (orderListDetails != null) {
                        orderListDetails.setOrderStatus("0");
                        System.out.println("status setOrder end");
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }
//        if (AppUtils.isNetworkAvailable(getActivity())) {
//            try {
//                PauseOrderOffline();
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
        return view;
    }

    private void deleteUnusedBooth() {
        final int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();
                    for (int i=0;i<orderListDetails.size();i++)
                    {
                        RealmList<BoothDetails> boothDetails = orderListDetails.get(i).getBoothDetailses();
                        for (int j=0;j<boothDetails.size();j++)
                        {
                            int usedBoothCount=0;
                            RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();
                            for (int k=0;k<itemDetails.size();k++)
                            {
                                if (boothDetails.get(j).getBoothId().equals(itemDetails.get(k).getBoothId()))
                                {
                                    usedBoothCount++;
                                }

                            }
                            if (usedBoothCount==0)
                            {
                                boothDetails.deleteFromRealm(j);
                            }
                            mRealm.insertOrUpdate(boothDetails);
                        }
                        mRealm.insertOrUpdate(orderListDetails);
                    }
                }
            });

        }
        catch (Exception e)
        {
            e.getMessage();
        }
        finally {
            if (mRealm!=null)
            {
                mRealm.close();
            }
        }

    }

    private void getSingleOrderDetailsOffline() {
        final int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");
        String prefItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ITEM_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();
                    if (!OrderListDetailsDetails.isEmpty()) {

                        for (int i = OrderListDetailsDetails.size() - 1; i >= 0; i--) {

                            tvOrdernumber.setText(OrderListDetailsDetails.get(i).getOrderNumber());
                            tvDolerVlaue.setText("$" + OrderListDetailsDetails.get(i).getTotalOrderUsValue());
                            tvRMBValue.setText("¥" + OrderListDetailsDetails.get(i).getTotalOrderRmbValue());
                            tvTotalWeight.setText(OrderListDetailsDetails.get(i).getTotalOrderWeight());
                            tvTotalCBM.setText(OrderListDetailsDetails.get(i).getTotalOrderCBM());


                            boothDetailses = OrderListDetailsDetails.get(i).getBoothDetailses();
                            containerDetailses = OrderListDetailsDetails.get(i).getContainerDetailses();

                            for (int k = 0; k < OrderListDetailsDetails.get(i).getContainerDetailses().size(); k++) {

//                                if (OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getStatus().equals("0")) {
                                    double volum = Double.parseDouble(OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getVolumepercentage());
                                    double weight = Double.parseDouble(OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getWeightpercentage());
                                    if ((int) volum > (int) weight) {
                                        progressBar.setProgress((int) volum);
                                    }
                                    if ((int) volum < (int) weight) {
                                        progressBar.setProgress((int) weight);
                                    }
//                                }
                            }


                        }

                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


    }

    private void inilestener() {
        btEndOrder.setOnClickListener(this);
        btNewItem.setOnClickListener(this);
        btNewBooth.setOnClickListener(this);
    }

    private void initview(View view) {
        btNewItem = (Button) view.findViewById(R.id.btNewItem);
        btNewBooth = (Button) view.findViewById(R.id.btNewBooth);
        btEndOrder = (Button) view.findViewById(R.id.btEndOrder);
        progressBarSmall = (ProgressBar) view.findViewById(R.id.progressBarSmall);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        tvOrdernumber = (TextView) view.findViewById(R.id.tvOrdernumber);

        tvDolerVlaue = (TextView) view.findViewById(R.id.tvDolerVlaue);
        tvRMBValue = (TextView) view.findViewById(R.id.tvRMBValue);
        tvTotalCBM = (TextView) view.findViewById(R.id.tvTotalCBM);
        tvTotalWeight = (TextView) view.findViewById(R.id.tvTotalWeight);
    }

    @Override
    public void onClick(View view) {
        if (view == btNewBooth) {
            progressBarSmall.setVisibility(View.VISIBLE);
//            newBooth();
            newBoothOffline();
        } else if (view == btNewItem) {

            btNewItem.setTextColor(getResources().getColor(R.color.white));
            btNewItem.setBackground(getResources().getDrawable(R.drawable.yellow_button_click));
            SharedPreferences.Editor PREFe = PreferenceUtil.edit(getActivity());
            PREFe.putString(PreferenceUtil.ITEM_ID, "0");
            PREFe.apply();
            BusFactory.getBus().post(new RefreshSingleViewOrder());
            getActivity().finish();
            startActivity(ItemDeatailActivity.getnewIntent(getActivity()));
            getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
        } else if (view == btEndOrder) {
            btEndOrder.setTextColor(getResources().getColor(R.color.white));
            btEndOrder.setBackground(getResources().getDrawable(R.drawable.yellow_button_click));
            progressBarSmall.setVisibility(View.VISIBLE);

            EndOrderViewOrderOffline();

        }
    }

    private void newBoothOffline() {

        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        final String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");
        String prefItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ITEM_ID, "0");

        SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
        PREF.remove(PreferenceUtil.USER_ITEMDETAIL_ID);
        PREF.remove(PreferenceUtil.USER_BOOTH_ID);
        PREF.commit();
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    BoothDetails boothExistOrNot = mRealm.where(BoothDetails.class)
                         .equalTo("OrderId", prefOrderId).equalTo("BoothId", prefBoothId).findFirst();
                    if (boothExistOrNot != null) {

                        boothExistOrNot.setBoothStatus("1");


                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

        SharedPreferences.Editor PREFe = PreferenceUtil.edit(getActivity());
        PREFe.putString(PreferenceUtil.BOOTH_ID, "0");
        PREFe.apply();
        getActivity().finish();

        startActivity(Boothdetailactivity.getnewIntent(getActivity(), ""));
        getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

    private void EndOrderViewOrderOffline() {
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        final String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");
        final String prefToken = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AUTH_TOKEN,"0");
        String prefItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ITEM_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    BoothDetails boothExistOrNot = mRealm.where(BoothDetails.class)
                         .equalTo("OrderId", prefOrderId).equalTo("BoothId", prefBoothId).findFirst();
                    if (boothExistOrNot != null) {
                        boothExistOrNot.setBoothStatus("1");
                        System.out.println("status setbooth end");

                    }
                    OrderListDetails orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).findFirst();
                    if (orderListDetails != null) {
                        orderListDetails.setOrderStatus("1");
                        System.out.println("status setOrder end");
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }
        if (AppUtils.isNetworkAvailable(getActivity())) {
            try {
                uploadToserver();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            SharedPreferences.Editor PREFe = PreferenceUtil.edit(getActivity());
            PREFe.putString(PreferenceUtil.ITEM_ID, "0");
            PREFe.putString(PreferenceUtil.BOOTH_ID, "0");
            PREFe.putString(PreferenceUtil.ORDER_ID, "0");
            PREFe.apply();
//            Intent intent = new Intent(getActivity(), OrderListActivity.class);
//            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//            startActivity(intent);
            BusFactory.getBus().post(new RefreshEvent());
            getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            getActivity().finish();

        }
        if (AppUtils.isNetworkAvailable(getActivity())) {
            sendMail(prefToken, prefOrderId);
        } else {
            Toast.makeText(getActivity(), getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
        }

    }

    private void sendMail(String token, String orderId) {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        Ion.with(getActivity())
                .load(URL.Send_Mail_Order + token + "&orderid=" + orderId)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override

                    public void onCompleted(Exception e, JsonObject result) {
                        progressDialog.dismiss();
                        try {
                            String requestCode = result.get("resultCode").getAsString();
                            Log.d("RESPONSE", new Gson().toJson(result.toString()));

                            if (requestCode.equals("1")) {
                                Toast.makeText(getActivity(),result.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(getActivity(), result.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                            }
                        } catch (JsonParseException c) {
                            Toast.makeText(getActivity(), c.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            c.printStackTrace();
                        } catch (Exception e1) {
                            Toast.makeText(getActivity(), e1.getMessage().toString(), Toast.LENGTH_SHORT).show();
                            e1.printStackTrace();
                        }
                    }

                });
    }


    private void uploadToserver() {
        final int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");
        String prefItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ITEM_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    JSONObject dataMain = null;
                    dataMain = new JSONObject();
                    JSONObject data = null;
                    JSONArray jsonData = new JSONArray();
                    RealmResults<OrderListDetails> OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();
                    if (!OrderListDetailsDetails.isEmpty()) {

                        for (int i = OrderListDetailsDetails.size() - 1; i >= 0; i--) {
                            data = new JSONObject();

                            String OrderNumber = OrderListDetailsDetails.get(i).getOrderNumber();
                            OrderListDetailsDetails.get(i).setSynch("1");
                            String CompanyId = String.valueOf(OrderListDetailsDetails.get(i).getCompanyID());
                            String Trade = String.valueOf(OrderListDetailsDetails.get(i).getTrade());
                            String ProductType = String.valueOf(OrderListDetailsDetails.get(i).getProductType());
                            String TotalOrderUsValue = String.valueOf(OrderListDetailsDetails.get(i).getTotalOrderUsValue());
                            String TotalOrderRmbValue = String.valueOf(OrderListDetailsDetails.get(i).getTotalOrderRmbValue());
                            String TotalOrderZarValue = String.valueOf(OrderListDetailsDetails.get(i).getTotalOrderZarValue());
                            String Total3rdCurrencyValue = String.valueOf(OrderListDetailsDetails.get(i).getTotal3rdCurrencyValue());
                            String TotalCartons = String.valueOf(OrderListDetailsDetails.get(i).getTotalCartons());
                            String TotalBooth = String.valueOf(OrderListDetailsDetails.get(i).getBoothDetailses().size());
                            String TotalContainer = String.valueOf(OrderListDetailsDetails.get(i).getTotalContainer());
                            String TotalOrderWeight = String.valueOf(OrderListDetailsDetails.get(i).getTotalOrderWeight());
                            String TotalOrderCBM = String.valueOf(OrderListDetailsDetails.get(i).getTotalOrderCBM());
                            String OrderStatus = String.valueOf(OrderListDetailsDetails.get(i).getOrderStatus());
                            String LastUpdate = String.valueOf(OrderListDetailsDetails.get(i).getLastUpdate());
                            try {
                                data.put("OrderId", OrderListDetailsDetails.get(i).getOrderId());

                                OrderListDetailsDetails.get(i).setSynch("1");

                                data.put("OrderNumber", OrderNumber);
                                data.put("CompanyId", CompanyId);
                                data.put("Trade", Trade);
                                data.put("ProductType", ProductType);
                                data.put("TotalOrderUsValue", TotalOrderUsValue);
                                data.put("TotalOrderRmbValue", TotalOrderRmbValue);
                                data.put("TotalOrderZarValue", TotalOrderZarValue);
                                data.put("Total3rdCurrencyValue", Total3rdCurrencyValue);
                                data.put("TotalCartons", TotalCartons);
                                data.put("TotalBooth", TotalBooth);
                                data.put("TotalContainer", TotalContainer);
                                data.put("TotalOrderWeight", TotalOrderWeight);
                                data.put("TotalOrderCBM", TotalOrderCBM);
                                data.put("OrderStatus", OrderStatus);
                                data.put("LastUpdate", LastUpdate);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            JSONObject dataObj = null;
                            JSONArray containerDetailsOnlineModels = new JSONArray();

                            for (int k = 0; k < OrderListDetailsDetails.get(i).getContainerDetailses().size(); k++) {

                                dataObj = new JSONObject();
                                String ContainerId = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerId();
                                String ContainerType = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerType();
                                try {
                                    dataObj.put("ContainerId", ContainerId);
                                    dataObj.put("ContainerType", ContainerType);
                                    dataObj.put("OrderId", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getOrderId());
                                    dataObj.put("CompanyId", String.valueOf(companyId));
                                    dataObj.put("FilledWeight", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getFilledWeight());
                                    dataObj.put("FilledVolume", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getFilledVolume());

                                    dataObj.put("Volumepercentage", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getVolumepercentage());

                                    dataObj.put("Weightpercentage", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getWeightpercentage());
                                    dataObj.put("Status", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getStatus());
                                    dataObj.put("ItemSaveStatus", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getStatus());
                                    dataObj.put("ID", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerId());
                                    dataObj.put("Container", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerType());
                                    dataObj.put("MaximumVolume", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getMaximumVolume());
                                    dataObj.put("MaximumWeight", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getMaximumWeight());
                                    containerDetailsOnlineModels.put(dataObj);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                            try {
                                data.put("containerdetailsbyorderid", containerDetailsOnlineModels);

                                JSONObject dataObjj = null;
                                JSONArray boothDetailOnlineModels = new JSONArray();
                                int getBoothDetailsesSize = OrderListDetailsDetails.get(i).getBoothDetailses().size();
                                for (int k = 0; k < getBoothDetailsesSize; k++) {
                                    dataObjj = new JSONObject();
                                    dataObjj.put("BoothId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBoothId());
                                    dataObjj.put("OrderId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getOrderId());
                                    dataObjj.put("CompanyId", String.valueOf(companyId));
                                    dataObjj.put("BoothNumber", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBoothNumber());
                                    dataObjj.put("BoothName", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBoothName());
                                    dataObjj.put("Phone", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getPhone());
                                    dataObjj.put("BusinessScope", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBusinessScope());
                                    dataObjj.put("ValuePurchase", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getValuePurchase());
                                    dataObjj.put("ValuePurchaseRMB", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getValuePurchaseRMB());
                                    dataObjj.put("BusinessCard", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBusinessCard());
                                    dataObjj.put("BoothStatus", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBoothStatus());
                                    dataObjj.put("TradeId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getTradeId());
                                    dataObjj.put("HallId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getHallId());

                                    JSONObject dataObjjj = null;
                                    JSONArray itemDetailOnlineModels = new JSONArray();

//                                    for (int f = 0; f < OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().size(); f++) {
//                                        dataObjjj = new JSONObject();
//                                        dataObjjj.put("ItemId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getItemId());
//                                        dataObjjj.put("CompanyId", String.valueOf(companyId));
//                                        dataObjjj.put("OrderId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getOrderId());
//                                        dataObjjj.put("BoothId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getBoothId());
//                                        dataObjjj.put("ContainerId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getContainerId());
//                                        dataObjjj.put("ItemNo", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getItemNo());
//                                        dataObjjj.put("DescriptionFrom", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getDescriptionFrom());
//                                        dataObjjj.put("DescriptionTo", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getDescriptionTo());
//                                        dataObjjj.put("UnitperCarton", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getUnitperCarton());
//                                        dataObjjj.put("WeightofCarton", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getWeightofCarton());
//                                        dataObjjj.put("Length", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getLength());
//                                        dataObjjj.put("Width", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getWidth());
//                                        dataObjjj.put("Height", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getHeight());
//                                        dataObjjj.put("CBM", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getCBM());
//                                        dataObjjj.put("Picture", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getPicture());
//                                        dataObjjj.put("PriceInUs", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getPriceInUs());
//                                        dataObjjj.put("PriceInRmb", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getPriceInRmb());
//                                        dataObjjj.put("ThirdCurrencyType", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getThirdCurrencyType());
//                                        dataObjjj.put("PriceInThirdCurrency", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getPriceInThirdCurrency());
//                                        dataObjjj.put("UnitQuantity", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getUnitQuantity());
//                                        dataObjjj.put("CartonQuantity", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getCartonQuantity());
//                                        dataObjjj.put("TotalPriceInUS", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getTotalPriceInUS());
//                                        dataObjjj.put("TotalPriceInRMB", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getTotalPriceInRMB());
//                                        dataObjjj.put("TotalPriceIn3rdCurrency", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getTotalPriceIn3rdCurrency());
//                                        dataObjjj.put("Date", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getItemDetailses().get(f).getDate());
//                                        itemDetailOnlineModels.put(dataObjjj);
//                                    }
//                                    dataObjj.put("itemdetailsbyboothorderid", itemDetailOnlineModels);
                                    boothDetailOnlineModels.put(dataObjj);

                                }
                                data.put("boothdetailsbyorderid", boothDetailOnlineModels);
                                jsonData.put(data);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }


                        try {
                            dataMain.put("data", jsonData);
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                        }

                        String url = "https://mixmycontainer.com/QA/Api_controller/offlinesyncorder";
// Tag used to cancel the request
                        String tag_json_obj = "json_obj_req";
                        JSONObject params = new JSONObject();
                        try {
                            params.put("data", jsonData);

                        } catch (JSONException e) {
                            e.printStackTrace();

                        }

//                        final ProgressDialog pDialog = new ProgressDialog(getActivity());
//                        pDialog.setMessage("Loading...");
//                        pDialog.setCanceledOnTouchOutside(false);
//                        pDialog.show();

                        JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                             url, params,
                             new Response.Listener<JSONObject>() {

                                 @Override
                                 public void onResponse(JSONObject response) {

//                                     pDialog.hide();
                                     String resultcode = null;
                                     try {
                                         resultcode = response.getString("resultCode").toString();
                                         if (resultcode.equals("1")) {
                                             Toast.makeText(getContext(), response.getString("message").toString() + " ", Toast.LENGTH_SHORT).show();
                                             SharedPreferences.Editor PREFe = PreferenceUtil.edit(getActivity());
                                             PREFe.putString(PreferenceUtil.ITEM_ID, "0");
                                             PREFe.putString(PreferenceUtil.BOOTH_ID, "0");
                                             PREFe.putString(PreferenceUtil.ORDER_ID, "0");
                                             PREFe.apply();
//                                             Intent intent = new Intent(getActivity(), OrderListActivity.class);
////                                             intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//                                             startActivity(intent);
                                             BusFactory.getBus().post(new RefreshEvent());
                                             getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
                                             getActivity().finish();
                                         } else {
                                             Toast.makeText(getContext(), "not success try again.", Toast.LENGTH_SHORT).show();
                                         }
                                     } catch (JSONException e) {
                                         e.printStackTrace();
                                     }

                                 }
                             }, new Response.ErrorListener() {

                            @Override
                            public void onErrorResponse(VolleyError error) {

//                                pDialog.hide();
                            }
                        });

// Adding request to request queue
                        MyApplication.getInstance().addToRequestQueue(jsonObjReq, tag_json_obj);


                        System.out.println("jsonData" + jsonData);
                        generateNoteOnSD("EternityLog.txt", String.valueOf(params));


                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });


        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();

            }
        }


    }

    private void generateNoteOnSD(String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "Eternity");
            if (!root.exists()) {
                root.mkdirs();

            }
            File gpxfile = new File(root, sFileName);
            try {
                //BufferedWriter for performance, true to set append to file flag
                BufferedWriter buf = new BufferedWriter(new FileWriter(gpxfile, true));
                buf.append(sBody);
                buf.newLine();
                buf.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
