package com.mixmycontainer.yiwu.fragment;

import android.app.Dialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.Boothdetailactivity;
import com.mixmycontainer.yiwu.apis.SaveNewOrderService;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.ResultCode;
import com.mixmycontainer.yiwu.apis.requests.SaveNewOrderRequest;
import com.mixmycontainer.yiwu.apis.responses.SaveNewOrderResponse;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.models.ContainerDetails;
import com.mixmycontainer.yiwu.models.OrderListDetails;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 15-11-2016.
 */
public class NewOrderFragment extends BaseFragment {
    RealmResults<CompanySetUpDetails> companySetUpDetails;
    RealmResults<CompanySetUpDetails> companySetUpDetailsFull;
    String companyName = "", userName = "", order = "";
    private EditText etOrderNumber;
    private Spinner spinnerCartonSize;
    private int companyId;
    private MenuItem menuItem;
    private ProgressBar progressBar;
    private Realm mRealm;
    private ContainerDetails containerDetails;
    private RealmList<ContainerDetails> containerDetailsInfos;
    private String[] containerNameList, containerIdList, containerMaximumVolume, containerMaximumWeight;

    public static NewOrderFragment newinstnace() {
        NewOrderFragment fragment = new NewOrderFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_new_order_first_screen, container, false);

        mRealm = Realm.getDefaultInstance();

        initview(view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuItem = menu.add("action");
        menuItem.setTitle(getResources().getString(R.string.txt_next));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getResources().getString(R.string.txt_next))) {
            validation();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public int getTitle() {
        return R.string.txt_ceate_new;
    }

    private void initview(View view) {
        etOrderNumber = (EditText) view.findViewById(R.id.etOrderNumber);
        spinnerCartonSize = (Spinner) view.findViewById(R.id.spinnerCartonSize);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        spinnerCartonSize.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        Setspinnervalue();
        generateOfflineOrderNumber();
    }

    public void validation() {
        if (etOrderNumber.getText().toString().trim().equals("")) {
            DisplayDialog("Please provide a valid order number");
        } else {
            int CartonSize = spinnerCartonSize.getSelectedItemPosition();
            SharedPreferences.Editor pref = PreferenceUtil.edit(getActivity());
            pref.putInt(PreferenceUtil.CONTAINER_TYPE, Integer.parseInt(containerIdList[CartonSize]));
            pref.putString(PreferenceUtil.EDIT_BOOTH_ID,"0");
            pref.apply();
            if (AppUtils.isNetworkAvailable(getActivity())) {
                SaveNewOrder(Integer.parseInt(containerIdList[CartonSize]), containerNameList[CartonSize]);

            } else {
                String offlineOrderNo = generateOfflineOrderNumber();
                showDisplayDialog("Offline mode so order Number ", offlineOrderNo, "offline", Integer.parseInt(containerIdList[CartonSize]), Integer.parseInt(containerMaximumWeight[CartonSize]), Integer.parseInt(containerMaximumVolume[CartonSize]));
            }
        }
    }

    /* method to generate order number offline*/
    private String generateOfflineOrderNumber() {

        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();

        try { // I could use try-with-resources here
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (!companySetUpDetails.isEmpty()) {

                        for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {

                            companyName = companySetUpDetails.get(i).getCompanyName();
                            userName = companySetUpDetails.get(i).getOrderEmail();
                            companyName = companyName.substring(0, 1);
                            userName = userName.substring(0, 1);
                            long time = System.currentTimeMillis();
                            String orderNumber = "OD";
                            order = orderNumber.concat(String.valueOf(time));
                            etOrderNumber.setText(order);

                        }

                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


        return order;
    }

    /* method to save new order Online*/
    private void SaveNewOrder(final int cartonType, final String scartonName) {

        SaveNewOrderService service = new SaveNewOrderService(getActivity());
        SaveNewOrderRequest request = new SaveNewOrderRequest();
        request.setOrdernumber(etOrderNumber.getText().toString().trim());
        request.setContainertype(cartonType);
        request.setToken(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AUTH_TOKEN, null));
        service.post(request, new ApiResponse<SaveNewOrderResponse>() {
            @Override
            public void onSuccess(ResultCode resultCode, SaveNewOrderResponse response) {
                progressBar.setVisibility(View.INVISIBLE);
                SharedPreferences.Editor pref = PreferenceUtil.edit(getActivity());
                pref.putString(PreferenceUtil.USER_ORDER_ID, (response.getOrderId()));
                pref.putString(PreferenceUtil.USER_CONTAINER_ID, String.valueOf(response.getContainerId()));
                pref.putString(PreferenceUtil.USER_ORDER_NUMBER, etOrderNumber.getText().toString());
                pref.apply();
                String maxVolume = response.getContianerDetails().getMaximumVolume();
                String maxWeight = response.getContianerDetails().getMaximumWeight();
                offlineSaveNewOrder(String.valueOf(response.getContainerId()), Float.parseFloat(maxWeight), Float.parseFloat(maxVolume), response.getOrderId(), cartonType, scartonName);
                Toast.makeText(getActivity(), "Order created successfully", Toast.LENGTH_SHORT).show();
                getActivity().finish();
            }

            @Override
            public void onError(ResultCode resultCode, SaveNewOrderResponse response) {
                etOrderNumber.setText(response.getNewOrderNo());
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    progressBar.setVisibility(View.INVISIBLE);
                    showDisplayDialog("Order number exist, So new Order number ", response.getNewOrderNo(), "online", 0, 0, 0);

                }
            }
        });
    }

    /* method to display dialog box
    *  mode- offline or online
    *  if online then "Order number exist, So new Order number" message with new ordernumber generated from API will to displayed.
    *  if offline then new order Id and orderNumber will be generated and the dialog will show up with new order number generated
    *  along with offline notification.
    *
    *  if offline mode then, click on "OK" button of dialog , method to save new values to local database is invoked.*/
    private void showDisplayDialog(String message, String newOrderNumber, final String mode, final int carton, final int weght, final int volume) {
        final Dialog dialog = new Dialog(getActivity());

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        DisplayMetrics display = getActivity().getResources().getDisplayMetrics();
        int width = display.widthPixels;
        int height = display.heightPixels;
        dialog.setContentView(R.layout.dialog_validate_order);
        TextView text = (TextView) dialog.findViewById(R.id.txt_validate_dialog);
        ImageView btn_ok = (ImageView) dialog.findViewById(R.id.btn_ok);
        text.setText(message + newOrderNumber);
        btn_ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mode == "offline") {
                    String OrderId = UUID.randomUUID().toString();
                    String ContainerId = UUID.randomUUID().toString();
                    int CartonSize = spinnerCartonSize.getSelectedItemPosition();

                    offlineSaveNewOrder(ContainerId, weght, volume, OrderId, Integer.parseInt(containerIdList[CartonSize]), containerNameList[CartonSize]);
                    dialog.dismiss();
                } else {
                    dialog.dismiss();
                }
            }
        });
        dialog.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }




    /* method to save new Order number created online/ofline to the local database */
    private void offlineSaveNewOrder(final String containerId, final float weight, final float volume, final String orderId, final int containerType, final String containerName) {
        containerDetailsInfos = new RealmList<ContainerDetails>();
//             Not exist
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    OrderListDetails orderListSetUpNew = mRealm.createObject(OrderListDetails.class, orderId);

                    SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                    PREF.putString(PreferenceUtil.ORDER_ID, orderId);
                    PREF.putString(PreferenceUtil.CONTAINER_ID, containerId);
                    PREF.apply();
                    // save order id to preference
                    orderListSetUpNew.setCompanyID(companyId);
                    orderListSetUpNew.setOrderNumber(etOrderNumber.getText().toString().trim());
                    orderListSetUpNew.setTrade("0");
                    orderListSetUpNew.setProductType("0");
                    orderListSetUpNew.setSynch("0");

                    orderListSetUpNew.setTotalOrderUsValue("0");
                    orderListSetUpNew.setTotalOrderRmbValue("0");
                    orderListSetUpNew.setTotalOrderZarValue("0");
                    orderListSetUpNew.setTotal3rdCurrencyValue("0");

                    orderListSetUpNew.setTotalCartons("0");
                    orderListSetUpNew.setTotalBooth("0");
                    orderListSetUpNew.setTotalContainer("0");
                    orderListSetUpNew.setTotalOrderWeight("0");

                    orderListSetUpNew.setTotalOrderCBM("0");
                    orderListSetUpNew.setOrderStatus("0");
                    String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                    orderListSetUpNew.setLastUpdate(date); // todays date

                    containerDetails = mRealm.copyToRealm(new ContainerDetails());

                    containerDetails.setOrderId(orderId);
                    containerDetails.setContainerId(containerId);
                    containerDetails.setContainerType(String.valueOf(containerType));
                    containerDetails.setContainerName(containerName);
                    containerDetails.setFilledVolume("0");
                    containerDetails.setStatus("0");


                    containerDetails.setFilledWeight("0");
                    containerDetails.setVolumepercentage("0");
                    containerDetails.setWeightpercentage("0");
                    containerDetails.setMaximumVolume(String.valueOf(volume));
                    containerDetails.setMaximumWeight(String.valueOf(weight));
                    containerDetailsInfos.add(containerDetails);
                    orderListSetUpNew.setContainerDetailses(containerDetailsInfos);
                    mRealm.copyToRealm(orderListSetUpNew);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


        Toast.makeText(getActivity(), "Order created successfully", Toast.LENGTH_SHORT).show();
        startActivity(Boothdetailactivity.getnewIntent(getActivity(), ""));
        getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        getActivity().finish();
    }

    /* method to get list of container size from the database to spinner*/
    private void Setspinnervalue() {
        companySetUpDetailsFull = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();

        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (!companySetUpDetailsFull.isEmpty()) {

                        for (int i = companySetUpDetailsFull.size() - 1; i >= 0; i--) {
                            SharedPreferences.Editor preferences = PreferenceUtil.get(getActivity()).edit();
                            preferences.putString(PreferenceUtil.TRANSLATE_TO, companySetUpDetailsFull.get(i).getTransilateTo());
                            preferences.putString(PreferenceUtil.TRANSLATE_FROM, companySetUpDetailsFull.get(i).getTransilateFrom());
                            preferences.apply();

                            containerNameList = new String[companySetUpDetailsFull.get(i).getContainerDetails().size()];
                            containerIdList = new String[companySetUpDetailsFull.get(i).getContainerDetails().size()];
                            containerMaximumVolume = new String[companySetUpDetailsFull.get(i).getContainerDetails().size()];
                            containerMaximumWeight = new String[companySetUpDetailsFull.get(i).getContainerDetails().size()];
                            for (int j = 0; j < companySetUpDetailsFull.get(i).getContainerDetails().size(); j++) {
                                containerNameList[j] = companySetUpDetailsFull.get(i).getContainerDetails().get(j).getContainer();
                                containerIdList[j] = companySetUpDetailsFull.get(i).getContainerDetails().get(j).getID();
                                containerMaximumVolume[j] = companySetUpDetailsFull.get(i).getContainerDetails().get(j).getMaximumVolume();
                                containerMaximumWeight[j] = companySetUpDetailsFull.get(i).getContainerDetails().get(j).getMaximumWeight();
                            }
                        }


                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();

        SetSpinnerData(containerNameList, spinnerCartonSize, "");
    }

    /* method to set array of string to spinner */
    private void SetSpinnerData(String[] NameList, Spinner spinnerName, String name) {
        if (NameList==null) {
            String[] noValueArray = {"No Result"};
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, noValueArray);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinnerName.setAdapter(spinnerArrayAdapter);
        }
        else {

            if (NameList.length != 0) {
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, NameList);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                spinnerName.setAdapter(spinnerArrayAdapter);
                if (name.trim().length() != 0) {
                    for (int i = 0; i < NameList.length; i++) {
                        if (name.equals(NameList[i])) {
                            spinnerName.setSelection(i);
                        }
                    }
                }
            }
//            } else {
//                String[] noValueArray = {"No Result"};
//                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, noValueArray);
//                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
//                spinnerName.setAdapter(spinnerArrayAdapter);
//            }
        }
    }
}
