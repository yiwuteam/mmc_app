package com.mixmycontainer.yiwu.fragment;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.PorterDuff;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.iceteck.silicompressorr.SiliCompressor;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.ItemDeatailActivity;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.models.BoothDetails;
import com.mixmycontainer.yiwu.models.BoothDetailsInfo;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.models.ContainerInfo;
import com.mixmycontainer.yiwu.models.ItemDetails;
import com.mixmycontainer.yiwu.models.OrderListDetails;
import com.mixmycontainer.yiwu.models.TradeInfo;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.ImageUtil;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;
import com.mvc.imagepicker.ImagePicker;

import org.json.JSONException;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 15-11-2016.
 */
public class BoothDetailFragment extends BaseFragment implements View.OnClickListener {
    public static String ARG_EXTRA = "ARG_EXTRA";
    private static int RESULT_OK = -1;
    String containerName = "";
    RealmResults<OrderListDetails> OrderListDetailsDetails;
    RealmResults<CompanySetUpDetails> companySetUpDetails;
    BoothDetails boothNumberExistOrNot;
    RealmList<BoothDetails> boothDetailses;
    RealmList<BoothDetailsInfo> Booth;
    private int backIndex, companyId;
    private Realm mRealm;
    private String[] hallsName, hallsId, tradeName, tradeId, alltradeNames, parentList, cartonsUsed;
    private Spinner spinnerCartonUsed, spinnertrade, spinnerProductType;
    private Bitmap bitmap;
    private DecoView decoView;
    private String businesCard, flage, fileNameToSend = "0", Weightpercentage = "0", Volumepercentage = "0";
    private ProgressBar progressBar;
    private MenuItem menuItem;
    private ImageView ivProdectionType, ivTrade, ivboothImageview, btScanPicture, ivboothImageviewCopy;
    private EditText etBoothCompanyName, etBoothNumber, etTelephone, etBusinessScope;
    private TextView tvOrdernumber, tvTotalCBM, tvorderValueUs, tvTotalWeight, tvValueofPurchase;
    private LinearLayout lnr_etTelephone, lnr_etBusinessScope, lnr_etBoothNumber, lnr_etBoothName;
    private ProgressDialog mProgressDialog;
    private String orderId;
    Uri imageUri;
    Boolean isInEditMode;
    String prefEditBoothId, oldBoothNumber = "";
    Boolean changeAllBooth = false;
    //--Trade and hall used in Booth--//
    String usedHallId, usedTradeId;

    public static BoothDetailFragment newinstance(String flage) {
        BoothDetailFragment fragment = new BoothDetailFragment();
        Bundle arg = new Bundle();
        arg.putString(ARG_EXTRA, flage);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_boothdetails, container, false);


        mRealm = Realm.getDefaultInstance();

        prefEditBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_BOOTH_ID, "0");

        initView(view);
        initLestener();

        getoldHallAndTradeValues();

        setOldTradeValue();


        ImagePicker.setMinQuality(600, 600);
        flage = getArguments().getString(ARG_EXTRA);
        btScanPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etBoothNumber.clearFocus();


                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.CAMERA)) {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.CAMERA},
                                11);
                    } else {
                        // we can request the permission.
//                        Toast.makeText(getContext(), "Requesting permission", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.CAMERA},
                                11);
                    }
                } else {

                    captureImageDialog();
                }
            }
        });
//        if (AppUtils.isNetworkAvailable(getActivity())) {
//            /* flag to check whether it is a continue order*/
//            if (flage.equals("add")) {
//                progressBar.setVisibility(View.VISIBLE);
//                getOrderDetailsOffline();
////                try {
////                    getboothdetail();
////                } catch (Exception e) {
////                    e.printStackTrace();
////                }
////                getBoothDetailOffline();
//            }
//        } else {
////            Toast.makeText(getActivity(), getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
//        }

        etBoothNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    /*  to execute when EditText loses focus */
                    if (AppUtils.isNetworkAvailable(getActivity())) {
                        String boothNumber = etBoothNumber.getText().toString().toLowerCase().trim();
                        boothNumber = boothNumber.toString().replace(" ", "%20");
                        checkIfBoothExist(boothNumber);
                    } else {
                        checkIfBoothExistOffline(etBoothNumber.getText().toString().toLowerCase().trim());
                        if (!prefEditBoothId.equals("0")) {
                            String boothNumber = etBoothNumber.getText().toString().toLowerCase().trim();
                            if (!oldBoothNumber.equals(boothNumber)) {
                                if (checkBoothUsedInMultipleItem(oldBoothNumber) > 1) {
                                    boothChangeDialog();
                                }
                            }

                        }
                    }
                }
            }
        });
        etBoothNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etBoothNumber.setError(null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        return view;
    }

    private void setOldTradeValue() {
        for (int i = 0; i < tradeId.length; i++) {
            if (tradeId[i].equals(usedTradeId)) {
                spinnertrade.setSelection(i);
            }
        }
    }

    //load hall and trade to Spinners of booth
    private void getoldHallAndTradeValues() {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderList = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", orderId).equalTo("CompanyID", companyId).findAll();
                    for (int i = 0; i < orderList.size(); i++) {
                        RealmList<BoothDetails> boothDetails = orderList.get(i).getBoothDetailses();
                        for (int j = 0; j < boothDetails.size(); j++) {
                            if (boothDetails.get(j).getBoothId().equals(prefEditBoothId)) {
                                usedHallId = boothDetails.get(j).getHallId();
                                usedTradeId = boothDetails.get(j).getTradeId();
                            } else {
                                if (prefEditBoothId.equals("0")) {
                                    usedHallId = boothDetails.get(j).getHallId();
                                    usedTradeId = boothDetails.get(j).getTradeId();
                                }
                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.getMessage();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }
    }

    //Show dialog while editing an item.
    private void boothChangeDialog() {
        Button btn_ChangeAll, btn_ChangeCurrentItem;
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_boothchange);
        dialog.setCanceledOnTouchOutside(false);
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                Toast.makeText(getActivity(), "Booth will be changed for current item", Toast.LENGTH_SHORT).show();
                changeAllBooth = false;

            }
        });
        btn_ChangeCurrentItem = (Button) dialog.findViewById(R.id.btChangeBoothThisItem);
        btn_ChangeAll = (Button) dialog.findViewById(R.id.btChangeAllBooth);
        dialog.show();
        btn_ChangeAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeAllBooth = true;
                dialog.dismiss();
            }
        });
        btn_ChangeCurrentItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                changeAllBooth = false;
                dialog.dismiss();
            }
        });
    }

    /* checks if the booth already exist or not, if exist then fetches and bind the value */
    private void checkIfBoothExist(String text) {
        lnr_etBoothNumber.setClickable(false);
        lnr_etBoothName.setClickable(false);
        lnr_etTelephone.setClickable(false);
        lnr_etBusinessScope.setClickable(false);

        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Checking if booth details exist...");
        mProgressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        mProgressDialog.setCancelable(false);
        mProgressDialog.show();
        Ion.with(getActivity())

                .load(URL.CheckIfBoothExists + text)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override

                    public void onCompleted(Exception e, JsonObject result) {

                        try {
                            String requestCode = result.get("resultCode").getAsString();
                            if (requestCode.equals("1")) {
                                Toast.makeText(getActivity(), "Booth number exists.", Toast.LENGTH_SHORT).show();
                                mProgressDialog.dismiss();
                                JsonObject boothDeatils = result.getAsJsonObject("boothDeatils");
                                String BoothName = boothDeatils.get("BoothName").getAsString();
                                String Phone = boothDeatils.get("Phone").getAsString();
                                String BusinessScope = boothDeatils.get("BusinessScope").getAsString();
                                String BusinessCard = boothDeatils.get("BusinessCard").getAsString();
                                ivboothImageviewCopy.setVisibility(View.GONE);
                                ivboothImageview.setVisibility(View.VISIBLE);
                                fileNameToSend = BusinessCard;
                                Glide.with(getActivity()).
                                        load(URL.PhotView + BusinessCard)
                                        .into(ivboothImageview);
                                etBoothCompanyName.setText(BoothName);
                                etTelephone.setText(Phone);
                                etBusinessScope.setText(BusinessScope);
                            } else {
                                lnr_etBoothNumber.setClickable(true);
                                lnr_etTelephone.setClickable(true);
                                lnr_etBusinessScope.setClickable(true);
                                lnr_etBoothName.setClickable(true);
                                mProgressDialog.dismiss();
                                etBoothCompanyName.setText("");
                                etTelephone.setText("");
                                etBusinessScope.setText("");
                                ivboothImageviewCopy.setVisibility(View.VISIBLE
                                );
                                ivboothImageview.setVisibility(View.GONE);
                            }
                        } catch (JsonParseException c) {
                            c.printStackTrace();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                });

    }

    /* method to check if booth details already exist in the database*/
    private void checkIfBoothExistOffline(final String boothNumber) {

        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
                    for (int i = 0; i < companySetUpDetails.size(); i++) {
                        Booth = companySetUpDetails.get(i).getBoothDetailsInfos();
                        RealmResults<BoothDetailsInfo> bootResults = companySetUpDetails.get(i).getBoothDetailsInfos().where().equalTo("BoothNumber", boothNumber).findAll();
                        if (bootResults.size() != 0) {
                            etBoothCompanyName.setText(bootResults.get(0).getBoothName());
                            etBusinessScope.setText(bootResults.get(0).getBusinessScope());
                            etTelephone.setText(bootResults.get(0).getPhone());
                            ivboothImageview.setImageDrawable(null);

                        } else {
                            etBoothCompanyName.setText("");
                            etBusinessScope.setText("");
                            etTelephone.setText("");
                            ivboothImageview.setImageDrawable(null);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

    private void newBoothOffline(final String boothNumber) {

        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
                    for (int i = 0; i < companySetUpDetails.size(); i++) {

                        RealmResults<BoothDetailsInfo> bootResults = companySetUpDetails.get(i).getBoothDetailsInfos().where().equalTo("BoothNumber", boothNumber).findAll();


                        CompanySetUpDetails setUp = mRealm.where(CompanySetUpDetails.class)
                                .equalTo("CompanyID", companyId).findFirst();

                        BoothDetailsInfo booth = mRealm.copyToRealm(new BoothDetailsInfo());
                        booth.setBoothName(etBoothCompanyName.getText().toString().trim());
                        booth.setBusinessCard(fileNameToSend);
                        booth.setBoothNumber(etBoothNumber.getText().toString().toLowerCase().trim());
                        booth.setPhone(etTelephone.getText().toString().trim());
                        booth.setBusinessScope(etBusinessScope.getText().toString().trim());
                        if (bootResults.size() == 0) {
                            setUp.getBoothDetailsInfos().add(booth);

                        }
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

    //check if booth is used in multiple items

    private int checkBoothUsedInMultipleItem(final String boothNumber) {
        final String[] boothId = {null};
        final int[] itemCount = {0};
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListSetUp = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", orderId).equalTo("CompanyID", companyId).findAll();
                    for (int i = 0; i < orderListSetUp.size(); i++) {
                        RealmList<BoothDetails> boothDetails = orderListSetUp.get(i).getBoothDetailses();
                        for (int j = 0; j < boothDetails.size(); j++) {
                            if (boothDetails.get(j).getBoothNumber().equals(boothNumber)) {
                                boothId[0] = boothDetails.get(j).getBoothId();
                                RealmList<ItemDetails> itemDetails = orderListSetUp.get(i).getItemDetailses();
                                for (int k = 0; k < itemDetails.size(); k++) {
                                    if (itemDetails.get(k).getBoothId().equals(boothId[0])) {
                                        itemCount[0]++;
                                    }

                                }

                            }

                        }
                    }


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }
        return itemCount[0];

    }


    /* method to show dialog to take bussiness card.*/
    private void captureImageDialog() {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        DisplayMetrics display = getResources().getDisplayMetrics();
        int width = display.widthPixels;
        int height = display.heightPixels;
        dialog.setContentView(R.layout.dialog_image_capture);
        TextView btn_takePhoto = (TextView) dialog.findViewById(R.id.btn_takePhoto);
        TextView btn_captureImage = (TextView) dialog.findViewById(R.id.btn_captureImage);
        btn_takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photo));
                imageUri = Uri.fromFile(photo);
                startActivityForResult(takePicture, 0);
                dialog.cancel();
            }
        });
        btn_captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
                dialog.cancel();
            }
        });
        dialog.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
//                    bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
//                    ivboothImageview.setVisibility(View.GONE);
//                    ivboothImageviewCopy.setVisibility(View.VISIBLE);
//                    ivboothImageviewCopy.setImageBitmap(bitmap);
//                    fileNameToSend = ImageUtil.convert(bitmap);
                    Uri selectedImage = imageUri;
                    getActivity().getContentResolver().notifyChange(selectedImage, null);
                    ContentResolver cr = getActivity().getContentResolver();
                    Bitmap bitmap;
                    try {
                        //Code to rotate image while taking through camera

                        ExifInterface ei = new ExifInterface(imageUri.toString());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_UNDEFINED);
                        Bitmap rotatedBitmap = null;
                        bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, selectedImage);
                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                rotatedBitmap = rotateImage(bitmap, 90);
                                break;

                            case ExifInterface.ORIENTATION_ROTATE_180:
                                rotatedBitmap = rotateImage(bitmap, 180);
                                break;

                            case ExifInterface.ORIENTATION_ROTATE_270:
                                rotatedBitmap = rotateImage(bitmap, 270);
                                break;

                            case ExifInterface.ORIENTATION_NORMAL:
                            default:
                                rotatedBitmap = bitmap;
                        }
                        //Rotate end


                        ivboothImageviewCopy.setImageBitmap(rotatedBitmap);
                        ivboothImageviewCopy.setVisibility(View.VISIBLE);
                        ivboothImageview.setVisibility(View.GONE);
                        bitmap = SiliCompressor.with(getContext()).getCompressBitmap(selectedImage.toString(), true);
                        fileNameToSend = ImageUtil.convert(bitmap);

                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }

                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    ivboothImageview.setVisibility(View.GONE);
                    ivboothImageviewCopy.setVisibility(View.VISIBLE);
                    Bitmap bitmap = null;
//                    try {
//                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                    String tempImageUrl = String.valueOf(selectedImage);
                    try {
                        bitmap = SiliCompressor.with(getContext()).getCompressBitmap(tempImageUrl);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ivboothImageviewCopy.setImageBitmap(bitmap);

                    fileNameToSend = ImageUtil.convert(bitmap);
                }
                break;
        }
    }

    /*Image rotation method*/
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    @Override
    public int getTitle() {
        return R.string.txt_boothdetails;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuItem = menu.add("action");
        menuItem.setTitle(getResources().getString(R.string.txt_next));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 11: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SharedPreferences.Editor pref = PreferenceUtil.edit(getActivity());
                    pref.putInt(PreferenceUtil.CAMERA_PERMISSION, 1);
                    pref.apply();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    SharedPreferences.Editor pref = PreferenceUtil.edit(getActivity());
                    pref.putInt(PreferenceUtil.CAMERA_PERMISSION, 0);
                    pref.apply();
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getResources().getString(R.string.txt_next))) {

            validation();

        }
        return super.onOptionsItemSelected(item);
    }

    private void initView(View view) {
        lnr_etBoothNumber = (LinearLayout) view.findViewById(R.id.lnr_etTelephone);
        lnr_etTelephone = (LinearLayout) view.findViewById(R.id.lnr_etTelephone);
        lnr_etBusinessScope = (LinearLayout) view.findViewById(R.id.lnr_etTelephone);
        lnr_etBoothName = (LinearLayout) view.findViewById(R.id.lnr_etBoothName);

        spinnerProductType = (Spinner) view.findViewById(R.id.spinnerProductType);
        spinnertrade = (Spinner) view.findViewById(R.id.spinnertrade);
        ivTrade = (ImageView) view.findViewById(R.id.ivTrade);
        ivProdectionType = (ImageView) view.findViewById(R.id.ivProdectionType);
        spinnerProductType.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);
        spinnertrade.getBackground().setColorFilter(getResources().getColor(R.color.colorPrimaryDark), PorterDuff.Mode.SRC_ATOP);

        btScanPicture = (ImageView) view.findViewById(R.id.btScanPicture);
        ivboothImageview = (ImageView) view.findViewById(R.id.ivboothImageview);
        ivboothImageviewCopy = (ImageView) view.findViewById(R.id.ivboothImageviewCopy);
        etBoothCompanyName = (EditText) view.findViewById(R.id.etBoothCompanyName);
        etBoothNumber = (EditText) view.findViewById(R.id.etBoothNumber);
        etTelephone = (EditText) view.findViewById(R.id.etTelephone);
        etBusinessScope = (EditText) view.findViewById(R.id.etBusinessScope);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        tvOrdernumber = (TextView) view.findViewById(R.id.tvOrdernumber);
        tvTotalCBM = (TextView) view.findViewById(R.id.tvTotalCBM);
        tvorderValueUs = (TextView) view.findViewById(R.id.tvorderValueUs);
        tvTotalWeight = (TextView) view.findViewById(R.id.tvTotalWeight);
        tvValueofPurchase = (TextView) view.findViewById(R.id.tvValueofPurchase);
        spinnerCartonUsed = (Spinner) view.findViewById(R.id.spinnerCartonUsed);
        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        orderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");


        setSpinnerValue(0, "", 0);
        spinnertrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                valueSetToHallsSpinner(position, "", 1);

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        decoView = (DecoView) view.findViewById(R.id.dynamicArcView);
        decoView.addSeries(new SeriesItem.Builder(Color.parseColor("#FFFFFF"))
                .setRange(0, 100, 100)
                .setInitialVisibility(true)
                .build());
        final SeriesItem seriesItem1 = new SeriesItem.Builder(Color.parseColor("#4CAF50"))
                .setRange(0, 100, 0)
                .build();

        backIndex = decoView.addSeries(seriesItem1);
        final TextView textPercentage = (TextView) view.findViewById(R.id.textPercentage);
        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilled = ((currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue()));
                textPercentage.setText(String.format("%.0f%%", percentFilled * 100f));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });

        final String itemIdForEdit = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_ITEM_ID, "0");
        if (itemIdForEdit.equals("0")) {
            isInEditMode = false;
        } else {
            isInEditMode = true;
            getBoothDetailsForEdit();
        }
        getOrderDetailsOffline();


        spinnertrade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int position, long l) {

                valueSetToHallsSpinner(position, "", 1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

//--Set used hall value to spinner
    private void setOldHallValue() {
        for (int i = 0; i < hallsId.length; i++) {
            if (hallsId[i].equals(usedHallId)) {
                spinnerProductType.setSelection(i);
            }
        }
    }


    @Override
    public void onResume() {
        super.onResume();
    }

    /* method to fetch data corresponding to the trade from the loacl database
    * param 1- selected position of trade
    * param 2- selected spinner data
    * para 3- from - 0 or 2 then the spinner value will be set to hall if it is 1 then spinner value set to trade.*/
    private void valueSetToHallsSpinner(final int position, final String selectedSpinnerData, final int from) {
        companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();

        try { // I could use try-with-resources here
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (!companySetUpDetails.isEmpty()) {

                        for (int m = companySetUpDetails.size() - 1; m >= 0; m--) {


                            int countSize = 0;
                            for (int j = 0; j < companySetUpDetails.get(m).getTradeDeatils().size(); j++) {
                                String parent = companySetUpDetails.get(m).getTradeDeatils().get(j).getParent();
                                if (parent.equals(tradeId[position])) {
                                    countSize++;
                                }

                            }
                            hallsName = new String[countSize];
                            hallsId = new String[countSize];

                            for (int j = 0, k = 0; j < companySetUpDetails.get(m).getTradeDeatils().size(); j++) {
                                String parent = companySetUpDetails.get(m).getTradeDeatils().get(j).getParent();
                                alltradeNames[j] = companySetUpDetails.get(m).getTradeDeatils().get(j).getName();

                                if (parent.equals(tradeId[position])) {
                                    hallsName[k] = companySetUpDetails.get(m).getTradeDeatils().get(j).getName();
                                    hallsId[k] = companySetUpDetails.get(m).getTradeDeatils().get(j).getId();
                                    k++;
                                }

                            }

                            if (from == 1) {
                                setSpinnerData(hallsName, spinnerProductType, "");
                            } else {
                                setSpinnerData(hallsName, spinnerProductType, selectedSpinnerData);
                            }

                        }

                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

    /*
    * Setspinnervalue
    * 1 st param - if 0 then initial request ,
    * 2nd param -   seleted string to be binded to spinner,
    * 3rd param - the flag to check to which spinner the value to be set
    * if 3 rd param is 0 or 2 then the spinner value will be set to hall if it is 1 then spinner value set to trade. */

    private void setSpinnerValue(int flag, String setSpinerDataTo, int chooseSpinner) {
        companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();

        try { // I could use try-with-resources here
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (!companySetUpDetails.isEmpty()) {

                        for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {

                            SharedPreferences.Editor preferences = PreferenceUtil.get(getActivity()).edit();
                            preferences.putString(PreferenceUtil.TRANSLATE_TO, companySetUpDetails.get(i).getTransilateTo());
                            preferences.putString(PreferenceUtil.TRANSLATE_FROM, companySetUpDetails.get(i).getTransilateFrom());
                            preferences.apply();
                            int countSize = 0;
                            for (int j = 0; j < companySetUpDetails.get(i).getTradeDeatils().size(); j++) {
                                String parent = companySetUpDetails.get(i).getTradeDeatils().get(j).getParent();

                                if (parent.equals("0")) {
                                    countSize++;
                                }

                            }
                            tradeName = new String[countSize];
                            tradeId = new String[countSize];
                            parentList = new String[companySetUpDetails.get(i).getTradeDeatils().size()];
                            alltradeNames = new String[companySetUpDetails.get(i).getTradeDeatils().size()];
                            for (int j = 0, k = 0; j < companySetUpDetails.get(i).getTradeDeatils().size(); j++) {
                                String parent = companySetUpDetails.get(i).getTradeDeatils().get(j).getParent();
                                alltradeNames[j] = companySetUpDetails.get(i).getTradeDeatils().get(j).getName();
                                parentList[j] = companySetUpDetails.get(i).getTradeDeatils().get(j).getParent();
                                if (parent.equals("0")) {
                                    tradeName[k] = companySetUpDetails.get(i).getTradeDeatils().get(j).getName();
                                    tradeId[k] = companySetUpDetails.get(i).getTradeDeatils().get(j).getId();
                                    k++;
                                }

                            }

                        }

                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


        if (flag == 0) {
            setSpinnerData(tradeName, spinnertrade, "");
        } else {
            if (chooseSpinner == 1) {
                setSpinnerData(tradeName, spinnertrade, setSpinerDataTo);
            } else {
                int k = spinnertrade.getSelectedItemPosition();
                valueSetToHallsSpinner(k, setSpinerDataTo, 2);

            }
        }
    }

    /* SetSpinnerData
    1st param - String array  to spinner ,
    2nd param - spinner id,
    3rd param - selected spinner data name */
    private void setSpinnerData(String[] NameList, Spinner spinnerId, String name) {

        if (NameList.length != 0) {
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, NameList);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinnerId.setAdapter(spinnerArrayAdapter);

            if (name.trim().length() != 0) {
                for (int i = 0; i < NameList.length; i++) {
                    if (name.equals(NameList[i])) {
                        spinnerId.setSelection(i);
                    }
                }
            }
            if (!(hallsId == null)) {
                setOldHallValue();
            }
        } else {
            String[] noValueArray = {"No Result"};
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, noValueArray);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinnerId.setAdapter(spinnerArrayAdapter);
        }

    }

    public void validation() {
        if (!etBoothNumber.getText().toString().trim().equals("") && !spinnerProductType.getSelectedItem().toString().trim().equals("No Result") && !spinnertrade.getSelectedItem().toString().trim().equals("")) {

            if (changeAllBooth) {
                updateBoothDetails(oldBoothNumber);
            }

            saveBoothDetailsOfline();

        } else {
            if (etBoothNumber.getText().toString().trim().equals("") && spinnerProductType.getSelectedItem().toString().trim().equals("No Result")) {

                DisplayDialog("Please provide a valid Booth Number and Hall.");
            } else if (etBoothNumber.getText().toString().trim().equals("") && !spinnerProductType.getSelectedItem().toString().trim().equals("No Result")) {

                DisplayDialog("Please provide a valid Booth Number.");
            } else if (!etBoothNumber.getText().toString().trim().equals("") && spinnerProductType.getSelectedItem().toString().trim().equals("No Result")) {

                DisplayDialog("Please provide a valid Hall.");
            }
        }
    }

    /* method to save the booth details offline */
    private void saveBoothDetailsOfline() {

        int spinnertradeItemPosition = spinnertrade.getSelectedItemPosition();
        int spinnerProductTypeItemPosition = spinnerProductType.getSelectedItemPosition();
        final String spinnerTradeName = tradeName[spinnertradeItemPosition];
        final String spinnerHallId = hallsId[spinnerProductTypeItemPosition];

        final String spinnerTradeItemName = spinnertrade.getSelectedItem().toString();

        final String spinnerTradeId = tradeId[spinnertradeItemPosition];

        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");


//        final String boothCount = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_COUNT, "0");

        final String containerCount = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_COUNT, "0");
        final OrderListDetails orderListSetUp = mRealm.where(OrderListDetails.class)
                .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findFirst();

//        boothNumberExistOrNot = mRealm.where(BoothDetails.class)
//                .equalTo("OrderId", prefOrderId).equalTo("BoothNumber", etBoothNumber.getText().toString().toLowerCase().trim()).findFirst();

        newBoothOffline(etBoothNumber.getText().toString().toLowerCase().trim());
//        final String BoothTESTID = checkBoothIdTEST(etBoothNumber.getText().toString());

        String boothNumber = etBoothNumber.getText().toString().toLowerCase().trim();
        final String boothID = checkBoothExist(boothNumber, prefOrderId);

        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (boothID != null) {
                        // already exist

//
                        String boothNumber = etBoothNumber.getText().toString().toLowerCase().trim();
                        SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                        String boothId = checkBoothExist(boothNumber, prefOrderId);
                        PREF.putString(PreferenceUtil.BOOTH_ID, boothId);
                        PREF.apply();

                        RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                                .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();
                        for (int i = 0; i < orderListDetails.size(); i++) {
                            orderListDetails.get(i).setCompanyID(companyId);
                            orderListDetails.get(i).setTrade(String.valueOf(spinnerTradeId));
                            orderListDetails.get(i).setTradeName(spinnerTradeName);
                            orderListDetails.get(i).setProductType(String.valueOf(spinnerHallId));
                            // no change to booth count
                            int count = orderListDetails.get(i).getBoothDetailses().size();
                            orderListDetails.get(i).setTotalBooth(String.valueOf(count));
                            orderListDetails.get(i).setOrderStatus("0");
                            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                            orderListDetails.get(i).setLastUpdate(date);
                            RealmList<BoothDetails> boothDetail = orderListDetails.get(i).getBoothDetailses();
                            for (int j = 0; j < boothDetail.size(); j++) {
                                if (boothDetail.get(j).getBoothNumber().equals(boothNumber)) {

                                    boothDetail.get(j).setBoothName(etBoothCompanyName.getText().toString().trim());
                                    boothDetail.get(j).setBoothNumber(etBoothNumber.getText().toString().toLowerCase().trim());

                                    boothDetail.get(j).setBoothStatus("0");
                                    boothDetail.get(j).setBusinessCard(fileNameToSend);
                                    boothDetail.get(j).setBusinessScope(etBusinessScope.getText().toString().trim());

                                    boothDetail.get(j).setTradeId(String.valueOf(spinnerTradeId));
                                    boothDetail.get(j).setHallId(String.valueOf(spinnerHallId));
                                    boothDetail.get(j).setPhone(etTelephone.getText().toString().trim());
                                }
                                mRealm.insertOrUpdate(boothDetail);

                            }
                            mRealm.insertOrUpdate(orderListDetails);
                        }
                        startActivity(ItemDeatailActivity.getnewIntent(getActivity()));
                        getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                        getActivity().finish();


                    } else {

                        // new booth


                        RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                                .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();
                        for (int i = 0; i < orderListDetails.size(); i++) {
                            orderListDetails.get(i).setTrade(String.valueOf(spinnerTradeId));
                            orderListDetails.get(i).setTradeName(String.valueOf(spinnerTradeItemName));
                            orderListDetails.get(i).setProductType(String.valueOf(spinnerHallId));
                            //change booth count
                            int count = orderListDetails.get(i).getBoothDetailses().size() + 1;
                            orderListDetails.get(i).setTotalBooth(String.valueOf(count));
                            orderListDetails.get(i).setTotalContainer(containerCount);
//                            orderListDetails.get(i).setTotalOrderWeight("0");
//                            orderListDetails.get(i).setTotalOrderCBM("0");
                            orderListDetails.get(i).setOrderStatus("0");
                            String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                            orderListDetails.get(i).setLastUpdate(date);

                            //New Booth Creation
                            BoothDetails boothdetail = new BoothDetails();
                            boothdetail.setOrderId(prefOrderId);

                            String boothIdd = UUID.randomUUID().toString();
                            SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                            PREF.putString(PreferenceUtil.BOOTH_ID, boothIdd);

                            PREF.apply();
                            boothdetail.setBoothId(boothIdd);

                            boothdetail.setBoothName(etBoothCompanyName.getText().toString().trim());
                            boothdetail.setBoothNumber(etBoothNumber.getText().toString().toLowerCase().trim());

                            boothdetail.setBoothStatus("0");
                            boothdetail.setBusinessCard(fileNameToSend);
                            boothdetail.setBusinessScope(etBusinessScope.getText().toString().trim());

                            boothdetail.setTradeId(String.valueOf(spinnerTradeId));
                            boothdetail.setHallId(String.valueOf(spinnerHallId));
                            boothdetail.setValuePurchaseRMB("0");
                            boothdetail.setValuePurchase("0");
                            boothdetail.setPhone(etTelephone.getText().toString().trim());
                            mRealm.insertOrUpdate(boothdetail);
                            orderListDetails.get(i).getBoothDetailses().add(boothdetail);
                            mRealm.insertOrUpdate(orderListDetails);
                        }
                        startActivity(ItemDeatailActivity.getnewIntent(getActivity()));
                        getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                        getActivity().finish();
                    }
                }
            });

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


    }
//-- When user want to change booth details whis is used in multiple items in single order
    //-Method executed when yes button on changebooth dialog is pressed.
    private void updateBoothDetails(final String oldBoothNumber) {

        int spinnertradeItemPosition = spinnertrade.getSelectedItemPosition();
        int spinnerProductTypeItemPosition = spinnerProductType.getSelectedItemPosition();
        final String spinnerHallId = hallsId[spinnerProductTypeItemPosition];
        final String spinnerTradeId = tradeId[spinnertradeItemPosition];

        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListSetUp = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", orderId).equalTo("CompanyID", companyId).findAll();
                    for (int i = 0; i < orderListSetUp.size(); i++) {
                        RealmList<BoothDetails> boothDetails = orderListSetUp.get(i).getBoothDetailses();
                        for (int j = 0; j < boothDetails.size(); j++) {
                            if (boothDetails.get(j).getBoothNumber().equals(oldBoothNumber)) {
                                boothDetails.get(j).setBoothName(etBoothCompanyName.getText().toString().trim());
                                boothDetails.get(j).setBoothNumber(etBoothNumber.getText().toString().toLowerCase().trim());

                                boothDetails.get(j).setBusinessCard(fileNameToSend);
                                boothDetails.get(j).setBusinessScope(etBusinessScope.getText().toString().trim());

                                boothDetails.get(j).setTradeId(String.valueOf(spinnerTradeId));
                                boothDetails.get(j).setHallId(String.valueOf(spinnerHallId));
                                boothDetails.get(j).setPhone(etTelephone.getText().toString().trim());
                                mRealm.insertOrUpdate(boothDetails);

                            }

                        }
                        mRealm.insertOrUpdate(orderListSetUp);
                    }


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

//    private void getboothdetail() throws Exception {
//        String df = URL.continue_order + "token=" + PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AUTH_TOKEN, null) +
//                "&orderid=" + PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_ORDER_ID, "0");
//        Ion.with(getActivity())
//                .load(URL.continue_order + "token=" + PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AUTH_TOKEN, null) +
//                        "&orderid=" + PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_ORDER_ID, "0"))
//                .asJsonObject()
//                .setCallback(new FutureCallback<JsonObject>() {
//                    @Override
//                    public void onCompleted(Exception e, JsonObject result) {
//
//                        progressBar.setVisibility(View.INVISIBLE);
//                        try {
//                            String ContainerType = result.get("ContainerType").getAsString();
//                            String ContainerId = result.get("ContainerId").getAsString();
//                            SharedPreferences.Editor pref = PreferenceUtil.edit(getActivity());
//                            pref.putString(PreferenceUtil.USER_CONTAINER_ID, String.valueOf(ContainerId));
//                            pref.putInt(PreferenceUtil.CONTAINER_TYPE, Integer.parseInt(ContainerType));
//                            pref.apply();
//
//                        } catch (NumberFormatException e1) {
//                            e1.printStackTrace();
//                        }
//
//
//                        try {
//                            JsonObject jsonObject = result.get("LastBooth").getAsJsonObject();
//
//                            etBoothCompanyName.setText(jsonObject.get("BoothName").getAsString());
//                            etBoothNumber.setText(jsonObject.get("BoothNumber").getAsString());
//                            etTelephone.setText(jsonObject.get("Phone").getAsString());
//                            etBusinessScope.setText(jsonObject.get("BusinessScope").getAsString());
//                            businesCard = jsonObject.get("BusinessCard").getAsString();
//                            fileNameToSend = businesCard;
//                            ivboothImageviewCopy.setVisibility(View.GONE);
//                            ivboothImageview.setVisibility(View.VISIBLE);
//
//                            Glide.with(getActivity()).
//                                    load(URL.PhotView + businesCard)
//                                    .into(ivboothImageview);
//
//
//                        } catch (Exception e1) {
//                            e1.printStackTrace();
//                        }
//
//                    }
//                });
//
//    }

    /* method to get the order details from the database to show up in the dashboard.*/
    private void getOrderDetailsOffline() {
        String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");

        OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();

        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    if (!OrderListDetailsDetails.isEmpty()) {


                        for (int i = OrderListDetailsDetails.size() - 1; i >= 0; i--) {

                            cartonsUsed = new String[OrderListDetailsDetails.get(i).getContainerDetailses().size()];
                            for (int k = 0; k < OrderListDetailsDetails.get(i).getContainerDetailses().size(); k++) {
                                String cartonName = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerName();

                                if (cartonName.matches("\\d+")) {
                                    cartonsUsed[k] = getContainerDetailsOffline(cartonName);
                                } else {
                                    cartonsUsed[k] = cartonName;
                                }

                                Volumepercentage = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getVolumepercentage();
                                Weightpercentage = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getWeightpercentage();
                            }

                            setSpinnerData(cartonsUsed, spinnerCartonUsed, "0");
                            tvOrdernumber.setText(OrderListDetailsDetails.get(i).getOrderNumber());
                            tvorderValueUs.setText(OrderListDetailsDetails.get(i).getTotalOrderUsValue() + "$(" + OrderListDetailsDetails.get(i).getTotalOrderRmbValue() + "¥)");
                            double totCBB = Double.parseDouble(OrderListDetailsDetails.get(i).getTotalOrderCBM());
                            String orderCbm = String.format("%.2f", totCBB);
                            tvTotalWeight.setText(OrderListDetailsDetails.get(i).getTotalOrderWeight());
                            tvTotalCBM.setText(orderCbm);
                            boothDetailses = OrderListDetailsDetails.get(i).getBoothDetailses();
                            //Checks if the page is in edit mode or not
                            if (!isInEditMode) {

                                for (int m = 0; m < boothDetailses.size(); m++) {
                                    if (boothDetailses.get(m).getBoothStatus().equals("0")) {
                                        etBoothCompanyName.setText(boothDetailses.get(m).getBoothName());
                                        etBoothNumber.setText(boothDetailses.get(m).getBoothNumber());
                                        etTelephone.setText(boothDetailses.get(m).getPhone());
                                        etBusinessScope.setText(boothDetailses.get(m).getBusinessScope());
                                        businesCard = boothDetailses.get(m).getBusinessCard();
                                        String boothId = boothDetailses.get(m).getBoothId();
                                        SharedPreferences.Editor PREFERENCE = PreferenceUtil.edit(getActivity());
                                        PREFERENCE.putString(PreferenceUtil.BOOTH_ID, boothId);
                                        PREFERENCE.apply();
                                        fileNameToSend = businesCard;
                                        ivboothImageviewCopy.setVisibility(View.GONE);
                                        ivboothImageview.setVisibility(View.VISIBLE);
                                        if (AppUtils.isNetworkAvailable(getActivity())) {
                                            Glide.with(getActivity()).
                                                    load(URL.PhotView + businesCard)
                                                    .into(ivboothImageview);
                                        }
                                    }
                                }
                            }

                            tvValueofPurchase.setText(0 + "$(" + 0 + "¥)");


                            try {
                                float Vol_Percent_float = Float.parseFloat(Volumepercentage);
                                Float Weight_Percent_float = Float.parseFloat(Weightpercentage);
                                int Vol_Percent = Math.round(Vol_Percent_float);
                                int Weight_Percent = Math.round(Weight_Percent_float);
                                if (Vol_Percent >= Weight_Percent) {

                                    decoView.addEvent(new DecoEvent.Builder(Vol_Percent).setIndex(backIndex).setDelay(250).build());
                                } else {
                                    decoView.addEvent(new DecoEvent.Builder(Weight_Percent).setIndex(backIndex).setDelay(250).build());
                                }
                            } catch (NumberFormatException ee) {
                                ee.printStackTrace();
                            }


                        }


                    } else {
                        System.out.println("Something" + "aam");
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

    //TO get booth id from order list
    private String checkBoothExist(String boothNumber, String prefOrderId) {
        String boothId = null;
        RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();
        for (int i = 0; i < orderListDetails.size(); i++) {
            RealmList<BoothDetails> boothDetailses = orderListDetails.get(i).getBoothDetailses();
            for (int j = 0; j < boothDetailses.size(); j++) {
                String dbboothNumber = boothDetailses.get(j).getBoothNumber().toString();
                if (boothNumber.equals(dbboothNumber)) {
                    boothId = boothDetailses.get(j).getBoothId();
                }
            }
        }
        return boothId;
    }

    private String getContainerDetailsOffline(final String containerType) {


        int prefCompanyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", prefCompanyId).findAll();
        for (int i = 0; i < companySetUpDetails.size(); i++) {

            RealmResults<ContainerInfo> containerResults = companySetUpDetails.get(i).getContainerDetails().where().equalTo("ID", containerType).findAll();
            if (containerResults != null) {
                containerName = containerResults.get(0).getContainer();

            }
        }

        return containerName;

    }

    private void initLestener() {
        ivTrade.setOnClickListener(this);
        ivProdectionType.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        if (view == ivTrade) {
            tradeDialog(0);
        } else if (view == ivProdectionType) {
            tradeDialog(1);
        }
    }

    //Retriving boothDetails for edit
    public void getBoothDetailsForEdit() {

        final String itemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_ITEM_ID, "0");
        final String boothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_BOOTH_ID, "0");
        final String orderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");

        mRealm = Realm.getDefaultInstance();
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<OrderListDetails> orderListSetUp = mRealm.where(OrderListDetails.class)
                        .equalTo("OrderId", orderId).equalTo("CompanyID", companyId).findAll();
                for (int i = 0; i < orderListSetUp.size(); i++) {
                    RealmList<ItemDetails> itemDetails = orderListSetUp.get(i).getItemDetailses();
                    for (int k = 0; k < itemDetails.size(); k++) {
                        if (itemDetails.get(k).getItemId().equals(itemId)) {
                            String itemsBoothID = itemDetails.get(k).getBoothId();
                            RealmList<BoothDetails> boothDetails = orderListSetUp.get(i).getBoothDetailses();
                            for (int j = 0; j < boothDetails.size(); j++) {
                                if (boothDetails.get(j).getBoothId().toString().equals(itemsBoothID)) {
                                    etBoothNumber.setText(boothDetails.get(j).getBoothNumber().toString());
                                    oldBoothNumber = boothDetails.get(j).getBoothNumber().toString().toLowerCase();
                                    etBoothCompanyName.setText(boothDetails.get(j).getBoothName().toString());
                                    etBusinessScope.setText(boothDetails.get(j).getBusinessScope().toString());
                                    etTelephone.setText(boothDetails.get(j).getPhone().toString());
                                    businesCard = boothDetails.get(j).getBusinessCard();
                                    String boothId = boothDetails.get(j).getBoothId();
                                    SharedPreferences.Editor PREFERENCE = PreferenceUtil.edit(getActivity());
                                    PREFERENCE.putString(PreferenceUtil.BOOTH_ID, boothId);
                                    PREFERENCE.apply();
                                    fileNameToSend = businesCard;
                                    ivboothImageviewCopy.setVisibility(View.GONE);
                                    ivboothImageview.setVisibility(View.VISIBLE);
                                    if (AppUtils.isNetworkAvailable(getActivity())) {
                                        Glide.with(getActivity()).
                                                load(URL.PhotView + businesCard)
                                                .into(ivboothImageview);
                                    }
                                }
                            }
                        }
                    }

                }

            }
        });
    }

    /* to add new trade or hall,
    * param 1- if 0 saves trade details
    * if 1 saves hall details*/
    public void tradeDialog(final int flage) {
        final Dialog dialog = new Dialog(getActivity());
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_hall_traide_options);

        Button btcancel = (Button) dialog.findViewById(R.id.btCancel);
        Button btsend = (Button) dialog.findViewById(R.id.btSend);
        final TextView tv_trade = (TextView) dialog.findViewById(R.id.tv_trade);
        final EditText etValuesTrade_Hall = (EditText) dialog.findViewById(R.id.tvValuesTrade_Hall);
        if (flage == 0) {
            tv_trade.setText("Enter new Trade District/ Area");
        } else {
            tv_trade.setText("Enter new hall");
        }
        btcancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });
        btsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (flage == 0) {

                    try {
                        if (etValuesTrade_Hall.getText().toString().trim().length() != 0) {
                            dialog.dismiss();
                            inserNewTradeOffline(etValuesTrade_Hall.getText().toString().trim(), "0", 1);
                        } else {
                            etValuesTrade_Hall.setError(getResources().getString(R.string.txt_field_null));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                    try {
                        if (etValuesTrade_Hall.getText().toString().trim().length() != 0) {
                            dialog.dismiss();
                            inserNewTradeOffline(etValuesTrade_Hall.getText().toString().trim(), tradeId[spinnertrade.getSelectedItemPosition()], 2);
                        } else {
                            etValuesTrade_Hall.setError(getResources().getString(R.string.txt_field_null));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        dialog.show();
    }

    /* method to save new  trade or hall offline.
    * param 1- name of trade or hall
    * param 2- if 0 then its trade otherwise hall
    * param 3- if 1 from trade if 2 then from hall*/
    public void inserNewTradeOffline(final String name, final String parent, final int fromOption) throws JSONException {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    int companyid = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
                    TradeInfo trade = new TradeInfo();

                    trade.setId(UUID.randomUUID().toString());
                    trade.setName(name);
                    trade.setParent(String.valueOf(parent));
                    trade.setCompanyId(String.valueOf(companyid));
                    mRealm.insertOrUpdate(trade);
                    CompanySetUpDetails trades = mRealm.where(CompanySetUpDetails.class)
                            .equalTo("CompanyID", companyid).findFirst();
                    if (trades != null) {
                        trades.getTradeDeatils().add(trade);
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

        if (fromOption == 1) {
            setSpinnerValue(1, name, 1);
        } else {
            setSpinnerValue(1, name, 2);
        }
    }

}
