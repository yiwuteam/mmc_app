package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ADMIN on 14-11-2016.
 */
public class LanguageTimeFragment extends BaseFragment {

    String Language = "0", Locale = "0", emaleSetting = "0";
    String[] language = {"English", "Chinese, Simplified"};
    private MenuItem menuItem;
    private String TimeZone[];
    private Spinner spTimeZone, spLanguage;
    private CheckBox isActivatedPlainEmail;
    private ProgressBar progressBar;
    private RequestQueue mQueue;

    public static LanguageTimeFragment newInstance() {
        LanguageTimeFragment fragment = new LanguageTimeFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_languagetime, container, false);
        mQueue = Volley.newRequestQueue(getActivity());
        initview(view);
        initlestener();
        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuItem = menu.add("action");
        menuItem.setTitle(getResources().getString(R.string.txt_save));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getTitle().equals(getResources().getString(R.string.txt_save))) {
            int chechedStatePlainText = 0;
            if (isActivatedPlainEmail.isChecked()) {
                chechedStatePlainText = 1;
            }
            try {
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    saveLanguageTime(chechedStatePlainText);

                } else {
                    DisplayDialog(getResources().getString(R.string.error_no_internet));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return super.onOptionsItemSelected(item);
    }


    public void setlanguageandTimeZone() throws Exception {
        progressBar.setVisibility(View.VISIBLE);
        String url = URL.setlanguage + "token=" + PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AUTH_TOKEN, null);
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    progressBar.setVisibility(View.GONE);
                    JSONArray jsonArray = response.getJSONArray("accountDeatils");
                    if (jsonArray.length() == 0) {

                    } else {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            emaleSetting = jsonObject.getString("EmailSettings");
                            Language = jsonObject.getString("Language");
                            Locale = jsonObject.getString("Locale");

                        }

                    }
                    if (emaleSetting.equals("0")) {
                        isActivatedPlainEmail.setChecked(false);
                    } else {
                        isActivatedPlainEmail.setChecked(true);
                    }
                    try {
                        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, language);
                        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                        spLanguage.setAdapter(spinnerArrayAdapter);
                        if (Language.trim().equals("")) {

                        } else {
                            for (int i = 0; i < language.length; i++) {
                                if (Language.equals(language[i])) {
                                    spLanguage.setSelection(i);
                                }
                            }
                        }
                    } catch (NullPointerException e) {
                        e.printStackTrace();
                    }


                    try {
                        getTimeZone(Locale);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(
             5000,
             DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
             DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(request);
    }

    public void setlangTime() throws Exception {

        emaleSetting = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EMAIL_SETTINGS, "0");
        Language = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.LANGUAGE, "");
        Locale = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.LOCALE, "");

        if (emaleSetting.equals("0")) {
            isActivatedPlainEmail.setChecked(false);
        } else {
            isActivatedPlainEmail.setChecked(true);
        }
        try {
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, language);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spLanguage.setAdapter(spinnerArrayAdapter);
            if (Language.trim().equals("")) {

            } else {
                for (int i = 0; i < language.length; i++) {
                    if (Language.equals(language[i])) {
                        spLanguage.setSelection(i);
                    }
                }
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }


        try {
            getTimeZone(Locale);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void saveLanguageTime(final int chechedStatePlainText) throws JSONException {
        progressBar.setVisibility(View.VISIBLE);
        JSONObject param = new JSONObject();
        param.put("token", PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AUTH_TOKEN, null));
        param.put("plaintext", chechedStatePlainText);
        param.put("language", spLanguage.getSelectedItem().toString());
        param.put("localset", spTimeZone.getSelectedItem().toString());
        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL.SaveLanguageDetail, param, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String message = response.getString("message");
                    int resultCode = response.getInt("resultCode");
                    if (resultCode == 0) {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                    } else if (resultCode == 1) {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                        SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                        PREF.putString(PreferenceUtil.EMAIL_SETTINGS, String.valueOf(chechedStatePlainText));
                        PREF.putString(PreferenceUtil.LANGUAGE, spLanguage.getSelectedItem().toString());
                        PREF.putString(PreferenceUtil.LOCALE, spTimeZone.getSelectedItem().toString());
                        PREF.apply();
                    }
                    Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(
             5000,
             DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
             DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(request);
    }


    @Override
    public int getTitle() {
        return R.string.txt_language_time;
    }

    private void initlestener() {

    }

    private void initview(View view) {
        spTimeZone = (Spinner) view.findViewById(R.id.spTimeZone);
        spLanguage = (Spinner) view.findViewById(R.id.spLanguage);
        isActivatedPlainEmail = (CheckBox) view.findViewById(R.id.isActivatedPlainEmail);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        if (AppUtils.isNetworkAvailable(getActivity())) {
            try {
                setlanguageandTimeZone();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            try {
                setlangTime();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("TimeZone.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public void getTimeZone(String locale) throws Exception {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray zones_jArry = obj.getJSONArray("zones");
            TimeZone = new String[zones_jArry.length()];
            for (int i = 0; i < zones_jArry.length(); i++) {
                JSONObject jo_inside = zones_jArry.getJSONObject(i);

                TimeZone[i] = jo_inside.getString("zoneName");

            }
            try {
                ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, TimeZone);
                spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
                spTimeZone.setAdapter(spinnerArrayAdapter);
                if (locale.trim().equals("")) {

                } else {
                    for (int i = 0; i < TimeZone.length; i++) {
                        if (locale.equals(TimeZone[i])) {
                            spTimeZone.setSelection(i);
                        }
                    }
                }
            } catch (NullPointerException e) {
                e.printStackTrace();
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
