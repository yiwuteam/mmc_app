package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.adapter.GoldAgentListAddapter;
import com.mixmycontainer.yiwu.bus.BusFactory;
import com.mixmycontainer.yiwu.bus.events.GoldAgentEvent;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.models.GoldAgentInfo;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by minusbug on 12/22/2016.
 */
public class GoldAgentFragment extends BaseFragment {

    int companyId;
    ProgressBar progressBar;
    RealmList<GoldAgentInfo> goldAgentList;
    private Realm mRealm;
    private RecyclerView rvagentlist;
    private LinearLayoutManager mlayoutManager;
    private GoldAgentListAddapter adapter;
    private MenuItem fav;

    public static GoldAgentFragment newinstnace() {
        GoldAgentFragment fragment = new GoldAgentFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_gold_agent, container, false);

        mRealm =  Realm.getDefaultInstance();

        initview(view);
        return view;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(false);
    }


    @Override
    public int getTitle() {
        return R.string.txt_goldagent;
    }

    private void initview(View view) {
        rvagentlist = (RecyclerView) view.findViewById(R.id.rvagentlist);

        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));

        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
        if (!companySetUpDetails.isEmpty()) {
            try {

                for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {
                    mRealm.beginTransaction();
                    goldAgentList = companySetUpDetails.get(i).getGoldAgentDeatils();
                    rvagentlist.setHasFixedSize(true);
                    mlayoutManager = new LinearLayoutManager(getActivity());
                    rvagentlist.setLayoutManager(mlayoutManager);
                    adapter = new GoldAgentListAddapter(getActivity(), companySetUpDetails.get(i).getGoldAgentDeatils(), new GoldAgentListAddapter.itemclick() {
                        @Override
                        public void click(int position) {
                            SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                            PREF.putString(PreferenceUtil.USER_AGENT_ID, goldAgentList.get(position).getId());
                            PREF.putString(PreferenceUtil.USER_AGENT_NAME, goldAgentList.get(position).getCompanyName());
                            PREF.apply();
                            BusFactory.getBus().post(new GoldAgentEvent());
                            getActivity().finish();
                        }
                    });
                    rvagentlist.setAdapter(adapter);
                    adapter.notifyDataSetChanged();
                    mRealm.commitTransaction();
                }

            } catch (Exception e) {
                mRealm.cancelTransaction();
            }
        } else {
            Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        fav = menu.add("Done");
        fav.setTitle(R.string.txt_ceate_new);
        fav.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

}
