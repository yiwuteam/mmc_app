package com.mixmycontainer.yiwu.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.LogInActivity;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by minusbug on 12/28/2016.
 */

public class OtpConfirmFragment extends BaseFragment {

    EditText etPassword, etConfirmPassword, etOTP;
    Button btLogin, btConfirm, btContinue;
    ProgressBar progressBar;
    LinearLayout lnr_etPassword, lnr_etConfirmPassword, lnr_etOTP;
    String Otp;
    private RequestQueue mQueue;

    public static OtpConfirmFragment newInstnce() {
        OtpConfirmFragment fragment = new OtpConfirmFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_otp_confirm, container, false);
        mQueue = Volley.newRequestQueue(getActivity());
        initView(view);
        final String OTP_email = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_OTP_email, "0");

        btContinue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String OTP = etOTP.getText().toString();
                Otp = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_OTP, "0");
                if (etOTP.getText().toString().trim().length() != 0) {
                    if (OTP.equals(Otp)) {

                        lnr_etOTP.setVisibility(View.GONE);
                        lnr_etConfirmPassword.setVisibility(View.VISIBLE);
                        lnr_etPassword.setVisibility(View.VISIBLE);
                        btContinue.setVisibility(View.GONE);
                        btConfirm.setVisibility(View.VISIBLE);

                    } else {

                        etOTP.setError("Invalid OTP");
                    }

                } else {

                    etOTP.setError("Field can't be blank");

                }

            }
        });
        btConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (etConfirmPassword.getText().toString().trim().length() == 0 || etPassword.getText().toString().trim().length() == 0) {
                    if (etConfirmPassword.getText().toString().trim().length() == 0) {
                        etConfirmPassword.setError("Field can't be blank");
                    }
                    if (etPassword.getText().toString().trim().length() == 0) {
                        etPassword.setError("Field can't be blank");
                    }
                } else {
                    if (AppUtils.isNetworkAvailable(getActivity())) {
                        if (etPassword.getText().toString().length() >= 6) {
                            if (etConfirmPassword.getText().toString().equals(etPassword.getText().toString())) {
                                String password = etPassword.getText().toString();
                                password = password.replaceAll(" ", "%20");
                                try {
                                    saveNewPassword(OTP_email, password);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }

                            } else {
                                etConfirmPassword.setError("Password mismatch");
                            }
                        } else {
                            etPassword.setError("Must contain minimum 6 characters.");
                        }
                    } else {
                        DisplayDialog(getResources().getString(R.string.error_no_internet));
                    }


                }
            }
        });
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(LogInActivity.getnewIntent(getActivity()));
                getActivity().overridePendingTransition(R.anim.scale_center_in, R.anim.scale_center_out);
            }
        });

        return view;
    }

    public void initView(View view) {

        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) view.findViewById(R.id.etConfirmPassword);
        etOTP = (EditText) view.findViewById(R.id.etOTP);
        btLogin = (Button) view.findViewById(R.id.btLogin);
        btConfirm = (Button) view.findViewById(R.id.btConfirm);
        btContinue = (Button) view.findViewById(R.id.btContinue);
        lnr_etPassword = (LinearLayout) view.findViewById(R.id.lnr_etPassword);
        lnr_etConfirmPassword = (LinearLayout) view.findViewById(R.id.lnr_etConfirmPassword);
        lnr_etOTP = (LinearLayout) view.findViewById(R.id.lnr_etOTP);

    }

    public void saveNewPassword(String OTP_email, String newPassword) throws JSONException {

        JSONObject param = new JSONObject();
        param.put("newPassword", newPassword);
        param.put("email", OTP_email);

        final JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, URL.SaveNewPassword, param, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    String message = response.getString("message");
                    int resultCode = response.getInt("resultCode");
                    if (resultCode == 0) {
                        Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                        progressBar.setVisibility(View.INVISIBLE);
                    } else if (resultCode == 1) {
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast.makeText(getActivity(), message + "", Toast.LENGTH_SHORT).show();
                        startActivity(LogInActivity.getnewIntent(getActivity()));
                        getActivity().overridePendingTransition(R.anim.scale_center_in, R.anim.scale_center_out);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressBar.setVisibility(View.INVISIBLE);
                Toast.makeText(getActivity(), error.toString(), Toast.LENGTH_SHORT).show();
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(
             5000,
             DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
             DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(request);
    }
}
