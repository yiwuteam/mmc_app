package com.mixmycontainer.yiwu.fragment;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Environment;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.NewOrderActivity;
import com.mixmycontainer.yiwu.adapter.OrderListAdapter;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.apis.models.BoothDetailOnlineModel;
import com.mixmycontainer.yiwu.apis.models.ContainerDetailsOnlineModel;
import com.mixmycontainer.yiwu.apis.models.ItemDetailOnlineModel;
import com.mixmycontainer.yiwu.apis.models.OrderDetailOnlineModel;
import com.mixmycontainer.yiwu.apis.models.OrderListModel;
import com.mixmycontainer.yiwu.bus.events.RefreshEvent;
import com.mixmycontainer.yiwu.models.BoothDetails;
import com.mixmycontainer.yiwu.models.BoothDetailsInfo;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.models.ContactInfo;
import com.mixmycontainer.yiwu.models.ContainerDetails;
import com.mixmycontainer.yiwu.models.ContainerInfo;
import com.mixmycontainer.yiwu.models.GoldAgentInfo;
import com.mixmycontainer.yiwu.models.ItemDetails;
import com.mixmycontainer.yiwu.models.OrderListDetails;
import com.mixmycontainer.yiwu.models.TradeInfo;
import com.mixmycontainer.yiwu.realm.MyApplication;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

//import com.mixmycontainer.yiwu.activity.NewOrderActivity;

/**
 * Created by ADMIN on 09-11-2016.
 */
public class OrderListFragment extends BaseFragment {
    String authToken;
    JsonObject companyData;
    JsonObject jsonObjectContainerDetails;
    JsonObject jsonObjecttradeDeatils;
    JsonObject jsonObjectboothDeatils;
    JsonObject jsonObjectgoldAgentDeatils;
    JsonObject jsonObject;

    JsonArray goldAgentDeatils;
    JsonArray boothDeatils;
    JsonArray tradeDetails;
    JsonArray containerDetails;

    CompanySetUpDetails companySetUpDetails;
    private SharedPreferences sharedPref;
    private OrderListModel listModel;
    private OrderDetailOnlineModel orderDetailOnlineModel;
    private ContainerDetailsOnlineModel containerDetailsOnlineModel;
    private BoothDetailOnlineModel boothDetailOnlineModel;
    private ItemDetailOnlineModel itemDetailOnlineModel;

    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ContainerInfo containerInfo;
    private int flagToNewOrder = 0, flag = 0;
    private TradeInfo tradeInfo;
    private BoothDetailsInfo boothDetailsInfo;
    private GoldAgentInfo goldAgentInfo;
    private ContactInfo contactInfo;
    private List<OrderListModel> orderListModels;
    private List<OrderDetailOnlineModel> orderDetailOnlineModels;
    private List<ContainerDetailsOnlineModel> containerDetailsOnlineModels;
    private List<BoothDetailOnlineModel> boothDetailOnlineModels;
    private List<ItemDetailOnlineModel> itemDetailOnlineModels;
    private RealmList<ContainerInfo> containerInfos;
    private RealmList<TradeInfo> tradeInfos;
    private RealmList<ContactInfo> contactInfos;
    private RealmList<GoldAgentInfo> goldAgentInfos;
    private RealmList<BoothDetailsInfo> boothDeatilsInfos;
    private LinearLayout lnr_orderlist;

    private String URL_OrderList = com.mixmycontainer.yiwu.apis.core.URL.OrderList;
    private String URL_CompanyDetails = com.mixmycontainer.yiwu.apis.core.URL.CompanyDetails;
    private Realm mRealm;
    private RecyclerView rvOrderlist;
    private LinearLayoutManager mlayoutManager;
    private OrderListAdapter adapter;
    private RequestQueue mQueue;

    public static OrderListFragment newInstence() {
        OrderListFragment fragment = new OrderListFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_orderlist, container, false);
        mQueue = Volley.newRequestQueue(getActivity());
        initview(view);

        mRealm = Realm.getDefaultInstance();

        sharedPref = PreferenceUtil.get(getActivity());
        authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);
        System.out.println("authToken: " + authToken);

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                sharedPref = PreferenceUtil.get(getActivity());
                String authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);
                if (AppUtils.isNetworkAvailable(getActivity())) {

                    OrderList(authToken);
                    mSwipeRefreshLayout.setRefreshing(false);
                } else {
                    mSwipeRefreshLayout.setRefreshing(false);
                    getOrderDetailsOffline();
                    Toast.makeText(getContext(), "No internet connection.", Toast.LENGTH_SHORT).show();

                }
            }
        });

        if (AppUtils.isNetworkAvailable(getActivity())) {
            String flage = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.FLAG, "0");
            if (flage.equals("0")) {

                GetCompanyDetails(authToken);
                SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                PREF.putString(PreferenceUtil.FLAG, "1");
                PREF.apply();

            } else {

            }
        } else {
//            DisplayDialog(getResources().getString(R.string.error_no_internet));
        }
        if (AppUtils.isNetworkAvailable(getActivity())) {
            try {
                setlanguageandTimeZone();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {

        }
        if (AppUtils.isNetworkAvailable(getActivity())) {
            OrderList(authToken);
        } else {
            getOrderDetailsOffline();
        }

        return view;
    }

    private void uploadToserver() {

        mRealm = Realm.getDefaultInstance();
        int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        RealmResults<OrderListDetails> OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("Synch", "0").findAll();
        JSONObject dataMain = new JSONObject();
        JSONObject data = null;


        final JSONArray jsonData = new JSONArray();
        if (!OrderListDetailsDetails.isEmpty()) {
            System.out.println("not empty");
            try {

                for (int i = 0; i < OrderListDetailsDetails.size(); i++) {

                    data = new JSONObject();
                    try {
                        data.put("OrderId", OrderListDetailsDetails.get(i).getOrderId());
                        data.put("OrderNumber", OrderListDetailsDetails.get(i).getOrderNumber());
                        data.put("CompanyId", String.valueOf(OrderListDetailsDetails.get(i).getCompanyID()));
                        data.put("Trade", OrderListDetailsDetails.get(i).getTrade());

                        data.put("TradeName", OrderListDetailsDetails.get(i).getTradeName());
//                        data.put("TradeName","Testing Purpose");


                        data.put("ProductType", OrderListDetailsDetails.get(i).getProductType());
                        //ToGetHall name
                        try {
                            RealmResults<TradeInfo> tradeInfo = mRealm.where(TradeInfo.class).equalTo("CompanyId", String.valueOf(companyId)).findAll();
                            for (int z = 0; z < tradeInfo.size(); z++) {
                                if (tradeInfo.get(z).getId().toString().equals(OrderListDetailsDetails.get(i).getProductType().toString())) {
                                    data.put("HallName", tradeInfo.get(z).getName().toString());

                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }


                        data.put("TotalOrderUsValue", OrderListDetailsDetails.get(i).getTotalOrderUsValue());
                        data.put("TotalOrderRmbValue", OrderListDetailsDetails.get(i).getTotalOrderRmbValue());
                        data.put("TotalOrderZarValue", OrderListDetailsDetails.get(i).getTotalOrderZarValue());
                        data.put("Total3rdCurrencyValue", OrderListDetailsDetails.get(i).getTotal3rdCurrencyValue());
                        data.put("TotalCartons", OrderListDetailsDetails.get(i).getTotalCartons());
                        data.put("TotalBooth", OrderListDetailsDetails.get(i).getTotalBooth());
                        data.put("TotalContainer", OrderListDetailsDetails.get(i).getTotalContainer());
                        data.put("TotalOrderWeight", OrderListDetailsDetails.get(i).getTotalOrderWeight());
                        data.put("TotalOrderCBM", OrderListDetailsDetails.get(i).getTotalOrderCBM());
                        data.put("OrderStatus", OrderListDetailsDetails.get(i).getOrderStatus());
                        data.put("LastUpdate", OrderListDetailsDetails.get(i).getLastUpdate());


                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }


                    JSONObject dataObj = null;
                    JSONArray containerDetailsOnlineModels = new JSONArray();
                    for (int k = 0; k < OrderListDetailsDetails.get(i).getContainerDetailses().size(); k++) {
                        dataObj = new JSONObject();
                        dataObj.put("ContainerId", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerId());
                        dataObj.put("ContainerType", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerType());
                        dataObj.put("OrderId", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getOrderId());
                        dataObj.put("CompanyId", String.valueOf(companyId));
                        dataObj.put("FilledWeight", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getFilledWeight());
                        dataObj.put("FilledVolume", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getFilledVolume());
                        dataObj.put("Volumepercentage", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getVolumepercentage());
                        dataObj.put("Weightpercentage", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getWeightpercentage());
                        dataObj.put("Status", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getStatus());
                        dataObj.put("ItemSaveStatus", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getStatus());
                        dataObj.put("ID", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerId());
                        dataObj.put("Container", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerType());
                        dataObj.put("MaximumVolume", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getMaximumVolume());
                        dataObj.put("MaximumWeight", OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getMaximumWeight());
                        containerDetailsOnlineModels.put(dataObj);
                    }
                    data.put("containerdetailsbyorderid", containerDetailsOnlineModels);

                    JSONObject dataObjj = null;
                    JSONArray boothDetailOnlineModels = new JSONArray();
                    for (int k = 0; k < OrderListDetailsDetails.get(i).getBoothDetailses().size(); k++) {
                        dataObjj = new JSONObject();
                        dataObjj.put("BoothId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBoothId());
                        dataObjj.put("OrderId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getOrderId());
                        dataObjj.put("CompanyId", String.valueOf(companyId));
                        dataObjj.put("BoothNumber", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBoothNumber());
                        dataObjj.put("BoothName", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBoothName());
                        dataObjj.put("Phone", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getPhone());
                        dataObjj.put("BusinessScope", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBusinessScope());
                        dataObjj.put("ValuePurchase", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getValuePurchase());
                        dataObjj.put("ValuePurchaseRMB", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getValuePurchaseRMB());
                        dataObjj.put("BusinessCard", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBusinessCard());
                        dataObjj.put("BoothStatus", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getBoothStatus());
                        dataObjj.put("TradeId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getTradeId());
                        dataObjj.put("HallId", OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getHallId());
                        try {
                            RealmResults<TradeInfo> tradeInfo = mRealm.where(TradeInfo.class).equalTo("CompanyId", String.valueOf(companyId)).findAll();
                            for (int z = 0; z < tradeInfo.size(); z++) {
                                if (tradeInfo.get(z).getId().toString().equals(OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getTradeId().toString())) {
                                    String tradeName = tradeInfo.get(z).getName().toString();
                                    dataObjj.put("TradeName", tradeInfo.get(z).getName().toString());

                                }
                                if (tradeInfo.get(z).getId().toString().equals(OrderListDetailsDetails.get(i).getBoothDetailses().get(k).getHallId().toString())) {
                                    String hallName = tradeInfo.get(z).getName().toString();
                                    dataObjj.put("HallName", tradeInfo.get(z).getName().toString());

                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        boothDetailOnlineModels.put(dataObjj);
                    }
                    data.put("boothdetailsbyorderid", boothDetailOnlineModels);

                    JSONObject dataObjjj = null;
                    JSONArray itemDetailOnlineModels = new JSONArray();


                    for (int f = 0; f < OrderListDetailsDetails.get(i).getItemDetailses().size(); f++) {
                        dataObjjj = new JSONObject();
                        dataObjjj.put("ItemId", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getItemId());
                        dataObjjj.put("CompanyId", String.valueOf(companyId));
                        dataObjjj.put("OrderId", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getOrderId());
                        dataObjjj.put("BoothId", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getBoothId());
                        dataObjjj.put("ContainerId", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getContainerId());
                        dataObjjj.put("ItemNo", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getItemNo());
                        dataObjjj.put("DescriptionFrom", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getDescriptionFrom());
                        dataObjjj.put("DescriptionTo", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getDescriptionTo());
                        dataObjjj.put("UnitperCarton", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getUnitperCarton());
                        dataObjjj.put("WeightofCarton", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getWeightofCarton());
                        dataObjjj.put("Length", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getLength());
                        dataObjjj.put("Width", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getWidth());
                        dataObjjj.put("Height", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getHeight());
                        dataObjjj.put("CBM", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getCBM());
                        dataObjjj.put("Picture", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getPicture());
                        dataObjjj.put("PriceInUs", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getPriceInUs());
                        dataObjjj.put("PriceInRmb", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getPriceInRmb());
                        dataObjjj.put("ThirdCurrencyType", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getThirdCurrencyType());
                        dataObjjj.put("PriceInThirdCurrency", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getPriceInThirdCurrency());
                        dataObjjj.put("UnitQuantity", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getUnitQuantity());
                        dataObjjj.put("CartonQuantity", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getCartonQuantity());
                        dataObjjj.put("TotalPriceInUS", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getTotalPriceInUS());
                        dataObjjj.put("TotalPriceInRMB", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getTotalPriceInRMB());
                        dataObjjj.put("TotalPriceIn3rdCurrency", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getTotalPriceIn3rdCurrency());
                        dataObjjj.put("Date", OrderListDetailsDetails.get(i).getItemDetailses().get(f).getDate());
                        itemDetailOnlineModels.put(dataObjjj);
                    }
                    data.put("itemdetailsbyboothorderid", itemDetailOnlineModels);
                    jsonData.put(data);

                }


                dataMain.put("data", jsonData);
            } catch (Exception e) {
                mRealm.cancelTransaction();
            }

            String url = "https://mixmycontainer.com/QA/Api_controller/offlinesyncorder";
// Tag used to cancel the request
            String tag_json_obj = "json_obj_req";
            final JSONObject params = new JSONObject();
            try {
                params.put("data", jsonData);
                System.out.println("jsn:" + jsonData);

            } catch (JSONException e) {
                e.printStackTrace();

            }

            final ProgressDialog pDialog = new ProgressDialog(getActivity());
            pDialog.setMessage("Loading...");
            pDialog.setCanceledOnTouchOutside(false);
            pDialog.show();

            JsonObjectRequest jsonObjReq = new JsonObjectRequest(Request.Method.POST,
                    url, params,
                    new Response.Listener<JSONObject>() {

                        @Override
                        public void onResponse(JSONObject response) {

                            pDialog.hide();
                            String resultcode = null;
                            try {
                                resultcode = response.getString("resultCode").toString();
                                if (resultcode.equals("1")) {

                                    deleteTempTable();
                                    getOrderFromApi();
                                    GetCompanyDetails(authToken);
                                    Toast.makeText(getActivity(), response.getString("message").toString() + " ", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(getActivity(), "Sync Failed", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {

                    System.out.println("Jsn: " + params);
                    String err = error.getMessage();
                    JSONArray aa = jsonData;
                    pDialog.hide();
                    Toast.makeText(getActivity(), "Sync Failed", Toast.LENGTH_SHORT).show();
//                    getOrderFromApi();
                }
            });

// Adding request to request queue
            MyApplication.getInstance().addToRequestQueue(jsonObjReq, "getOrderApi");


            System.out.println("jsonData" + jsonData);

        } else {
//            Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
            deleteTempTable();
            if (AppUtils.isNetworkAvailable(getActivity())) {
                getOrderFromApi();
            } else {
                getOrderDetailsOffline();

            }
        }
        generateNoteOnSD("MMClog.txt", String.valueOf(dataMain));

    }

    private void generateNoteOnSD(String sFileName, String sBody) {
        try {
            File root = new File(Environment.getExternalStorageDirectory(), "MMCLogs");
            if (!root.exists()) {
                root.mkdirs();

            }
            File gpxfile = new File(root, sFileName);
            try {
                //BufferedWriter for performance, true to set append to file flag
                BufferedWriter buf = new BufferedWriter(new FileWriter(gpxfile, true));
                buf.append(sBody);
                buf.newLine();
                buf.close();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    private void getOrderDetailsOffline() {

        mRealm = Realm.getDefaultInstance();
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));

        RealmResults<OrderListDetails> OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).findAll();
        if (!OrderListDetailsDetails.isEmpty()) {
            lnr_orderlist.setVisibility(View.GONE);
            try {
                orderListModels = new ArrayList<OrderListModel>();
                for (int i = 0; i < OrderListDetailsDetails.size(); i++) {

                    listModel = new OrderListModel();
                    listModel.setOrderNumber(OrderListDetailsDetails.get(i).getOrderNumber());

                    listModel.setOrderId(OrderListDetailsDetails.get(i).getOrderId());
                    listModel.setTotalOrderUsValue(OrderListDetailsDetails.get(i).getTotalOrderUsValue());
                    listModel.setTotalOrderRmbValue(OrderListDetailsDetails.get(i).getTotalOrderRmbValue());

                    listModel.setTotalCartons(OrderListDetailsDetails.get(i).getTotalCartons());
                    listModel.setTotalBooth(OrderListDetailsDetails.get(i).getTotalBooth());
                    listModel.setTotalContainer(OrderListDetailsDetails.get(i).getTotalContainer());
                    listModel.setTotalOrderCBM(OrderListDetailsDetails.get(i).getTotalOrderCBM());
                    listModel.setOrderStatus(OrderListDetailsDetails.get(i).getOrderStatus());
                    listModel.setLastUpdate(OrderListDetailsDetails.get(i).getLastUpdate());
                    RealmList<ContainerDetails> containerDetail = OrderListDetailsDetails.get(i).getContainerDetailses();
                    for (int m = 0; m < containerDetail.size(); m++) {
                        if (containerDetail.get(m).getStatus().toString().equals("0")) {
                            listModel.setContainerId(containerDetail.get(m).getContainerId().toString());
                        }
                    }
                    orderListModels.add(listModel);
                }
                rvOrderlist.setHasFixedSize(true);
                mlayoutManager = new LinearLayoutManager(getActivity());
                rvOrderlist.setLayoutManager(mlayoutManager);
                Collections.reverse(orderListModels);

                adapter = new OrderListAdapter(getActivity(), orderListModels);

                rvOrderlist.setAdapter(adapter);
                adapter.notifyDataSetChanged();
                rvOrderlist.setVisibility(View.VISIBLE);
                pDialog.dismiss();

            } catch (Exception e) {
//                pDialog.dismiss();
                mRealm.cancelTransaction();
            }
        } else {
            pDialog.dismiss();
            lnr_orderlist.setVisibility(View.VISIBLE);
//            Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onResume() {
        flagToNewOrder = 0;
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    public void setlanguageandTimeZone() throws Exception {

        String url = URL.setlanguage + "token=" + PreferenceUtil.get(getContext()).getString(PreferenceUtil.USER_AUTH_TOKEN, "0");
        JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new com.android.volley.Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {

                    JSONArray jsonArray = response.getJSONArray("accountDeatils");
                    if (jsonArray.length() == 0) {
                        SharedPreferences.Editor PREF = PreferenceUtil.edit(getContext());
                        PREF.putString(PreferenceUtil.EMAIL_SETTINGS, "0");
                        PREF.putString(PreferenceUtil.LANGUAGE, "");
                        PREF.putString(PreferenceUtil.LOCALE, "");
                        PREF.apply();
                    } else {
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject = jsonArray.getJSONObject(i);
                            SharedPreferences.Editor PREF = PreferenceUtil.edit(getContext());
                            PREF.putString(PreferenceUtil.EMAIL_SETTINGS, jsonObject.getString("EmailSettings"));
                            PREF.putString(PreferenceUtil.LANGUAGE, jsonObject.getString("Language"));
                            PREF.putString(PreferenceUtil.LOCALE, jsonObject.getString("Locale"));
                            PREF.apply();
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new com.android.volley.Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                Toast.makeText(getActivity(), "something went wrong please try again later", Toast.LENGTH_SHORT).show();
            }
        });
        request.setRetryPolicy(new DefaultRetryPolicy(
                5000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        mQueue.add(request);
    }

    private void initview(View view) {
        rvOrderlist = (RecyclerView) view.findViewById(R.id.rvOrderlist);


        lnr_orderlist = (LinearLayout) view.findViewById(R.id.lnr_orderlist);

        mSwipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.activity_main_swipe_refresh_layout);
        FloatingActionButton floatingActionButton = (FloatingActionButton) view.findViewById(R.id.fbAddnewOrder);

        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                if (AppUtils.isNetworkAvailable(getActivity())) {
//                if (flag == 1) {
                if (flagToNewOrder == 0) {
                    flagToNewOrder = 1;
                    startActivity(NewOrderActivity.getnewIntent(getActivity()));
                    getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                }


            }
        });

        SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
        PREF.putString(PreferenceUtil.ContinueItemAdd, "0");
        PREF.apply();

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.orderlist_menus, menu);
        super.onCreateOptionsMenu(menu, inflater);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView searchView = new SearchView(getSupportActionBar().getThemedContext());
        EditText searchEditText = (EditText) searchView.findViewById(android.support.v7.appcompat.R.id.search_src_text);
        searchEditText.setTextColor(getResources().getColor(R.color.white));
        ImageView searchClose = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_close_btn);
        searchClose.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
        ImageView searchgo = (ImageView) searchView.findViewById(android.support.v7.appcompat.R.id.search_go_btn);
        searchgo.setColorFilter(Color.parseColor("#ffffff"), PorterDuff.Mode.SRC_ATOP);
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, searchView);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                try {
                    adapter.filter(newText);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return false;
            }
        });
        searchView.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View v) {

                                          }
                                      }
        );
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void deleteTempTable() {
        final int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                final RealmResults<OrderListDetails> students = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).findAll();
                students.deleteAllFromRealm();
            }
        });
    }

    private void OrderList(String authToken) {

        uploadToserver();


    }

    private void getOrderFromApi() {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setMessage("Loading...");
        pDialog.show();
        Ion.with(getActivity())
                .load(URL_OrderList + authToken)
                .asJsonObject()

                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, final JsonObject result) {
                        if (AppUtils.isNetworkAvailable(getActivity())) {
                            try {
                                String requestCode = result.get("resultCode").toString();

                                if (requestCode.equals("1")) {
                                    pDialog.dismiss();
                                    lnr_orderlist.setVisibility(View.GONE);


                                    try { // I could use try-with-resources here
                                        final Realm mRealmThread = Realm.getDefaultInstance();
                                        mRealmThread.executeTransactionAsync(new Realm.Transaction() {
                                            @Override
                                            public void execute(Realm realm) {
                                                JsonArray orderData = result.getAsJsonArray("data");
                                                for (int i = orderData.size() - 1; i >= 0; i--) {

                                                    jsonObject = orderData.get(i).getAsJsonObject();
                                                    String OrderId = getNullAsEmptyString(jsonObject.get("OrderId"));
                                                    OrderListDetails orderListSetUpNew = realm.createObject(OrderListDetails.class, OrderId);

                                                    // save order id to preference
                                                    orderListSetUpNew.setCompanyID(Integer.parseInt(getNullAsEmptyString(jsonObject.get("CompanyId"))));
                                                    orderListSetUpNew.setOrderNumber(getNullAsEmptyString(jsonObject.get("OrderNumber")));
                                                    orderListSetUpNew.setTrade(getNullAsEmptyString(jsonObject.get("Trade")));
                                                    orderListSetUpNew.setProductType(getNullAsEmptyString(jsonObject.get("ProductType")));
                                                    orderListSetUpNew.setSynch("1");

                                                    orderListSetUpNew.setTotalOrderUsValue(getNullAsEmptyString(jsonObject.get("TotalOrderUsValue")));
                                                    orderListSetUpNew.setTotalOrderRmbValue(getNullAsEmptyString(jsonObject.get("TotalOrderRmbValue")));
                                                    orderListSetUpNew.setTotalOrderZarValue(getNullAsEmptyString(jsonObject.get("TotalOrderZarValue")));
                                                    orderListSetUpNew.setTotal3rdCurrencyValue(getNullAsEmptyString(jsonObject.get("Total3rdCurrencyValue")));

                                                    orderListSetUpNew.setTotalCartons(getNullAsEmptyString(jsonObject.get("TotalCartons")));
                                                    orderListSetUpNew.setTotalBooth(getNullAsEmptyString(jsonObject.get("TotalBooth")));
                                                    orderListSetUpNew.setTotalContainer(getNullAsEmptyString(jsonObject.get("TotalContainer")));
                                                    orderListSetUpNew.setTotalOrderWeight(getNullAsEmptyString(jsonObject.get("TotalOrderWeight")));

                                                    orderListSetUpNew.setTotalOrderCBM(getNullAsEmptyString(jsonObject.get("TotalOrderCBM")));
                                                    orderListSetUpNew.setOrderStatus(getNullAsEmptyString(jsonObject.get("OrderStatus")));
                                                    orderListSetUpNew.setLastUpdate(getNullAsEmptyString(jsonObject.get("LastUpdate"))); // todays date
                                                    JsonObject jsonObjectboothdetail;
                                                    JsonArray boothdetailsbyorderid = jsonObject.getAsJsonArray("boothdetailsbyorderid");
                                                    for (int k = 0; k < boothdetailsbyorderid.size(); k++) {
                                                        jsonObjectboothdetail = boothdetailsbyorderid.get(k).getAsJsonObject();
                                                        BoothDetails boothExistOrNxot = realm.copyToRealm(new BoothDetails());
                                                        boothExistOrNxot.setOrderId(getNullAsEmptyString(jsonObjectboothdetail.get("OrderId")));

                                                        boothExistOrNxot.setBoothId(getNullAsEmptyString(jsonObjectboothdetail.get("BoothId")));
                                                        boothExistOrNxot.setBoothName(getNullAsEmptyString(jsonObjectboothdetail.get("BoothName")));
                                                        boothExistOrNxot.setBoothNumber(getNullAsEmptyString(jsonObjectboothdetail.get("BoothNumber")));

                                                        boothExistOrNxot.setBoothStatus(getNullAsEmptyString(jsonObjectboothdetail.get("BoothStatus")));
                                                        boothExistOrNxot.setBusinessCard(getNullAsEmptyString(jsonObjectboothdetail.get("BusinessCard")));
                                                        boothExistOrNxot.setBusinessScope(getNullAsEmptyString(jsonObjectboothdetail.get("BusinessScope")));

                                                        boothExistOrNxot.setTradeId(getNullAsEmptyString(jsonObjectboothdetail.get("TradeId")));
                                                        boothExistOrNxot.setHallId(getNullAsEmptyString(jsonObjectboothdetail.get("HallId")));
                                                        boothExistOrNxot.setValuePurchaseRMB(getNullAsEmptyString(jsonObjectboothdetail.get("ValuePurchaseRMB")));
                                                        boothExistOrNxot.setValuePurchase(getNullAsEmptyString(jsonObjectboothdetail.get("ValuePurchase")));
                                                        boothExistOrNxot.setPhone(getNullAsEmptyString(jsonObjectboothdetail.get("Phone")));
                                                        orderListSetUpNew.getBoothDetailses().add(boothExistOrNxot);

                                                    }
                                                    //Item Details Fetching
                                                    JsonObject itemdetails;
                                                    JsonArray JAitemdetails = jsonObject.getAsJsonArray("itemdetailsbyboothorderid");
                                                    for (int b = 0; b < JAitemdetails.size(); b++) {
                                                        itemdetails = JAitemdetails.get(b).getAsJsonObject();
                                                        ItemDetails itemDetails = realm.copyToRealm(new ItemDetails());
                                                        itemDetails.setOrderId(getNullAsEmptyString(itemdetails.get("OrderId")));
                                                        itemDetails.setBoothId(getNullAsEmptyString(itemdetails.get("BoothId")));
                                                        itemDetails.setItemId(getNullAsEmptyString(itemdetails.get("ItemId")));
                                                        itemDetails.setItemNo(getNullAsEmptyString(itemdetails.get("ItemNo")));
                                                        itemDetails.setDescriptionFrom(getNullAsEmptyString(itemdetails.get("DescriptionFrom")));
                                                        itemDetails.setDescriptionTo(getNullAsEmptyString(itemdetails.get("DescriptionTo")));
                                                        itemDetails.setUnitperCarton(getNullAsEmptyString(itemdetails.get("UnitperCarton")));
                                                        itemDetails.setContainerId(getNullAsEmptyString(itemdetails.get("ContainerId")));
                                                        itemDetails.setWeightofCarton(getNullAsEmptyString(itemdetails.get("WeightofCarton")));
                                                        itemDetails.setLength(getNullAsEmptyString(itemdetails.get("Length")));
                                                        itemDetails.setWidth(getNullAsEmptyString(itemdetails.get("Width")));
                                                        itemDetails.setHeight(getNullAsEmptyString(itemdetails.get("Height")));
                                                        itemDetails.setCBM(getNullAsEmptyString(itemdetails.get("CBM")));
                                                        itemDetails.setPicture(getNullAsEmptyString(itemdetails.get("Picture")));
                                                        itemDetails.setPriceInUs(getNullAsEmptyString(itemdetails.get("PriceInUs")));
                                                        itemDetails.setPriceInRmb(getNullAsEmptyString(itemdetails.get("PriceInRmb")));
                                                        itemDetails.setThirdCurrencyType(getNullAsEmptyString(itemdetails.get("ThirdCurrencyType")));
                                                        itemDetails.setPriceInThirdCurrency(getNullAsEmptyString(itemdetails.get("PriceInThirdCurrency")));
                                                        itemDetails.setUnitQuantity(getNullAsEmptyString(itemdetails.get("UnitQuantity")));
                                                        itemDetails.setCartonQuantity(getNullAsEmptyString(itemdetails.get("CartonQuantity")));
                                                        itemDetails.setTotalPriceInUS(getNullAsEmptyString(itemdetails.get("TotalPriceInUS")));
                                                        itemDetails.setTotalPriceInRMB(getNullAsEmptyString(itemdetails.get("TotalPriceInRMB")));
                                                        itemDetails.setTotalPriceIn3rdCurrency(getNullAsEmptyString(itemdetails.get("TotalPriceIn3rdCurrency")));
                                                        itemDetails.setDate(getNullAsEmptyString(itemdetails.get("Date")));
//                                                            boothExistOrNxot.getItemDetailses().add(itemDetails);
                                                        orderListSetUpNew.getItemDetailses().add(itemDetails);
                                                    }

                                                    JsonObject contdetails;
                                                    JsonArray JAcontdetails = jsonObject.getAsJsonArray("containerdetailsbyorderid");
                                                    RealmList<ContainerDetails> containerDetailsInfos = new RealmList<ContainerDetails>();
                                                    for (int bv = 0; bv < JAcontdetails.size(); bv++) {
                                                        contdetails = JAcontdetails.get(bv).getAsJsonObject();
                                                        ContainerDetails containerDetails = realm.copyToRealm(new ContainerDetails());
                                                        containerDetails.setOrderId(getNullAsEmptyString(contdetails.get("OrderId")));
                                                        containerDetails.setContainerId(getNullAsEmptyString(contdetails.get("ContainerId")));
                                                        containerDetails.setContainerType(String.valueOf(getNullAsEmptyString(contdetails.get("ContainerType"))));
                                                        String containerName = getContainerName(String.valueOf(getNullAsEmptyString(contdetails.get("ContainerType"))), realm);

                                                        containerDetails.setContainerName(containerName);
                                                        containerDetails.setFilledVolume(getNullAsEmptyString(contdetails.get("FilledVolume")));
                                                        containerDetails.setStatus(getNullAsEmptyString(contdetails.get("Status")));


                                                        containerDetails.setFilledWeight(getNullAsEmptyString(contdetails.get("FilledWeight")));
                                                        containerDetails.setVolumepercentage(getNullAsEmptyString(contdetails.get("Volumepercentage")));
                                                        containerDetails.setWeightpercentage(getNullAsEmptyString(contdetails.get("Weightpercentage")));
                                                        float MaxVol = getContainerMaxVol(String.valueOf(getNullAsEmptyString(contdetails.get("ContainerType"))), realm);

                                                        float MaxWeight = getContainerMaxWeight(String.valueOf(getNullAsEmptyString(contdetails.get("ContainerType"))), realm);

                                                        containerDetails.setMaximumVolume(String.valueOf(MaxVol));
                                                        containerDetails.setMaximumWeight(String.valueOf(MaxWeight));
                                                        containerDetailsInfos.add(containerDetails);
                                                        orderListSetUpNew.setContainerDetailses(containerDetailsInfos);
                                                    }


                                                    realm.insertOrUpdate(orderListSetUpNew);

                                                }
                                            }
                                        }, new Realm.Transaction.OnSuccess() {
                                            @Override
                                            public void onSuccess() {
                                                pDialog.dismiss();

                                                getOrderDetailsOffline();
                                            }
                                        }, new Realm.Transaction.OnError() {
                                            @Override
                                            public void onError(Throwable error) {
                                                // transaction is automatically rolled-back, do any cleanup here
                                                System.out.println("error" + error.getMessage());
                                                getOrderDetailsOffline();
                                                pDialog.dismiss();
                                            }
                                        });
//
                                    } catch (Exception ew) {
                                        pDialog.dismiss();
                                        ew.printStackTrace();
                                        System.out.println(ew.getMessage());
                                        Toast.makeText(getContext(), ew.getMessage() + "111", Toast.LENGTH_SHORT).show();
                                    }


                                } else {
                                    pDialog.dismiss();
                                    lnr_orderlist.setVisibility(View.VISIBLE);


                                }
                            } catch (Exception e1) {
                                pDialog.dismiss();
                                e1.printStackTrace();
                                Toast.makeText(getContext(), e1.getMessage() + "222", Toast.LENGTH_SHORT).show();
                            }

                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
    }

    private String getContainerName(final String containerType, Realm realm) {
        String ContainerName = "";

        int prefCompanyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        RealmResults<CompanySetUpDetails> companySetUpDetails = realm.where(CompanySetUpDetails.class).equalTo("CompanyID", prefCompanyId).findAll();
        for (int i = 0; i < companySetUpDetails.size(); i++) {

            RealmResults<ContainerInfo> containerResults = companySetUpDetails.get(i).getContainerDetails().where().equalTo("ID", containerType).findAll();
            if (containerResults != null) {
                ContainerName = containerResults.get(0).getContainer();

            }
        }

        return ContainerName;

    }

    private float getContainerMaxVol(final String containerType, Realm realm) {
        float maxVol = 0;

        int prefCompanyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        RealmResults<CompanySetUpDetails> companySetUpDetails = realm.where(CompanySetUpDetails.class).equalTo("CompanyID", prefCompanyId).findAll();
        for (int i = 0; i < companySetUpDetails.size(); i++) {

            RealmResults<ContainerInfo> containerResults = companySetUpDetails.get(i).getContainerDetails().where().equalTo("ID", containerType).findAll();
            if (containerResults != null) {
                maxVol = Float.parseFloat(containerResults.get(0).getMaximumVolume());

            }
        }

        return maxVol;

    }

    private float getContainerMaxWeight(final String containerType, Realm realm) {
        float maxWeight = 0;

        int prefCompanyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        RealmResults<CompanySetUpDetails> companySetUpDetails = realm.where(CompanySetUpDetails.class).equalTo("CompanyID", prefCompanyId).findAll();
        for (int i = 0; i < companySetUpDetails.size(); i++) {

            RealmResults<ContainerInfo> containerResults = companySetUpDetails.get(i).getContainerDetails().where().equalTo("ID", containerType).findAll();
            if (containerResults != null) {
//                maxWeight = Float.parseFloat(containerResults.get(0).getMaximumVolume());
                maxWeight = Float.parseFloat(containerResults.get(0).getMaximumWeight());


            }
        }

        return maxWeight;

    }

    private void GetCompanyDetails(String authToken) {
        Ion.with(getActivity())
                .load(URL_CompanyDetails + "token=" + authToken)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            String requestCode = result.get("resultCode").getAsString();

                            rvOrderlist.setVisibility(View.VISIBLE);
                            SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                            PREF.putString(PreferenceUtil.USER_COMPANY_SETUP_STATUS, result.get("isCompanyset").getAsString());
                            PREF.apply();
                            if (requestCode.equals("1")) {

                                companyData = result.getAsJsonObject("companyDetails");

                                int companyid = companyData.get("CompanyID").getAsInt();

                                companySetUpDetails = mRealm.where(CompanySetUpDetails.class)
                                        .equalTo("CompanyID", companyData.get("CompanyID").getAsInt()).findFirst();

                                SharedPreferences.Editor PREFERENCE = PreferenceUtil.edit(getActivity());
                                PREFERENCE.putString(PreferenceUtil.USER_COMPANY_ID, companyData.get("CompanyID").getAsString());
                                PREFERENCE.apply();

                                containerDetails = result.getAsJsonArray("containerDetails");
                                containerInfos = new RealmList<ContainerInfo>();

                                tradeDetails = result.getAsJsonArray("tradeDeatils");
                                tradeInfos = new RealmList<TradeInfo>();

                                goldAgentDeatils = result.getAsJsonArray("goldAgentDeatils");
                                goldAgentInfos = new RealmList<GoldAgentInfo>();

                                boothDeatils = result.getAsJsonArray("boothDeatils");
                                boothDeatilsInfos = new RealmList<BoothDetailsInfo>();
                                try {


                                    mRealm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            if (companySetUpDetails != null) {
                                                //Exist
                                                companySetUpDetails.setCompanyName(companyData.get("CompanyName").getAsString());
                                                companySetUpDetails.setCountry(companyData.get("Country").getAsString());
                                                companySetUpDetails.setAddressType(companyData.get("AddressType").getAsString());
                                                companySetUpDetails.setAddress(companyData.get("Address").getAsString());
                                                companySetUpDetails.setCityTown(companyData.get("Town/City").getAsString());
                                                companySetUpDetails.setZipCode(companyData.get("ZipCode").getAsString());

                                                companySetUpDetails.setPhone(companyData.get("Phone").getAsString());
                                                companySetUpDetails.setMobile(companyData.get("Mobile").getAsString());
                                                companySetUpDetails.setFax(companyData.get("Fax").getAsString());
                                                companySetUpDetails.setSkype(companyData.get("Skype").getAsString());
                                                companySetUpDetails.setMsnID(companyData.get("MsnID").getAsString());

                                                companySetUpDetails.setQqID(companyData.get("QqID").getAsString());
                                                companySetUpDetails.setOrderEmail(companyData.get("OrderEmail").getAsString());
                                                companySetUpDetails.setMailOrder(companyData.get("MailOrder").getAsString());
                                                companySetUpDetails.setAgentEmail(companyData.get("AgentEmail").getAsString());
                                                companySetUpDetails.setExchangeRate(companyData.get("ExchangeRate").getAsString());
                                                companySetUpDetails.setActive3dCurrency(companyData.get("Active3dCurrency").getAsString());

                                                companySetUpDetails.setThirdCurrency(companyData.get("ThirdCurrency").getAsString());
                                                companySetUpDetails.setExchangeRate3dCurrency(companyData.get("ExchangeRate3dCurrency").getAsString());
                                                companySetUpDetails.setTransilateFrom(companyData.get("TransilateFrom").getAsString());
                                                companySetUpDetails.setTransilateTo(companyData.get("TransilateTo").getAsString());
                                                companySetUpDetails.setGrossweightCarton(companyData.get("GrossweightCarton").getAsString());

                                                companySetUpDetails.setWidth(companyData.get("Width").getAsString());
                                                companySetUpDetails.setHeight(companyData.get("Height").getAsString());
                                                companySetUpDetails.setLength(companyData.get("Length").getAsString());
                                                companySetUpDetails.setVolume(companyData.get("Volume").getAsString());
                                                companySetUpDetails.setAgentContact(companyData.get("AgentContact").getAsString());

                                                companySetUpDetails.setUserMail(companyData.get("UserMail").getAsString());
                                                companySetUpDetails.setUserToken(companyData.get("userToken").getAsString());


                                                for (int i = 0; i < containerDetails.size(); i++) {
                                                    jsonObjectContainerDetails = containerDetails.get(i).getAsJsonObject();
                                                    containerInfo = mRealm.copyToRealm(new ContainerInfo());
                                                    containerInfo.setContainer(jsonObjectContainerDetails.get("Container").getAsString());
                                                    containerInfo.setID(jsonObjectContainerDetails.get("ID").getAsString());
                                                    containerInfo.setMaximumVolume(jsonObjectContainerDetails.get("MaximumVolume").getAsString());
                                                    containerInfo.setMaximumWeight(jsonObjectContainerDetails.get("MaximumWeight").getAsString());
                                                    containerInfos.add(containerInfo);
                                                }

                                                for (int i = 0; i < tradeDetails.size(); i++) {
                                                    jsonObjecttradeDeatils = tradeDetails.get(i).getAsJsonObject();
                                                    tradeInfo = mRealm.copyToRealm(new TradeInfo());
                                                    tradeInfo.setCompanyId(jsonObjecttradeDeatils.get("CompanyId").getAsString());
                                                    tradeInfo.setId(jsonObjecttradeDeatils.get("Id").getAsString());
                                                    tradeInfo.setName(jsonObjecttradeDeatils.get("Name").getAsString());
                                                    tradeInfo.setParent(jsonObjecttradeDeatils.get("Parent").getAsString());
                                                    tradeInfos.add(tradeInfo);
                                                }
                                                for (int i = 0; i < boothDeatils.size(); i++) {
                                                    jsonObjectboothDeatils = boothDeatils.get(i).getAsJsonObject();
                                                    boothDetailsInfo = mRealm.copyToRealm(new BoothDetailsInfo());
                                                    boothDetailsInfo.setBoothName(getNullAsEmptyString(jsonObjectboothDeatils.get("BoothName")));
                                                    boothDetailsInfo.setBoothNumber(getNullAsEmptyString(jsonObjectboothDeatils.get("BoothNumber")));
                                                    boothDetailsInfo.setBusinessCard(getNullAsEmptyString(jsonObjectboothDeatils.get("BusinessCard")));
                                                    boothDetailsInfo.setBusinessScope(getNullAsEmptyString(jsonObjectboothDeatils.get("BusinessScope")));
                                                    boothDetailsInfo.setPhone(getNullAsEmptyString(jsonObjectboothDeatils.get("Phone")));
                                                    boothDeatilsInfos.add(boothDetailsInfo);
                                                }


                                                for (int i = 0; i < goldAgentDeatils.size(); i++) {
                                                    jsonObjectgoldAgentDeatils = goldAgentDeatils.get(i).getAsJsonObject();
                                                    goldAgentInfo = mRealm.copyToRealm(new GoldAgentInfo());
                                                    goldAgentInfo.setId(jsonObjectgoldAgentDeatils.get("Id").getAsString());
                                                    goldAgentInfo.setCompanyName(jsonObjectgoldAgentDeatils.get("CompanyName").getAsString());
                                                    goldAgentInfo.setOfficeAddress(jsonObjectgoldAgentDeatils.get("OfficeAddress").getAsString());
                                                    goldAgentInfo.setPhoneNumber(jsonObjectgoldAgentDeatils.get("PhoneNumber").getAsString());
                                                    goldAgentInfo.setBusinessYear(jsonObjectgoldAgentDeatils.get("BusinessYear").getAsString());
                                                    goldAgentInfo.setFaxNumber(jsonObjectgoldAgentDeatils.get("FaxNumber").getAsString());
                                                    goldAgentInfo.setGoldAgentYears(jsonObjectgoldAgentDeatils.get("GoldAgentYears").getAsString());
                                                    contactInfos = new RealmList<ContactInfo>();
                                                    JsonArray contactDetails = jsonObjectgoldAgentDeatils.getAsJsonArray("contact");
                                                    JsonObject jsonObjectgoldContactDetails;
                                                    for (int j = 0; j < contactDetails.size(); j++) {

                                                        jsonObjectgoldContactDetails = contactDetails.get(j).getAsJsonObject();
                                                        contactInfo = mRealm.copyToRealm(new ContactInfo());
                                                        contactInfo.setId(jsonObjectgoldContactDetails.get("Id").getAsString());
                                                        contactInfo.setGoldAgentId(jsonObjectgoldContactDetails.get("GoldAgentId").getAsString());
                                                        contactInfo.setName(jsonObjectgoldContactDetails.get("Name").getAsString());
                                                        contactInfo.setEmail(jsonObjectgoldContactDetails.get("Email").getAsString());
                                                        contactInfo.setMobile(jsonObjectgoldContactDetails.get("Mobile").getAsString());
                                                        contactInfo.setSkype(jsonObjectgoldContactDetails.get("Skype").getAsString());
                                                        contactInfo.setImageName(jsonObjectgoldContactDetails.get("ImageName").getAsString());
                                                        contactInfos.add(contactInfo);
                                                    }
                                                    goldAgentInfo.setContact(contactInfos);
                                                    goldAgentInfos.add(goldAgentInfo);

                                                }
                                                companySetUpDetails.setGoldAgentDeatils(goldAgentInfos);
                                                companySetUpDetails.setBoothDetailsInfos(boothDeatilsInfos);
                                                companySetUpDetails.setTradeDeatils(tradeInfos);
                                                companySetUpDetails.setContainerDetails(containerInfos);
                                                companySetUpDetails.setAgentId(companyData.get("AgentId").getAsString());
                                                companySetUpDetails.setAgentName(companyData.get("AgentName").getAsString());
                                                mRealm.insertOrUpdate(companySetUpDetails);
                                            } else {
                                                // Not exist

                                                String CompanyID = companyData.get("CompanyID").getAsString();
                                                CompanySetUpDetails companySetUp = mRealm.createObject(CompanySetUpDetails.class, CompanyID);

//                                             companySetUp.setCompanyID(Integer.parseInt(CompanyID));
                                                companySetUp.setCompanyName(companyData.get("CompanyName").getAsString());
                                                companySetUp.setCountry(companyData.get("Country").getAsString());
                                                companySetUp.setAddressType(companyData.get("AddressType").getAsString());
                                                companySetUp.setAddress(companyData.get("Address").getAsString());
                                                companySetUp.setZipCode(companyData.get("ZipCode").getAsString());
                                                companySetUp.setCityTown(companyData.get("Town/City").getAsString());

                                                companySetUp.setPhone(companyData.get("Phone").getAsString());
                                                companySetUp.setMobile(companyData.get("Mobile").getAsString());
                                                companySetUp.setFax(companyData.get("Fax").getAsString());
                                                companySetUp.setSkype(companyData.get("Skype").getAsString());
                                                companySetUp.setMsnID(companyData.get("MsnID").getAsString());

                                                companySetUp.setQqID(companyData.get("QqID").getAsString());
                                                companySetUp.setOrderEmail(companyData.get("OrderEmail").getAsString());
                                                companySetUp.setMailOrder(companyData.get("MailOrder").getAsString());
                                                companySetUp.setAgentEmail(companyData.get("AgentEmail").getAsString());
                                                companySetUp.setExchangeRate(companyData.get("ExchangeRate").getAsString());
                                                companySetUp.setActive3dCurrency(companyData.get("Active3dCurrency").getAsString());

                                                companySetUp.setThirdCurrency(companyData.get("ThirdCurrency").getAsString());
                                                companySetUp.setExchangeRate3dCurrency(companyData.get("ExchangeRate3dCurrency").getAsString());
                                                companySetUp.setTransilateFrom(companyData.get("TransilateFrom").getAsString());
                                                companySetUp.setTransilateTo(companyData.get("TransilateTo").getAsString());
                                                companySetUp.setGrossweightCarton(companyData.get("GrossweightCarton").getAsString());

                                                companySetUp.setWidth(companyData.get("Width").getAsString());
                                                companySetUp.setHeight(companyData.get("Height").getAsString());
                                                companySetUp.setLength(companyData.get("Length").getAsString());
                                                companySetUp.setVolume(companyData.get("Volume").getAsString());
                                                companySetUp.setAgentContact(companyData.get("AgentContact").getAsString());

                                                companySetUp.setUserMail(companyData.get("UserMail").getAsString());
                                                companySetUp.setUserToken(companyData.get("userToken").getAsString());

                                                for (int i = 0; i < containerDetails.size(); i++) {
                                                    jsonObjectContainerDetails = containerDetails.get(i).getAsJsonObject();
                                                    containerInfo = mRealm.copyToRealm(new ContainerInfo());
                                                    containerInfo.setContainer(jsonObjectContainerDetails.get("Container").getAsString());
                                                    containerInfo.setID(jsonObjectContainerDetails.get("ID").getAsString());
                                                    containerInfo.setMaximumVolume(jsonObjectContainerDetails.get("MaximumVolume").getAsString());
                                                    containerInfo.setMaximumWeight(jsonObjectContainerDetails.get("MaximumWeight").getAsString());
                                                    containerInfos.add(containerInfo);
                                                }

                                                for (int i = 0; i < tradeDetails.size(); i++) {
                                                    jsonObjecttradeDeatils = tradeDetails.get(i).getAsJsonObject();
                                                    tradeInfo = mRealm.copyToRealm(new TradeInfo());
                                                    tradeInfo.setCompanyId(jsonObjecttradeDeatils.get("CompanyId").getAsString());
                                                    tradeInfo.setId(jsonObjecttradeDeatils.get("Id").getAsString());
                                                    tradeInfo.setName(jsonObjecttradeDeatils.get("Name").getAsString());
                                                    tradeInfo.setParent(jsonObjecttradeDeatils.get("Parent").getAsString());
                                                    tradeInfos.add(tradeInfo);
                                                }
                                                for (int i = 0; i < boothDeatils.size(); i++) {
                                                    jsonObjectboothDeatils = boothDeatils.get(i).getAsJsonObject();
                                                    boothDetailsInfo = mRealm.copyToRealm(new BoothDetailsInfo());
                                                    boothDetailsInfo.setBoothName(getNullAsEmptyString(jsonObjectboothDeatils.get("BoothName")));
                                                    boothDetailsInfo.setBoothNumber(getNullAsEmptyString(jsonObjectboothDeatils.get("BoothNumber")));
                                                    boothDetailsInfo.setBusinessCard(getNullAsEmptyString(jsonObjectboothDeatils.get("BusinessCard")));
                                                    boothDetailsInfo.setBusinessScope(getNullAsEmptyString(jsonObjectboothDeatils.get("BusinessScope")));
                                                    boothDetailsInfo.setPhone(getNullAsEmptyString(jsonObjectboothDeatils.get("Phone")));
                                                    boothDeatilsInfos.add(boothDetailsInfo);
                                                }
                                                for (int i = 0; i < goldAgentDeatils.size(); i++) {
                                                    jsonObjectgoldAgentDeatils = goldAgentDeatils.get(i).getAsJsonObject();
                                                    goldAgentInfo = mRealm.copyToRealm(new GoldAgentInfo());
                                                    goldAgentInfo.setId(jsonObjectgoldAgentDeatils.get("Id").getAsString());
                                                    goldAgentInfo.setCompanyName(jsonObjectgoldAgentDeatils.get("CompanyName").getAsString());
                                                    goldAgentInfo.setOfficeAddress(jsonObjectgoldAgentDeatils.get("OfficeAddress").getAsString());
                                                    goldAgentInfo.setPhoneNumber(jsonObjectgoldAgentDeatils.get("PhoneNumber").getAsString());
                                                    goldAgentInfo.setBusinessYear(jsonObjectgoldAgentDeatils.get("BusinessYear").getAsString());
                                                    goldAgentInfo.setFaxNumber(jsonObjectgoldAgentDeatils.get("FaxNumber").getAsString());
                                                    goldAgentInfo.setGoldAgentYears(jsonObjectgoldAgentDeatils.get("GoldAgentYears").getAsString());
                                                    contactInfos = new RealmList<ContactInfo>();
                                                    JsonArray contactDetails = jsonObjectgoldAgentDeatils.getAsJsonArray("contact");
                                                    JsonObject jsonObjectgoldContactDetails;

                                                    for (int j = 0; j < contactDetails.size(); j++) {
                                                        jsonObjectgoldContactDetails = contactDetails.get(j).getAsJsonObject();
                                                        contactInfo = mRealm.copyToRealm(new ContactInfo());
                                                        contactInfo.setId(jsonObjectgoldContactDetails.get("Id").getAsString());
                                                        contactInfo.setGoldAgentId(jsonObjectgoldContactDetails.get("GoldAgentId").getAsString());
                                                        contactInfo.setName(jsonObjectgoldContactDetails.get("Name").getAsString());
                                                        contactInfo.setEmail(jsonObjectgoldContactDetails.get("Email").getAsString());
                                                        contactInfo.setMobile(jsonObjectgoldContactDetails.get("Mobile").getAsString());
                                                        contactInfo.setSkype(jsonObjectgoldContactDetails.get("Skype").getAsString());
                                                        contactInfo.setImageName(jsonObjectgoldContactDetails.get("ImageName").getAsString());
                                                        contactInfos.add(contactInfo);
                                                    }
                                                    goldAgentInfo.setContact(contactInfos);
                                                    goldAgentInfos.add(goldAgentInfo);

                                                }
                                                companySetUp.setGoldAgentDeatils(goldAgentInfos);
                                                companySetUp.setTradeDeatils(tradeInfos);
                                                companySetUp.setBoothDetailsInfos(boothDeatilsInfos);
                                                companySetUp.setContainerDetails(containerInfos);
                                                companySetUp.setAgentId(companyData.get("AgentId").getAsString());
                                                companySetUp.setAgentName(companyData.get("AgentName").getAsString());
                                                mRealm.insertOrUpdate(companySetUp);
//                                 mRealm.commitTransaction();
                                            }
                                        }
                                    });
                                } catch (Exception em) {
                                    em.printStackTrace();
                                } finally {
                                    if (mRealm != null)
                                        mRealm.close();
                                }
                                flag = 1;
                            } else {
                                Toast.makeText(getActivity(), "something went wrong!", Toast.LENGTH_SHORT).show();
                            }
//                            Toast.makeText(getActivity(), "DONE!", Toast.LENGTH_SHORT).show();

                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                });
    }

    private String getNullAsEmptyString(JsonElement jsonElement) {
        return jsonElement.isJsonNull() ? "" : jsonElement.getAsString();
    }

    @Subscribe
    public void NavigateVlue(RefreshEvent event) {
        sharedPref = PreferenceUtil.get(getActivity());
        String authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);

        if (AppUtils.isNetworkAvailable(getActivity())) {
            GetCompanyDetails(authToken);
        } else {
//            DisplayDialog(getResources().getString(R.string.error_no_internet));
        }
        if (AppUtils.isNetworkAvailable(getActivity())) {
//            GetCompanyDetails(authToken);
            OrderList(authToken);
            mSwipeRefreshLayout.setRefreshing(false);
        } else {
            getOrderDetailsOffline();
//            DisplayDialog(getResources().getString(R.string.error_no_internet));
        }

    }
}