package com.mixmycontainer.yiwu.fragment;

import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.apis.core.ResultCode;
import com.mixmycontainer.yiwu.bus.BusFactory;
import com.mixmycontainer.yiwu.error.EmptyViewManager;

/**
 * Created by Sanif on 04-01-2016.
 */
public class BaseFragment extends Fragment {


    private boolean enableBackButton = true;

    private TextView toolBarTitle;
    private EmptyViewManager emptyViewManager;

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        BusFactory.getBus().register(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupToolbar(view);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        BusFactory.getBus().unregister(this);

    }

    /**
     * This method is called to set the Toolbar with id 'toolbar' as ActionBar
     * Override this method to give custom implementation
     *
     * @param v: The container view
     */
    public void setupToolbar(View v) {
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar);
        if (toolbar != null) {
            toolBarTitle = (TextView) v.findViewById(R.id.tvTitle);

            ((AppCompatActivity) getActivity()).setSupportActionBar(toolbar);
            if (getTitle() != 0 && toolBarTitle != null) {
                toolBarTitle.setText(getTitle());
            }

        }

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle(null);//for disabling default title shown in action bar
            if (enableBackButton) {
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_back);
            }
        }
    }

    /**
     * override this method to give title to toolBar
     *
     * @return
     */
    public
    @StringRes
    int getTitle() {
        return 0;
    }

    public
    @StringRes
    int getAction() {
        return 0;
    }

    public ActionBar getSupportActionBar() {
        return ((AppCompatActivity) getActivity()).getSupportActionBar();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {

            getActivity().onBackPressed();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void setEnableBackButton(boolean enableBackButton) {
        this.enableBackButton = enableBackButton;
    }

    public void showEmptyView(ViewGroup emptyContainer, ResultCode code, View.OnClickListener listner) {
        if (emptyViewManager == null) {
            emptyViewManager = new EmptyViewManager(getActivity(), emptyContainer);
        }
        emptyViewManager.showEmptyView(code, listner);
    }

    public void DisplayDialog(String showMessage) {
        final Dialog dialog = new Dialog(getActivity());

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        DisplayMetrics display = getActivity().getResources().getDisplayMetrics();
        int width = display.widthPixels;
        int height = display.heightPixels;
        dialog.setContentView(R.layout.dialog_validate);
        TextView text = (TextView) dialog.findViewById(R.id.txt_validate_dialog);
        text.setText(showMessage);
        dialog.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

}
