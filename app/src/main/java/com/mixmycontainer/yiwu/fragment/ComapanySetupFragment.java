package com.mixmycontainer.yiwu.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.AvarageCartonActivity;
import com.mixmycontainer.yiwu.activity.BuyerCompanyAddressActivity;
import com.mixmycontainer.yiwu.activity.ContactDetailActivity;
import com.mixmycontainer.yiwu.activity.ExchangeRateActivity;
import com.mixmycontainer.yiwu.activity.TranslationActivity;

/**
 * Created by ADMIN on 11-11-2016.
 */
public class ComapanySetupFragment extends BaseFragment implements View.OnClickListener {

    private LinearLayout llBuyercompanyAdrress, llContactDetail, llExchange, llTraslation, llAvarageCartons;

    public static ComapanySetupFragment newInstance() {
        ComapanySetupFragment fragment = new ComapanySetupFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_companysetup, container, false);
        initview(view);
        initLestener();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public int getTitle() {
        return R.string.txt_company_setup;
    }

    private void initLestener() {
        llBuyercompanyAdrress.setOnClickListener(this);
        llContactDetail.setOnClickListener(this);
        llExchange.setOnClickListener(this);
        llTraslation.setOnClickListener(this);
        llAvarageCartons.setOnClickListener(this);

    }

    private void initview(View view) {

        llBuyercompanyAdrress = (LinearLayout) view.findViewById(R.id.llBuyercompanyAdrress);
        llContactDetail = (LinearLayout) view.findViewById(R.id.llContactDetail);
        llExchange = (LinearLayout) view.findViewById(R.id.llExchange);
        llTraslation = (LinearLayout) view.findViewById(R.id.llTraslation);
        llAvarageCartons = (LinearLayout) view.findViewById(R.id.llAvarageCartons);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (view == llBuyercompanyAdrress) {
            startActivity(BuyerCompanyAddressActivity.getnewIntent(getActivity()));
            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        } else if (view == llContactDetail) {
            startActivity(ContactDetailActivity.getnewIntent(getActivity()));
            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        } else if (view == llExchange) {
            startActivity(ExchangeRateActivity.getnewIntent(getActivity()));
            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        } else if (view == llTraslation) {
            startActivity(TranslationActivity.getnewIntent(getActivity()));
            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        } else if (view == llAvarageCartons) {
            startActivity(AvarageCartonActivity.getnewIntent(getActivity()));
            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        }
    }

}
