package com.mixmycontainer.yiwu.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Loader;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.FloatRange;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.PriceActivity;
import com.mixmycontainer.yiwu.bus.BusFactory;
import com.mixmycontainer.yiwu.bus.events.RefreshEvent;
import com.mixmycontainer.yiwu.bus.events.RefreshSingleViewOrder;
import com.mixmycontainer.yiwu.models.BoothDetails;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.models.ContainerDetails;
import com.mixmycontainer.yiwu.models.ContainerInfo;
import com.mixmycontainer.yiwu.models.ItemDetails;
import com.mixmycontainer.yiwu.models.OrderListDetails;
import com.mixmycontainer.yiwu.models.TempContainerDetails;
import com.mixmycontainer.yiwu.models.TempOrderListDetails;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 29-11-2016.
 */
public class CartonDetailFragment extends BaseFragment {
    RealmList<BoothDetails> boothDetailses;
    String[] cartonsUsed;
    String containerName = "";
    String containerNameDetails = "";
    float containerMaxVolumeF = 0, containerMaxWeightF = 0, currentContainerFilledWeight = 0, currentContainerFilledVolume = 0;
    float volPercent = 0, wgtPercent = 0;
    String containerType = null;
    String currentContainerName = null;
    RealmList<ContainerDetails> ContainerData;
    String prefOrderId, cartainerIdUsedInItem, lastUsedContainer, Weightpercentage = "0", Volumepercentage = "0";
    String containeridCollection;//:->tempororaly storing container id if addnew container occur
    private TempContainerDetails tempContainerDetails;
    private DecoView decoView;
    private String[] containerNameList, containerIdList, containerMaxVolume, containerMaxWeight, container_NameList, container_IdList, container_MaxVolume, container_MaxWeight;
    private MenuItem menuItem;
    private EditText etUnitPerCarton, etGrossWeight, etCartonWeidth, etCartonLegth, etCartonHeigth, etCBM, etUnitQty, etCartonQty;
    private TextView tvOrdernumber, tvorderValueUs, tvTotalWeight, tvTotalCBM, tvValueofPurchase;
    private double totalCBM;
    private int backIndex, useTempTableData = 0, flag = 1, prefCompanyId;
    private Realm mRealm;
    private Spinner spinnerCartonUsed;
    private TextWatcher txtWatcherUnitQty, txtWatcherCartonQty;
    private RealmList<TempContainerDetails> tempContainerDetailsesList;
    private float currentItemVolume, currentItemWeight, totalItemVolume, totalItemWeight;
    float availableVolume, availableWeight, cartonqtyDMY;
    private String prefEditBoothId = "0", prefEditItemId = "0", prefDeleteItem = "0";
    //prefEditBoothId :-> if this variable is 0 then, not in editmode, if its not empty then in edit mode..
    private String commonItemId = "", prefItemId;
    //commonItemId:-> storing itemid one by one in case of edit order
    //                  Since using a single method
    private static float cartonQty, grossWight, cbm;//-> cbm per carton

    private float previousItemWeight=0;

    String oldContainerId;
    RealmResults<OrderListDetails> orderListDetails, editOrderListDetails;
    //editOrderListDetails:-> For storing all OrderListFor refilling

    // useTempTableData - if 0 then temporary table not used , if 1 then temporary table is used.
    public static CartonDetailFragment newInstance() {
        CartonDetailFragment fragment = new CartonDetailFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_carton_detail, container, false);

        mRealm = Realm.getDefaultInstance();

        prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        prefCompanyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));

        prefEditBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_BOOTH_ID, "0");
        prefEditItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_ITEM_ID, "0");

        prefDeleteItem = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.DELETE_ITEM, "0");

        if (prefDeleteItem.equals("Delete")) {
            deleteItemDialog();

        }

        initView(view);
        setEnableBackButton(false);
        return view;
    }


    @Override
    public int getTitle() {
        return R.string.txt_carton_detail;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuItem = menu.add("action");
        menuItem.setTitle(getResources().getString(R.string.txt_next));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getResources().getString(R.string.txt_next))) {
            validation();
        }
        return super.onOptionsItemSelected(item);
    }

    private void initView(View view) {
        etUnitPerCarton = (EditText) view.findViewById(R.id.etUnitPerCarton);
        etGrossWeight = (EditText) view.findViewById(R.id.etGrossWeight);
        etCartonWeidth = (EditText) view.findViewById(R.id.etCartonWeidth);
        etCartonLegth = (EditText) view.findViewById(R.id.etCartonLegth);
        etCartonHeigth = (EditText) view.findViewById(R.id.etCartonHeigth);
        spinnerCartonUsed = (Spinner) view.findViewById(R.id.spinnerCartonUsed);
        etCartonQty = (EditText) view.findViewById(R.id.etCartonQty);
        etCBM = (EditText) view.findViewById(R.id.etCBM);
        etUnitQty = (EditText) view.findViewById(R.id.etUnitQty);
        tvOrdernumber = (TextView) view.findViewById(R.id.tvOrdernumber);
        tvorderValueUs = (TextView) view.findViewById(R.id.tvorderValueUs);
        tvTotalWeight = (TextView) view.findViewById(R.id.tvTotalWeight);
        tvTotalCBM = (TextView) view.findViewById(R.id.tvTotalCBM);
        tvValueofPurchase = (TextView) view.findViewById(R.id.tvValueofPurchase);

        decoView = (DecoView) view.findViewById(R.id.dynamicArcView);
        decoView.addSeries(new SeriesItem.Builder(Color.parseColor("#FFFFFF"))
                .setRange(0, 100, 100)
                .setInitialVisibility(true)
                .build());
        final SeriesItem seriesItem1 = new SeriesItem.Builder(Color.parseColor("#4CAF50"))
                .setRange(0, 100, 0)
                .build();

        backIndex = decoView.addSeries(seriesItem1);
        final TextView textPercentage = (TextView) view.findViewById(R.id.textPercentage);
        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilled = ((currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue()));
                textPercentage.setText(String.format("%.0f%%", percentFilled * 100f));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });
        getOrderDetailsOffline();


        // get carton details from the database
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", prefCompanyId).findAll();
                    if (!companySetUpDetails.isEmpty()) {

                        for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {

                            etGrossWeight.setText(companySetUpDetails.get(i).getGrossweightCarton());
                            etCartonLegth.setText(companySetUpDetails.get(i).getLength());
                            etCartonWeidth.setText(companySetUpDetails.get(i).getWidth());
                            etCartonHeigth.setText(companySetUpDetails.get(i).getHeight());

                        }

                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

        if (!prefEditBoothId.equals("0")) {
            getItemDetailsForEdit();
        }


        totalCBM = Integer.parseInt(etCartonLegth.getText().toString().trim()) * Integer.parseInt(etCartonHeigth.getText().toString().trim()) *
                Integer.parseInt(etCartonWeidth.getText().toString().trim()) * 0.000001;
        etCBM.setText(String.valueOf(totalCBM));


        etCartonLegth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    totalCBM = Integer.parseInt(etCartonLegth.getText().toString().trim()) * Integer.parseInt(etCartonHeigth.getText().toString().trim()) *
                            Integer.parseInt(etCartonWeidth.getText().toString().trim()) * 0.000001;
                    etCBM.setText(String.valueOf(totalCBM));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etCBM.setText("0");

                }
            }
        });

        etCartonWeidth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    totalCBM = Integer.parseInt(etCartonLegth.getText().toString().trim()) * Integer.parseInt(etCartonHeigth.getText().toString().trim()) *
                            Integer.parseInt(etCartonWeidth.getText().toString().trim()) * 0.000001;
                    etCBM.setText(String.valueOf(totalCBM));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etCBM.setText("0");

                }
            }
        });

        etCartonHeigth.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                try {
                    double totalCBM = Integer.parseInt(etCartonLegth.getText().toString().trim()) * Integer.parseInt(etCartonHeigth.getText().toString().trim()) *
                            Integer.parseInt(etCartonWeidth.getText().toString().trim()) * 0.000001;
                    etCBM.setText(String.valueOf(totalCBM));
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etCBM.setText("0");
                }
            }
        });

        etUnitPerCarton.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                try {
                    int unitpercart = Integer.parseInt(etUnitPerCarton.getText().toString().trim());
                    int unitQTY = Integer.parseInt(etUnitQty.getText().toString().trim());
                    double mode = 0, divition = 0;

                    etCartonQty.setError(null);

                    if (unitpercart != 0) {
                        mode = (int) unitQTY % (int) unitpercart;
                        divition = (int) unitQTY / (int) unitpercart;
                    }


                    if (mode == 0) {
                        // alert(division);
                        etCartonQty.setText((int) divition + "");
                    }
                    if (mode != 0) {
                        if (unitpercart > unitQTY) {
                            etCartonQty.setText("1");
                        }
                        if (unitpercart < unitQTY) {

                            int division1 = (int) (divition + 1);
                            etCartonQty.setText((int) division1 + "");
                        }
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etCartonQty.setText("0");
                }
            }
        });
        txtWatcherUnitQty = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int defaultUnitQty = 0;
                etCartonQty.setError(null);
                etUnitQty.setError(null);
                if (etUnitQty.getText().toString().trim().equals("")) {

                } else {
                    defaultUnitQty = Integer.parseInt(etUnitQty.getText().toString().trim());
                }
                try {
                    etCartonQty.removeTextChangedListener(txtWatcherCartonQty);
                    int unitpercart = Integer.parseInt(etUnitPerCarton.getText().toString().trim());

                    double mode = 0, divition = 0;

                    if (unitpercart != 0) {
                        mode = defaultUnitQty % unitpercart;
                        divition = defaultUnitQty / unitpercart;
                    }
                    if (mode == 0) {
                        // alert(division);
                        etCartonQty.setText((int) divition + "");
                    }
                    if (mode != 0) {
                        if (unitpercart > defaultUnitQty) {
                            etCartonQty.setText("1");
                        }
                        if (unitpercart < defaultUnitQty) {

                            int division1 = (int) (divition + 1);
                            etCartonQty.setText((int) division1 + "");
                        }
                    }
                    etCartonQty.addTextChangedListener(txtWatcherCartonQty);
                } catch (Exception e) {
                    e.printStackTrace();
                    etCartonQty.setText("0");
                }

            }
        };

        txtWatcherCartonQty = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                int defaultCartonqty = 0;
                etCartonQty.setError(null);
                etUnitQty.setError(null);
                if (etCartonQty.getText().toString().trim().equals("")) {

                } else {
                    defaultCartonqty = Integer.parseInt(etCartonQty.getText().toString().trim());
                }
                try {
                    etUnitQty.removeTextChangedListener(txtWatcherUnitQty);
                    int unitPerCarton = Integer.parseInt(etUnitPerCarton.getText().toString().trim());
                    double unitqt = unitPerCarton * defaultCartonqty;
                    etUnitQty.setText((int) unitqt + "");
                    etUnitQty.addTextChangedListener(txtWatcherUnitQty);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etUnitQty.setText("0");
                }
            }
        };
        etUnitQty.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    int defaultCartonqty = 0;
                    if (etCartonQty.getText().toString().trim().equals("")) {

                    } else {
                        defaultCartonqty = Integer.parseInt(etCartonQty.getText().toString().trim());
                    }
                    try {
                        etUnitQty.removeTextChangedListener(txtWatcherUnitQty);
                        int unitPerCarton = Integer.parseInt(etUnitPerCarton.getText().toString().trim());
                        double unitqt = unitPerCarton * defaultCartonqty;
                        etUnitQty.setText((int) unitqt + "");
                        etUnitQty.addTextChangedListener(txtWatcherUnitQty);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                        etUnitQty.setText("0");
                    }
                }
            }
        });
        etUnitQty.addTextChangedListener(txtWatcherUnitQty);
        etCartonQty.addTextChangedListener(txtWatcherCartonQty);

    }

    private void getItemDetailsForEdit() {
        mRealm = Realm.getDefaultInstance();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
                    final int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));

                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {
                        RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();
                        for (int j = 0; j < itemDetails.size(); j++) {
                            if (itemDetails.get(j).getItemId().equals(prefEditItemId)) {
                                etGrossWeight.setText(itemDetails.get(j).getWeightofCarton().toString());
                                etCartonLegth.setText(itemDetails.get(j).getLength().toString());
                                float itemWeight = Float.parseFloat(itemDetails.get(j).getWeightofCarton().toString());
                                etCartonWeidth.setText(itemDetails.get(j).getWidth().toString());
                                etCartonHeigth.setText(itemDetails.get(j).getHeight().toString());

                                etCBM.setText(itemDetails.get(j).getCBM().toString());
                                etUnitPerCarton.setText(itemDetails.get(j).getUnitperCarton().toString());
                                etUnitQty.setText(itemDetails.get(j).getUnitQuantity().toString());
                                etCartonQty.setText(itemDetails.get(j).getCartonQuantity());
                                float cartonQty = Float.parseFloat(itemDetails.get(j).getCartonQuantity());

                                previousItemWeight = cartonQty*itemWeight;

                            }
                        }
                    }

                }
            });
        } catch (Exception e) {
            e.getMessage();
        }
    }

    public void validation() {
        if (!etCartonQty.getText().toString().trim().equals("") && !etCartonQty.getText().toString().trim().equals("0") && !etUnitPerCarton.getText().toString().trim().equals("") && !etUnitPerCarton.getText().toString().trim().equals("0") && !etGrossWeight.getText().toString().trim().equals("") && !etUnitQty.getText().toString().trim().equals("0") && !etCartonLegth.getText().toString().trim().equals("") &&
                !etCartonWeidth.getText().toString().trim().equals("") && !etCartonHeigth.getText().toString().trim().equals("") && !etCBM.getText().toString().trim().equals("")
                && !etUnitQty.getText().toString().trim().equals("")) {
//
//            if (flag == 1) {
//            }
            if (prefEditBoothId.equals("0")) {
                checkCartonFullOrNot();
            } else {
                updateItemDetailsAndRemoveCointainerIdOfAllItem();
                freeUpAllContainers();
                reAllocateAllItems();
            }


        } else {

            if (etUnitPerCarton.getText().toString().trim().equals("") || etUnitPerCarton.getText().toString().trim().equals("0")) {
                etUnitPerCarton.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etGrossWeight.getText().toString().trim().equals("")) {
                etGrossWeight.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etCartonQty.getText().toString().trim().equals("") || etCartonQty.getText().toString().trim().equals("0")) {
                etCartonQty.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etCartonLegth.getText().toString().trim().equals("")) {
                etCartonLegth.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etCartonWeidth.getText().toString().trim().equals("")) {
                etCartonWeidth.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etCartonHeigth.getText().toString().trim().equals("")) {
                etCartonHeigth.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etCBM.getText().toString().trim().equals("")) {
                etCBM.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etUnitQty.getText().toString().trim().equals("") || etUnitQty.getText().toString().trim().equals("0")) {
                etUnitQty.setError(getResources().getString(R.string.txt_field_null));
            }

        }
    }

    /* mehod to set spinner data*/
    private void setSpinnerData(String[] cartonsUsed, Spinner spinnerCartonUsed) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, cartonsUsed);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerCartonUsed.setAdapter(spinnerArrayAdapter);
    }

    /* get OrderDetails from the database to show in the dashboard*/
    private void getOrderDetailsOffline() {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", prefCompanyId).equalTo("OrderId", prefOrderId).findAll();
                    if (!OrderListDetailsDetails.isEmpty()) {


                        for (int i = OrderListDetailsDetails.size() - 1; i >= 0; i--) {

                            tvOrdernumber.setText(OrderListDetailsDetails.get(i).getOrderNumber());
                            tvorderValueUs.setText(OrderListDetailsDetails.get(i).getTotalOrderUsValue() + "$(" + OrderListDetailsDetails.get(i).getTotalOrderRmbValue() + "¥)");
                            double totCBB = Double.parseDouble(OrderListDetailsDetails.get(i).getTotalOrderCBM());
                            String orderCbm = String.format("%.2f", totCBB);
                            tvTotalCBM.setText(orderCbm);
                            tvTotalWeight.setText(OrderListDetailsDetails.get(i).getTotalOrderWeight());
                            cartonsUsed = new String[OrderListDetailsDetails.get(i).getContainerDetailses().size()];
                            for (int k = 0; k < OrderListDetailsDetails.get(i).getContainerDetailses().size(); k++) {
                                String cartonName = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerName();
                                cartonsUsed[k] = cartonName;
                                if (cartonName.matches("\\d+")) {
                                    cartonsUsed[k] = getContainerNameOffline(cartonName);
                                } else {
                                    cartonsUsed[k] = cartonName;
                                }

//                                if (OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getStatus().equals("0")) {
                                Volumepercentage = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getVolumepercentage();
                                Weightpercentage = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getWeightpercentage();
//                                }
                            }

                            setSpinnerData(cartonsUsed, spinnerCartonUsed);
                            boothDetailses = OrderListDetailsDetails.get(i).getBoothDetailses();
                            for (int j = 0; j < boothDetailses.size(); j++) {
                                String boothUS = boothDetailses.get(j).getValuePurchase();
                                String bothRMB = boothDetailses.get(j).getValuePurchaseRMB();
                                tvValueofPurchase.setText(boothUS + "$(" + bothRMB + "¥)");
//
//                        itemDetailses = boothDetailses.get(j).getItemDetailses();
//                        for (int k = 0; k < itemDetailses.size(); k++) {
//                            String itemId = itemDetailses.get(k).getItemId();
//                            String itemNo = itemDetailses.get(k).getItemNo();

//                        }
                            }
                            try {
                                float Vol_Percent_float = Float.parseFloat(Volumepercentage);
                                Float Weight_Percent_float = Float.parseFloat(Weightpercentage);
                                int Vol_Percent = Math.round(Vol_Percent_float);
                                int Weight_Percent = Math.round(Weight_Percent_float);
                                if (Vol_Percent >= Weight_Percent) {

                                    decoView.addEvent(new DecoEvent.Builder(Vol_Percent).setIndex(backIndex).setDelay(250).build());
                                } else {
                                    decoView.addEvent(new DecoEvent.Builder(Weight_Percent).setIndex(backIndex).setDelay(250).build());
                                }
                            } catch (NumberFormatException ee) {
                                ee.printStackTrace();
                            }


                        }


                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

    private String getContainerNameOffline(final String containerType) {


        int prefCompanyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", prefCompanyId).findAll();
        for (int i = 0; i < companySetUpDetails.size(); i++) {

            RealmResults<ContainerInfo> containerResults = companySetUpDetails.get(i).getContainerDetails().where().equalTo("ID", containerType).findAll();
            if (containerResults != null) {
                containerName = containerResults.get(0).getContainer();

            }
        }


        return containerName;

    }

    //Method to update edited item and remove all item's container id(inOrder to refill)
    private void updateItemDetailsAndRemoveCointainerIdOfAllItem() {
        mRealm = Realm.getDefaultInstance();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {
                        RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();
                        for (int j = 0; j < itemDetails.size(); j++) {
                            itemDetails.get(j).setContainerId("0");
                            itemDetails.get(j).setNoOfCartonFilled("0");
                            if (itemDetails.get(j).getItemId().equals(prefEditItemId)) {
                                itemDetails.get(j).setWeightofCarton(etGrossWeight.getText().toString().trim());
                                itemDetails.get(j).setLength(etCartonLegth.getText().toString().trim());
                                itemDetails.get(j).setWidth(etCartonWeidth.getText().toString().trim());
                                itemDetails.get(j).setHeight(etCartonHeigth.getText().toString().trim());
                                itemDetails.get(j).setCBM(etCBM.getText().toString().trim());
                                itemDetails.get(j).setUnitperCarton(etUnitPerCarton.getText().toString().trim());
                                itemDetails.get(j).setUnitQuantity(etUnitQty.getText().toString().trim());
                                itemDetails.get(j).setCartonQuantity(etCartonQty.getText().toString().trim());
                                mRealm.insertOrUpdate(itemDetails);
                            }
                        }
                        mRealm.insertOrUpdate(orderListDetails);
                    }
                }
            });
        } catch (Exception e) {
            e.getMessage();
        }
    }

    //Method for, Free up all container used in order.
    private void freeUpAllContainers() {
        mRealm = Realm.getDefaultInstance();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {
                        RealmList<ContainerDetails> containerDetails = orderListDetails.get(i).getContainerDetailses();
                        for (int j = 0; j < containerDetails.size(); j++) {
                            containerDetails.get(j).setFilledVolume("0");
                            containerDetails.get(j).setFilledWeight("0");
                            containerDetails.get(j).setVolumepercentage("0");
                            containerDetails.get(j).setWeightpercentage("0");
                            mRealm.insertOrUpdate(containerDetails);

                        }
                        mRealm.insertOrUpdate(orderListDetails);
                    }
                }
            });
        } catch (Exception e) {
            e.getMessage();
        }

    }

    //== For ReAllocating all intem in order to container
    private void reAllocateAllItems() {
        mRealm = Realm.getDefaultInstance();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    editOrderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();
                }
            });
        } catch (Exception e) {
            e.getMessage();
        }

        for (int i = 0; i < editOrderListDetails.size(); i++) {
            RealmList<ItemDetails> itemDetails = editOrderListDetails.get(i).getItemDetailses();
            for (int j = 0; j < itemDetails.size(); j++) {
                String itemName = itemDetails.get(j).getItemNo();
                String totalCartons = itemDetails.get(j).getCartonQuantity().toString();
                String noOfCartonFilled = itemDetails.get(j).getNoOfCartonFilled().toString();
                commonItemId = itemDetails.get(j).getItemId();
                if (totalCartons.equals(noOfCartonFilled)) {
                    //Checking if last item
                    if (j == itemDetails.size() - 1) {
                        //Delete empty containers if exists
                        deleteEmptyContainers();
                        if (prefDeleteItem.equals("Delete")) {
                            SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                            PREF.putString(PreferenceUtil.DELETE_ITEM, "0");
                            PREF.apply();
                            BusFactory.getBus().post(new RefreshSingleViewOrder());
                            BusFactory.getBus().post(new RefreshEvent());
                            getActivity().finish();
                        } else {
                            //Move to price detail
                            startActivity(PriceActivity.getnewIntent(getActivity()));
                            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                            getActivity().finish();
                        }
                    } else {
                        continue;
                    }
                } else {
                    // may be we need to calculate cartonQty - NofCartonRemaing
                    String cartonQuantity = itemDetails.get(j).getCartonQuantity();
                    String cartonCBM = itemDetails.get(j).getCBM();
                    String cartonGrossWeight = itemDetails.get(j).getWeightofCarton();
                    checkCartonFullOrNotForEdit(cartonQuantity, cartonCBM, cartonGrossWeight);
                    //Checking if last item
                    if (j == itemDetails.size()) {
                        //control not get in to this block.!

                        deleteEmptyContainers();
                        if (prefDeleteItem.equals("Delete")) {
                            SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                            PREF.putString(PreferenceUtil.DELETE_ITEM, "0");
                            PREF.apply();
                            BusFactory.getBus().post(new RefreshSingleViewOrder());
                            BusFactory.getBus().post(new RefreshEvent());
                            getActivity().finish();
                        } else {
                            //Move to price detail

                            startActivity(PriceActivity.getnewIntent(getActivity()));
                            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                            getActivity().finish();
                        }
                    }
                    break;
                }
            }
            if (itemDetails.size()==0)
            {
                SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                PREF.putString(PreferenceUtil.DELETE_ITEM, "0");
                PREF.apply();
                BusFactory.getBus().post(new RefreshSingleViewOrder());
                BusFactory.getBus().post(new RefreshEvent());
                getActivity().finish();
            }
        }


    }

    private void deleteEmptyContainers() {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {
                        RealmList<ContainerDetails> containerDetails = orderListDetails.get(i).getContainerDetailses();
                        if (containerDetails.size() > 1) {
                            for (int j = 0; j < containerDetails.size(); j++) {
                                if (containerDetails.get(j).getFilledVolume().equals("0")) {
                                    containerDetails.deleteFromRealm(j);
                                    mRealm.insertOrUpdate(containerDetails);
                                }
                            }
                        }
                        mRealm.insertOrUpdate(orderListDetails);

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }
    }


    /* check if the container is full while editing*/
    private void checkCartonFullOrNotForEdit(String editCartonQty, String editCbm, String editGrossWeight) {


        String prefContainerId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0");


        cartonqtyDMY = Float.parseFloat(editCartonQty);
        float cbmDMY = Float.parseFloat(editCbm);
        float grossWightDMY = Float.parseFloat(editGrossWeight);

        cartonQty = cartonqtyDMY;
        cbm = cbmDMY;
        grossWight = grossWightDMY;


        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

        //Execution outside on qwery statement
        for (int i = 0; i < orderListDetails.size(); i++) {
            ContainerData = orderListDetails.get(i).getContainerDetailses();
//            RealmResults<ContainerDetails> bootResults = orderListDetails.get(i).getContainerDetailses().where().equalTo("Status", "0").findAll();
            for (int j = 0; j < ContainerData.size(); j++) {
//                            if (ContainerData.get(j).getStatus().equals("0")) {
                currentContainerFilledWeight = Float.parseFloat(ContainerData.get(j).getFilledWeight());
                currentContainerFilledVolume = Float.parseFloat(ContainerData.get(j).getFilledVolume());
                containerMaxVolumeF = Float.parseFloat(ContainerData.get(j).getMaximumVolume());
                containerMaxWeightF = Float.parseFloat(ContainerData.get(j).getMaximumWeight());
                containerType = ContainerData.get(j).getContainerType();
                containerNameDetails = ContainerData.get(j).getContainerName();
                cartainerIdUsedInItem = ContainerData.get(j).getContainerId();
                currentContainerName = ContainerData.get(j).getContainerName();
                volPercent = Float.parseFloat(ContainerData.get(j).getVolumepercentage());
                wgtPercent = Float.parseFloat(ContainerData.get(j).getWeightpercentage());

                availableVolume = containerMaxVolumeF - currentContainerFilledVolume;
                availableWeight = containerMaxWeightF - currentContainerFilledWeight;


                if (availableVolume >= cbm && availableWeight >= grossWight) {
                    int movableQtyOnVolume = (int) (availableVolume / cbm);
                    int movableQtyOnWeight = (int) (availableWeight / grossWight);
                    int movableQty;
                    //Checks if carton can be filled by volume or weight
                    if (movableQtyOnVolume > movableQtyOnWeight) {
                        movableQty = movableQtyOnWeight;
                    } else {
                        movableQty = movableQtyOnVolume;
                    }
                    //Checks if carton can be filled by volume or weight ends here.

                    if (movableQty <= cartonqtyDMY) {
                        if (cartonqtyDMY == 0 || movableQty == 0) {

                        } else {
                            cartonQty = movableQty;
                            continueScatteredFill(containerMaxVolumeF, containerMaxWeightF, currentContainerFilledVolume, currentContainerFilledWeight, cartainerIdUsedInItem, currentContainerName);
                            cartonqtyDMY = cartonqtyDMY - movableQty;
                        }

                    } else {
                        //
                        if (cartonqtyDMY > 0) {
                            //All remaing cartom will be suited in this container so use and move to next page.
                            cartonQty = cartonqtyDMY;
                            continueScatteredFill(containerMaxVolumeF, containerMaxWeightF, currentContainerFilledVolume, currentContainerFilledWeight, cartainerIdUsedInItem, currentContainerName);
                            cartonqtyDMY = 0;
                            //Next pageCode.--NotNeeded
//

                        }
                        Log.e("me", "hai");
                    }

                } else {
                    cartonQty = cartonqtyDMY;
                }

//                            }
            }

        }

        updateContainerDetails();

        //checking if carton quantity have left if not no code should work
        if (cartonqtyDMY > 0) {
            // fetch the details of container whose status is 0.
            cartonQty = cartonqtyDMY;
            currentItemVolume = cartonQty * cbm;//-> Item volume of cartonQty items
            currentItemWeight = cartonQty * grossWight;//-> current item weight

            float readQty = Float.parseFloat(editCartonQty);
            float readCbm = Float.parseFloat(editCbm);
            float readWeight = Float.parseFloat(editGrossWeight);
            //== To check if cartons of single items are stroing to multiple containers.
            totalItemVolume = readCbm * readQty;
            totalItemWeight = readWeight * readQty;
            //=====

            if (currentItemVolume < availableVolume && currentItemWeight < availableWeight) {
                //if not full normal process.
                useTempTableData = 0;
                continueOrder(containerMaxVolumeF, containerMaxWeightF, currentContainerFilledVolume, currentContainerFilledWeight, cartainerIdUsedInItem, containerNameDetails);
            } else {
                if (containeridCollection != null) {
                    cartainerIdUsedInItem = containeridCollection;
                }
                if (currentItemVolume > availableVolume && currentItemWeight < availableWeight) {
                    float cartonVolume = cbm;
                    float cartonWeight = grossWight;
                    float balanceVolume = currentItemVolume - availableVolume;//:->Remaining volume to be filled
                    float numberOfCartonsMoved = balanceVolume / cartonVolume;//:->Remaining carton to be filled
                    numberOfCartonsMoved = (float) Math.ceil(numberOfCartonsMoved);//:->Rounded backwards
                    float newContainerWeight = numberOfCartonsMoved * cartonWeight;//:-> new weight of remaining cartons
                    float newContainerVolume = numberOfCartonsMoved * cartonVolume;//:-> new volume of remaining cartons
                    float usedVolume = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerVolume);
                    float usedWeight = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerWeight);
                    saveToPreference(newContainerWeight, newContainerVolume, usedVolume, usedWeight);
                } else if (currentItemWeight > availableWeight && currentItemVolume < availableVolume) {
                    float cartonVolume = cbm;
                    float cartonWeight = grossWight;
                    float balanceWeight = currentItemWeight - availableWeight;
                    float numberOfCartonsMoved = balanceWeight / cartonWeight;
                    numberOfCartonsMoved = (float) Math.ceil(numberOfCartonsMoved);
                    float newContainerVolume = numberOfCartonsMoved * cartonVolume;
                    float newContainerWeight = numberOfCartonsMoved * cartonWeight;
                    float usedVolume = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerVolume);
                    float usedWeight = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerWeight);
                    saveToPreference(newContainerWeight, newContainerVolume, usedVolume, usedWeight);
                } else if (currentItemVolume > availableVolume && currentItemWeight > availableWeight) {
                    float cartonVolume = cbm;
                    float cartonWeight = grossWight;
                    float balanceVolume = currentItemVolume - availableVolume;
                    float numberOfCartonsMovedForVolume = balanceVolume / cartonVolume;
                    numberOfCartonsMovedForVolume = (float) Math.ceil(numberOfCartonsMovedForVolume);
                    float balanceWeight = currentItemWeight - availableWeight;
                    float numberOfCartonsMovedForWeight = balanceWeight / cartonWeight;
                    numberOfCartonsMovedForWeight = (float) Math.ceil(numberOfCartonsMovedForWeight);
                    if (numberOfCartonsMovedForVolume > numberOfCartonsMovedForWeight) {
                        float newContainerForVolume = numberOfCartonsMovedForVolume * cartonVolume;
                        float newContainerForWeight = numberOfCartonsMovedForWeight * cartonWeight;
                        float usedVolumeForVolume = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerForVolume);
                        float usedWeightForVolume = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerForWeight);
                        float newContainerWeightForVolume = numberOfCartonsMovedForVolume * cartonWeight;
                        saveToPreference(newContainerWeightForVolume, newContainerForVolume, usedVolumeForVolume, usedWeightForVolume);
                    } else if (numberOfCartonsMovedForVolume < numberOfCartonsMovedForWeight) {
                        float newContainerVolumeForVolume = numberOfCartonsMovedForWeight * cartonVolume;
                        float newContainerWeightForWeight = numberOfCartonsMovedForWeight * cartonWeight;
                        float usedWeightForWeight = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerWeightForWeight);
                        float usedVolumeForWeight = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerVolumeForVolume);
                        float newContainerVolumeForWeight = numberOfCartonsMovedForWeight * cartonVolume;
                        saveToPreference(newContainerWeightForWeight, newContainerVolumeForWeight, usedVolumeForWeight, usedWeightForWeight);

                    } else {
                        float newContainerVolumeForVolume = numberOfCartonsMovedForVolume * cartonVolume;
                        float newContainerWeightForWeight = numberOfCartonsMovedForVolume * cartonWeight;
                        float usedVolumeForVolume = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerVolumeForVolume);
                        float usedWeightForVolume = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerWeightForWeight);
                        float newContainerWeightForVolume = numberOfCartonsMovedForVolume * cartonWeight;
                        saveToPreference(newContainerWeightForVolume, newContainerVolumeForVolume, usedVolumeForVolume, usedWeightForVolume);

                    }
                }
                String balance_Volume = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BAL_VOLUME, "0");
                String balance_Weight = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BAL_WEIGHT, "0");
                String used_Weight = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USED_WEIGHT, "0");
                String used_Volume = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USED_VOLUME, "0");
                try {
                    mRealm = Realm.getDefaultInstance();
                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            String containerId = (PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0"));

                            tempContainerDetailsesList = new RealmList<TempContainerDetails>();

                            TempOrderListDetails tempOrderListDetails1 = mRealm.createObject(TempOrderListDetails.class);
                            tempOrderListDetails1.setCompanyID(prefCompanyId);
                            tempOrderListDetails1.setOrderId(prefOrderId);

                            tempContainerDetails = mRealm.copyToRealm(new TempContainerDetails());
                            tempContainerDetails.setStatus("0");
                            tempContainerDetails.setContainerId(String.valueOf(containerId));
                            tempContainerDetails.setContainerName(containerNameDetails);
                            tempContainerDetails.setContainerType(containerType);
                            tempContainerDetails.setFilledWeight(String.valueOf(currentContainerFilledWeight));
                            tempContainerDetails.setFilledVolume(String.valueOf(currentContainerFilledVolume));
                            tempContainerDetails.setMaximumWeight(String.valueOf(containerMaxWeightF));
                            tempContainerDetails.setMaximumVolume(String.valueOf(containerMaxVolumeF));
                            tempContainerDetails.setWeightpercentage(String.valueOf(wgtPercent));
                            tempContainerDetails.setVolumepercentage(String.valueOf(volPercent));
//                tempContainerDetails.setOrginalContainerId("0"); // already stored as containerId
                            tempContainerDetailsesList.add(tempContainerDetails);
                            tempOrderListDetails1.setContainerDetailsesTemp(tempContainerDetailsesList);
                            mRealm.insertOrUpdate(tempOrderListDetails1);


                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (mRealm != null) {
                        mRealm.close();
                    }
                }

                useTempTableData = 1;
                try {
                    cartenFullDialogBox(balance_Volume, balance_Weight, used_Volume, used_Weight, prefContainerId, containerType, currentContainerName);
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        } else {
            if (!prefEditBoothId.equals("0")) {
                reAllocateAllItems();
            }
        }


    }

    /* second call to check if the container is full after upgrade or add new container
    * */
    private void checkCartonFullOrNotSecondCall(float balancVolume1, float containerMaxVolume, float balanceWeigt1, float containerMaxWeight, String oldContainerType, String oldContaineName, String containerId) {

        float cartonvolume = Float.parseFloat(etCBM.getText().toString().trim());
        float cartonweight = Float.parseFloat(etGrossWeight.getText().toString().trim());

        if (balancVolume1 > containerMaxVolume || balanceWeigt1 > containerMaxWeight) {
            if (balancVolume1 > containerMaxVolume && balanceWeigt1 < containerMaxWeight) {

                float balancevolume = balancVolume1 - containerMaxVolume; // calculation for balance volume after fill the item in the container
                float noofcartonmoved = (balancevolume / cartonvolume);
                noofcartonmoved = (float) Math.ceil(noofcartonmoved);
                float newcontainervolume = noofcartonmoved * cartonvolume;
                float newcontainerweight = noofcartonmoved * cartonweight;
                float usedvolume = balancVolume1 - newcontainervolume;
                float usedweight = balanceWeigt1 - newcontainerweight;

                saveToPreference(newcontainerweight, newcontainervolume, usedvolume, usedweight);
                repeatAddCarton();
            } else if (balanceWeigt1 > containerMaxWeight && balancVolume1 < containerMaxVolume) {

                float balanceweight = balanceWeigt1 - containerMaxWeight;
                float noofcartonmoved = (balanceweight / cartonweight);
                noofcartonmoved = (float) Math.ceil(noofcartonmoved);
                float newcontainerweight = noofcartonmoved * cartonweight;
                float newcontainervolume = noofcartonmoved * cartonvolume;
                float usedweight = balanceWeigt1 - newcontainerweight;
                float usedvolume = balancVolume1 - newcontainervolume;
                saveToPreference(newcontainerweight, newcontainervolume, usedvolume, usedweight);

                /**return balance weight, balance volume used volume, used weight**/
                repeatAddCarton();

            } else if (balancVolume1 > containerMaxVolume && balanceWeigt1 > containerMaxWeight) {

                float balanceVolume = balancVolume1 - containerMaxVolume; //find over flow volume
                float numberOfCartonsMovedForVolume = (balanceVolume / cartonvolume); //cartons for balance volume
                numberOfCartonsMovedForVolume = (float) Math.ceil(numberOfCartonsMovedForVolume);
                float balanceWeight = balanceWeigt1 - containerMaxWeight; //find over flow weight
                float numberOfCartonsMovedForWeight = (balanceWeight / cartonweight); //find cartons for balace volume
                numberOfCartonsMovedForWeight = (float) Math.ceil(numberOfCartonsMovedForWeight);

                if (numberOfCartonsMovedForVolume > numberOfCartonsMovedForWeight) {
                    float newcontainervolumeforvolume = numberOfCartonsMovedForVolume * cartonvolume;
                    float newcontainerweightforvolume = numberOfCartonsMovedForVolume * cartonweight;
                    float usedVolume = balancVolume1 - newcontainervolumeforvolume;
                    float usedWeight = balanceWeigt1 - newcontainerweightforvolume;
                    float newcontinerweightforvolume = numberOfCartonsMovedForWeight * cartonweight;
                    /**return balance volume, balance weight, used volume, used weight**/
                    saveToPreference(newcontinerweightforvolume, newcontainervolumeforvolume, usedVolume, usedWeight);

                } else if (numberOfCartonsMovedForVolume < numberOfCartonsMovedForWeight) {
                    float newcontainervolumeforweight = numberOfCartonsMovedForWeight * cartonvolume;
                    float newcontainerweightforweight = numberOfCartonsMovedForWeight * cartonweight;
                    float usedvolume = balancVolume1 - newcontainervolumeforweight;
                    float usedweight = balanceWeigt1 - newcontainerweightforweight;
                    float newcontinerweightforweight = numberOfCartonsMovedForWeight * cartonweight;
                    /**return balance volume, balance weight, used volume, used weight**/
                    saveToPreference(newcontinerweightforweight, newcontainervolumeforweight, usedvolume, usedweight);

                } else {
                    float newcontainervolumeforvolume = numberOfCartonsMovedForVolume * cartonvolume;
                    float newcontainerweightforvolume = numberOfCartonsMovedForVolume * cartonweight;
                    float usedvolume = balancVolume1 - newcontainervolumeforvolume;
                    float usedweight = balanceWeigt1 - newcontainerweightforvolume;
                    float newcontinerweightforvolume = numberOfCartonsMovedForWeight * cartonweight;
                    /**return balance volume, balance weight, used volume, used weight**/
                    saveToPreference(newcontinerweightforvolume, newcontainervolumeforvolume, usedvolume, usedweight);

                }
                repeatAddCarton();
            } else {
                Toast.makeText(getActivity(), "carton save failed", Toast.LENGTH_SHORT).show();
            }

        } else {
            /**process when container is not full**/
            getContainerDetailsOffline(oldContainerType);
            String containerID = splitLastContainerType(cartainerIdUsedInItem);
            if (containerID.equals("")) {
                cartainerIdUsedInItem = containerId;
            } else {
//                cartainerIdUsedInItem = cartainerIdUsedInItem + "," + containerId;

            }
            float max_Wgt = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.MX_WEIGHT, "0"));
            float max_Vol = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.MX_VOLUME, "0"));
            float volumepercentage = (balancVolume1 / max_Vol) * 100;
            float weightpercentage = (balanceWeigt1 / max_Wgt) * 100;

            setContainerStatusFullOffline(volumepercentage, weightpercentage, oldContaineName, 0, max_Vol, max_Wgt, balancVolume1, balanceWeigt1, oldContainerType);


            continueOrder(containerMaxVolume, containerMaxWeight, balancVolume1, balanceWeigt1, cartainerIdUsedInItem, oldContaineName);


        }
    }

    public void upgradeAllCartonName(final String containerType) {
        try {
            mRealm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    String containerId = (PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0"));
                    RealmResults<OrderListDetails> orderList = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).findAll();
                    for (int i = 0; i < orderList.size(); i++) {
                        RealmList<ContainerDetails> containerDetails = orderList.get(i).getContainerDetailses();
                        for (int j = 0; j < containerDetails.size(); j++) {
                            String itemContainerId = containerDetails.get(j).getContainerId();
                            if (itemContainerId.equals(containerId)) {
                                containerDetails.get(j).setContainerType(containerType);
                                mRealm.insertOrUpdate(containerDetails);
                            }

                        }
                    }
                }


            });

        } catch (Exception e) {

        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }


    //Used to fill all items if in edit mode
    private void checkCartonFullOrNot() {


        String prefContainerId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0");


        cartonqtyDMY = Float.parseFloat(etCartonQty.getText().toString().trim());
        float cbmDMY = Float.parseFloat(etCBM.getText().toString().trim());
        float grossWightDMY = Float.parseFloat(etGrossWeight.getText().toString().trim());

        cartonQty = cartonqtyDMY;
        cbm = cbmDMY;
        grossWight = grossWightDMY;


        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

        //Execution outside on qwery statement
        for (int i = 0; i < orderListDetails.size(); i++) {
            ContainerData = orderListDetails.get(i).getContainerDetailses();
//            RealmResults<ContainerDetails> bootResults = orderListDetails.get(i).getContainerDetailses().where().equalTo("Status", "0").findAll();
            for (int j = 0; j < ContainerData.size(); j++) {
//                            if (ContainerData.get(j).getStatus().equals("0")) {
                currentContainerFilledWeight = Float.parseFloat(ContainerData.get(j).getFilledWeight());
                currentContainerFilledVolume = Float.parseFloat(ContainerData.get(j).getFilledVolume());
                containerMaxVolumeF = Float.parseFloat(ContainerData.get(j).getMaximumVolume());
                containerMaxWeightF = Float.parseFloat(ContainerData.get(j).getMaximumWeight());
                containerType = ContainerData.get(j).getContainerType();
                containerNameDetails = ContainerData.get(j).getContainerName();
                cartainerIdUsedInItem = ContainerData.get(j).getContainerId();
                currentContainerName = ContainerData.get(j).getContainerName();
                volPercent = Float.parseFloat(ContainerData.get(j).getVolumepercentage());
                wgtPercent = Float.parseFloat(ContainerData.get(j).getWeightpercentage());

                availableVolume = containerMaxVolumeF - currentContainerFilledVolume;
                availableWeight = containerMaxWeightF - currentContainerFilledWeight;


                if (availableVolume >= cbm && availableWeight >= grossWight) {
                    int movableQtyOnVolume = (int) (availableVolume / cbm);
                    int movableQtyOnWeight = (int) (availableWeight / grossWight);
                    int movableQty;
                    //Checks if carton can be filled by volume or weight
                    if (movableQtyOnVolume > movableQtyOnWeight) {
                        movableQty = movableQtyOnWeight;
                    } else {
                        movableQty = movableQtyOnVolume;
                    }
                    //Checks if carton can be filled by volume or weight ends here.

                    if (movableQty <= cartonqtyDMY) {
                        if (cartonqtyDMY == 0 || movableQty == 0) {

                        } else {
                            cartonQty = movableQty;
                            continueScatteredFill(containerMaxVolumeF, containerMaxWeightF, currentContainerFilledVolume, currentContainerFilledWeight, cartainerIdUsedInItem, currentContainerName);
                            cartonqtyDMY = cartonqtyDMY - movableQty;

                            //All cartons are filled- move to price detail
                            startActivity(PriceActivity.getnewIntent(getActivity()));
                            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                            getActivity().finish();

                        }

                    } else {
                        //
                        if (cartonqtyDMY > 0) {
                            //All remaing cartom will be suited in this container so use and move to next page.
                            cartonQty = cartonqtyDMY;
                            continueScatteredFill(containerMaxVolumeF, containerMaxWeightF, currentContainerFilledVolume, currentContainerFilledWeight, cartainerIdUsedInItem, currentContainerName);
                            cartonqtyDMY = 0;
                            //Next pageCode.
                            startActivity(PriceActivity.getnewIntent(getActivity()));
                            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                            getActivity().finish();

                        }
                        Log.e("me", "hai");
                    }

                } else {
                    cartonQty = cartonqtyDMY;
                }

//                            }
            }

        }

        updateContainerDetails();

        //checking if carton quantity have left if not no code should work
        if (cartonqtyDMY > 0) {
            // fetch the details of container whose status is 0.
            cartonQty = cartonqtyDMY;
            currentItemVolume = cartonQty * cbm;//-> Item volume of cartonQty items
            currentItemWeight = cartonQty * grossWight;//-> current item weight

            float readQty = Float.parseFloat(etCartonQty.getText().toString().trim());
            float readCbm = Float.parseFloat(etCBM.getText().toString().trim());
            float readWeight = Float.parseFloat(etGrossWeight.getText().toString().trim());
            //== To check if cartons of single items are stroing to multiple containers.
            totalItemVolume = readCbm * readQty;
            totalItemWeight = readWeight * readQty;
            //=====

            if (currentItemVolume < availableVolume && currentItemWeight < availableWeight) {
                //if not full normal process.
                useTempTableData = 0;
                continueOrder(containerMaxVolumeF, containerMaxWeightF, currentContainerFilledVolume, currentContainerFilledWeight, cartainerIdUsedInItem, containerNameDetails);
            } else {
                if (containeridCollection != null) {
                    cartainerIdUsedInItem = containeridCollection;
                }
                if (currentItemVolume > availableVolume && currentItemWeight < availableWeight) {
                    float cartonVolume = cbm;
                    float cartonWeight = grossWight;
                    float balanceVolume = currentItemVolume - availableVolume;//:->Remaining volume to be filled
                    float numberOfCartonsMoved = balanceVolume / cartonVolume;//:->Remaining carton to be filled
                    numberOfCartonsMoved = (float) Math.ceil(numberOfCartonsMoved);//:->Rounded backwards
                    float newContainerWeight = numberOfCartonsMoved * cartonWeight;//:-> new weight of remaining cartons
                    float newContainerVolume = numberOfCartonsMoved * cartonVolume;//:-> new volume of remaining cartons
                    float usedVolume = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerVolume);
                    float usedWeight = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerWeight);
                    saveToPreference(newContainerWeight, newContainerVolume, usedVolume, usedWeight);
                } else if (currentItemWeight > availableWeight && currentItemVolume < availableVolume) {
                    float cartonVolume = cbm;
                    float cartonWeight = grossWight;
                    float balanceWeight = currentItemWeight - availableWeight;
                    float numberOfCartonsMoved = balanceWeight / cartonWeight;
                    numberOfCartonsMoved = (float) Math.ceil(numberOfCartonsMoved);
                    float newContainerVolume = numberOfCartonsMoved * cartonVolume;
                    float newContainerWeight = numberOfCartonsMoved * cartonWeight;
                    float usedVolume = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerVolume);
                    float usedWeight = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerWeight);
                    saveToPreference(newContainerWeight, newContainerVolume, usedVolume, usedWeight);
                } else if (currentItemVolume > availableVolume && currentItemWeight > availableWeight) {
                    float cartonVolume = cbm;
                    float cartonWeight = grossWight;
                    float balanceVolume = currentItemVolume - availableVolume;
                    float numberOfCartonsMovedForVolume = balanceVolume / cartonVolume;
                    numberOfCartonsMovedForVolume = (float) Math.ceil(numberOfCartonsMovedForVolume);
                    float balanceWeight = currentItemWeight - availableWeight;
                    float numberOfCartonsMovedForWeight = balanceWeight / cartonWeight;
                    numberOfCartonsMovedForWeight = (float) Math.ceil(numberOfCartonsMovedForWeight);
                    if (numberOfCartonsMovedForVolume > numberOfCartonsMovedForWeight) {
                        float newContainerForVolume = numberOfCartonsMovedForVolume * cartonVolume;
                        float newContainerForWeight = numberOfCartonsMovedForWeight * cartonWeight;
                        float usedVolumeForVolume = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerForVolume);
                        float usedWeightForVolume = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerForWeight);
                        float newContainerWeightForVolume = numberOfCartonsMovedForVolume * cartonWeight;
                        saveToPreference(newContainerWeightForVolume, newContainerForVolume, usedVolumeForVolume, usedWeightForVolume);
                    } else if (numberOfCartonsMovedForVolume < numberOfCartonsMovedForWeight) {
                        float newContainerVolumeForVolume = numberOfCartonsMovedForWeight * cartonVolume;
                        float newContainerWeightForWeight = numberOfCartonsMovedForWeight * cartonWeight;
                        float usedWeightForWeight = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerWeightForWeight);
                        float usedVolumeForWeight = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerVolumeForVolume);
                        float newContainerVolumeForWeight = numberOfCartonsMovedForWeight * cartonVolume;
                        saveToPreference(newContainerWeightForWeight, newContainerVolumeForWeight, usedVolumeForWeight, usedWeightForWeight);

                    } else {
                        float newContainerVolumeForVolume = numberOfCartonsMovedForVolume * cartonVolume;
                        float newContainerWeightForWeight = numberOfCartonsMovedForVolume * cartonWeight;
                        float usedVolumeForVolume = currentContainerFilledVolume + ((cartonVolume * cartonQty) - newContainerVolumeForVolume);
                        float usedWeightForVolume = currentContainerFilledWeight + ((cartonWeight * cartonQty) - newContainerWeightForWeight);
                        float newContainerWeightForVolume = numberOfCartonsMovedForVolume * cartonWeight;
                        saveToPreference(newContainerWeightForVolume, newContainerVolumeForVolume, usedVolumeForVolume, usedWeightForVolume);

                    }
                }
                String balance_Volume = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BAL_VOLUME, "0");
                String balance_Weight = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BAL_WEIGHT, "0");
                String used_Weight = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USED_WEIGHT, "0");
                String used_Volume = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USED_VOLUME, "0");
                try {
                    mRealm = Realm.getDefaultInstance();
                    mRealm.executeTransaction(new Realm.Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            String containerId = (PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0"));

                            tempContainerDetailsesList = new RealmList<TempContainerDetails>();

                            TempOrderListDetails tempOrderListDetails1 = mRealm.createObject(TempOrderListDetails.class);
                            tempOrderListDetails1.setCompanyID(prefCompanyId);
                            tempOrderListDetails1.setOrderId(prefOrderId);

                            tempContainerDetails = mRealm.copyToRealm(new TempContainerDetails());
                            tempContainerDetails.setStatus("0");
                            tempContainerDetails.setContainerId(String.valueOf(containerId));
                            tempContainerDetails.setContainerName(containerNameDetails);
                            tempContainerDetails.setContainerType(containerType);
                            tempContainerDetails.setFilledWeight(String.valueOf(currentContainerFilledWeight));
                            tempContainerDetails.setFilledVolume(String.valueOf(currentContainerFilledVolume));
                            tempContainerDetails.setMaximumWeight(String.valueOf(containerMaxWeightF));
                            tempContainerDetails.setMaximumVolume(String.valueOf(containerMaxVolumeF));
                            tempContainerDetails.setWeightpercentage(String.valueOf(wgtPercent));
                            tempContainerDetails.setVolumepercentage(String.valueOf(volPercent));
//                tempContainerDetails.setOrginalContainerId("0"); // already stored as containerId
                            tempContainerDetailsesList.add(tempContainerDetails);
                            tempOrderListDetails1.setContainerDetailsesTemp(tempContainerDetailsesList);
                            mRealm.insertOrUpdate(tempOrderListDetails1);


                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    if (mRealm != null) {
                        mRealm.close();
                    }
                }

                useTempTableData = 1;
                try {
                    cartenFullDialogBox(balance_Volume, balance_Weight, used_Volume, used_Weight, prefContainerId, containerType, currentContainerName);
                } catch (Exception e) {
                    e.printStackTrace();

                }
            }
        }


    }


    /*method used to replace the last container used with new upgraded container
    * param 1- container types used*/
    private String splitLastContainerType(String cartonTypeUsedInItem) {
        String finalContainerTypesUsedInItem = "";
        if (null != cartonTypeUsedInItem && cartonTypeUsedInItem.length() > 0) {
            int endIndex = cartonTypeUsedInItem.lastIndexOf(",");
            if (endIndex != -1) {
                finalContainerTypesUsedInItem = cartonTypeUsedInItem.substring(0, endIndex); // not forgot to put check if(endIndex != -1)
            }
        }
        return finalContainerTypesUsedInItem;
    }

    /* method used to show the container full dialog if the container is full after a
    * new container is added */
    private void repeatAddCarton() {
        String balance_Volume = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BAL_VOLUME, "0");
        String balance_Weight = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BAL_WEIGHT, "0");
        String used_Weight = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USED_WEIGHT, "0");
        String used_Volume = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USED_VOLUME, "0");
        String containerID = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0");
        String newcontinerType = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_type, "0");
        String contanierName = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_NAME, "0");
        try {
            cartenFullDialogBox(balance_Volume, balance_Weight, used_Volume, used_Weight, containerID, newcontinerType, contanierName);
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    /* method save the values to preference for further calculation
    * param 1- new container weight
    * param 2- new container volume
    * param 3- used volume
    * param 4- used weight*/
    private void saveToPreference(float newContainerWeight, float newContainerVolume,
                                  float usedVolume, float usedWeight) {
        SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
        PREF.putString(PreferenceUtil.BAL_WEIGHT, String.valueOf(newContainerWeight));
        PREF.putString(PreferenceUtil.BAL_VOLUME, String.valueOf(newContainerVolume));
        PREF.putString(PreferenceUtil.USED_VOLUME, String.valueOf(usedVolume));
        PREF.putString(PreferenceUtil.USED_WEIGHT, String.valueOf(usedWeight));
        PREF.apply();
    }

    /*when container not full
    * param 1- current container max volume
    * param 2- current container max weight
    * param 3- current item filled volume
    * param 4- current item illed weight
    * param 5- current container type used
    * useTempTableData - if 0 then temporary table not used , if 1 then temporary table is used. */
    private void continueOrder(final float containerMaxVolume, final float containerMaxWeight, final float currentitemfiledVolume, final float currentItemFilledWeight, final String cartainerIdUsedInItem, final String containerName) {

//        final float cartonQty = Float.parseFloat(etCartonQty.getText().toString().trim());
//        final float cbm = Float.parseFloat(etCBM.getText().toString().trim());
//        final float grossWight = Float.parseFloat(etGrossWeight.getText().toString().trim());

        prefItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ITEM_ID, "0");
        final String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");
        final String prefContainerId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListSetUp = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();

                    ItemDetails itemExistOrNot = mRealm.where(ItemDetails.class)
                            .equalTo("orderId", prefOrderId).equalTo("BoothId", prefBoothId).equalTo("ItemId", prefItemId).findFirst();
                    ContainerDetails containerDetails = null;

                    if (useTempTableData == 0) {
                        containerDetails = mRealm.where(ContainerDetails.class)
                                .equalTo("orderId", prefOrderId).equalTo("ContainerId", prefContainerId).findFirst();
                    } else {

                    }
                    float totalCartonQty = 0;
                    float totalCBM = 0;
                    float totalGrossWeight = 0;
                    for (int i = 0; i < orderListSetUp.size(); i++) {
                        totalCartonQty = Float.parseFloat(orderListSetUp.get(i).getTotalCartons()) + cartonQty;
                        totalCBM = Float.parseFloat(orderListSetUp.get(i).getTotalOrderCBM()) + (cartonQty * cbm);
                        totalGrossWeight = Float.parseFloat(orderListSetUp.get(i).getTotalOrderWeight()) + (cartonQty * grossWight);
                    }

                    float containerFilledVolume = cartonQty * cbm;
                    float containerFilledWeight = cartonQty * grossWight;


                    float volumePercentage = ((containerFilledVolume + currentitemfiledVolume) / containerMaxVolume) * 100;
                    float weightPercentage = ((containerFilledWeight + currentItemFilledWeight) / containerMaxWeight) * 100;
                    if (useTempTableData == 0) {
//                        if (containerDetails != null) {
                        for (int i = 0; i < orderListSetUp.size(); i++) {
                            RealmResults<ContainerDetails> containerDetail = orderListSetUp.get(i).getContainerDetailses().where().equalTo("orderId", prefOrderId).equalTo("ContainerId", prefContainerId).findAll();
                            for (int j = 0; j < containerDetail.size(); j++) {
                                if (containerDetail.get(j).getOrderId().equals(prefOrderId) && containerDetail.get(j).getContainerId().equals(prefContainerId)) {
                                    containerDetail.get(j).setFilledVolume(String.valueOf((containerFilledVolume + currentitemfiledVolume)));
                                    containerDetail.get(j).setFilledWeight(String.valueOf((containerFilledWeight + currentItemFilledWeight)));
                                    containerDetail.get(j).setVolumepercentage(String.valueOf(volumePercentage));
                                    containerDetail.get(j).setWeightpercentage(String.valueOf(weightPercentage));
                                    containerDetail.get(j).setContainerName(containerName);
                                } else {
//                                    Toast.makeText(getActivity(), "not exist", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
//                            containerDetails.setFilledVolume(String.valueOf((containerFilledVolume + currentitemfiledVolume)));
//                            containerDetails.setFilledWeight(String.valueOf((containerFilledWeight + currentItemFilledWeight)));
//                            containerDetails.setVolumepercentage(String.valueOf(volumePercentage));
//                            containerDetails.setWeightpercentage(String.valueOf(weightPercentage));
//                            containerDetails.setContainerName(containerName);
                        mRealm.insertOrUpdate(containerDetails);
//                        } else {
//                            Toast.makeText(getActivity(), "not exist", Toast.LENGTH_SHORT).show();
//
//                        }
                    }


                    String date = "";
                    for (int i = 0; i < orderListSetUp.size(); i++) {
                        orderListSetUp.get(i).setCompanyID(prefCompanyId);
                        orderListSetUp.get(i).setTotalCartons(String.valueOf(totalCartonQty));
                        orderListSetUp.get(i).setTotalContainer("0");
                        orderListSetUp.get(i).setTotalOrderWeight(String.valueOf(totalGrossWeight));

                        orderListSetUp.get(i).setTotalOrderCBM(String.valueOf(totalCBM));
                        orderListSetUp.get(i).setOrderStatus("0");
                        orderListSetUp.get(i).setSynch("0");
                        date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                        orderListSetUp.get(i).setLastUpdate(date);
                    }

                    for (int j = 0; j < orderListSetUp.size(); j++) {
                        RealmList<ItemDetails> itemDetail = orderListSetUp.get(j).getItemDetailses();
                        for (int k = 0; k < itemDetail.size(); k++) {
                            if (!prefEditBoothId.equals("0")) {
                                prefItemId = commonItemId;
                            }
                            if (itemDetail.get(k).getOrderId().toString().equals(prefOrderId) && itemDetail.get(k).getItemId().toString().equals(prefItemId)) {
                                itemDetail.get(k).setOrderId(prefOrderId);
                                itemDetail.get(k).setItemId(prefItemId);
                                itemDetail.get(k).setContainerId(cartainerIdUsedInItem);//uniqid.... with comma
                                if (prefEditBoothId.equals("0")) {
                                    itemDetail.get(k).setBoothId(prefBoothId);
                                    String unitPerCarton = etUnitPerCarton.getText().toString().trim();
                                    itemDetail.get(k).setUnitperCarton(unitPerCarton);
                                    String grossWeight = etGrossWeight.getText().toString().trim();
                                    itemDetail.get(k).setWeightofCarton(grossWeight);
                                    String cartonLength = etCartonLegth.getText().toString().trim();
                                    itemDetail.get(k).setLength(cartonLength);
                                    String cartonWidth = etCartonWeidth.getText().toString().trim();
                                    itemDetail.get(k).setWidth(cartonWidth);
                                    String cartonHeight = etCartonHeigth.getText().toString().trim();
                                    itemDetail.get(k).setHeight(cartonHeight);
                                    String CBM = etCBM.getText().toString().trim();
                                    itemDetail.get(k).setCBM(CBM);
                                    String unitQTY = etUnitQty.getText().toString().trim();
                                    itemDetail.get(k).setUnitQuantity(unitQTY);
                                    String cartonQTY = etCartonQty.getText().toString().trim();
                                    itemDetail.get(k).setCartonQuantity(cartonQTY);
                                    itemDetail.get(k).setDate(date);
                                }
                                int cartonFilled = Integer.parseInt(itemDetail.get(k).getNoOfCartonFilled());
                                cartonFilled = cartonFilled + (int) (cartonQty);
                                itemDetail.get(k).setNoOfCartonFilled(String.valueOf(cartonFilled));
                                mRealm.insertOrUpdate(itemDetail);

                            }
                        }
                    }


                    mRealm.insertOrUpdate(orderListSetUp);


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

//        displayAll();

        if (useTempTableData == 1) {
            saveCartonDetailesFromTempTable();
        }
        deleteTempTable();

        //if not in edit mode Move to Price detail page
        if (prefEditBoothId.equals("0")) {
            startActivity(PriceActivity.getnewIntent(getActivity()));
            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
            getActivity().finish();
        } else {
            reAllocateAllItems();
        }

    }

    /* method to delete the temporary table.*/
    private void deleteTempTable() {
        mRealm.executeTransaction(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                final RealmResults<TempOrderListDetails> students = mRealm.where(TempOrderListDetails.class).findAll();
                TempOrderListDetails userdatabase = students.where().equalTo("OrderId", prefOrderId).findFirst();
                if (userdatabase != null) {
                    userdatabase.deleteFromRealm();
                }

            }
        });
    }

    /* save the carton details from the temporary table to the main table*/
    private void saveCartonDetailesFromTempTable() {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    TempOrderListDetails tempOrderListDetails = mRealm.where(TempOrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).findFirst();

                    if (tempOrderListDetails != null) {
                        for (int i = 0; i < tempOrderListDetails.getContainerDetailsesTemp().size(); i++) {

                            ContainerDetails containerDetails = mRealm.where(ContainerDetails.class)
                                    .equalTo("orderId", prefOrderId).equalTo("ContainerId", tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerId()).findFirst();

                            OrderListDetails orderListSetUp = mRealm.where(OrderListDetails.class)
                                    .equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findFirst();

                            if (containerDetails != null) {
//Check each container in orderlist for particular container id to update its details.
                                RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                                        .equalTo("OrderId", prefOrderId).findAll();
                                for (int l = 0; l < orderListDetails.size(); l++) {
                                    RealmList<ContainerDetails> containerDetailses = orderListDetails.get(l).getContainerDetailses();

                                    for (int k = 0; k < containerDetailses.size(); k++) {

                                        if (containerDetailses.get(k).getContainerId().equals(tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerId())) {
//                                            String containerId = tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerId();
//                                            String filledVolume = tempOrderListDetails.getContainerDetailsesTemp().get(i).getFilledVolume();
//                                            String containerName = tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerName();
//                                            String containerType = tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerType();
//                                            String volumePercentage = tempOrderListDetails.getContainerDetailsesTemp().get(i).getVolumepercentage();


                                            containerDetailses.get(k).setFilledVolume(tempOrderListDetails.getContainerDetailsesTemp().get(i).getFilledVolume());
                                            containerDetailses.get(k).setContainerName(tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerName());
                                            containerDetailses.get(k).setContainerType(tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerType());
                                            containerDetailses.get(k).setMaximumWeight(tempOrderListDetails.getContainerDetailsesTemp().get(i).getMaximumWeight());
                                            containerDetailses.get(k).setMaximumVolume(tempOrderListDetails.getContainerDetailsesTemp().get(i).getMaximumVolume());
                                            containerDetailses.get(k).setFilledWeight(tempOrderListDetails.getContainerDetailsesTemp().get(i).getFilledWeight());
                                            containerDetailses.get(k).setVolumepercentage(tempOrderListDetails.getContainerDetailsesTemp().get(i).getVolumepercentage());
                                            containerDetailses.get(k).setWeightpercentage(tempOrderListDetails.getContainerDetailsesTemp().get(i).getWeightpercentage());
                                            containerDetailses.get(k).setStatus(tempOrderListDetails.getContainerDetailsesTemp().get(i).getStatus());

                                            mRealm.insertOrUpdate(containerDetailses);
                                        }
                                    }
                                    mRealm.insertOrUpdate(orderListDetails);

                                }


                            } else {
                                ContainerDetails detailes = mRealm.copyToRealm(new ContainerDetails());
                                detailes.setOrderId(prefOrderId);
                                detailes.setContainerId(tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerId());
                                detailes.setContainerType(tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerType());
                                detailes.setContainerName(tempOrderListDetails.getContainerDetailsesTemp().get(i).getContainerName());
                                detailes.setFilledVolume(tempOrderListDetails.getContainerDetailsesTemp().get(i).getFilledVolume());
                                detailes.setFilledWeight(tempOrderListDetails.getContainerDetailsesTemp().get(i).getFilledWeight());
                                detailes.setVolumepercentage(tempOrderListDetails.getContainerDetailsesTemp().get(i).getVolumepercentage());
                                detailes.setWeightpercentage(tempOrderListDetails.getContainerDetailsesTemp().get(i).getWeightpercentage());
                                detailes.setMaximumWeight(tempOrderListDetails.getContainerDetailsesTemp().get(i).getMaximumWeight());
                                detailes.setMaximumVolume(tempOrderListDetails.getContainerDetailsesTemp().get(i).getMaximumVolume());
                                detailes.setStatus(tempOrderListDetails.getContainerDetailsesTemp().get(i).getStatus());
                                orderListSetUp.getContainerDetailses().add(detailes);
                            }
                        }


                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


    }

    /* display the carton full dialog box
    * param 1- balance volume
    * param 2- balance weight
    * param 3- used volume
    * param 4- used weight
    * param 5- current container id
    * param 6- current container type
    * param 7- current container name*/
    public void cartenFullDialogBox(final String balancevolume, final String balanceweight,
                                    final String usedVolume, final String usedWeight, final String containerId, final String oldContainerType, final String oldContaineName) throws Exception {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_new_container);
        dialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);

        Button btUpgraidNewContainer = (Button) dialog.findViewById(R.id.btUpgraidNewContainer);
        final Button btFilllNewContainer = (Button) dialog.findViewById(R.id.btFilllNewContainer);
        final TextView tv_upgradecontainer = (TextView) dialog.findViewById(R.id.tv_upgradecontainer);
        Button btIgnoreChanges = (Button) dialog.findViewById(R.id.btIgnoreChanges);

        final Spinner spinnerContainerNew = (Spinner) dialog.findViewById(R.id.spinnerContainerNew);
        final Spinner spinnerContainerUpdate = (Spinner) dialog.findViewById(R.id.spinnerContainerUpdate);

        final float balVolume = Float.parseFloat(balancevolume) + Float.parseFloat(usedVolume);
        final float balWeight = Float.parseFloat(balanceweight) + Float.parseFloat(usedWeight);

//        float cbm = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_CBM, "0"));
//        float grossWeight = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.GrossWeight, "0"));

        if (prefEditBoothId.equals("0")) {
            float cartonQty = Float.parseFloat(etCartonQty.getText().toString().trim());
            float cbm = Float.parseFloat(etCBM.getText().toString().trim());
            float grossWight = Float.parseFloat(etGrossWeight.getText().toString().trim());
            getSpinnerFromDatabase(balVolume, balWeight, spinnerContainerNew, spinnerContainerUpdate, cbm, grossWight);
        } else {
            getSpinnerFromDatabase(balVolume, balWeight, spinnerContainerNew, spinnerContainerUpdate, cbm, grossWight);
        }


        if (containerNameList.length == 0) {
            btFilllNewContainer.setVisibility(View.INVISIBLE);
        }
        if (container_NameList.length == 0) {
            btUpgraidNewContainer.setVisibility(View.INVISIBLE);
            tv_upgradecontainer.setText("No containers available for upgrade container");
        }
        btFilllNewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                addnewContainerOffline(containerIdList[spinnerContainerNew.getSelectedItemPosition()], usedVolume, usedWeight, balancevolume, balanceweight, oldContainerType, oldContaineName);
            }
        });

        btUpgraidNewContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                upgradecontainerOffline(container_IdList[spinnerContainerUpdate.getSelectedItemPosition()], balVolume, balWeight);
            }
        });
        btIgnoreChanges.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                deleteTempTable();
                dialog.dismiss();
            }
        });
        dialog.show();

    }

    /* method to upgrade container
    * param 1- new container type
    * param 2- balance volume
    * param 3- balance weight*/
    private void upgradecontainerOffline(String containerType, float balVolume, float balWeight) {

        getContainerDetailsOffline(containerType);
//        cartainerIdUsedInItem = splitLastContainerType(cartainerIdUsedInItem);
//        if (cartainerIdUsedInItem.equals("")) {
//            cartainerIdUsedInItem = containerType;
//        } else {
//            cartainerIdUsedInItem = cartainerIdUsedInItem + "," + containerType;
//        }

        float max_Wgt = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.MX_WEIGHT, "0"));
        float max_Vol = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.MX_VOLUME, "0"));
        String containerName = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_NAME, "0");
        float volumepercentage = (balVolume / max_Vol) * 100;
        float weightpercentage = (balWeight / max_Wgt) * 100;
        float filledVolume = balVolume;
        float filledWeight = balWeight;

        setContainerStatusUpgradeOffline(volumepercentage, weightpercentage, containerName, 0, max_Vol, max_Wgt, filledVolume, filledWeight, containerType);


//        upgradeAllCartonName(containerType);//To update name of container in cartons if upgrade container called

        continueOrder(max_Vol, max_Wgt, filledVolume, filledWeight, cartainerIdUsedInItem, containerName);


    }

    /* method to add new container
    * param 1- new container type
    * param 2- used volume
    * param 3- used weight
    * param 4- balance volume
    * param 5- balance weight
    * param 6- old container type
    * param 7- old container name */
    private void addnewContainerOffline(String newcontinerType, String
            usedVolume, String usedWeight, String balancevolume, String balanceweight, String oldContainerType, String oldContaineName) {
        Toast.makeText(getActivity(), "New container added", Toast.LENGTH_SHORT).show();


        lastUsedContainer = newcontinerType;
        float usedVolum = Float.parseFloat(usedVolume);
        float usedWeigh = Float.parseFloat(usedWeight);
        float balancevolum = Float.parseFloat(balancevolume);
        float balanceWeigt = Float.parseFloat(balanceweight);

        getContainerDetailsOffline(oldContainerType);

        float max_Wgt = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.MX_WEIGHT, "0"));
        float max_Vol = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.MX_VOLUME, "0"));
        float volumepercentage = (usedVolum / max_Vol) * 100;
        float weightpercentage = (usedWeigh / max_Wgt) * 100;
        float filledVolume = usedVolum;
        float filledWeight = usedWeigh;

        setContainerStatusFullOffline(volumepercentage, weightpercentage, oldContaineName, 1, max_Vol, max_Wgt, filledVolume, filledWeight, oldContainerType);

        getContainerDetailsOffline(newcontinerType);

        max_Wgt = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.MX_WEIGHT, "0"));
        max_Vol = Float.parseFloat(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.MX_VOLUME, "0"));

        String contanierName = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_NAME, "0");

        oldContainerId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0");
        String containerId = UUID.randomUUID().toString();


        if (totalItemVolume == balancevolum && totalItemWeight == balanceWeigt) {
            cartainerIdUsedInItem = containerId;
        } else {
            cartainerIdUsedInItem = cartainerIdUsedInItem + "," + containerId;

        }


        SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
        PREF.putString(PreferenceUtil.CONTAINER_ID, containerId);
        PREF.putString(PreferenceUtil.CONTAINER_type, newcontinerType);
        PREF.apply();
//temprory table

        saveToTempOrderDetails(newcontinerType, contanierName);

        checkCartonFullOrNotSecondCall(balancevolum, max_Vol, balanceWeigt, max_Wgt, newcontinerType, contanierName, cartainerIdUsedInItem);


    }

    /* method to set the container status to 1 during upgrade and update the fields in the temporary table with new values
    * param 1- volume percentage
    * param 2- weight percentage
    * param 3- new container name
    * param 4- flag if 1 status ful 0 status not full
    * param 5- maximum volume
    * param 6- maximum weight
    * param 7- filled volume
    * param 8-filled weight
    * param 9-newContainerType*/

    private void setContainerStatusUpgradeOffline(final float volumepercentage, final float weightpercentage, final String newContaineName, final int flag, final float maxVol, final float maxWgt, final float filledVolume, final float filledWeght, final String newContainerType) {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    int companyid = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
                    TempOrderListDetails orderListSetUp = mRealm.where(TempOrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyid).findFirst();

                    for (int i = 0; i < orderListSetUp.getContainerDetailsesTemp().size(); i++) {

                        if (orderListSetUp.getContainerDetailsesTemp().get(i).getStatus().equals("0")) {
                            orderListSetUp.getContainerDetailsesTemp().get(i).setStatus(String.valueOf(flag));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setWeightpercentage(String.valueOf(weightpercentage));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setVolumepercentage(String.valueOf(volumepercentage));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setContainerName(String.valueOf(newContaineName));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setContainerType(newContainerType);

                            orderListSetUp.getContainerDetailsesTemp().get(i).setFilledVolume(String.valueOf(filledVolume));

                            orderListSetUp.getContainerDetailsesTemp().get(i).setFilledWeight(String.valueOf(filledWeght));

                            orderListSetUp.getContainerDetailsesTemp().get(i).setMaximumVolume(String.valueOf(maxVol));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setMaximumWeight(String.valueOf(maxWgt));
                            mRealm.insertOrUpdate(orderListSetUp);

                        }

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


    }

    /* method to set the container status to 1 during add new container and update the fields in the temporary table with new values
     param 1- volume percentage
    * param 2- weight percentage
    * param 3- old container name
    * param 4- flag if 1 status ful 0 status not full
    * param 5- maximum volume
    * param 6- maximum weight
    * param 7- filled volume
    * param 8-filled weight*/
    private void setContainerStatusFullOffline(final float volumepercentage, final float weightpercentage, final String oldContaineName, final int flag, final float maxVol, final float maxWgt, final float filledVolume, final float filledWeght, final String oldContainerType) {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    int companyid = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
                    TempOrderListDetails orderListSetUp = mRealm.where(TempOrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyid).findFirst();//only container details, container id

                    for (int i = 0; i < orderListSetUp.getContainerDetailsesTemp().size(); i++) {

                        if (orderListSetUp.getContainerDetailsesTemp().get(i).getStatus().equals("0")) {

                            orderListSetUp.getContainerDetailsesTemp().get(i).setStatus(String.valueOf(flag));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setWeightpercentage(String.valueOf(weightpercentage));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setVolumepercentage(String.valueOf(volumepercentage));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setContainerName(String.valueOf(oldContaineName));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setContainerType(oldContainerType);

                            orderListSetUp.getContainerDetailsesTemp().get(i).setFilledVolume(String.valueOf(filledVolume));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setFilledWeight(String.valueOf(filledWeght));

                            orderListSetUp.getContainerDetailsesTemp().get(i).setMaximumVolume(String.valueOf(maxVol));
                            orderListSetUp.getContainerDetailsesTemp().get(i).setMaximumWeight(String.valueOf(maxWgt));
                            mRealm.insertOrUpdate(orderListSetUp);
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


    }

    /* save the carton details to temporary table
    * param 1- container type
    * param 2- container name*/
    private void saveToTempOrderDetails(final String containerType, final String contanierName) {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    TempOrderListDetails tempOrderListDetails = mRealm.where(TempOrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).findFirst();


                    int companyid = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
                    String containerId = (PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0"));

                    TempContainerDetails tempList = mRealm.copyToRealm(new TempContainerDetails());
                    if (tempOrderListDetails != null) {
                        tempOrderListDetails.setCompanyID(companyid);
                        tempOrderListDetails.setOrderId(prefOrderId);
                        tempList.setStatus("0");
                        tempList.setContainerName(contanierName);
                        tempList.setContainerType(containerType);
                        tempList.setContainerId(String.valueOf(containerId));
                        tempList.setFilledWeight("0");
                        tempList.setFilledVolume("0");
                        tempList.setVolumepercentage("0");
                        tempList.setWeightpercentage("0");

                        if (tempList != null) {
                            tempOrderListDetails.getContainerDetailsesTemp().add(tempList);
                        } else {
                            Toast.makeText(getActivity(), "nothing", Toast.LENGTH_SHORT).show();
                        }
                        mRealm.insertOrUpdate(tempOrderListDetails);

                    } else {
                        tempContainerDetailsesList = new RealmList<TempContainerDetails>();

                        TempOrderListDetails tempOrderListDetails1 = mRealm.createObject(TempOrderListDetails.class);
                        tempOrderListDetails1.setCompanyID(prefCompanyId);
                        tempOrderListDetails1.setOrderId(prefOrderId);

                        tempContainerDetails = mRealm.copyToRealm(new TempContainerDetails());
                        tempContainerDetails.setStatus("0");
                        tempContainerDetails.setContainerName(contanierName);
                        tempContainerDetails.setContainerId(String.valueOf(containerId));
                        tempContainerDetails.setFilledWeight("0");
                        tempContainerDetails.setFilledVolume("0");
                        tempContainerDetails.setVolumepercentage("0");
                        tempContainerDetails.setWeightpercentage("0");

                        tempContainerDetailsesList.add(tempContainerDetails);
                        tempOrderListDetails1.setContainerDetailsesTemp(tempContainerDetailsesList);
                        mRealm.insertOrUpdate(tempOrderListDetails1);

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


    }

    /* method to get the container details
    * param 1- container type*/
    private void getContainerDetailsOffline(final String containerType) {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", prefCompanyId).findAll();
                    for (int i = 0; i < companySetUpDetails.size(); i++) {
//                Container = companySetUpDetails.get(i).getContainerDetails();
                        RealmResults<ContainerInfo> containerResults = companySetUpDetails.get(i).getContainerDetails().where().equalTo("ID", containerType).findAll();
                        if (containerResults != null) {
//                    Toast.makeText(getActivity(), containerResults.get(0).getMaximumVolume() + "", Toast.LENGTH_SHORT).show();
                            SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                            PREF.putString(PreferenceUtil.MX_WEIGHT, containerResults.get(0).getMaximumWeight());
                            PREF.putString(PreferenceUtil.MX_VOLUME, containerResults.get(0).getMaximumVolume());
                            PREF.putString(PreferenceUtil.CONTAINER_NAME, containerResults.get(0).getContainer());
                            PREF.apply();
                        }
                    }

                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


    }

    /* get container list
    * param 1- balance volume
    * param 2- balance weight
    * param 3- spinner id of add new container
    * param 4- spinner id of upgrade container
    * param 5- CBM
    * param 6- Gross weight*/
    private void getSpinnerFromDatabase(float balVolume, float balWeight, Spinner
            spinnerContainerNew, Spinner spinnerContainerUpdate, float cbm, float grossWeight) {

        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", prefCompanyId).findAll();
        if (!companySetUpDetails.isEmpty()) {
            try {

                for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {

                    int totalCount = 0;
                    for (int j = 0; j < companySetUpDetails.get(i).getContainerDetails().size(); j++) {
                        float maxVolume = Float.parseFloat(companySetUpDetails.get(i).getContainerDetails().get(j).getMaximumVolume());
                        float maxWeight = Float.parseFloat(companySetUpDetails.get(i).getContainerDetails().get(j).getMaximumWeight());
                        if (grossWeight < maxWeight && cbm < maxVolume) {
                            totalCount++;
                        }

                    }
                    containerNameList = new String[totalCount];
                    containerIdList = new String[totalCount];
                    containerMaxVolume = new String[totalCount];
                    containerMaxWeight = new String[totalCount];
                    for (int j = 0, m = 0; j < companySetUpDetails.get(i).getContainerDetails().size(); j++) {
                        float maxVolume = Float.parseFloat(companySetUpDetails.get(i).getContainerDetails().get(j).getMaximumVolume());
                        float maxWeight = Float.parseFloat(companySetUpDetails.get(i).getContainerDetails().get(j).getMaximumWeight());
                        if (grossWeight < maxWeight && cbm < maxVolume) {
                            containerNameList[m] = companySetUpDetails.get(i).getContainerDetails().get(j).getContainer();
                            containerIdList[m] = companySetUpDetails.get(i).getContainerDetails().get(j).getID();
                            containerMaxVolume[m] = companySetUpDetails.get(i).getContainerDetails().get(j).getMaximumVolume();
                            containerMaxWeight[m] = companySetUpDetails.get(i).getContainerDetails().get(j).getMaximumWeight();
                            m++;
                        }

                    }
                    int count = 0;
                    for (int k = 0; k < containerIdList.length; k++) {
                        float maxVolume = Float.parseFloat(containerMaxVolume[k]);
                        float MaxWeight = Float.parseFloat(containerMaxWeight[k]);
                        if (maxVolume > balVolume && MaxWeight > balWeight) {
                            count = count + 1;
                        }

                    }

                    SetSpinnerData(containerNameList, spinnerContainerNew, "");
                    container_NameList = new String[count];
                    container_IdList = new String[count];
                    container_MaxVolume = new String[count];
                    container_MaxWeight = new String[count];

                    for (int l = 0, m = 0; m < containerIdList.length; m++) {
                        if (Integer.parseInt(containerMaxVolume[m]) > balVolume && Integer.parseInt(containerMaxWeight[m]) > balWeight) {
                            container_NameList[l] = containerNameList[m];
                            container_IdList[l] = containerIdList[m];
                            container_MaxVolume[l] = containerMaxVolume[m];
                            container_MaxWeight[l] = containerMaxWeight[m];
                            l++;
                        }
                    }
                    SetSpinnerData(container_NameList, spinnerContainerUpdate, "");
                }

            } catch (Exception e) {

            }
        } else {
            Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
        }

    }


    /* method to set the containers used to spinner data
    * param 1- string array to adapt to the spinner
    * param 2- spinner Id
    * param 3- spinner data selected*/
    private void SetSpinnerData(String[] NameList, Spinner spinnerId, String name) {
        if (NameList.length != 0) {
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, NameList);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinnerId.setAdapter(spinnerArrayAdapter);
            if (name.trim().length() != 0) {
                for (int i = 0; i < NameList.length; i++) {
                    if (name.equals(NameList[i])) {
                        spinnerId.setSelection(i);
                    }
                }
            }
        }
    }

    //Anandhu--_Modded
    public String getContainerDetailsUsedInItem() {
        final String[] oldContainersUsed = new String[1];
//        String prefContainerId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.CONTAINER_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {
                        ContainerData = orderListDetails.get(i).getContainerDetailses();
//            RealmResults<ContainerDetails> bootResults = orderListDetails.get(i).getContainerDetailses().where().equalTo("Status", "0").findAll();
                        for (int j = 0; j < ContainerData.size(); j++) {
                            if (ContainerData.get(j).getStatus().equals("0")) {

                                oldContainersUsed[0] = ContainerData.get(j).getContainerType();
                            }
                        }

                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }
        return oldContainersUsed[0];
    }
    //Ended

    //Method to fill carton to used container if carton fits.
    private void continueScatteredFill(final float containerMaxVolume, final float containerMaxWeight, final float currentitemfiledVolume, final float currentItemFilledWeight, final String containerIdUsedInItem, final String containerName) {


        prefItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ITEM_ID, "0");
        final String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");
        final String[] containerid = new String[1];//:->For storing container id in an item
        try {
            if (mRealm != null) {
                mRealm.close();
            }
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListSetUp = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();

                    ContainerDetails containerDetails = null;

                    containerDetails = mRealm.where(ContainerDetails.class)
                            .equalTo("orderId", prefOrderId).equalTo("ContainerId", containerIdUsedInItem).findFirst();
                    float totalCartonQty = 0;
                    float totalCBM = 0;
                    float totalGrossWeight = 0;
                    for (int i = 0; i < orderListSetUp.size(); i++) {
                        totalCartonQty = Float.parseFloat(orderListSetUp.get(i).getTotalCartons()) + cartonQty;
                        totalCBM = Float.parseFloat(orderListSetUp.get(i).getTotalOrderCBM()) + (cartonQty * cbm);
                        totalGrossWeight = Float.parseFloat(orderListSetUp.get(i).getTotalOrderWeight()) + (cartonQty * grossWight);
                    }
                    //Incase of edit previous item weight should be substracted.
                    if (!prefBoothId.equals("0"))
                    {
                        totalGrossWeight = totalGrossWeight - previousItemWeight;
                    }

                    float containerFilledVolume = cartonQty * cbm;
                    float containerFilledWeight = cartonQty * grossWight;


                    float volumePercentage = ((containerFilledVolume + currentitemfiledVolume) / containerMaxVolume) * 100;
                    float weightPercentage = ((containerFilledWeight + currentItemFilledWeight) / containerMaxWeight) * 100;

//                    if (useTempTableData == 0) {
                    if (containerDetails != null) {
                        for (int i = 0; i < orderListSetUp.size(); i++) {
                            RealmResults<ContainerDetails> containerDetail = orderListSetUp.get(i).getContainerDetailses().where().equalTo("orderId", prefOrderId).findAll();
                            for (int j = 0; j < containerDetail.size(); j++) {
                                if (containerDetail.get(j).getOrderId().equals(prefOrderId) && containerDetail.get(j).getContainerId().equals(containerIdUsedInItem)) {
                                    containerDetail.get(j).setFilledVolume(String.valueOf((containerFilledVolume + currentitemfiledVolume)));
                                    containerDetail.get(j).setFilledWeight(String.valueOf((containerFilledWeight + currentItemFilledWeight)));
                                    containerDetail.get(j).setVolumepercentage(String.valueOf(volumePercentage));
                                    containerDetail.get(j).setWeightpercentage(String.valueOf(weightPercentage));
                                    containerDetail.get(j).setContainerName(containerName);
                                } else {
//                                    Toast.makeText(getActivity(), "not exist", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                        mRealm.insertOrUpdate(containerDetails);
                    }


                    String date = "";
                    for (int i = 0; i < orderListSetUp.size(); i++) {
                        orderListSetUp.get(i).setCompanyID(prefCompanyId);
                        orderListSetUp.get(i).setTotalCartons(String.valueOf(totalCartonQty));
                        orderListSetUp.get(i).setTotalContainer("0");
                        orderListSetUp.get(i).setTotalOrderWeight(String.valueOf(totalGrossWeight));

                        orderListSetUp.get(i).setTotalOrderCBM(String.valueOf(totalCBM));
                        orderListSetUp.get(i).setOrderStatus("0");
                        orderListSetUp.get(i).setSynch("0");
                        date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                        orderListSetUp.get(i).setLastUpdate(date);
                    }

                    for (int i = 0; i < orderListSetUp.size(); i++) {
                        RealmList<ItemDetails> itemDetail = orderListSetUp.get(i).getItemDetailses();
                        for (int k = 0; k < itemDetail.size(); k++) {
                            if (!prefEditBoothId.equals("0")) {
                                prefItemId = commonItemId;
                            }
                            if (itemDetail.get(k).getOrderId().toString().equals(prefOrderId) && itemDetail.get(k).getItemId().toString().equals(prefItemId)) {
                                itemDetail.get(k).setOrderId(prefOrderId);
                                itemDetail.get(k).setItemId(prefItemId);
                                containerid[0] = itemDetail.get(k).getContainerId();
                                if (containerid[0].equals("") || containerid[0].equals("0")) {
                                    containerid[0] = containerIdUsedInItem;

                                } else {
                                    containerid[0] = containerid[0] + "," + containerIdUsedInItem;

                                }
                                containeridCollection = containerid[0];
                                itemDetail.get(k).setContainerId(containerid[0]);//uniqid.... with comma
                                if (prefEditBoothId.equals("0")) {
                                    itemDetail.get(k).setBoothId(prefBoothId);

                                    String unitPerCarton = etUnitPerCarton.getText().toString().trim();
                                    itemDetail.get(k).setUnitperCarton(unitPerCarton);

                                    String grossWeight = etGrossWeight.getText().toString().trim();
                                    itemDetail.get(k).setWeightofCarton(grossWeight);

                                    String cartonLength = etCartonLegth.getText().toString().trim();
                                    itemDetail.get(k).setLength(cartonLength);

                                    String cartonWidth = etCartonWeidth.getText().toString().trim();
                                    itemDetail.get(k).setWidth(cartonWidth);

                                    String cartonHeight = etCartonHeigth.getText().toString().trim();
                                    itemDetail.get(k).setHeight(cartonHeight);

                                    String CBM = etCBM.getText().toString().trim();
                                    itemDetail.get(k).setCBM(CBM);

                                    String unitQTY = etUnitQty.getText().toString().trim();
                                    itemDetail.get(k).setUnitQuantity(unitQTY);

                                    String cartonQTY = etCartonQty.getText().toString().trim();
                                    itemDetail.get(k).setCartonQuantity(cartonQTY);
                                }
                                itemDetail.get(k).setDate(date);
                                int cartonFilled = Integer.parseInt(itemDetail.get(k).getNoOfCartonFilled());
                                cartonFilled = cartonFilled + (int) (cartonQty);
                                itemDetail.get(k).setNoOfCartonFilled(String.valueOf(cartonFilled));
                                mRealm.insertOrUpdate(itemDetail);

                            }
                        }
                    }

                    mRealm.insertOrUpdate(orderListSetUp);
                    mRealm.insertOrUpdate(containerDetails);


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


    }

    //-Used to update variables immediate aftre a write operation in realmDB
    public void updateContainerDetails() {

        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    orderListDetails = mRealm.where(OrderListDetails.class).equalTo("OrderId", prefOrderId).equalTo("CompanyID", prefCompanyId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {
                        ContainerData = orderListDetails.get(i).getContainerDetailses();
//            RealmResults<ContainerDetails> bootResults = orderListDetails.get(i).getContainerDetailses().where().equalTo("Status", "0").findAll();
                        for (int j = 0; j < ContainerData.size(); j++) {
//                            if (ContainerData.get(j).getStatus().equals("0")) {
                            currentContainerFilledWeight = Float.parseFloat(ContainerData.get(j).getFilledWeight());
                            currentContainerFilledVolume = Float.parseFloat(ContainerData.get(j).getFilledVolume());
                            containerMaxVolumeF = Float.parseFloat(ContainerData.get(j).getMaximumVolume());
                            containerMaxWeightF = Float.parseFloat(ContainerData.get(j).getMaximumWeight());
                            containerType = ContainerData.get(j).getContainerType();
                            containerNameDetails = ContainerData.get(j).getContainerName();
                            cartainerIdUsedInItem = ContainerData.get(j).getContainerId();
                            currentContainerName = ContainerData.get(j).getContainerName();
                            volPercent = Float.parseFloat(ContainerData.get(j).getVolumepercentage());
                            wgtPercent = Float.parseFloat(ContainerData.get(j).getWeightpercentage());

                            availableVolume = containerMaxVolumeF - currentContainerFilledVolume;
                            availableWeight = containerMaxWeightF - currentContainerFilledWeight;
                        }
                    }


                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }
    }

    public void deleteItemDialog() {
        Button btn_Yes, btn_Cancel;
        final Dialog dialog = new Dialog(getActivity());
        dialog.setContentView(R.layout.dialog_delete_item);
        dialog.setCanceledOnTouchOutside(false);
        btn_Yes = (Button) dialog.findViewById(R.id.btYes);
        btn_Cancel = (Button) dialog.findViewById(R.id.btCancel);

        dialog.show();

        btn_Yes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                updateOrderPriceDetailsDelete();
                deleteItem();
                deleteUnusedBooth();
                updateOrderDetails();
                freeUpAllContainers();
                reAllocateAllItems();

            }
        });

        btn_Cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                getActivity().finish();
            }
        });
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                getActivity().finish();

            }
        });
    }

    //Update Order details(Use booth count) while deleting
    private void updateOrderDetails() {
        final int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();
                    for (int i=0;i<orderListDetails.size();i++)
                    {
                        RealmList<BoothDetails> boothDetails = orderListDetails.get(i).getBoothDetailses();
                        orderListDetails.get(i).setTotalBooth(String.valueOf(boothDetails.size()));
                        mRealm.insertOrUpdate(orderListDetails);
                    }
                }
            });

        }
        catch (Exception e)
        {
            e.getMessage();
        }
        finally {
            if (mRealm!=null)
            {
                mRealm.close();
            }
        }
    }

    //Delete un used booths in an order.
    private void deleteUnusedBooth() {
        final int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();
                    for (int i=0;i<orderListDetails.size();i++)
                    {
                        RealmList<BoothDetails> boothDetails = orderListDetails.get(i).getBoothDetailses();
                        for (int j=0;j<boothDetails.size();j++)
                        {
                            int usedBoothCount=0;
                            RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();
                            for (int k=0;k<itemDetails.size();k++)
                            {
                                if (boothDetails.get(j).getBoothId().equals(itemDetails.get(k).getBoothId()))
                                {
                                    usedBoothCount++;
                                }

                            }
                            if (usedBoothCount==0)
                            {
                                boothDetails.deleteFromRealm(j);
                            }
                            mRealm.insertOrUpdate(boothDetails);
                        }
                        mRealm.insertOrUpdate(orderListDetails);
                    }
                }
            });

        }
        catch (Exception e)
        {
            e.getMessage();
        }
        finally {
            if (mRealm!=null)
            {
                mRealm.close();
            }
        }

    }

    //Delete item from order
    private void deleteItem() {
        mRealm = Realm.getDefaultInstance();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    RealmResults<OrderListDetails> OrderListDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", prefCompanyId).equalTo("OrderId", prefOrderId).findAll();
                    for (int i = 0; i < OrderListDetails.size(); i++) {

                        RealmList<ItemDetails> itemDetails = OrderListDetails.get(i).getItemDetailses();
                        for (int j = 0; j < itemDetails.size(); j++) {
                            if (itemDetails.get(j).getItemId().equals(prefEditItemId)) {
                                itemDetails.deleteFromRealm(j);
                                Toast.makeText(getActivity(), "Item Deleted", Toast.LENGTH_SHORT).show();
                            }
                            mRealm.insertOrUpdate(itemDetails);
                        }
                        mRealm.insertOrUpdate(OrderListDetails);
                    }
                }
            });
        } catch (Exception e) {

        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

    //Update price details of order while deleting item
    private void updateOrderPriceDetailsDelete() {
        mRealm = Realm.getDefaultInstance();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    RealmResults<OrderListDetails> OrderListDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", prefCompanyId).equalTo("OrderId", prefOrderId).findAll();
                    for (int i = 0; i < OrderListDetails.size(); i++) {
                        float totalItemPriceInUs, totalItemPriceInRmb, totalItemPriceIn3rd;
                        float orderPriceinUS, orderPriceinRmb, orderTotal3rdCurr;

                        orderPriceinUS = Float.parseFloat(OrderListDetails.get(i).getTotalOrderUsValue());
                        orderPriceinRmb = Float.parseFloat(OrderListDetails.get(i).getTotalOrderRmbValue());
                        orderTotal3rdCurr = Float.parseFloat(OrderListDetails.get(i).getTotal3rdCurrencyValue());


                        RealmList<ItemDetails> itemDetails = OrderListDetails.get(i).getItemDetailses();
                        for (int j = 0; j < itemDetails.size(); j++) {


                            itemDetails.get(j).setContainerId("0");
                            itemDetails.get(j).setNoOfCartonFilled("0");
                            if (itemDetails.get(j).getItemId().equals(prefEditItemId)) {
                                totalItemPriceIn3rd = Float.parseFloat(itemDetails.get(j).getTotalPriceIn3rdCurrency());
                                totalItemPriceInRmb = Float.parseFloat(itemDetails.get(j).getTotalPriceInRMB());
                                totalItemPriceInUs = Float.parseFloat(itemDetails.get(j).getTotalPriceInUS());


                                OrderListDetails.get(i).setTotalOrderUsValue(String.valueOf(orderPriceinUS - totalItemPriceInUs));
                                OrderListDetails.get(i).setTotal3rdCurrencyValue(String.valueOf(orderTotal3rdCurr - totalItemPriceIn3rd));
                                OrderListDetails.get(i).setTotalOrderRmbValue(String.valueOf(orderPriceinRmb - totalItemPriceInRmb));


                                OrderListDetails.get(i).setTotalCartons("0");
                                OrderListDetails.get(i).setTotalOrderCBM("0");
                                OrderListDetails.get(i).setTotalOrderWeight("0");

                            }
                            mRealm.insertOrUpdate(itemDetails);
                        }
                        mRealm.insertOrUpdate(OrderListDetails);
                    }
                }
            });
        } catch (Exception e) {

        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }
    }
}

