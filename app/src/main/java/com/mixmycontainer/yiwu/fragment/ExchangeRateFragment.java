package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.apis.CompanySetUpExchangeRateService;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.ResultCode;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpExchangeRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpExchangeRateResponse;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 14-11-2016.
 */
public class ExchangeRateFragment extends BaseFragment {
    private SharedPreferences sharedPref;
    private Spinner spinnerCurrencyNames;
    private CheckBox isActivatedCurrency;
    private EditText etDollerRMBexcahange, etDoller3rdCurrencyexchange;
    private ProgressBar progressBar;
    private LinearLayout lnr_view3Dcurrency;
    private MenuItem fav;
    private Realm mRealm;
    private int companyId;
    private String ExchangeRate3dCurrency, ThirdCurrency, activated3dCurrency;

    public static ExchangeRateFragment newInstance() {
        ExchangeRateFragment fragment = new ExchangeRateFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_exchange_rate, container, false);

        mRealm =  Realm.getDefaultInstance();

        initview(view);
        initlestere();
        isActivatedCurrency.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                           @Override
                                                           public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                                                               if (isChecked) {
                                                                   lnr_view3Dcurrency.setVisibility(View.VISIBLE);

                                                               } else {
                                                                   lnr_view3Dcurrency.setVisibility(View.GONE);

                                                               }
                                                           }
                                                       }
        );
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public int getTitle() {
        return R.string.txt_buyer_company;
    }

    private void initlestere() {

    }

    private void initview(View view) {
        spinnerCurrencyNames = (Spinner) view.findViewById(R.id.spinnerCurrencyNames);
        etDoller3rdCurrencyexchange = (EditText) view.findViewById(R.id.etDoller3rdCurrencyexchange);
        etDollerRMBexcahange = (EditText) view.findViewById(R.id.etDollerRMBexcahange);
        isActivatedCurrency = (CheckBox) view.findViewById(R.id.isActivatedCurrency);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        lnr_view3Dcurrency = (LinearLayout) view.findViewById(R.id.lnr_view3Dcurrency);
        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));

        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
        if (!companySetUpDetails.isEmpty()) {
            try {
                for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {

                    mRealm.beginTransaction();
                    if (companySetUpDetails.get(i).getExchangeRate().equals("0")) {
                        etDollerRMBexcahange.setText("");

                    } else {
                        etDollerRMBexcahange.setText(companySetUpDetails.get(i).getExchangeRate());
                    }
                    ThirdCurrency = companySetUpDetails.get(i).getThirdCurrency();
                    ExchangeRate3dCurrency = companySetUpDetails.get(i).getExchangeRate3dCurrency();
                    activated3dCurrency = companySetUpDetails.get(i).getActive3dCurrency();
                    mRealm.commitTransaction();
                }
            } catch (Exception e) {
                mRealm.cancelTransaction();
            }
        } else {
            Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
        }
        if (AppUtils.isNetworkAvailable(getActivity())) {

            CurrencyNames(ThirdCurrency);
        } else {
            DisplayDialog(getResources().getString(R.string.error_no_internet));
        }
        if (activated3dCurrency.equals("0")) {
            isActivatedCurrency.setChecked(false);
            etDoller3rdCurrencyexchange.setText("");

        } else {
            isActivatedCurrency.setChecked(true);
            if (ExchangeRate3dCurrency.equals("0")) {
                etDoller3rdCurrencyexchange.setText("");
            } else {
                etDoller3rdCurrencyexchange.setText(ExchangeRate3dCurrency);
            }
            lnr_view3Dcurrency.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        fav = menu.add("Done");
        fav.setTitle(R.string.txt_save);
        fav.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("Save")) {

            try {
                sharedPref = PreferenceUtil.get(getActivity());
                String authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);
                String DollerRMBexcahange = etDollerRMBexcahange.getText().toString();
                String Doller3rdCurrencyexchange = etDoller3rdCurrencyexchange.getText().toString();

                if (AppUtils.isNetworkAvailable(getActivity())) {

                    if (isActivatedCurrency.isChecked()) {
                        if (etDoller3rdCurrencyexchange.getText().toString().trim().length() != 0 && etDollerRMBexcahange.getText().toString().trim().length() != 0) {
                            SaveExchangeRateSetUp(DollerRMBexcahange, Doller3rdCurrencyexchange, spinnerCurrencyNames.getSelectedItem().toString(), authToken, "1");
                        } else {
                            if (etDoller3rdCurrencyexchange.getText().toString().trim().length() == 0) {
                                etDoller3rdCurrencyexchange.setError(getResources().getString(R.string.txt_field_null));
                            }
                            if (etDollerRMBexcahange.getText().toString().trim().length() == 0) {
                                etDollerRMBexcahange.setError(getResources().getString(R.string.txt_field_null));
                            }
                        }
                    } else {
                        if (etDollerRMBexcahange.getText().toString().trim().length() != 0) {
                            SaveExchangeRateSetUp(DollerRMBexcahange, Doller3rdCurrencyexchange, "0", authToken, "0");
                        } else {
                            etDollerRMBexcahange.setError(getResources().getString(R.string.txt_field_null));

                        }
                    }

                } else {
                    DisplayDialog(getResources().getString(R.string.error_no_internet));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);

    }

    private void SaveExchangeRateSetUp(final String DollerRMBexcahange, final String Doller3rdCurrencyexchange, final String currency, String authToken, final String setCheked) {
        progressBar.setVisibility(View.VISIBLE);
        CompanySetUpExchangeRateService service = new CompanySetUpExchangeRateService(getActivity());
        CompanySetUpExchangeRequest request = new CompanySetUpExchangeRequest();
        request.setExchangeRate(DollerRMBexcahange);
        request.setActivate3dCurrency(setCheked);
        request.setThirdCurrency(currency);
        request.setExchangeRate3d(Doller3rdCurrencyexchange);
        request.setToken(authToken);
        service.post(request, new ApiResponse<CompanySetUpExchangeRateResponse>() {
            @Override
            public void onSuccess(ResultCode resultCode, CompanySetUpExchangeRateResponse response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                CompanySetUpDetails companySetUpDetails = mRealm.where(CompanySetUpDetails.class)
                     .equalTo("CompanyID", companyId).findFirst();

                if (companySetUpDetails != null) {
                    // Exists
                    mRealm.beginTransaction();
                    companySetUpDetails.setExchangeRate(DollerRMBexcahange);
                    companySetUpDetails.setThirdCurrency(currency);
                    companySetUpDetails.setExchangeRate3dCurrency(Doller3rdCurrencyexchange);
                    companySetUpDetails.setActive3dCurrency(setCheked);
                    mRealm.commitTransaction();
                }

                progressBar.setVisibility(View.GONE);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            }

            @Override
            public void onError(ResultCode resultCode, CompanySetUpExchangeRateResponse response) {
                progressBar.setVisibility(View.GONE);
                DisplayDialog(response.getMessage());
            }


        });
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("Currency.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public void CurrencyNames(String thirdCurrency) {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray zones_jArry = obj.getJSONArray("data");
            String[] curencyList = new String[zones_jArry.length()];
            for (int i = 0; i < zones_jArry.length(); i++) {
                JSONObject jo_inside = zones_jArry.getJSONObject(i);

                curencyList[i] = jo_inside.getString("CurrencyName");

            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, curencyList);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinnerCurrencyNames.setAdapter(spinnerArrayAdapter);
            if (thirdCurrency.trim().equals("")) {

            } else {
                for (int i = 0; i < curencyList.length; i++) {
                    if (thirdCurrency.equals(curencyList[i])) {
                        spinnerCurrencyNames.setSelection(i);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
