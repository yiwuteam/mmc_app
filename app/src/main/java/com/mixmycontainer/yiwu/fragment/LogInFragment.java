package com.mixmycontainer.yiwu.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.OrderListActivity;
import com.mixmycontainer.yiwu.activity.ResetPasswordActivity;
import com.mixmycontainer.yiwu.activity.SignUpActivity;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

/**
 * Created by ADMIN on 09-11-2016.
 */
public class LogInFragment extends BaseFragment implements View.OnClickListener {

    private ProgressBar progressBar;
    private ScrollView sv_Main;
    private Button btSignup, btLogin, btResetPassword;
    private EditText etUserName, etPassword;
    private boolean login_click = true;
    Boolean isPermissionGranted = false;
    private Button btn_ShowSettings;

    public static LogInFragment newInstsnce() {
        LogInFragment fragment = new LogInFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_login, container, false);
        initview(view);
        initLestener();
        sv_Main.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                etPassword.setError(null);
                etUserName.setError(null);
                return false;
            }
        });
        btResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(ResetPasswordActivity.getnewIntent(getActivity()));
                getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            }
        });
        return view;
    }

    private void initview(View view) {
        btLogin = (Button) view.findViewById(R.id.btLogin);
        btSignup = (Button) view.findViewById(R.id.btSignup);
        btResetPassword = (Button) view.findViewById(R.id.btResetPassword);
        etUserName = (EditText) view.findViewById(R.id.etUserName);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        sv_Main = (ScrollView) view.findViewById(R.id.sv_Main);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
    }

    private void initLestener() {
        btLogin.setOnClickListener(this);
        btSignup.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view == btSignup) {
            startActivity(SignUpActivity.getnewIntent(getActivity()));
            getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            getActivity().finish();
        } else if (view == btLogin) {

            if (checkPermission()) {
                if (login_click == true) {
                    View viewv = getActivity().getCurrentFocus();
                    if (viewv != null) {
                        InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                    }
                    validation();
                }
            } else {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        11);
            }

        }
    }


    public void validation() {
        if (!etUserName.getText().toString().trim().equals("") && etPassword.getText().toString().trim().length() != 0) {
            if (AppUtils.isNetworkAvailable(getActivity())) {
                if (isValidEmail(etUserName.getText().toString())) {
                    String password = etPassword.getText().toString();
                    password = password.replaceAll(" ", "%20");
                    password = password.replaceAll("#", "%23");

                    UserLogin(etUserName.getText().toString(), password);
                } else {
                    etUserName.setError(getResources().getString(R.string.txt_validatio_email));
                }
            } else {
                DisplayDialog(getResources().getString(R.string.error_no_internet));

            }
        } else {
            if (etUserName.getText().toString().trim().equals("")) {
                etUserName.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etPassword.getText().toString().trim().equals("")) {
                etPassword.setError(getResources().getString(R.string.txt_field_null));
            }
        }
    }


    private void UserLogin(final String Username, final String Password) {

        btLogin.setClickable(false);
        btResetPassword.setClickable(false);
        progressBar.setVisibility(View.VISIBLE);
        Ion.with(getActivity())
                .load(URL.Login + Password + "&username=" + Username)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override

                    public void onCompleted(Exception e, JsonObject result) {

                        try {
                            String requestCode = result.get("resultCode").getAsString();
                            if (requestCode.equals("1")) {
                                btLogin.setClickable(true);
                                btResetPassword.setClickable(true);
                                progressBar.setVisibility(View.INVISIBLE);
                                String token = result.get("token").getAsString();
                                JsonArray jsonArray = result.get("accountDeatils").getAsJsonArray();
                                if (jsonArray.size() == 0) {
                                } else {
                                    SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                                    for (int i = 0; i < jsonArray.size(); i++) {
                                        JsonObject jsonObject = jsonArray.get(i).getAsJsonObject();
                                        String image = jsonObject.get("Image").getAsString();
                                        PREF.putString(PreferenceUtil.USER_PIC, image);
                                        PREF.apply();
                                    }

                                }
                                SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                                PREF.putString(PreferenceUtil.USER_AUTH_TOKEN, token);
                                PREF.putString(PreferenceUtil.USER_PASSWORD, Password);
                                PREF.putString(PreferenceUtil.USER_NAME, Username);
                                PREF.apply();
                                startActivity(OrderListActivity.getnewInstance(getActivity()));
                                getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
                                getActivity().finish();
                            } else {
                                btLogin.setClickable(true);
                                btResetPassword.setClickable(true);
                                progressBar.setVisibility(View.INVISIBLE);
                                DisplayDialog(result.get("message").getAsString());
                            }
                        } catch (JsonParseException c) {

                            c.printStackTrace();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }

                    }

                });
    }

    public Boolean checkPermission() {

        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                // we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        11);
            }
            isPermissionGranted = false;
        } else {
            isPermissionGranted = true;

        }
        return isPermissionGranted;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch(requestCode)
        {
            case 11:
                checkPermission();
                return;
        }
    }
}
