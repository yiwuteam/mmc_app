package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.apis.CompanySetUpCartonService;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.ResultCode;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpCartonRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpCartonResponse;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 15-11-2016.
 */
public class AvarageCartonFragment extends BaseFragment {
    SharedPreferences sharedPref;
    EditText etGrosswait, etLegth, etWidth, etheight;
    ProgressBar progressBar;
    String authToken;
    int companyId;
    private MenuItem fav;
    private Realm mRealm;
    private String URL_CompanyDetails = com.mixmycontainer.yiwu.apis.core.URL.CompanyDetails;
    JsonObject companyDetails;


    public static AvarageCartonFragment newinstance() {
        AvarageCartonFragment fragment = new AvarageCartonFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;

    }

    @Override
    public int getTitle() {
        return R.string.txt_buyer_company;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_avaragecarton, container, false);

        mRealm = Realm.getDefaultInstance();

        sharedPref = PreferenceUtil.get(getActivity());
        authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);
        initview(view);
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }


    private void initview(View view) {
        etGrosswait = (EditText) view.findViewById(R.id.etGrosswait);
        etLegth = (EditText) view.findViewById(R.id.etLegth);
        etWidth = (EditText) view.findViewById(R.id.etWidth);
        etheight = (EditText) view.findViewById(R.id.etheight);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));

        if (AppUtils.isNetworkAvailable(getActivity())) {
            getCompanyDetails(authToken);


        }
        else {
            getCompanyDetailsOffline();
        }



    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        fav = menu.add("Done");
        fav.setTitle(R.string.txt_save);
        fav.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("Save")) {

            try {
                sharedPref = PreferenceUtil.get(getActivity());
                String authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);
                String grossWeight = etGrosswait.getText().toString();
                String width = etWidth.getText().toString();
                String height = etheight.getText().toString();
                String length = etLegth.getText().toString();
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    if (etGrosswait.getText().toString().trim().length() != 0 && etWidth.getText().toString().trim().length() != 0 && etheight.getText().toString().trim().length() != 0 && etLegth.getText().toString().trim().length() != 0) {
                        SaveCartonSetUp(grossWeight, width, height, length, authToken);
                    } else {
                        if (etGrosswait.getText().toString().trim().length() == 0) {
                            etGrosswait.setError(getResources().getString(R.string.txt_field_null));

                        }
                        if (etWidth.getText().toString().trim().length() == 0) {
                            etWidth.setError(getResources().getString(R.string.txt_field_null));
                        }
                        if (etheight.getText().toString().trim().length() == 0) {
                            etheight.setError(getResources().getString(R.string.txt_field_null));

                        }
                        if (etLegth.getText().toString().trim().length() == 0) {
                            etLegth.setError(getResources().getString(R.string.txt_field_null));

                        }
                    }
                } else {
                    DisplayDialog(getResources().getString(R.string.error_no_internet));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);

    }

    private void SaveCartonSetUp(final String grossWeight, final String width, final String height, final String length, String authToken) {
        progressBar.setVisibility(View.VISIBLE);
        CompanySetUpCartonService service = new CompanySetUpCartonService(getActivity());
        CompanySetUpCartonRequest request = new CompanySetUpCartonRequest();
        request.setGrossWeight(grossWeight);
        request.setWidth(width);
        request.setHeight(height);
        request.setLength(length);
        request.setToken(authToken);
        service.post(request, new ApiResponse<CompanySetUpCartonResponse>() {
            @Override
            public void onSuccess(ResultCode resultCode, CompanySetUpCartonResponse response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                CompanySetUpDetails companySetUpDetails = mRealm.where(CompanySetUpDetails.class)
                        .equalTo("CompanyID", companyId).findFirst();

                if (companySetUpDetails != null) {
                    // Exists
                    try {

                        mRealm.beginTransaction();
                        companySetUpDetails.setGrossweightCarton(grossWeight);
                        companySetUpDetails.setWidth(width);
                        companySetUpDetails.setHeight(height);
                        companySetUpDetails.setLength(length);
                        mRealm.commitTransaction();
                    } catch (Exception e) {
                        mRealm.cancelTransaction();
                    }
                }

                progressBar.setVisibility(View.GONE);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            }

            @Override
            public void onError(ResultCode resultCode, CompanySetUpCartonResponse response) {
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    progressBar.setVisibility(View.GONE);
                    DisplayDialog(response.getMessage());
                } else {
                    Toast.makeText(getActivity(), getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }


    private void getCompanyDetails(String authToken) {
        progressBar.setVisibility(View.VISIBLE);
        Ion.with(getActivity())
                .load(URL_CompanyDetails + "token=" + authToken)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            String requestCode = result.get("resultCode").getAsString();
                            if (requestCode.equals("1")) {


                                companyDetails = result.getAsJsonObject("companyDetails");


                                try {


                                    mRealm.getDefaultInstance();
                                    mRealm.executeTransaction(new Realm.Transaction() {
                                        @Override
                                        public void execute(Realm realm) {
                                            CompanySetUpDetails companySetUpDetails = mRealm.where(CompanySetUpDetails.class)
                                                    .equalTo("CompanyID", companyId).findFirst();

                                            companySetUpDetails.setGrossweightCarton(companyDetails.get("GrossweightCarton").getAsString());
                                            companySetUpDetails.setWidth(companyDetails.get("Width").getAsString());
                                            companySetUpDetails.setHeight(companyDetails.get("Height").getAsString());
                                            companySetUpDetails.setLength(companyDetails.get("Length").getAsString());
                                            mRealm.insertOrUpdate(companySetUpDetails);
                                        }
                                    });
                                }catch (Exception em)
                                {
                                    em.getMessage();
                                }
                                finally {
                                    if (mRealm!=null)
                                    {
                                        mRealm.close();
                                    }
                                }

                                getCompanyDetailsOffline();
                                progressBar.setVisibility(View.GONE);

                            }
//                            Toast.makeText(getActivity(), "DONE!", Toast.LENGTH_SHORT).show();

                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                });
    }

    private  void getCompanyDetailsOffline()
    {
        try {


            mRealm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                    RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
                    if (!companySetUpDetails.isEmpty()) {

                        for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {

                            etGrosswait.setText(companySetUpDetails.get(i).getGrossweightCarton());
                            etLegth.setText(companySetUpDetails.get(i).getLength());
                            etWidth.setText(companySetUpDetails.get(i).getWidth());
                            etheight.setText(companySetUpDetails.get(i).getHeight());
                            mRealm.commitTransaction();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();


                    }


                }
            });
        } catch (Exception e) {
            e.getMessage();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

}
