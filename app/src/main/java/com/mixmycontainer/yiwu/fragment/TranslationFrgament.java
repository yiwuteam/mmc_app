package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.apis.CompanySetUpTranslationService;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.ResultCode;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpTranslateRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpTranslateResponse;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 14-11-2016.
 */
public class TranslationFrgament extends BaseFragment {
    Spinner spinner_to, spinner_from;
    private MenuItem fav;
    SharedPreferences sharedPref;
    String[] translationfrom = {"English", "Chinese, Simplified"};
    ProgressBar progressBar;
    private Realm mRealm;
    private int companyId;
    String TransilateFrom, TransilateTo;

    public static TranslationFrgament newInstance() {
        TranslationFrgament frgament = new TranslationFrgament();
        Bundle arg = new Bundle();
        frgament.setArguments(arg);
        return frgament;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_translaction, container, false);

        mRealm =  Realm.getDefaultInstance();

        initview(view);
        initletener();

        return view;
    }

    private void TranslationFrom(String transilateFrom) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, translationfrom);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner_from.setAdapter(spinnerArrayAdapter);
        if (transilateFrom.trim().equals("")) {

        } else {
            for (int i = 0; i < translationfrom.length; i++) {
                if (transilateFrom.equals(translationfrom[i])) {
                    spinner_from.setSelection(i);
                }
            }
        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mRealm.close();
    }

    private void TranslationTo(String transilateTo) {
        progressBar.setVisibility(View.VISIBLE);
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, translationfrom);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinner_to.setAdapter(spinnerArrayAdapter);
        if (transilateTo.trim().equals("")) {

        } else {
            for (int i = 0; i < translationfrom.length; i++) {
                if (transilateTo.equals(translationfrom[i])) {
                    spinner_to.setSelection(i);
                }
            }
        }
        progressBar.setVisibility(View.GONE);

    }

    @Override
    public int getTitle() {
        return R.string.txt_buyer_company;
    }

    private void initletener() {

    }

    private void initview(View view) {
        spinner_to = (Spinner) view.findViewById(R.id.spinner_to);
        spinner_from = (Spinner) view.findViewById(R.id.spinner_from);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mRealm.beginTransaction();
        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));

        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
        if (!companySetUpDetails.isEmpty()) {
            try {
                for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {
                    TransilateFrom = companySetUpDetails.get(i).getTransilateFrom();
                    TransilateTo = companySetUpDetails.get(i).getTransilateTo();
                }
            } catch (Exception e) {
                mRealm.cancelTransaction();
            }
        } else {
            Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
        }
        mRealm.commitTransaction();
        if(TransilateFrom.equals("en")){
            TransilateFrom="English";
        }else if(TransilateFrom.equals("zh")){
            TransilateFrom="Chinese, Simplified";
        }

        if(TransilateTo.equals("en")){
            TransilateTo="English";
        }else if(TransilateTo.equals("zh")){
            TransilateTo="Chinese, Simplified";
        }
        TranslationFrom(TransilateFrom);
        TranslationTo(TransilateTo);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        fav = menu.add("Done");
        fav.setTitle(R.string.txt_save);
        fav.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("Save")) {

            try {
                sharedPref = PreferenceUtil.get(getActivity());
                String authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);
                String translateTo = spinner_to.getSelectedItem().toString();
                String translateFrom = spinner_from.getSelectedItem().toString();
                if(translateFrom.equals("English")){
                    translateFrom="en";
                }else {
                    translateFrom="zh";
                }
                if(translateTo.equals("English")){
                    translateTo="en";
                }else {
                    translateTo="zh";
                }
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    SaveCompanyTranslation(translateTo, translateFrom, authToken);
                } else {
                    DisplayDialog(getResources().getString(R.string.error_no_internet));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return super.onOptionsItemSelected(item);
    }

    private void SaveCompanyTranslation(final String translateTo, final String translateFrom, String authToken) {
        progressBar.setVisibility(View.VISIBLE);

        CompanySetUpTranslationService service = new CompanySetUpTranslationService(getActivity());
        CompanySetUpTranslateRequest request = new CompanySetUpTranslateRequest();
        request.setTranslateTo(translateTo);
        request.setTranslateFrom(translateFrom);

        request.setToken(authToken);
        service.post(request, new ApiResponse<CompanySetUpTranslateResponse>() {
            @Override
            public void onSuccess(ResultCode resultCode, CompanySetUpTranslateResponse response) {

                CompanySetUpDetails companySetUpDetails = mRealm.where(CompanySetUpDetails.class)
                        .equalTo("CompanyID", companyId).findFirst();

                if (companySetUpDetails != null) {
                    // Exists
                    mRealm.beginTransaction();
                    companySetUpDetails.setTransilateTo(translateTo);
                    companySetUpDetails.setTransilateFrom(translateFrom);
                    mRealm.commitTransaction();
                }

                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                progressBar.setVisibility(View.GONE);
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);
            }

            @Override
            public void onError(ResultCode resultCode, CompanySetUpTranslateResponse response) {
                progressBar.setVisibility(View.GONE);
                DisplayDialog(response.getMessage());
            }
        });
    }
}
