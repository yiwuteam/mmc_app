package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.apis.CompanySetUpAdressService;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.ResultCode;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpAdressRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpAdressResponse;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 14-11-2016.
 */
public class BuyerCompanyAddressFragment extends BaseFragment {
    private Spinner spinnerCountyNames;
    private EditText etBuyerCompany, etAddress, etZipcode, etPhone, etMobile;
    private RadioGroup radioAddressGroup;
    private RadioButton radioBtnPhysical, radioBtnPostal;
    private SharedPreferences sharedPref;
    private String[] countryNames;
    private ProgressBar progressBar;
    private int companyId;
    private MenuItem fav;
    private Realm mRealm;
    private String country="", addressType="";
    private EditText etCityTown;

    public static BuyerCompanyAddressFragment newInstance() {
        BuyerCompanyAddressFragment fragment = new BuyerCompanyAddressFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_buyercompanyaddress, container, false);

        mRealm =  Realm.getDefaultInstance();

        initent(view);

        return view;
    }

    @Override
    public int getTitle() {
        return R.string.txt_buyer_company;
    }

    private void initent(View view) {

        spinnerCountyNames = (Spinner) view.findViewById(R.id.spinnerCountyNames);
        radioAddressGroup = (RadioGroup) view.findViewById(R.id.radioAddressGroup);
        etCityTown = (EditText)view.findViewById(R.id.etCity) ;
        etBuyerCompany = (EditText) view.findViewById(R.id.etBuyerCompany);
        etAddress = (EditText) view.findViewById(R.id.etAddress);
        etZipcode = (EditText) view.findViewById(R.id.etZipcode);
        etPhone = (EditText) view.findViewById(R.id.etPhone);
        etMobile = (EditText) view.findViewById(R.id.etMobile);
        radioBtnPostal = (RadioButton) view.findViewById(R.id.radioBtnPostal);
        radioBtnPhysical = (RadioButton) view.findViewById(R.id.radioBtnPhysical);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));

        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
        if (companySetUpDetails.isEmpty()) {
            Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();

        } else {
            try {
                for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {

                    mRealm.beginTransaction();
                    etAddress.setText(companySetUpDetails.get(i).getAddress());
                    if (companySetUpDetails.get(i).getZipCode().equals("0")) {
                        etZipcode.setText("");
                    } else {
                        etZipcode.setText(companySetUpDetails.get(i).getZipCode());
                    }
                    etPhone.setText(companySetUpDetails.get(i).getPhone());
                    etMobile.setText(companySetUpDetails.get(i).getMobile());
                    etCityTown.setText(companySetUpDetails.get(i).getCityTown());
                    etBuyerCompany.setText(companySetUpDetails.get(i).getCompanyName());
                    country = companySetUpDetails.get(i).getCountry();
                    addressType = companySetUpDetails.get(i).getAddressType();
                    mRealm.commitTransaction();
                }
            } catch (Exception e) {
                mRealm.cancelTransaction();
            }
        }

        if (AppUtils.isNetworkAvailable(getActivity())) {
            getCountryNames(country);
        } else {
            DisplayDialog(getResources().getString(R.string.error_no_internet));
        }
        if ((addressType).equals("physical address")) {
            radioBtnPhysical.setChecked(true);
        } else {
            radioBtnPostal.setChecked(true);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        fav = menu.add("Done");
        fav.setTitle(R.string.txt_save);
        fav.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("Save")) {
            if (AppUtils.isNetworkAvailable(getActivity())) {
                int index = radioAddressGroup.indexOfChild(radioAddressGroup.findViewById(radioAddressGroup.getCheckedRadioButtonId()));
                if (radioAddressGroup.getCheckedRadioButtonId() == -1) {

                } else {
                    if (index == 0) {
                        addressType = "postal address";
                    } else {
                        addressType = "physical address";
                    }
                }
                try {
                    sharedPref = PreferenceUtil.get(getActivity());
                    String authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);
                    String companyName = etBuyerCompany.getText().toString();
                    String companyAddress = etAddress.getText().toString();
                    String zipCode = etZipcode.getText().toString();
                    String phone = etPhone.getText().toString();
                    String mobile = etMobile.getText().toString();
                    String country = spinnerCountyNames.getSelectedItem().toString();
                    String townCity = etCityTown.getText().toString().trim();
                    if (AppUtils.isNetworkAvailable(getActivity())) {
                        if (etBuyerCompany.getText().toString().trim().length() != 0) {
                            SaveCompanyAdress(companyAddress, country, addressType, companyName, zipCode, phone, mobile,townCity, authToken);
                        } else {
                            if (etBuyerCompany.getText().toString().trim().length() == 0) {
                                etBuyerCompany.setError(getResources().getString(R.string.txt_field_null));
                            }

                        }
                    } else {
                        DisplayDialog(getResources().getString(R.string.error_no_internet));
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                DisplayDialog(getResources().getString(R.string.error_no_internet));
            }

        }

        return super.onOptionsItemSelected(item);

    }

    private void SaveCompanyAdress(final String companyAddress, final String country, final String addressType, final String companyName, final String zipCode, final String phone, final String mobile,final String townCity, String authToken) {
        progressBar.setVisibility(View.VISIBLE);
        CompanySetUpAdressService service = new CompanySetUpAdressService(getActivity());
        CompanySetUpAdressRequest request = new CompanySetUpAdressRequest();
        request.setAddressType(addressType);
        request.setAddress(companyAddress);
        request.setMobile(mobile);
        request.setCompanyname(companyName);
        request.setPhone(phone);
        request.setZipCode(zipCode);
        request.setCountry(country);
        request.setTownCity(townCity);
        request.setToken(authToken);
        service.post(request, new ApiResponse<CompanySetUpAdressResponse>() {
            @Override
            public void onSuccess(ResultCode resultCode, CompanySetUpAdressResponse response) {
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();

                CompanySetUpDetails companySetUpDetails = mRealm.where(CompanySetUpDetails.class)
                     .equalTo("CompanyID", companyId).findFirst();

                if (companySetUpDetails != null) {
                    // Exists
                    try {

                        mRealm.beginTransaction();
                        companySetUpDetails.setCompanyName(companyName);
                        companySetUpDetails.setCountry(country);
                        companySetUpDetails.setAddressType(addressType);
                        companySetUpDetails.setAddress(companyAddress);
                        companySetUpDetails.setZipCode(zipCode);
                        companySetUpDetails.setPhone(phone);
                        companySetUpDetails.setMobile(mobile);
                        companySetUpDetails.setCityTown(townCity);
                        mRealm.commitTransaction();
                    } catch (Exception e) {
                        mRealm.cancelTransaction();
                    }
                }

                progressBar.setVisibility(View.GONE);
                getActivity().finish();

                getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            }

            @Override
            public void onError(ResultCode resultCode, CompanySetUpAdressResponse response) {
                progressBar.setVisibility(View.GONE);
                DisplayDialog(response.getMessage());
            }


        });
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("Country.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }


    public void getCountryNames(String selectedCountry) {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray zones_jArry = obj.getJSONArray("data");
            countryNames = new String[zones_jArry.length()];
            for (int i = 0; i < zones_jArry.length(); i++) {
                JSONObject jo_inside = zones_jArry.getJSONObject(i);

                countryNames[i] = jo_inside.getString("CountryName");

            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, countryNames);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinnerCountyNames.setAdapter(spinnerArrayAdapter);

            if (selectedCountry.trim().equals("")) {

            } else {
                for (int i = 0; i < countryNames.length; i++) {
                    if (selectedCountry.equals(countryNames[i])) {
                        spinnerCountyNames.setSelection(i);
                    }
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
