package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.apis.ChangePasswordService;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.ResultCode;
import com.mixmycontainer.yiwu.apis.requests.ChangePasswordRequest;
import com.mixmycontainer.yiwu.apis.responses.ChangePasswordResponse;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

/**
 * Created by ADMIN on 14-11-2016.
 */
public class ChangePasswordFragment extends BaseFragment {

    SharedPreferences sharedPref;
    ProgressBar progressBar;
    private MenuItem menuItem;
    private EditText etCurrentPasswword, etNewPassword, etConfirmPassword;

    public static ChangePasswordFragment newInstsnce() {
        ChangePasswordFragment fragment = new ChangePasswordFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_changepassword, container, false);
        initview(view);

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuItem = menu.add("action");
        menuItem.setTitle(getResources().getString(R.string.txt_submit));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    }

    @Override
    public int getTitle() {
        return R.string.txt_Change_password;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getResources().getString(R.string.txt_submit))) {
            if (AppUtils.isNetworkAvailable(getActivity())) {
                validation();
            } else {
                DisplayDialog(getResources().getString(R.string.error_no_internet));
            }
        }

        return super.onOptionsItemSelected(item);
    }


    private void initview(View view) {

        etCurrentPasswword = (EditText) view.findViewById(R.id.etCurrentPasswword);
        etNewPassword = (EditText) view.findViewById(R.id.etNewPassword);
        etConfirmPassword = (EditText) view.findViewById(R.id.etConfirmPassword);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);


    }

    private void ChangePassword(String currentPassword, final String newPassword, String authToken) {
        progressBar.setVisibility(View.VISIBLE);
        ChangePasswordService service = new ChangePasswordService(getActivity());
        ChangePasswordRequest request = new ChangePasswordRequest();
        request.setCurrentPassword(currentPassword);
        request.setNewPassword(newPassword);
        request.setToken(authToken);
        service.post(request, new ApiResponse<ChangePasswordResponse>() {

            @Override
            public void onSuccess(ResultCode resultCode, ChangePasswordResponse response) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                PREF.putString(PreferenceUtil.USER_PASSWORD, newPassword);
                PREF.apply();
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.scale_center_in, R.anim.scale_center_out);
            }

            @Override
            public void onError(ResultCode resultCode, ChangePasswordResponse response) {
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void validation() {

        if (etCurrentPasswword.getText().toString().trim().equals("") || etNewPassword.getText().toString().trim().length() < 6 || etNewPassword.getText().toString().trim().equals("") || etConfirmPassword.getText().toString().trim().equals("")) {
            if (etCurrentPasswword.getText().toString().trim().equals("")) {
                etCurrentPasswword.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etNewPassword.getText().toString().trim().equals("")) {
                etNewPassword.setError(getResources().getString(R.string.txt_field_null));
            } else {
                etNewPassword.setError("Must have minimum 6 characters.");
            }
            if (etConfirmPassword.getText().toString().trim().equals("")) {
                etConfirmPassword.setError(getResources().getString(R.string.txt_field_null));
            }
        } else {
            sharedPref = PreferenceUtil.get(getActivity());
            String authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);
            String password = sharedPref.getString(PreferenceUtil.USER_PASSWORD, "0");
            if (etCurrentPasswword.getText().toString().equals(password)) {
                if (etConfirmPassword.getText().toString().equals(etNewPassword.getText().toString())) {

                    ChangePassword(etCurrentPasswword.getText().toString(), etNewPassword.getText().toString(), authToken);

                } else {
                    etNewPassword.setError("Password does not match");
                }
            } else {
                etCurrentPasswword.setError("Incorrect current password");
            }

        }

    }
}
