package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.Boothdetailactivity;
import com.mixmycontainer.yiwu.adapter.SingleOrderAdapter;
import com.mixmycontainer.yiwu.apis.models.OrderListItemsModel;
import com.mixmycontainer.yiwu.bus.events.RefreshSingleViewOrder;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.models.ContainerDetails;
import com.mixmycontainer.yiwu.models.ContainerInfo;
import com.mixmycontainer.yiwu.models.OrderListDetails;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;
import com.squareup.otto.Subscribe;

import java.util.ArrayList;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by minusbug on 12/04/2016.
 */
public class

ViewSingleOrderDetailsFragment extends BaseFragment {
    public static String ARG_EXTRA = "ARG_EXTRA";
    SharedPreferences sharedPref;
    ProgressBar progressBar;
    String containerName = "";
    OrderListItemsModel orderListItemsModel;
    OrderListItemsModel orderListItemsModelForItems;
    List<OrderListItemsModel> orderListItemsModels;
    SingleOrderAdapter singleOrderAdapter;
    Realm mRealm;
    private String OrderStatus;
    private MenuItem fav;
    private LinearLayoutManager mlayoutManager;
    private RecyclerView rvOrderlist;
    private TextView tvTotalContainers, tvOrderValue, tvTotalCbmInOrder, tvTotalWeight, tvOrderDate, tvOrderNumber;
    private LinearLayout lnr_OrderView;
    String orderId;

    public static ViewSingleOrderDetailsFragment newInstence(String orderStatuss) {
        ViewSingleOrderDetailsFragment fragment = new ViewSingleOrderDetailsFragment();
        Bundle arg = new Bundle();
        arg.putString(ARG_EXTRA, orderStatuss);
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_view_single_order_details, container, false);
        initview(view);
        sharedPref = PreferenceUtil.get(getActivity());
        mRealm = Realm.getDefaultInstance();


        String authToken = sharedPref.getString(PreferenceUtil.USER_AUTH_TOKEN, null);
        orderId = sharedPref.getString(PreferenceUtil.ORDER_ID, "0");
        System.out.println("Auth :"+authToken);
        getSingleOrderDetailsOffline(orderId);
//        if (AppUtils.isNetworkAvailable(getActivity())) {
//            getSingleOrderDetails(orderId, authToken);
//        } else {
//            DisplayDialog(getResources().getString(R.string.error_no_internet));
//        }

        return view;
    }

    private void getSingleOrderDetailsOffline(String orderId) {
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
                    String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
                    RealmResults<OrderListDetails> OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();

                    if (!OrderListDetailsDetails.isEmpty()) {
                        rvOrderlist.setVisibility(View.VISIBLE);
                        lnr_OrderView.setVisibility(View.VISIBLE);
                        int totalCount = 0;
                        String orderIdToAdapter = null;
                        for (int k = 0; k < OrderListDetailsDetails.size(); k++) {
                            orderIdToAdapter = OrderListDetailsDetails.get(k).getOrderId();
                            tvOrderNumber.setText(OrderListDetailsDetails.get(k).getOrderNumber());
                            tvOrderDate.setText(OrderListDetailsDetails.get(k).getLastUpdate());
                            tvTotalWeight.setText(OrderListDetailsDetails.get(k).getTotalOrderWeight());
                            tvTotalCbmInOrder.setText(OrderListDetailsDetails.get(k).getTotalOrderCBM());

                            tvOrderValue.setText("US$" + OrderListDetailsDetails.get(k).getTotalOrderUsValue() + " (RMB¥" + OrderListDetailsDetails.get(k).getTotalOrderRmbValue() + ")");
                            String cartonsUsed = "";
                            for (int b = 0; b < OrderListDetailsDetails.get(k).getContainerDetailses().size(); b++) {
                                String cartonName = OrderListDetailsDetails.get(k).getContainerDetailses().get(b).getContainerName();
                                String container;
                                if (cartonName.matches("\\d+")) {
                                    container = getContainerDetailsOffline(cartonName);
                                } else {
                                    container = cartonName;
                                }

                                cartonsUsed = cartonsUsed + "," + container;

                            }
                            tvTotalContainers.setText(cartonsUsed.substring(1));
//                            for (int j = 0; j < OrderListDetailsDetails.get(k).getBoothDetailses().size(); j++) {
//                                totalCount = totalCount + OrderListDetailsDetails.get(k).getBoothDetailses().get(j).getItemDetailses().size();
//
//                            }

                            orderListItemsModels = new ArrayList<OrderListItemsModel>();
//                    for (int i = 0; i < totalCount; i++) {
                            orderListItemsModel = new OrderListItemsModel();

                                for (int b = 0; b < OrderListDetailsDetails.get(k).getItemDetailses().size(); b++) {
                                    orderListItemsModelForItems = new OrderListItemsModel();
                                    orderListItemsModelForItems.setTotalPriceInUS(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getTotalPriceInUS());
                                    orderListItemsModelForItems.setTotalPriceInRMB(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getTotalPriceInRMB());

                                    orderListItemsModelForItems.setItemNo(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getItemNo());
                                    String boothId = OrderListDetailsDetails.get(k).getItemDetailses().get(b).getBoothId();
                                    orderListItemsModelForItems.setBoothId(boothId);

                                    for (int j = 0; j < OrderListDetailsDetails.get(k).getBoothDetailses().size(); j++) {
                                        if (OrderListDetailsDetails.get(k).getBoothDetailses().get(j).getBoothId().equals(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getBoothId())) {
                                            orderListItemsModel.setBoothNumber(OrderListDetailsDetails.get(k).getBoothDetailses().get(j).getBoothNumber());
                                            orderListItemsModel.setTotalPriceInUS(OrderListDetailsDetails.get(k).getBoothDetailses().get(j).getValuePurchase());
                                            orderListItemsModel.setTotalPriceInRMB(OrderListDetailsDetails.get(k).getBoothDetailses().get(j).getValuePurchaseRMB());
                                            orderListItemsModel.setBoothId(OrderListDetailsDetails.get(k).getBoothDetailses().get(j).getBoothId());
                                        }
                                    }
                                    orderListItemsModelForItems.setBoothNumber(orderListItemsModel.getBoothNumber());


                                    String itemID = OrderListDetailsDetails.get(k).getItemDetailses().get(b).getItemId();
                                    orderListItemsModelForItems.setItemId(itemID);
                                    orderListItemsModelForItems.setDescriptionFrom(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getDescriptionFrom());
                                    orderListItemsModelForItems.setDescriptionTo(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getDescriptionTo());
                                    orderListItemsModelForItems.setPicture("https://mixmycontainer.com/QA/assets/testimages/"+OrderListDetailsDetails.get(k).getItemDetailses().get(b).getPicture());

                                    String[] containerIdArray = OrderListDetailsDetails.get(k).getItemDetailses().get(b).getContainerId().split(",");
                                    String originalListContainers = "";
                                    int containerArraySize = containerIdArray.length;
                                    for (int i = 0; i < containerArraySize; i++) {
                                        String container = getContainerDetailsOffline(containerIdArray[i], prefOrderId, companyId);

                                        originalListContainers = originalListContainers + "," + container;
                                    }
//
                                    orderListItemsModelForItems.setContainerType(originalListContainers.substring(1));
                                    orderListItemsModelForItems.setUnitQuantity(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getUnitQuantity());
                                    orderListItemsModelForItems.setCartonQuantity(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getCartonQuantity());
                                    orderListItemsModelForItems.setPriceInRmb(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getPriceInRmb());
                                    orderListItemsModelForItems.setPriceInUs(OrderListDetailsDetails.get(k).getItemDetailses().get(b).getPriceInUs());
                                    orderListItemsModels.add(orderListItemsModelForItems);
                                }



                        }

                        rvOrderlist.setHasFixedSize(true);
                        mlayoutManager = new LinearLayoutManager(getActivity());
                        rvOrderlist.setLayoutManager(mlayoutManager);
                        singleOrderAdapter = new SingleOrderAdapter(getActivity(), orderListItemsModels, orderIdToAdapter);
                        rvOrderlist.setAdapter(singleOrderAdapter);

                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }
        mRealm = Realm.getDefaultInstance();


    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        OrderStatus = getArguments().getString(ARG_EXTRA);
        if (OrderStatus.equals("0")) {
            setHasOptionsMenu(true);
        }
    }

    private String getContainerDetailsOffline(final String containerType) {


        int prefCompanyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", prefCompanyId).findAll();
        for (int i = 0; i < companySetUpDetails.size(); i++) {

            RealmResults<ContainerInfo> containerResults = companySetUpDetails.get(i).getContainerDetails().where().equalTo("ID", containerType).findAll();
            if (containerResults.size() > 0) {
                containerName = containerResults.get(0).getContainer();

            }
        }


        return containerName;

    }


    private String getContainerDetailsOffline(final String containerId, final String prefOrderId, final int companyId) {


        RealmResults<OrderListDetails> OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();
        String name = "";

        for (int i = 0; i < OrderListDetailsDetails.size(); i++) {

            RealmList<ContainerDetails> containerDetails = OrderListDetailsDetails.get(i).getContainerDetailses();
            for (int j = 0; j < containerDetails.size(); j++) {
                String id = containerDetails.get(j).getContainerId();
                if (id.equals(containerId)) {
                    name = containerDetails.get(j).getContainerName();
                }

            }


        }


        return name;

    }


    private void initview(View view) {
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        rvOrderlist = (RecyclerView) view.findViewById(R.id.rvOrderlist);
        lnr_OrderView = (LinearLayout) view.findViewById(R.id.lnr_OrderView);
        tvOrderNumber = (TextView) view.findViewById(R.id.tvOrderNumber);
        tvOrderDate = (TextView) view.findViewById(R.id.tvOrderDate);
        tvTotalWeight = (TextView) view.findViewById(R.id.tvTotalWeight);
        tvTotalCbmInOrder = (TextView) view.findViewById(R.id.tvTotalCbmInOrder);
        tvOrderValue = (TextView) view.findViewById(R.id.tvOrderValue);
        tvTotalContainers = (TextView) view.findViewById(R.id.tvTotalContainers);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        fav = menu.add("Done");
        fav.setTitle("ADD");
        fav.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public int getTitle() {
        return R.string.txt_order_details;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("ADD")) {
            SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
            PREF.putString(PreferenceUtil.ORDER_ADD, "continueOrder");
            PREF.putString(PreferenceUtil.ORDER_ID, orderId);
            String containerId = getlastFilledOpenContainer(orderId);
            PREF.putString(PreferenceUtil.CONTAINER_ID, containerId);
            PREF.putString(PreferenceUtil.EDIT_ITEM_ID,"0");
            PREF.putString(PreferenceUtil.EDIT_BOOTH_ID,"0");

            PREF.apply();
            startActivity(Boothdetailactivity.getnewIntent(getActivity(), "add"));
            getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        }
        return super.onOptionsItemSelected(item);
    }

    private String getlastFilledOpenContainer(final String orderId) {
        final String[] containerId = new String[1];
       final int compId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", orderId).equalTo("CompanyID", compId).findAll();
                    for (int i=0;i<orderListDetails.size();i++)
                    {
                    RealmList<ContainerDetails> containerDetails = orderListDetails.get(i).getContainerDetailses();
                        for (int j = 0 ; j < containerDetails.size(); j ++)
                        {
                                containerId[0] = containerDetails.get(j).getContainerId();
                        }
                    }

                }
            });
        } catch (Exception e) {

        }
        return containerId[0];
    }

//    private void getSingleOrderDetails(String orderid, String authToken) {
//
//        progressBar.setVisibility(View.VISIBLE);
//
//        Ion.with(getActivity())
//             .load(URL.ViewOrderItems + authToken + "&orderid=" + orderid)
//             .asJsonObject()
//             .setCallback(new FutureCallback<JsonObject>() {
//                 @Override
//                 public void onCompleted(Exception e, JsonObject result) {
//                     try {
//
//                         String requestCode = result.get("resultCode").toString();
//                         progressBar.setVisibility(View.GONE);
//                         rvOrderlist.setVisibility(View.VISIBLE);
//                         lnr_OrderView.setVisibility(View.VISIBLE);
//                         if (requestCode.equals("1")) {
//
//                             JsonArray orderData = result.getAsJsonArray("data");
//                             JsonArray orderDeatils = result.getAsJsonArray("orderDeatils");
//                             JsonObject jsonObject, jsonObjectOrderDeatils;
//                             for (int k = 0; k < orderDeatils.size(); k++) {
//                                 jsonObjectOrderDeatils = orderDeatils.get(k).getAsJsonObject();
//                                 tvOrderNumber.setText(jsonObjectOrderDeatils.get("OrderNumber").getAsString());
//                                 tvOrderDate.setText(jsonObjectOrderDeatils.get("LastUpdate").getAsString());
//                                 tvTotalWeight.setText(jsonObjectOrderDeatils.get("TotalOrderWeight").getAsString());
//                                 tvTotalCbmInOrder.setText(jsonObjectOrderDeatils.get("TotalOrderCBM").getAsString());
//                                 tvTotalContainers.setText(jsonObjectOrderDeatils.get("Containers").getAsString());
//                                 tvOrderValue.setText("US$" + jsonObjectOrderDeatils.get("TotalOrderUsValue").getAsString() + " (RMB¥" + jsonObjectOrderDeatils.get("TotalOrderRmbValue").getAsString() + ")");
//
//                             }
//                             orderListItemsModels = new ArrayList<OrderListItemsModel>();
//                             for (int i = 0; i < orderData.size(); i++) {
//                                 jsonObject = orderData.get(i).getAsJsonObject();
//                                 orderListItemsModel = new OrderListItemsModel();
//
//                                 orderListItemsModel.setItemId(jsonObject.get("ItemId").getAsString());
//                                 orderListItemsModel.setCompanyId(jsonObject.get("CompanyId").getAsString());
//                                 orderListItemsModel.setOrderId(jsonObject.get("OrderId").getAsString());
//                                 orderListItemsModel.setBoothId(jsonObject.get("BoothId").getAsString());
//                                 orderListItemsModel.setContainerId(jsonObject.get("ContainerId").getAsString());
//
//                                 orderListItemsModel.setItemNo(jsonObject.get("ItemNo").getAsString());
//                                 orderListItemsModel.setDescriptionFrom(jsonObject.get("DescriptionFrom").getAsString());
//                                 orderListItemsModel.setWeightofCarton(jsonObject.get("WeightofCarton").getAsString());
//                                 orderListItemsModel.setLength(jsonObject.get("Length").getAsString());
//                                 orderListItemsModel.setWidth(jsonObject.get("Width").getAsString());
//
//                                 orderListItemsModel.setHeight(jsonObject.get("Height").getAsString());
//                                 orderListItemsModel.setCBM(jsonObject.get("CBM").getAsString());
//                                 orderListItemsModel.setPicture(jsonObject.get("Picture").getAsString());
//                                 orderListItemsModel.setPriceInUs(jsonObject.get("PriceInUs").getAsString());
//                                 orderListItemsModel.setPriceInRmb(jsonObject.get("PriceInRmb").getAsString());
//
//                                 orderListItemsModel.setThirdCurrencyType(jsonObject.get("ThirdCurrencyType").getAsString());
//                                 orderListItemsModel.setPriceInThirdCurrency(jsonObject.get("PriceInThirdCurrency").getAsString());
//                                 orderListItemsModel.setUnitQuantity(jsonObject.get("UnitQuantity").getAsString());
//                                 orderListItemsModel.setCartonQuantity(jsonObject.get("CartonQuantity").getAsString());
//                                 orderListItemsModel.setTotalPriceInUS(jsonObject.get("TotalPriceInUS").getAsString());
//
//                                 orderListItemsModel.setTotalPriceInRMB(jsonObject.get("TotalPriceInRMB").getAsString());
//                                 orderListItemsModel.setTotalPriceIn3rdCurrency(jsonObject.get("TotalPriceIn3rdCurrency").getAsString());
//                                 orderListItemsModel.setDate(jsonObject.get("Date").getAsString());
//
//                                 orderListItemsModel.setBoothNumber(jsonObject.get("BoothNumber").getAsString());
//
//                                 orderListItemsModel.setBoothName(jsonObject.get("BoothName").getAsString());
//                                 orderListItemsModel.setPhone(jsonObject.get("Phone").getAsString());
//                                 orderListItemsModel.setBusinessScope(jsonObject.get("BusinessScope").getAsString());
//                                 orderListItemsModel.setValuePurchase(jsonObject.get("ValuePurchase").getAsString());
//                                 orderListItemsModel.setValuePurchaseRMB(jsonObject.get("ValuePurchaseRMB").getAsString());
//
//                                 orderListItemsModel.setBusinessCard(jsonObject.get("BusinessCard").getAsString());
//                                 orderListItemsModel.setBoothStatus(jsonObject.get("BoothStatus").getAsString());
//
//
//                                 orderListItemsModels.add(orderListItemsModel);
//
//                             }
//
//                             rvOrderlist.setHasFixedSize(true);
//                             mlayoutManager = new LinearLayoutManager(getActivity());
//                             rvOrderlist.setLayoutManager(mlayoutManager);
//                             singleOrderAdapter = new SingleOrderAdapter(getActivity(), orderListItemsModels);
//                             rvOrderlist.setAdapter(singleOrderAdapter);
//                         } else {
//
//                             DisplayDialog("No Items Added");
//
//                         }
//                     } catch (JsonParseException c) {
//
//                         c.printStackTrace();
//                     } catch (Exception e1) {
//
//                         e1.printStackTrace();
//                     }
//
//                 }
//             });
//    }

    @Subscribe
    public void NavigateValue(RefreshSingleViewOrder event) {
        sharedPref = PreferenceUtil.get(getActivity());

        Toast.makeText(getActivity(), "Order refresh", Toast.LENGTH_SHORT).show();
        orderId = sharedPref.getString(PreferenceUtil.ORDER_ID, "0");
        getSingleOrderDetailsOffline(orderId);

    }
}
