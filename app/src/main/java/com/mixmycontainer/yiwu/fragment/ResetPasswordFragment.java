package com.mixmycontainer.yiwu.fragment;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.LogInActivity;
import com.mixmycontainer.yiwu.activity.OtpConfirmActivity;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

/**
 * Created by minusbug on 12/28/2016.
 */

public class ResetPasswordFragment extends BaseFragment {

    private EditText etEmail;
    private Button btLogin, btResetPassword;
    private ProgressBar progressBar;

    public static ResetPasswordFragment newInstnce() {
        ResetPasswordFragment fragment = new ResetPasswordFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_reset_password, container, false);
        initView(view);

        btResetPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View viewv = getActivity().getCurrentFocus();
                if (viewv != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                String email = etEmail.getText().toString();
                if (etEmail.getText().toString().trim().length() != 0) {
                    if (isValidEmail(email)) {
                        if (AppUtils.isNetworkAvailable(getActivity())) {
                            ResetPassword((etEmail.getText().toString()));
                        } else {
                            Toast.makeText(getActivity(), getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        etEmail.setError("Invalid Email Id");
                    }

                } else {
                    if (etEmail.getText().toString().trim().length() == 0) {
                        etEmail.setError("Field can't be blank");
                    }

                }

            }
        });
        btLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(LogInActivity.getnewIntent(getActivity()));
                getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
                getActivity().finish();
            }
        });

        return view;
    }

    public void initView(View view) {
        btLogin = (Button) view.findViewById(R.id.btLogin);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        btResetPassword = (Button) view.findViewById(R.id.btResetPassword);

    }

    private void ResetPassword(final String Email) {
        btLogin.setClickable(false);
        btResetPassword.setClickable(false);
        progressBar.setVisibility(View.VISIBLE);

        Ion.with(getActivity())
             .load(URL.ForgotPassword + Email)
             .asJsonObject()
             .setCallback(new FutureCallback<JsonObject>() {
                 @Override

                 public void onCompleted(Exception e, JsonObject result) {

                     try {
                         String requestCode = result.get("resultCode").getAsString();
                         if (requestCode.equals("1")) {
                             btLogin.setClickable(true);
                             btResetPassword.setClickable(true);
                             progressBar.setVisibility(View.INVISIBLE);
                             Toast.makeText(getActivity(), "OTP code sent to your Email id", Toast.LENGTH_SHORT).show();
                             SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                             PREF.putString(PreferenceUtil.USER_OTP, result.get("otpCode").getAsString());
                             PREF.putString(PreferenceUtil.USER_OTP_email, Email);
                             PREF.apply();
                             startActivity(OtpConfirmActivity.getnewIntent(getActivity()));
                             getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
                             getActivity().finish();
                         } else {
                             btLogin.setClickable(true);
                             btResetPassword.setClickable(true);
                             progressBar.setVisibility(View.INVISIBLE);
                             DisplayDialog(result.get("message").getAsString());
                         }

                     } catch (JsonParseException c) {

                         c.printStackTrace();
                     } catch (Exception e1) {

                         e1.printStackTrace();
                     }
                 }

             });
    }
}
