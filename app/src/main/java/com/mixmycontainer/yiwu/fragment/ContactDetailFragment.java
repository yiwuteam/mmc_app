package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.GoldAgentActivity;
import com.mixmycontainer.yiwu.apis.CompanySetUpContactService;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.ResultCode;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpContactRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpContactResponse;
import com.mixmycontainer.yiwu.bus.events.GoldAgentEvent;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;
import com.squareup.otto.Subscribe;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 14-11-2016.
 */
public class ContactDetailFragment extends BaseFragment {
    private EditText etFax, etSkypeAddress, etMSID, etQQID, etorderemailid, etAgentemailid;
    private MenuItem fav;
    private ProgressBar progressBar;
    private Realm mRealm;
    private int companyId;
    private Button btAgentEmail;
    private String idAgent, nameAgent, agentEmailid, goldAgentName, goldAgentId;
    private LinearLayout lnr_EditGoldAgent, lnr_ViewOnlyGoldAgent;
    private TextView tvAddAgent;
    private ImageView imgRemoveAgent;

    public static ContactDetailFragment newInstance() {
        ContactDetailFragment fragment = new ContactDetailFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }


    @Override
    public int getTitle() {
        return R.string.txt_buyer_company;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contactdetail, container, false);

        mRealm =  Realm.getDefaultInstance();

        initview(view);
        initlestener();
        btAgentEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(GoldAgentActivity.getnewIntent(getActivity()));
                getActivity().overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);
            }
        });

        imgRemoveAgent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                lnr_EditGoldAgent.setVisibility(View.VISIBLE);
                lnr_ViewOnlyGoldAgent.setVisibility(View.GONE);
                SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                PREF.putString(PreferenceUtil.USER_AGENT_ID, "0");
                PREF.putString(PreferenceUtil.USER_AGENT_NAME, "");
                PREF.apply();
            }
        });
        return view;
    }

    private void initlestener() {

    }

    private void initview(View view) {
        etFax = (EditText) view.findViewById(R.id.etFax);
        etSkypeAddress = (EditText) view.findViewById(R.id.etSkypeAddress);
        etMSID = (EditText) view.findViewById(R.id.etMSID);
        etQQID = (EditText) view.findViewById(R.id.etQQID);
        etorderemailid = (EditText) view.findViewById(R.id.etorderemailid);
        etAgentemailid = (EditText) view.findViewById(R.id.etAgentemailid);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        btAgentEmail = (Button) view.findViewById(R.id.btAgentEmail);
        lnr_ViewOnlyGoldAgent = (LinearLayout) view.findViewById(R.id.lnr_ViewOnlyGoldAgent);
        lnr_EditGoldAgent = (LinearLayout) view.findViewById(R.id.lnr_EditGoldAgent);
        tvAddAgent = (TextView) view.findViewById(R.id.tvAddAgent);
        imgRemoveAgent = (ImageView) view.findViewById(R.id.imgRemoveAgent);
        mRealm.beginTransaction();
        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));

        RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
        if (!companySetUpDetails.isEmpty()) {
            for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {
                try {
                    etFax.setText(companySetUpDetails.get(i).getFax());
                    etSkypeAddress.setText(companySetUpDetails.get(i).getSkype());
                    etMSID.setText(companySetUpDetails.get(i).getMsnID());
                    etQQID.setText(companySetUpDetails.get(i).getQqID());
                    System.out.println(companySetUpDetails.get(i).getMailOrder()+"k");
                    etorderemailid.setText(companySetUpDetails.get(i).getMailOrder());
                    agentEmailid = companySetUpDetails.get(i).getAgentEmail();
                    goldAgentName = companySetUpDetails.get(i).getAgentName();
                    goldAgentId = companySetUpDetails.get(i).getAgentId();
                } catch (Exception e) {
                    mRealm.cancelTransaction();
                }

            }
        } else {
            Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
        }
        mRealm.commitTransaction();
        if (goldAgentId.equals("0")) {
            lnr_EditGoldAgent.setVisibility(View.VISIBLE);
            lnr_ViewOnlyGoldAgent.setVisibility(View.GONE);
            etAgentemailid.setText(agentEmailid);
            SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
            PREF.putString(PreferenceUtil.USER_AGENT_ID, "0");
            PREF.putString(PreferenceUtil.USER_AGENT_NAME, "");
            PREF.apply();
        } else {
            lnr_EditGoldAgent.setVisibility(View.GONE);
            lnr_ViewOnlyGoldAgent.setVisibility(View.VISIBLE);
            tvAddAgent.setText(goldAgentName);
            SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
            PREF.putString(PreferenceUtil.USER_AGENT_ID, goldAgentId);
            PREF.putString(PreferenceUtil.USER_AGENT_NAME, goldAgentName);
            PREF.apply();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        fav = menu.add("Done");
//        fav.setIcon(R.mipmap.ic_launcher);
        fav.setTitle(R.string.txt_save);
        fav.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("Save")) {
            try {
                String authToken = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AUTH_TOKEN, "0");
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    if (etorderemailid.getText().toString().trim().length() != 0) {
                        String agentId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AGENT_ID, "0");
                        String agentName = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AGENT_NAME, "");
                        SaveContact(etFax.getText().toString(), etSkypeAddress.getText().toString(), etMSID.getText().toString(), etQQID.getText().toString(), etorderemailid.getText().toString(), etAgentemailid.getText().toString(), authToken, agentId, agentName);
                    } else {
                        etorderemailid.setError(getResources().getString(R.string.txt_field_null));
                    }
                } else {
                    DisplayDialog(getResources().getString(R.string.error_no_internet));
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return super.onOptionsItemSelected(item);

    }

    private void SaveContact(final String Fax, final String SkypeAddress, final String MSID, final String QQID, final String orderemailid, final String Agentemailid, String authToken, final String agentId, final String agentName) {
        progressBar.setVisibility(View.VISIBLE);
        CompanySetUpContactService service = new CompanySetUpContactService(getActivity());
        CompanySetUpContactRequest request = new CompanySetUpContactRequest();
        request.setFax(Fax);
        request.setSkype(SkypeAddress);
        request.setMsnid(MSID);
        request.setQqid(QQID);
        request.setOrderemail(orderemailid);
        request.setAgentemail(Agentemailid);
        request.setAgentid(agentId);
        request.setAgentname(agentName);

        request.setToken(authToken);
        service.post(request, new ApiResponse<CompanySetUpContactResponse>() {
            @Override
            public void onSuccess(ResultCode resultCode, CompanySetUpContactResponse response) {

                CompanySetUpDetails companySetUpDetails = mRealm.where(CompanySetUpDetails.class)
                        .equalTo("CompanyID", companyId).findFirst();

                if (companySetUpDetails != null) {
                    // Exists
                    mRealm.beginTransaction();
                    companySetUpDetails.setFax(Fax);
                    companySetUpDetails.setSkype(SkypeAddress);
                    companySetUpDetails.setMsnID(MSID);
                    companySetUpDetails.setQqID(QQID);
                    companySetUpDetails.setMailOrder(orderemailid);
                    companySetUpDetails.setAgentEmail(Agentemailid);
                    companySetUpDetails.setAgentId(agentId);
                    companySetUpDetails.setAgentName(agentName);

                    mRealm.commitTransaction();
                }
                progressBar.setVisibility(View.GONE);
                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
                getActivity().finish();
                getActivity().overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);
            }

            @Override
            public void onError(ResultCode resultCode, CompanySetUpContactResponse response) {
                progressBar.setVisibility(View.GONE);
                DisplayDialog(response.getMessage());
            }
        });
    }

    @Subscribe
    public void NavigateVlue(GoldAgentEvent event) {

        idAgent = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AGENT_ID, "0");
        nameAgent = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AGENT_NAME, "");
        lnr_EditGoldAgent.setVisibility(View.GONE);
        lnr_ViewOnlyGoldAgent.setVisibility(View.VISIBLE);
        tvAddAgent.setText(nameAgent);
        etAgentemailid.setText("");


    }

}
