package com.mixmycontainer.yiwu.fragment;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.ProdectSummeryActivity;
import com.mixmycontainer.yiwu.apis.SavePriceOrederService;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.ResultCode;
import com.mixmycontainer.yiwu.apis.requests.SavePriceOrderRequest;
import com.mixmycontainer.yiwu.apis.responses.SavePriceOrderResponse;
import com.mixmycontainer.yiwu.bus.BusFactory;
import com.mixmycontainer.yiwu.bus.events.SummeryPageClickEvent;
import com.mixmycontainer.yiwu.models.BoothDetails;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.models.ItemDetails;
import com.mixmycontainer.yiwu.models.OrderListDetails;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import java.text.SimpleDateFormat;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 03-12-2016.
 */
public class PriceDetailesFragment extends BaseFragment {
    RealmList<BoothDetails> boothDetailses;
    String[] cartonsUsed;
    private TextInputLayout ethintPrizeZap;
    private int companyId, active3dCurrency;
    private DecoView decoView;
    private int backIndex;
    private LinearLayout lnr_3rdCurrency;
    private MenuItem menuItem;
    private TextView tvTotalCBM, tvOrdernumber, tvorderValueUs, tvTotalWeight, tvValueofPurchase;
    private ProgressBar progressBar;
    private EditText etPriceUSD, etPriceRMB, etPriceZAP;
    private Realm mRealm;
    //    private BoothDetails boothDetails;
    //    private ItemDetails itemDetails;
    private Spinner spinnerCartonUsed;
    private String ExchangeRate, ExchangeRate3dCurrency, ThirdCurrency, Weightpercentage = "0", Volumepercentage = "0";
    private TextWatcher txtWatcherPriceRMB, txtWatcherPriceUSD;
    private String prefEditboothID = "0", prefEditItemID = "0", prefOrderId;
    //-> "prefEditboothID" if 0 on Normal mode, if not on edit mode
    private float previousItemTotalPriceInUS, previousItemTotalPriceInRMB, previousItemTotalPrice3rdCurr;

    public static PriceDetailesFragment getnewInstance() {
        PriceDetailesFragment fragment = new PriceDetailesFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuItem = menu.add("action");
        menuItem.setTitle(getResources().getString(R.string.txt_next));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    }

    @Override
    public int getTitle() {
        return R.string.txt_price_detial;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getResources().getString(R.string.txt_next))) {
            validation();
        }
        return super.onOptionsItemSelected(item);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.frgament_price_deatils, container, false);

        prefEditboothID = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_BOOTH_ID, "0");
        prefEditItemID = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_ITEM_ID, "0");
        prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        mRealm = Realm.getDefaultInstance();

        initView(view);
        setEnableBackButton(false);
        return view;
    }

    private void initView(View view) {
        lnr_3rdCurrency = (LinearLayout) view.findViewById(R.id.lnr_3rdCurrency);
        spinnerCartonUsed = (Spinner) view.findViewById(R.id.spinnerCartonUsed);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        etPriceUSD = (EditText) view.findViewById(R.id.etPriceUSD);
        etPriceRMB = (EditText) view.findViewById(R.id.etPriceRMB);
        etPriceZAP = (EditText) view.findViewById(R.id.etPriceZAP);
        ethintPrizeZap = (TextInputLayout) view.findViewById(R.id.ethintPrizeZap);
        tvTotalCBM = (TextView) view.findViewById(R.id.tvTotalCBM);
        tvOrdernumber = (TextView) view.findViewById(R.id.tvOrdernumber);
        tvorderValueUs = (TextView) view.findViewById(R.id.tvorderValueUs);
        tvTotalWeight = (TextView) view.findViewById(R.id.tvTotalWeight);
        tvValueofPurchase = (TextView) view.findViewById(R.id.tvValueofPurchase);

        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        decoView = (DecoView) view.findViewById(R.id.dynamicArcView);
        decoView.addSeries(new SeriesItem.Builder(Color.parseColor("#FFFFFF"))
                .setRange(0, 100, 100)
                .setInitialVisibility(true)
                .build());
        final SeriesItem seriesItem1 = new SeriesItem.Builder(Color.parseColor("#4CAF50"))
                .setRange(0, 100, 0)
                .build();

        backIndex = decoView.addSeries(seriesItem1);
        final TextView textPercentage = (TextView) view.findViewById(R.id.textPercentage);
        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilled = ((currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue()));
                textPercentage.setText(String.format("%.0f%%", percentFilled * 100f));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });
        getfrderDetailsOffline();

        if (!prefEditboothID.equals("0")) {
            getPriceDetailForEdit();
        }
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
                    if (!companySetUpDetails.isEmpty()) {
                        for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {

                            active3dCurrency = Integer.parseInt(companySetUpDetails.get(i).getActive3dCurrency());
                            ExchangeRate = companySetUpDetails.get(i).getExchangeRate();
                            ExchangeRate3dCurrency = companySetUpDetails.get(i).getExchangeRate3dCurrency();
                            ThirdCurrency = companySetUpDetails.get(i).getThirdCurrency();
                            ThirdCurrency = companySetUpDetails.get(i).getThirdCurrency();


                        }
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }


        if (active3dCurrency == 0) {
            lnr_3rdCurrency.setVisibility(View.INVISIBLE);
        } else {
            lnr_3rdCurrency.setVisibility(View.VISIBLE);
        }
        ethintPrizeZap.setHint(ThirdCurrency);


        txtWatcherPriceUSD = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                etPriceRMB.setError(null);

            }

            @Override
            public void afterTextChanged(Editable editable) {
                double defaultUsd = 0;
                String defStringValue;
                defStringValue = etPriceUSD.getText().toString().trim();

                try {
                    if (etPriceUSD.getText().toString().trim().equals("")) {

                    } else {

                        if (defStringValue.contains("E")) {
                            defStringValue = defStringValue.replace("E", "0");
                            etPriceUSD.removeTextChangedListener(txtWatcherPriceUSD);
                            etPriceUSD.setText(defStringValue);
                            etPriceUSD.addTextChangedListener(txtWatcherPriceUSD);
                            defaultUsd = Double.parseDouble(defStringValue);
                        } else {
                            defaultUsd = Double.parseDouble(defStringValue);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }


                try {
                    if (ExchangeRate.equals("0")) {
                    } else {
                        etPriceRMB.removeTextChangedListener(txtWatcherPriceRMB);
                        Double rbmValue = Double.parseDouble(ExchangeRate) * defaultUsd;
                        etPriceRMB.setText(rbmValue + "");
                        double thirdCorrency_Value = Double.parseDouble(ExchangeRate3dCurrency) * defaultUsd;
                        etPriceZAP.setText(thirdCorrency_Value + "");
                        etPriceRMB.addTextChangedListener(txtWatcherPriceRMB);
                    }
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                    etPriceRMB.setText("0");
                }

            }
        };

        txtWatcherPriceRMB = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                double defaultRmb = 0;
                if (etPriceRMB.getText().toString().trim().equals("")) {


                } else {
                    String defaultValue;
                    defaultValue = etPriceRMB.getText().toString().trim();

//                    defaultRmb = (float) Double.valueOf(defaultValue).longValue();
                    if (defaultValue.contains("E")) {
                        defaultValue = defaultValue.toString().replace("E", "0");
                        etPriceRMB.removeTextChangedListener(txtWatcherPriceRMB);
                        etPriceRMB.setText("" + defaultValue);
                        etPriceRMB.addTextChangedListener(txtWatcherPriceRMB);
                        defaultRmb = Double.parseDouble(defaultValue);
                    } else {
                        defaultRmb = Float.parseFloat(defaultValue);
                    }
                }
                try {
                    etPriceUSD.removeTextChangedListener(txtWatcherPriceUSD);
                    double exchangerateValue = Double.parseDouble(ExchangeRate);

                    if (exchangerateValue != 0) {
                        double usdValue = defaultRmb / exchangerateValue;
                        etPriceUSD.setText(usdValue + "");

                    } else {

                    }
                    if (Integer.parseInt(ExchangeRate3dCurrency) != 0) {

                        double thirdCorrency_Value = Double.parseDouble(ExchangeRate3dCurrency) * Double.parseDouble(etPriceUSD.getText().toString().trim());
                        etPriceZAP.setText(thirdCorrency_Value + "");
                    } else {

                    }
                    etPriceUSD.addTextChangedListener(txtWatcherPriceUSD);
                } catch (NumberFormatException e) {
                    e.printStackTrace();
                }
            }
        };
        etPriceUSD.addTextChangedListener(txtWatcherPriceUSD);
        etPriceRMB.addTextChangedListener(txtWatcherPriceRMB);

    }

    private void getPriceDetailForEdit() {
        mRealm = Realm.getDefaultInstance();
        try {


            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {
                        RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();
                        for (int j = 0; j < itemDetails.size(); j++) {
                            if (itemDetails.get(j).getItemId().equals(prefEditItemID)) {
                                previousItemTotalPriceInUS = Float.parseFloat(itemDetails.get(j).getPriceInUs());
                                previousItemTotalPriceInRMB = Float.parseFloat(itemDetails.get(j).getPriceInRmb());
                                previousItemTotalPrice3rdCurr = Float.parseFloat(itemDetails.get(j).getPriceInThirdCurrency());
                                etPriceUSD.setText(itemDetails.get(j).getPriceInUs().toString());
                                etPriceRMB.setText(itemDetails.get(j).getPriceInRmb().toString());
                                etPriceZAP.setText(itemDetails.get(j).getPriceInThirdCurrency().toString());

                            }
                        }
                    }
                }
            });
        } catch (Exception e) {
            e.getMessage();
        }
    }

    private void validation() {
        if (etPriceUSD.getText().toString().equals("") && etPriceRMB.getText().toString().trim().equals("")) {
            etPriceUSD.setError(getResources().getString(R.string.txt_field_null));
            etPriceRMB.setError(getResources().getString(R.string.txt_field_null));
        } else if (!etPriceUSD.getText().toString().toString().equals("") && !etPriceRMB.getText().toString().trim().equals("")) {
            progressBar.setVisibility(View.VISIBLE);
//            savePriceOrder();
            if (prefEditboothID.equals("0")) {
                savePriceOrderOffline();
                deleteUnusedBooth();

                updateOrderPriceAndDetails();
                startActivity(ProdectSummeryActivity.getnewIntent(getActivity()));
                getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                getActivity().finish();
            } else {
                updateItemPrice();
                deleteUnusedBooth();

                updateOrderPriceAndDetails();
                updateBoothPrice();
                startActivity(ProdectSummeryActivity.getnewIntent(getActivity()));
                getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                getActivity().finish();
            }


        } else {
            if (etPriceUSD.getText().toString().trim().equals("")) {
                etPriceUSD.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etPriceRMB.getText().toString().trim().equals("")) {
                etPriceRMB.setError(getResources().getString(R.string.txt_field_null));
            }
        }
    }

    private void updateBoothPrice() {
        mRealm = Realm.getDefaultInstance();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {


                        RealmList<BoothDetails> boothDetails = orderListDetails.get(i).getBoothDetailses();
                        for (int k = 0; k < boothDetails.size(); k++) {

                            float totalBoothPriceUs = 0;
                            float totalBoothPriceRmb = 0;
                            RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();

                            for (int j = 0; j < itemDetails.size(); j++) {

                                if (boothDetails.get(k).getBoothId().equals(itemDetails.get(j).getBoothId())) {
                                    float itemPriceInUs = Float.parseFloat(itemDetails.get(j).getTotalPriceInUS());
                                    float itemPriceInRmb = Float.parseFloat(itemDetails.get(j).getTotalPriceInRMB());
                                    float itemPriceIn3rdCurr = Float.parseFloat(itemDetails.get(j).getTotalPriceIn3rdCurrency());
                                    totalBoothPriceUs = totalBoothPriceUs + itemPriceInUs;
                                    totalBoothPriceRmb = totalBoothPriceRmb + itemPriceInRmb;
                                }

                            }
                            boothDetails.get(k).setValuePurchase(String.valueOf(totalBoothPriceUs));
                            boothDetails.get(k).setValuePurchaseRMB(String.valueOf(totalBoothPriceRmb));
                            mRealm.insertOrUpdate(boothDetails);
                        }
                        mRealm.insertOrUpdate(orderListDetails);
                    }
                }
            });
        } catch (Exception e) {
            e.getMessage();
        }

    }

    private void updateOrderPriceAndDetails() {
        mRealm = Realm.getDefaultInstance();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {
                        float totalOrderPriceUs = 0;
                        float totalOrderPriceRmb = 0;
                        float totalOrderPricenIn3rdCurr = 0;

                        float orderTotalCbm = 0;
                        float orderTotalWeight = 0;
                        float orderTotalCartonCount = 0;

                        RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();

                        for (int j = 0; j < itemDetails.size(); j++) {
                            float itemPriceInUs = Float.parseFloat(itemDetails.get(j).getTotalPriceInUS());
                            float itemPriceInRmb = Float.parseFloat(itemDetails.get(j).getTotalPriceInRMB());
                            float itemPriceIn3rdCurr = Float.parseFloat(itemDetails.get(j).getTotalPriceIn3rdCurrency());
                            totalOrderPriceUs = totalOrderPriceUs + itemPriceInUs;
                            totalOrderPriceRmb = totalOrderPriceRmb + itemPriceInRmb;
                            totalOrderPricenIn3rdCurr = totalOrderPricenIn3rdCurr + itemPriceIn3rdCurr;
                            float itemCount = Float.parseFloat(itemDetails.get(j).getCartonQuantity());

                            orderTotalCartonCount = orderTotalCartonCount+itemCount;
                            orderTotalCbm = orderTotalCbm + (Float.parseFloat(itemDetails.get(j).getCBM())*itemCount);
                            orderTotalWeight = orderTotalWeight + (Float.parseFloat(itemDetails.get(j).getWeightofCarton())*itemCount);

                        }
                        orderListDetails.get(i).setTotalOrderUsValue(String.valueOf(totalOrderPriceUs));
                        orderListDetails.get(i).setTotalOrderRmbValue(String.valueOf(totalOrderPriceRmb));
                        orderListDetails.get(i).setTotal3rdCurrencyValue(String.valueOf(totalOrderPricenIn3rdCurr));
                        orderListDetails.get(i).setTotalCartons(String.valueOf(orderTotalCartonCount));

                        orderListDetails.get(i).setTotalOrderCBM(String.valueOf(orderTotalCbm));
                        orderListDetails.get(i).setTotalOrderWeight(String.valueOf(orderTotalWeight));
                        orderListDetails.get(i).setTotalBooth(String.valueOf(orderListDetails.get(i).getBoothDetailses().size()));
                        mRealm.insertOrUpdate(orderListDetails);
                    }
                }
            });
        } catch (Exception e) {
            String a;
            a = e.getMessage();
            e.printStackTrace();
        }
    }

    private void updateItemPrice() {
        mRealm = Realm.getDefaultInstance();
        try {
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();
                    for (int i = 0; i < orderListDetails.size(); i++) {
                        RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();
                        for (int j = 0; j < itemDetails.size(); j++) {
                            if (itemDetails.get(j).getItemId().equals(prefEditItemID)) {
                                int cartonQty = Integer.parseInt(itemDetails.get(j).getCartonQuantity());
                                float priceInUs = Float.parseFloat(etPriceUSD.getText().toString().trim());
                                float priceInRmb = Float.parseFloat(etPriceRMB.getText().toString().trim());
                                float priceIn3rdCurr = 0;
                                if (etPriceZAP.getText().toString().trim().equals("")) {
                                    priceIn3rdCurr = 0;
                                } else {
                                    Float.parseFloat(etPriceZAP.getText().toString().trim());
                                }

                                float totalPriceInUs = priceInUs * cartonQty;
                                float totalPriceInRmb = priceInRmb * cartonQty;
                                float totalPriceIn3rdCurr = priceIn3rdCurr * cartonQty;

                                itemDetails.get(j).setPriceInUs(String.valueOf(priceInUs));
                                itemDetails.get(j).setPriceInRmb(String.valueOf(priceInRmb));
                                itemDetails.get(j).setPriceInThirdCurrency(String.valueOf(priceIn3rdCurr));

                                itemDetails.get(j).setTotalPriceInUS(String.valueOf(totalPriceInUs));
                                itemDetails.get(j).setTotalPriceInRMB(String.valueOf(totalPriceInRmb));
                                itemDetails.get(j).setTotalPriceIn3rdCurrency(String.valueOf(totalPriceIn3rdCurr));
                                mRealm.insertOrUpdate(itemDetails);

                            }
                        }
                        mRealm.insertOrUpdate(orderListDetails);
                    }
                }
            });
        } catch (Exception e) {
            e.getMessage();
        }
    }

    /*get order details */
    private void getfrderDetailsOffline() {

        try {
            mRealm = Realm.getDefaultInstance();
//            mRealm.waitForChange();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();

//                    ContainerDetails containerDetails = mRealm.where(ContainerDetails.class)
//                         .equalTo("orderId", prefOrderId).findFirst();

                    if (!OrderListDetailsDetails.isEmpty()) {

                        for (int i = OrderListDetailsDetails.size() - 1; i >= 0; i--) {

                            tvOrdernumber.setText(OrderListDetailsDetails.get(i).getOrderNumber());
                            tvorderValueUs.setText(OrderListDetailsDetails.get(i).getTotalOrderUsValue() + "$(" + OrderListDetailsDetails.get(i).getTotalOrderRmbValue() + "¥)");
                            double totCBB = Double.parseDouble(OrderListDetailsDetails.get(i).getTotalOrderCBM());
                            String orderCbm = String.format("%.2f", totCBB);
                            tvTotalCBM.setText(orderCbm);
                            tvTotalWeight.setText(OrderListDetailsDetails.get(i).getTotalOrderWeight());
                            boothDetailses = OrderListDetailsDetails.get(i).getBoothDetailses();
                            cartonsUsed = new String[OrderListDetailsDetails.get(i).getContainerDetailses().size()];

                            for (int k = 0; k < OrderListDetailsDetails.get(i).getContainerDetailses().size(); k++) {
                                String cartonName = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerName();
                                cartonsUsed[k] = cartonName;
//                                if (OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getStatus().equals("0")) {
                                Volumepercentage = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getVolumepercentage();
                                Weightpercentage = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getWeightpercentage();
//                                }
                            }


                            setSpinnerData(cartonsUsed, spinnerCartonUsed);
                            for (int j = 0; j < boothDetailses.size(); j++) {
                                String boothUS = boothDetailses.get(j).getValuePurchase();
                                String bothRMB = boothDetailses.get(j).getValuePurchaseRMB();
                                tvValueofPurchase.setText(boothUS + "$(" + bothRMB + "¥)");
//                        itemDetailses = boothDetailses.get(j).getItemDetailses();
//                        for (int k = 0; k < itemDetailses.size(); k++) {
//                            String itemId = itemDetailses.get(k).getItemId();
//                            String itemNo = itemDetailses.get(k).getItemNo();
//                            System.out.println(itemId + "itemId" + "itemNo" + itemNo);
//                        }
                            }
                            try {
                                float Vol_Percent_float = Float.parseFloat(Volumepercentage);
                                Float Weight_Percent_float = Float.parseFloat(Weightpercentage);
                                int Vol_Percent = Math.round(Vol_Percent_float);
                                int Weight_Percent = Math.round(Weight_Percent_float);
                                if (Vol_Percent >= Weight_Percent) {

                                    decoView.addEvent(new DecoEvent.Builder(Vol_Percent).setIndex(backIndex).setDelay(250).build());
                                } else {
                                    decoView.addEvent(new DecoEvent.Builder(Weight_Percent).setIndex(backIndex).setDelay(250).build());
                                }
                            } catch (NumberFormatException ee) {
                                ee.printStackTrace();
                            }


                        }


                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

    /*set spinner data
    * param 1- array of used cantainers
    * param 2- spinner Id*/
    private void setSpinnerData(String[] cartonsUsed, Spinner spinnerCartonUsed) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, cartonsUsed);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerCartonUsed.setAdapter(spinnerArrayAdapter);
    }

    /* save price details offline*/
    private void savePriceOrderOffline() {
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        final String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");
        final String prefItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ITEM_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListSetUp = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();


                    if (orderListSetUp != null) {
                        float totalOrderValueUs = 0;
                        float totalOrderValueRmb = 0;
                        float totalOrderValue3rdCurr = 0;
                        for (int i = 0; i < orderListSetUp.size(); i++) {
                            totalOrderValueUs = Float.parseFloat(orderListSetUp.get(i).getTotalOrderUsValue());
                            totalOrderValueRmb = Float.parseFloat(orderListSetUp.get(i).getTotalOrderRmbValue());
                            totalOrderValue3rdCurr = Float.parseFloat(orderListSetUp.get(i).getTotal3rdCurrencyValue());

                        }

                        float itemUnitQuantity = 0;

                        for (int i = 0; i < orderListSetUp.size(); i++) {
                            RealmResults<ItemDetails> itemDetail = orderListSetUp.get(i).getItemDetailses().where().equalTo("orderId", prefOrderId).equalTo("BoothId", prefBoothId).equalTo("ItemId", prefItemId).findAll();
                            for (int k = 0; k < itemDetail.size(); k++) {

                                itemUnitQuantity = Float.parseFloat(itemDetail.get(k).getUnitQuantity().toString());
                            }

                        }

                        float boothValuePurchase = 0;
                        float boothValuePurchaseRmb = 0;
                        for (int i = 0; i < orderListSetUp.size(); i++) {
                            RealmResults<BoothDetails> boothdetail = orderListSetUp.get(i).getBoothDetailses().where().equalTo("OrderId", prefOrderId).equalTo("BoothId", prefBoothId).findAll();
                            for (int j = 0; j < boothdetail.size(); j++) {
                                boothValuePurchase = Float.parseFloat(boothdetail.get(j).getValuePurchase().toString());
                                boothValuePurchaseRmb = Float.parseFloat(boothdetail.get(j).getValuePurchaseRMB().toString());

                            }
                        }

                        String totalOrderUsvalue = "";
                        String totalOrderRmbvalue = "";
                        String totalOrderZarvalue = "";
                        String valuePurchase = "";
                        String valuePurchaseRmb = "";


                        valuePurchaseRmb = String.valueOf(boothValuePurchaseRmb + (Float.parseFloat(etPriceRMB.getText().toString().trim()) * itemUnitQuantity));
                        valuePurchase = String.valueOf(boothValuePurchase + (Float.parseFloat(etPriceUSD.getText().toString().trim()) * itemUnitQuantity));

                        totalOrderUsvalue = String.valueOf(totalOrderValueUs + (Float.parseFloat(etPriceUSD.getText().toString().trim()) * itemUnitQuantity));
                        totalOrderRmbvalue = String.valueOf(totalOrderValueRmb + (Float.parseFloat(etPriceRMB.getText().toString().trim()) * itemUnitQuantity));
                        totalOrderZarvalue = String.valueOf(totalOrderValue3rdCurr + (Float.parseFloat(etPriceZAP.getText().toString().trim()) * itemUnitQuantity));

                        String itemPriceUs = "";
                        String itemPriceRmb = "";
                        String itemPrice3rdCurr = "";

                        String itemTotalPriceUs = "";
                        String itemTotalPriceRmb = "";
                        String itemTotalPrice3rdCurr = "";

                        itemPriceUs = etPriceUSD.getText().toString().trim();
                        itemPriceRmb = etPriceRMB.getText().toString().trim();
                        itemPrice3rdCurr = etPriceZAP.getText().toString();

                        itemTotalPriceUs = String.valueOf(Float.parseFloat(itemPriceUs) * itemUnitQuantity);
                        itemTotalPriceRmb = String.valueOf(Float.parseFloat(itemPriceRmb) * itemUnitQuantity);
                        itemTotalPrice3rdCurr = String.valueOf(Float.parseFloat(etPriceZAP.getText().toString().trim()) * itemUnitQuantity);

                        String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());


                        //Updating Price details.
                        for (int i = 0; i < orderListSetUp.size(); i++) {


                            orderListSetUp.get(i).setCompanyID(companyId);
                            orderListSetUp.get(i).setTotalOrderUsValue(totalOrderUsvalue);
                            orderListSetUp.get(i).setTotalOrderRmbValue(totalOrderRmbvalue);
                            orderListSetUp.get(i).setTotalOrderZarValue(totalOrderZarvalue);
                            orderListSetUp.get(i).setTotal3rdCurrencyValue("0");
                            RealmResults<BoothDetails> boothdetail = orderListSetUp.get(i).getBoothDetailses().where().equalTo("OrderId", prefOrderId).equalTo("BoothId", prefBoothId).findAll();
                            for (int j = 0; j < boothdetail.size(); j++) {
                                boothdetail.get(j).setValuePurchaseRMB(valuePurchaseRmb);
                                boothdetail.get(j).setValuePurchase(valuePurchase);
                                mRealm.insertOrUpdate(boothdetail);
                            }
                            RealmResults<ItemDetails> itemDetail = orderListSetUp.get(i).getItemDetailses().where().equalTo("orderId", prefOrderId).equalTo("BoothId", prefBoothId).equalTo("ItemId", prefItemId).findAll();
                            for (int k = 0; k < itemDetail.size(); k++) {


                                itemDetail.get(k).setPriceInUs(itemPriceUs);
                                itemDetail.get(k).setPriceInRmb(itemPriceRmb);
                                itemDetail.get(k).setThirdCurrencyType("0");
                                itemDetail.get(k).setPriceInThirdCurrency(itemPrice3rdCurr);

                                itemDetail.get(k).setTotalPriceInUS(itemTotalPriceUs);
                                itemDetail.get(k).setTotalPriceInRMB(itemTotalPriceRmb);
                                itemDetail.get(k).setTotalPriceIn3rdCurrency(itemTotalPrice3rdCurr);

                                itemDetail.get(k).setDate(date);
                                mRealm.insertOrUpdate(itemDetail);

                            }

                        }

//                        mRealm.insertOrUpdate(boothExistOrNot);
//                        mRealm.insertOrUpdate(itemExistOrNot);
                        mRealm.insertOrUpdate(orderListSetUp);


                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

//    public void savePriceOrder() {
//        SavePriceOrederService service = new SavePriceOrederService(getActivity());
//        SavePriceOrderRequest request = new SavePriceOrderRequest();
//        request.setToken(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AUTH_TOKEN, null));
//        request.setItemId(PreferenceUtil.get(getActivity()).getInt(PreferenceUtil.USER_ITEMDETAIL_ID, 0) + "");
//        request.setPriceInUs(etPriceUSD.getText().toString().trim());
//        request.setPriceInRmb(etPriceRMB.getText().toString().trim());
//        request.setPriceInZar(etPriceZAP.getText().toString());
//        request.setOrderId(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_ORDER_ID, "0"));
//        request.setBoothid(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_BOOTH_ID, "0"));
//        service.post(request, new ApiResponse<SavePriceOrderResponse>() {
//            @Override
//            public void onSuccess(ResultCode resultCode, SavePriceOrderResponse response) {
//                progressBar.setVisibility(View.INVISIBLE);
//
//                BusFactory.getBus().post(new SummeryPageClickEvent());
//
//                SharedPreferences.Editor pref = PreferenceUtil.edit(getActivity());
//                pref.putString(PreferenceUtil.USER_ORDER_NUMBER, response.getSummary().getOrderNumber());
//                pref.putString(PreferenceUtil.TOTAL_USD, response.getSummary().getTotalOrderUsValue());
//                pref.putString(PreferenceUtil.TOTAL_RMB, response.getSummary().getTotalOrderRmbValue());
//                pref.putString(PreferenceUtil.VALUE_OF_PURCHASE, response.getSummary().getValuePurchase());
//                pref.putString(PreferenceUtil.VALUE_OF_PURCHASE_RMB, response.getSummary().getValuePurchaseRMB());
//                pref.putString(PreferenceUtil.TOTAL_ORDER_CBM, response.getSummary().getTotalOrderCBM());
//                pref.putString(PreferenceUtil.TOTAL_ORDER_WEIGHT, response.getSummary().getTotalOrderWeight());
//                pref.putString(PreferenceUtil.TOTAL_VOLUME_PERCENTAGE, response.getSummary().getVolumepercentage());
//                pref.putString(PreferenceUtil.TOTAL_WEIGHT_PERCENTAGE, response.getSummary().getWeightpercentage());
//                pref.apply();
//
//                startActivity(ProdectSummeryActivity.getnewIntent(getActivity()));
//                getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
//            }
//
//            @Override
//            public void onError(ResultCode resultCode, SavePriceOrderResponse response) {
//                progressBar.setVisibility(View.INVISIBLE);
//                Toast.makeText(getActivity(), response.getMessage(), Toast.LENGTH_SHORT).show();
//            }
//        });
//    }

    private void deleteUnusedBooth() {
        final int companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();
                    for (int i=0;i<orderListDetails.size();i++)
                    {
                        RealmList<BoothDetails> boothDetails = orderListDetails.get(i).getBoothDetailses();
                        for (int j=0;j<boothDetails.size();j++)
                        {
                            int usedBoothCount=0;
                            RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();
                            for (int k=0;k<itemDetails.size();k++)
                            {
                                if (boothDetails.get(j).getBoothId().equals(itemDetails.get(k).getBoothId()))
                                {
                                    usedBoothCount++;
                                }

                            }
                            if (usedBoothCount==0)
                            {
                                boothDetails.deleteFromRealm(j);
                            }
                            mRealm.insertOrUpdate(boothDetails);
                        }
                        mRealm.insertOrUpdate(orderListDetails);
                    }
                }
            });

        }
        catch (Exception e)
        {
            e.getMessage();
        }
        finally {
            if (mRealm!=null)
            {
                mRealm.close();
            }
        }

    }
}
