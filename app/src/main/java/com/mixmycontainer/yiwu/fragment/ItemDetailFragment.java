package com.mixmycontainer.yiwu.fragment;

import android.Manifest;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;
import com.iceteck.silicompressorr.SiliCompressor;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.CartonDetailActivity;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.models.BoothDetails;
import com.mixmycontainer.yiwu.models.CompanySetUpDetails;
import com.mixmycontainer.yiwu.models.ItemDetails;
import com.mixmycontainer.yiwu.models.OrderListDetails;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.ImageUtil;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import io.realm.Realm;
import io.realm.RealmList;
import io.realm.RealmResults;

/**
 * Created by ADMIN on 15-11-2016.
 */
public class ItemDetailFragment extends BaseFragment {

    private static int RESULT_OK = -1;
    RealmList<BoothDetails> boothDetailses;
    String[] cartonsUsed;
    private Bitmap bitmap;
    private int companyId, backIndex;
    private DecoView decoView;
    private Spinner spinnerCartonUsed;
    private TextView tvOrdernumber, tvTotalCBM, tvTotalWeight, tvorderValueUs, tvValueofPurchase;
    private String from = "", to = "", fileNameToSend = "0", Weightpercentage = "0", Volumepercentage = "0";
    private MenuItem menuItem;
    private EditText etItemNumber, etEnglishDiscription, etChainesDescription;
    private ProgressBar progressBar;
    private ImageView btTackPicture, ivItemImageview;
    private Realm mRealm;
    private ItemDetails itemDetails;
    private Uri imageUri;
    private String prefEditItemId = "0",prefEditBoothId;


    public static ItemDetailFragment newinstance() {
        ItemDetailFragment fragment = new ItemDetailFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public int getTitle() {
        return R.string.txt_item_details;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_item_number, container, false);

        mRealm = Realm.getDefaultInstance();
        prefEditBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_BOOTH_ID, "0");
        initView(view);

        btTackPicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                            Manifest.permission.CAMERA)) {
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.CAMERA},
                                11);
                    } else {
                        // we can request the permission.
//                        Toast.makeText(getContext(), "Requesting permission", Toast.LENGTH_SHORT).show();
                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.CAMERA},
                                11);
                    }
                } else {

                    captureImageDialog();
                }


            }
        });
        return view;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:

                if (resultCode == RESULT_OK) {
//                    bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
//                    ivItemImageview.setImageBitmap(bitmap);
//                    fileNameToSend = ImageUtil.convert(bitmap);

                    Uri selectedImage = imageUri;
                    getActivity().getContentResolver().notifyChange(selectedImage, null);
                    ContentResolver cr = getActivity().getContentResolver();
                    Bitmap bitmap;
                    try {
                        //Code to rotate image while taking through camera

                        ExifInterface ei = new ExifInterface(imageUri.toString());
                        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_UNDEFINED);
                        Bitmap rotatedBitmap = null;
                        bitmap = android.provider.MediaStore.Images.Media.getBitmap(cr, selectedImage);
                        switch (orientation) {

                            case ExifInterface.ORIENTATION_ROTATE_90:
                                rotatedBitmap = rotateImage(bitmap, 90);
                                break;

                            case ExifInterface.ORIENTATION_ROTATE_180:
                                rotatedBitmap = rotateImage(bitmap, 180);
                                break;

                            case ExifInterface.ORIENTATION_ROTATE_270:
                                rotatedBitmap = rotateImage(bitmap, 270);
                                break;

                            case ExifInterface.ORIENTATION_NORMAL:
                            default:
                                rotatedBitmap = bitmap;
                        }
                        //Rotate end


                        ivItemImageview.setImageBitmap(rotatedBitmap);
                        bitmap = SiliCompressor.with(getContext()).getCompressBitmap(selectedImage.toString(), true);
                        fileNameToSend = ImageUtil.convert(bitmap);

                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }

                }

                break;
            case 1:

                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), selectedImage);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    String tempImageUrl = String.valueOf(selectedImage);
                    try {
                        bitmap = SiliCompressor.with(getContext()).getCompressBitmap(tempImageUrl);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ivItemImageview.setImageBitmap(bitmap);
                    fileNameToSend = ImageUtil.convert(bitmap);
                }
                break;
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menuItem = menu.add("action");
        menuItem.setTitle(getResources().getString(R.string.txt_next));
        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

    }

    public void uploadImage(final File attachment) {
        Ion.with(getActivity())
                .load(URL.UploadItemImage)
                .uploadProgressHandler(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {
                        progressBar.setMax((int) total);
                        progressBar.setProgress((int) downloaded);
                    }
                })
                .setMultipartFile("image", attachment)
                .setMultipartParameter("token", PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_AUTH_TOKEN, null))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result.get("resultCode").getAsString().equals("0")) {
                            Toast.makeText(getActivity(), result.get("message").getAsString() + "", Toast.LENGTH_SHORT).show();

                        } else {
                            fileNameToSend = result.get("filename").getAsString();
                            Toast.makeText(getActivity(), "Item image uploaded successfully", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    /* dialog to take image of item*/
    private void captureImageDialog() {
        final Dialog dialog = new Dialog(getActivity());

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        DisplayMetrics display = getActivity().getResources().getDisplayMetrics();
        int width = display.widthPixels;
        int height = display.heightPixels;
        dialog.setContentView(R.layout.dialog_image_capture);
        TextView btn_takePhoto = (TextView) dialog.findViewById(R.id.btn_takePhoto);
        TextView btn_captureImage = (TextView) dialog.findViewById(R.id.btn_captureImage);
        btn_takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                File photo = new File(Environment.getExternalStorageDirectory(), "Pic.jpg");
                takePicture.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photo));
                imageUri = Uri.fromFile(photo);
                startActivityForResult(takePicture, 0);
                dialog.cancel();
            }
        });
        btn_captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
                dialog.cancel();
            }
        });
        dialog.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals(getResources().getString(R.string.txt_next))) {

            validation();

        }
        return super.onOptionsItemSelected(item);


    }

    private void initView(final View view) {
        btTackPicture = (ImageView) view.findViewById(R.id.btTackPicture);
        ivItemImageview = (ImageView) view.findViewById(R.id.ivItemImageview);
        etItemNumber = (EditText) view.findViewById(R.id.etItemNumber);
        etEnglishDiscription = (EditText) view.findViewById(R.id.etEnglishDiscription);
        etChainesDescription = (EditText) view.findViewById(R.id.etChainesDescription);
        spinnerCartonUsed = (Spinner) view.findViewById(R.id.spinnerCartonUsed);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        tvOrdernumber = (TextView) view.findViewById(R.id.tvOrdernumber);
        tvTotalCBM = (TextView) view.findViewById(R.id.tvTotalCBM);
        tvValueofPurchase = (TextView) view.findViewById(R.id.tvValueofPurchase);
        tvorderValueUs = (TextView) view.findViewById(R.id.tvorderValueUs);
        tvTotalWeight = (TextView) view.findViewById(R.id.tvTotalWeight);
        companyId = Integer.parseInt(PreferenceUtil.get(getActivity()).getString(PreferenceUtil.USER_COMPANY_ID, "0"));
        decoView = (DecoView) view.findViewById(R.id.dynamicArcView);
        decoView.addSeries(new SeriesItem.Builder(Color.parseColor("#FFFFFF"))
                .setRange(0, 100, 100)
                .setInitialVisibility(true)
                .build());
        final SeriesItem seriesItem1 = new SeriesItem.Builder(Color.parseColor("#4CAF50"))
                .setRange(0, 100, 0)
                .build();

        backIndex = decoView.addSeries(seriesItem1);
        final TextView textPercentage = (TextView) view.findViewById(R.id.textPercentage);
        seriesItem1.addArcSeriesItemListener(new SeriesItem.SeriesItemListener() {
            @Override
            public void onSeriesItemAnimationProgress(float percentComplete, float currentPosition) {
                float percentFilled = ((currentPosition - seriesItem1.getMinValue()) / (seriesItem1.getMaxValue() - seriesItem1.getMinValue()));
                textPercentage.setText(String.format("%.0f%%", percentFilled * 100f));
            }

            @Override
            public void onSeriesItemDisplayProgress(float percentComplete) {

            }
        });
        getfrderDetailsOffline();
        getItemDetailsForEdit();
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<CompanySetUpDetails> companySetUpDetails = mRealm.where(CompanySetUpDetails.class).equalTo("CompanyID", companyId).findAll();
                    if (!companySetUpDetails.isEmpty()) {
                        for (int i = companySetUpDetails.size() - 1; i >= 0; i--) {
                            from = companySetUpDetails.get(i).getTransilateFrom();
                            to = companySetUpDetails.get(i).getTransilateTo();
                        }
                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

        etEnglishDiscription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {

                } else {
                    if (from.equals(to)) {
                        etChainesDescription.setText(etEnglishDiscription.getText().toString());
                    } else {
                        etChainesDescription.setError(null);
                        if (AppUtils.isNetworkAvailable(getActivity())) {
                            translateText(from, to, etEnglishDiscription.getText().toString());
                        } else {
                            etChainesDescription.setText(etEnglishDiscription.getText().toString());
                            Toast.makeText(getActivity(), "Description will be converted, when online", Toast.LENGTH_SHORT).show();

                        }
                    }
                }
            }
        });


    }

    private void getItemDetailsForEdit() {

        if (!prefEditBoothId.equals("0"))
        {
            prefEditItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_ITEM_ID,"0");
            final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");

            try {
                mRealm= Realm.getDefaultInstance();
                mRealm.executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                                .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();
                        for (int i =0 ; i<orderListDetails.size();i++)
                        {
                            RealmList<ItemDetails> itemDetails = orderListDetails.get(i).getItemDetailses();
                            for (int j=0;j<itemDetails.size();j++)
                            {
                                if (itemDetails.get(j).getItemId().equals(prefEditItemId))
                                {
                                    etItemNumber.setText(itemDetails.get(j).getItemNo().toString());
                                    etEnglishDiscription.setText(itemDetails.get(j).getDescriptionFrom());
                                    etChainesDescription.setText(itemDetails.get(j).getDescriptionTo());
                                    Glide.with(getActivity()).load(URL.PhotView + itemDetails.get(j).getPicture()).into(ivItemImageview);
                                }
                            }
                        }
                    }
                });
            }
            catch (Exception e)
            {
                e.getMessage();
            }

        }
    }

    /* method to translate text online*/
    public void translateText(String fromstring, String tostring, String translatestring) {
        translatestring = translatestring.replaceAll(" ", "%20");
        Ion.with(getActivity())
                .load(URL.translate_text + "&source=" + fromstring + "&target=" + tostring + "&q=" + translatestring)
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        try {
                            JsonObject jsonObject = result.get("data").getAsJsonObject();
                            JsonArray jsonArray = jsonObject.get("translations").getAsJsonArray();
                            JsonObject jsonObject1 = jsonArray.get(0).getAsJsonObject();
                            String traslate_text = jsonObject1.get("translatedText").getAsString();
                            etChainesDescription.setText(traslate_text);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                });

    }

    private void validation() {
        if (!etItemNumber.getText().toString().toString().trim().equals("") &&
                !etEnglishDiscription.getText().toString().trim().equals("") &&
                !etChainesDescription.getText().toString().trim().equals("")) {

            saveItemOffline();


        } else {

            if (etItemNumber.getText().toString().toString().trim().equals("")) {
                etItemNumber.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etEnglishDiscription.getText().toString().trim().equals("")) {
                etEnglishDiscription.setError(getResources().getString(R.string.txt_field_null));
            }
            if (etChainesDescription.getText().toString().trim().equals("")) {
                etChainesDescription.setError(getResources().getString(R.string.txt_field_null));
            }
        }
    }

    /*save item details to database offline*/
    private void saveItemOffline() {
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        final String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");


        if (!prefEditBoothId.equals("0"))
        {
            prefEditItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_ITEM_ID,"0");
        }
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> orderListDetails = mRealm.where(OrderListDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("CompanyID", companyId).findAll();

                    ItemDetails itemExistOrNot = mRealm.where(ItemDetails.class)
                            .equalTo("orderId", prefOrderId).equalTo("ItemId", prefEditItemId).findFirst();


                    BoothDetails boothExistOrNot = mRealm.where(BoothDetails.class)
                            .equalTo("OrderId", prefOrderId).equalTo("BoothId", prefBoothId).findFirst();
                    if (itemExistOrNot == null) {
//            not Exists

//                        itemDetails = mRealm.copyToRealm(new ItemDetails());
                        for (int i = 0; i < orderListDetails.size(); i++) {


                                ItemDetails itemDetail = new ItemDetails();
                                itemDetail.setOrderId(prefOrderId);
                                itemDetail.setBoothId(prefBoothId);

                                String itemIdd = UUID.randomUUID().toString();
                                SharedPreferences.Editor PREF = PreferenceUtil.edit(getActivity());
                                PREF.putString(PreferenceUtil.ITEM_ID, itemIdd);
                                PREF.apply();

                                itemDetail.setItemId(itemIdd);
                                itemDetail.setItemNo(etItemNumber.getText().toString().trim());
                                itemDetail.setDescriptionFrom(etEnglishDiscription.getText().toString().trim());
                                itemDetail.setDescriptionTo(etChainesDescription.getText().toString().trim());
                                itemDetail.setUnitperCarton("0");
                                itemDetail.setContainerId("0");
                                itemDetail.setWeightofCarton("0");
                                itemDetail.setLength("0");
                                itemDetail.setWidth("0");
                                itemDetail.setHeight("0");
                                itemDetail.setCBM("0");
                                itemDetail.setPicture(fileNameToSend);
                                itemDetail.setPriceInUs("0");
                                itemDetail.setPriceInRmb("0");
                                itemDetail.setThirdCurrencyType("0");
                                itemDetail.setPriceInThirdCurrency("0");
                                itemDetail.setUnitQuantity("0");
                                itemDetail.setCartonQuantity("0");
                                itemDetail.setTotalPriceInUS("0");
                                itemDetail.setTotalPriceInRMB("0");
                                itemDetail.setTotalPriceIn3rdCurrency("0");
                                itemDetail.setIsEditFinished("1");
                                itemDetail.setNoOfCartonFilled("0");
                                String date = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
                                itemDetail.setDate(date);
                                mRealm.insertOrUpdate(itemDetail);
                                orderListDetails.get(i).getItemDetailses().add(itemDetail);
                                Toast.makeText(getActivity(), "Item added successfully", Toast.LENGTH_SHORT).show();


                            mRealm.insertOrUpdate(orderListDetails);
                        }


                    }
                    else {
                        for (int i =0;i<orderListDetails.size();i++)
                        {
                            RealmList<ItemDetails> itemDetail = orderListDetails.get(i).getItemDetailses();
                            for (int j=0;j<itemDetail.size();j++)
                            {
                                if (itemDetail.get(j).getItemId().equals(prefEditItemId))
                                {
                                    itemDetail.get(j).setBoothId(prefBoothId);
                                    itemDetail.get(j).setItemNo(etItemNumber.getText().toString().trim());
                                    itemDetail.get(j).setDescriptionFrom(etEnglishDiscription.getText().toString().trim());
                                    itemDetail.get(j).setDescriptionTo(etChainesDescription.getText().toString().trim());
                                    itemDetail.get(j).setPicture(fileNameToSend);
                                    mRealm.insertOrUpdate(itemDetail);
                                }

                            }
                            mRealm.insertOrUpdate(orderListDetails);
                        }

                    }
                    startActivity(CartonDetailActivity.getnewIntent(getActivity()));
                    getActivity().overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                    getActivity().finish();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

    //Method to save edited itemName,itemDescription and translate
    private void saveEditedItem()
    {
        final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        final String prefBoothId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.BOOTH_ID, "0");
        final String prefItemId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.EDIT_ITEM_ID,"0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {

                }
            });
        }
        catch (Exception e)
        {
            e.getMessage();
        }

    }

    /*Image rotation method*/
    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }

    /* get order status to show up in the databae*/
    private void getfrderDetailsOffline() {
          final String prefOrderId = PreferenceUtil.get(getActivity()).getString(PreferenceUtil.ORDER_ID, "0");
        try {
            mRealm = Realm.getDefaultInstance();
            mRealm.executeTransaction(new Realm.Transaction() {
                @Override
                public void execute(Realm realm) {
                    RealmResults<OrderListDetails> OrderListDetailsDetails = mRealm.where(OrderListDetails.class).equalTo("CompanyID", companyId).equalTo("OrderId", prefOrderId).findAll();
                    if (!OrderListDetailsDetails.isEmpty()) {
                        for (int i = OrderListDetailsDetails.size() - 1; i >= 0; i--) {

                            tvOrdernumber.setText(OrderListDetailsDetails.get(i).getOrderNumber());
                            tvorderValueUs.setText(OrderListDetailsDetails.get(i).getTotalOrderUsValue() + "$(" + OrderListDetailsDetails.get(i).getTotalOrderRmbValue() + "¥)");
                            double totCBB = Double.parseDouble(OrderListDetailsDetails.get(i).getTotalOrderCBM());
                            String orderCbm = String.format("%.2f", totCBB);
                            tvTotalWeight.setText(OrderListDetailsDetails.get(i).getTotalOrderWeight());
                            tvTotalCBM.setText(orderCbm);
                            cartonsUsed = new String[OrderListDetailsDetails.get(i).getContainerDetailses().size()];
                            for (int k = 0; k < OrderListDetailsDetails.get(i).getContainerDetailses().size(); k++) {
                                String cartonName = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getContainerName();
                                cartonsUsed[k] = cartonName;
//                                if (OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getStatus().equals("0")) {
                                    Volumepercentage = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getVolumepercentage();
                                    Weightpercentage = OrderListDetailsDetails.get(i).getContainerDetailses().get(k).getWeightpercentage();
//                                }
                            }

                            setSpinnerData(cartonsUsed, spinnerCartonUsed);
                            boothDetailses = OrderListDetailsDetails.get(i).getBoothDetailses();
                            for (int j = 0; j < boothDetailses.size(); j++) {
                                String boothUS = boothDetailses.get(j).getValuePurchase();
                                String bothRMB = boothDetailses.get(j).getValuePurchaseRMB();
                                tvValueofPurchase.setText(boothUS + "$(" + bothRMB + "¥)");

                            }
                            try {
                                float Vol_Percent_float = Float.parseFloat(Volumepercentage);
                                Float Weight_Percent_float = Float.parseFloat(Weightpercentage);
                                int Vol_Percent = Math.round(Vol_Percent_float);
                                int Weight_Percent = Math.round(Weight_Percent_float);
                                if (Vol_Percent >= Weight_Percent) {

                                    decoView.addEvent(new DecoEvent.Builder(Vol_Percent).setIndex(backIndex).setDelay(250).build());
                                } else {
                                    decoView.addEvent(new DecoEvent.Builder(Weight_Percent).setIndex(backIndex).setDelay(250).build());
                                }
                            } catch (NumberFormatException ee) {
                                ee.printStackTrace();
                            }


                        }


                    } else {
                        Toast.makeText(getActivity(), "Something went wrong please sign in again!", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (mRealm != null) {
                mRealm.close();
            }
        }

    }

    /* method to adapt string array to spinner*/
    private void setSpinnerData(String[] cartonsUsed, Spinner spinnerCartonUsed) {
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_text, cartonsUsed);
        spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
        spinnerCartonUsed.setAdapter(spinnerArrayAdapter);
    }

    @Override
    public void onResume() {

        super.onResume();
    }


}
