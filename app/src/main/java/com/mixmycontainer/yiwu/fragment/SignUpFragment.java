package com.mixmycontainer.yiwu.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.LogInActivity;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.utiles.AppUtils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ADMIN on 10-11-2016.
 */
public class SignUpFragment extends BaseFragment implements View.OnClickListener {

    private EditText etPassword, etConfirmPassword, etEmail, etZipPostal, etMobile, etCompanyName, etAddress, etPhone, etTownCity;
    private ProgressBar progressBar;
    private Spinner spinnerCountyNames;
    private String[] countryNames;
    private Button btLogin, btSignup;

    public static SignUpFragment newInstnce() {
        SignUpFragment fragment = new SignUpFragment();
        Bundle arg = new Bundle();
        fragment.setArguments(arg);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);
        initView(view);
        initLestener();
        getCountryNames();
        btSignup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                View viewv = getActivity().getCurrentFocus();
                if (viewv != null) {
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                }
                if (AppUtils.isNetworkAvailable(getActivity())) {
                    String Password = etPassword.getText().toString();
                    String ConfirmPassword = etConfirmPassword.getText().toString();
                    String email = etEmail.getText().toString().trim();
                    String zipcode = etZipPostal.getText().toString().trim();
                    String mobile = etMobile.getText().toString().trim();
                    String companyName = etCompanyName.getText().toString().trim();
                    String address = etAddress.getText().toString().trim();
                    String phone = etPhone.getText().toString().trim();
                    String country = spinnerCountyNames.getSelectedItem().toString();
                    String townCity = etTownCity.getText().toString().trim();
                    country = country.replaceAll(" ", "%20");
                    companyName = companyName.replaceAll(" ", "%20");
                    companyName = companyName.replaceAll("#","%23");
                    address = address.replaceAll(" ", "%20");
                    address = address.replaceAll("#","%23");
                    townCity= townCity.replaceAll(" ","%20");
                    townCity = townCity.replaceAll("#","%23");
                    //                    Toast.makeText(getActivity(), "" + address, Toast.LENGTH_SHORT).show();
                    if (etPhone.getText().toString().trim().length() != 0 && etAddress.getText().toString().trim().length() != 0 && etCompanyName.getText().toString().trim().length() != 0 && etMobile.getText().toString().trim().length() != 0 && etZipPostal.getText().toString().trim().length() != 0 && etPassword.getText().toString().trim().length() != 0 && etPassword.getText().toString().length() >= 6 && etEmail.getText().toString().trim().length() != 0 && etConfirmPassword.getText().toString().trim().length() != 0 && etTownCity.getText().toString().trim().length()!=0) {
                        if (isValidEmail(email)) {
                            if (Password.equals(ConfirmPassword)) {

                                Password = Password.replaceAll(" ", "%20");
                                Password = Password.replaceAll("#","%23");
                                UserSignUp(Password, email, zipcode, mobile, companyName, address, phone, country,townCity);

                            } else {
                                etConfirmPassword.setError("Password Mismatch");
                            }

                        } else {
                            etEmail.setError("Invalid Email Id");
                        }

                    } else {
                        if (etPassword.getText().toString().trim().length() == 0) {
                            etPassword.setError("Field can't be blank");
                        } else if (etPassword.getText().toString().length() < 6) {
                            etPassword.setError("Must have minimum 6 characters");
                        }
                        if (etEmail.getText().toString().trim().length() == 0) {
                            etEmail.setError("Field can't be blank");
                        }
                        if (etTownCity.getText().toString().trim().length()==0)
                        {
                            etTownCity.setError("Field can't be blank");
                        }
                        if (etPhone.getText().toString().trim().length() == 0) {
                            etPhone.setError("Field can't be blank");
                        }
                        if (etAddress.getText().toString().trim().length() == 0) {
                            etAddress.setError("Field can't be blank");
                        }
                        if (etCompanyName.getText().toString().trim().length() == 0) {
                            etCompanyName.setError("Field can't be blank");
                        }
                        if (etMobile.getText().toString().trim().length() == 0) {
                            etMobile.setError("Field can't be blank");
                        }
                        if (etZipPostal.getText().toString().trim().length() == 0) {
                            etZipPostal.setError("Field can't be blank");
                        }
                        if (etConfirmPassword.getText().toString().trim().length() == 0) {
                            etConfirmPassword.setError("Field can't be blank");
                        }
                    }
                } else {
                    DisplayDialog(getResources().getString(R.string.error_no_internet));
                }

            }
        });

        return view;
    }


    public void initView(View view) {
        etTownCity = (EditText)view.findViewById(R.id.etTownCity);
        btLogin = (Button) view.findViewById(R.id.btLogin);
        etEmail = (EditText) view.findViewById(R.id.etEmail);
        etZipPostal = (EditText) view.findViewById(R.id.etZipPostal);
        etMobile = (EditText) view.findViewById(R.id.etMobile);
        etCompanyName = (EditText) view.findViewById(R.id.etCompanyName);
        etPhone = (EditText) view.findViewById(R.id.etPhone);
        etAddress = (EditText) view.findViewById(R.id.etAddress);
        etPassword = (EditText) view.findViewById(R.id.etPassword);
        etConfirmPassword = (EditText) view.findViewById(R.id.etConfirmPassword);
        progressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        btSignup = (Button) view.findViewById(R.id.btSignup);
        spinnerCountyNames = (Spinner) view.findViewById(R.id.spinnerCountyNames);
    }

    public void initLestener() {
        btLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == btLogin) {
            startActivity(LogInActivity.getnewIntent(getActivity()));
            getActivity().overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
            getActivity().finish();
        }
    }

    private void UserSignUp(final String Password, final String emailId, final String zipcode,final String mobile,final String companyName,final String address,final String phone,final String country,final String towncity) {

        progressBar.setVisibility(View.VISIBLE);
        Ion.with(getActivity())
             .load(URL.SignUp + Password + "&emailid=" + emailId + "&companyname=" + companyName + "&country=" + country + "&address=" + address + "&zipcode=" + zipcode + "&mobile=" + mobile + "&phone=" + phone + "&towncity=" + towncity)
             .asJsonObject()
             .setCallback(new FutureCallback<JsonObject>() {
                 @Override

                 public void onCompleted(Exception e, JsonObject result) {

                     try {
                         String requestCode = result.get("resultCode").getAsString();
                         if (requestCode.equals("1")) {
                             btSignup.setClickable(true);
                             progressBar.setVisibility(View.INVISIBLE);
                             Toast.makeText(getActivity(), result.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                             startActivity(LogInActivity.getnewIntent(getActivity()));
                             getActivity().overridePendingTransition(R.anim.scale_center_in, R.anim.scale_center_out);
                             getActivity().finish();
                         } else {
                             btSignup.setClickable(true);
                             progressBar.setVisibility(View.INVISIBLE);
                             DisplayDialog(result.get("message").getAsString());

                         }
                     } catch (JsonParseException c) {

                         c.printStackTrace();
                     } catch (Exception e1) {

                         e1.printStackTrace();
                     }

                 }

             });
    }

    public String loadJSONFromAsset() {
        String json = null;
        try {
            InputStream is = getActivity().getAssets().open("Country.json");
            int size = is.available();
            byte[] buffer = new byte[size];
            is.read(buffer);
            is.close();
            json = new String(buffer, "UTF-8");
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;
    }

    public void getCountryNames() {
        try {
            JSONObject obj = new JSONObject(loadJSONFromAsset());
            JSONArray zones_jArry = obj.getJSONArray("data");
            countryNames = new String[zones_jArry.length()];
            for (int i = 0; i < zones_jArry.length(); i++) {
                JSONObject jo_inside = zones_jArry.getJSONObject(i);
                countryNames[i] = jo_inside.getString("CountryName");
            }
            ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, countryNames);
            spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item); // The drop down view
            spinnerCountyNames.setAdapter(spinnerArrayAdapter);

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
