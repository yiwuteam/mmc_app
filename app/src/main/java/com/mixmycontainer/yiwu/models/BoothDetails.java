package com.mixmycontainer.yiwu.models;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by ADMIN on 30-03-2017.
 */

public class BoothDetails extends RealmObject {
    private String OrderId;
    private String BoothId;
    private String BoothNumber;
    private String BoothName;
    private String Phone;
    private String BusinessScope;
    private String BusinessCard;
    private String ValuePurchase;
    private String ValuePurchaseRMB;
    private String BoothStatus;
    private String TradeId;
    private String HallId;

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getBoothId() {
        return BoothId;
    }

    public void setBoothId(String boothId) {
        BoothId = boothId;
    }

    public String getBoothStatus() {
        return BoothStatus;
    }

    public void setBoothStatus(String boothStatus) {
        BoothStatus = boothStatus;
    }

    public String getBoothNumber() {
        return BoothNumber;
    }

    public void setBoothNumber(String boothNumber) {
        BoothNumber = boothNumber;
    }

    public String getBoothName() {
        return BoothName;
    }

    public void setBoothName(String boothName) {
        BoothName = boothName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getBusinessCard() {
        return BusinessCard;
    }

    public void setBusinessCard(String businessCard) {
        BusinessCard = businessCard;
    }

    public String getBusinessScope() {
        return BusinessScope;
    }

    public void setBusinessScope(String businessScope) {
        BusinessScope = businessScope;
    }

    public String getValuePurchase() {
        return ValuePurchase;
    }

    public void setValuePurchase(String valuePurchase) {
        ValuePurchase = valuePurchase;
    }

    public String getValuePurchaseRMB() {
        return ValuePurchaseRMB;
    }

    public void setValuePurchaseRMB(String valuePurchaseRMB) {
        ValuePurchaseRMB = valuePurchaseRMB;
    }

    public String getTradeId() {
        return TradeId;
    }

    public void setTradeId(String tradeId) {
        TradeId = tradeId;
    }

    public String getHallId() {
        return HallId;
    }

    public void setHallId(String hallId) {
        HallId = hallId;
    }
}
