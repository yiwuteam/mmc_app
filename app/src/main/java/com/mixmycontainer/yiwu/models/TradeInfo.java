package com.mixmycontainer.yiwu.models;

import io.realm.RealmObject;

/**
 * Created by minusbug on 12/22/2016.
 */
public class TradeInfo extends RealmObject {
    private String Id;
    private String Name;
    private String CompanyId;
    private String Parent;

    public String getParent() {
        return Parent;
    }

    public void setParent(String parent) {
        Parent = parent;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }
}
