package com.mixmycontainer.yiwu.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by minusbug on 12/17/2016.
 */
public class CompanySetUpDetails extends RealmObject {

    @PrimaryKey
    private int CompanyID;
    private String CompanyName;
    private String Country;
    private String AddressType;
    private String Address;

    private String cityTown;
    private String ZipCode;
    private String Phone;
    private String Mobile;
    private String Fax;
    private String Skype;
    private String MsnID;
    private String QqID;
    private String OrderEmail;
    private String AgentEmail;
    private String MailOrder;
    private String ExchangeRate;
    private String Active3dCurrency;
    private String ThirdCurrency;
    private String ExchangeRate3dCurrency;
    private String TransilateFrom;
    private String TransilateTo;
    private String GrossweightCarton;
    private String Width;
    private String Height;
    private String Length;
    private String Volume;
    private String AgentContact;
    private String userToken;
    private RealmList<ContainerInfo> containerDetails;
    private RealmList<TradeInfo> tradeDeatils;
    private RealmList<GoldAgentInfo> goldAgentDeatils;
    private RealmList<BoothDetailsInfo> boothDetailsInfos;
    private String AgentId;
    private String AgentName;
    private String UserMail;

    public RealmList<BoothDetailsInfo> getBoothDetailsInfos() {
        return boothDetailsInfos;
    }

    public void setBoothDetailsInfos(RealmList<BoothDetailsInfo> boothDetailsInfos) {
        this.boothDetailsInfos = boothDetailsInfos;
    }

    public String getMailOrder() {
        return MailOrder;
    }

    public void setMailOrder(String mailOrder) {
        MailOrder = mailOrder;
    }

    public String getAgentId() {
        return AgentId;
    }

    public void setAgentId(String agentId) {
        AgentId = agentId;
    }

    public String getAgentName() {
        return AgentName;
    }

    public void setAgentName(String agentName) {
        AgentName = agentName;
    }

    public RealmList<GoldAgentInfo> getGoldAgentDeatils() {
        return goldAgentDeatils;
    }

    public void setGoldAgentDeatils(RealmList<GoldAgentInfo> goldAgentDeatils) {
        this.goldAgentDeatils = goldAgentDeatils;
    }

    public RealmList<TradeInfo> getTradeDeatils() {
        return tradeDeatils;
    }

    public void setTradeDeatils(RealmList<TradeInfo> tradeDeatils) {
        this.tradeDeatils = tradeDeatils;
    }

    public RealmList<ContainerInfo> getContainerDetails() {
        return containerDetails;
    }

    public void setContainerDetails(RealmList<ContainerInfo> containerDetails) {
        this.containerDetails = containerDetails;
    }
    public String getCityTown() {
        return cityTown;
    }

    public void setCityTown(String cityTown) {
        this.cityTown = cityTown;
    }


    public String getUserMail() {
        return UserMail;
    }

    public void setUserMail(String userMail) {
        UserMail = userMail;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getMsnID() {
        return MsnID;
    }

    public void setMsnID(String msnID) {
        MsnID = msnID;
    }

    public String getAgentEmail() {
        return AgentEmail;
    }

    public void setAgentEmail(String agentEmail) {
        AgentEmail = agentEmail;
    }

    public String getActive3dCurrency() {
        return Active3dCurrency;
    }

    public void setActive3dCurrency(String active3dCurrency) {
        Active3dCurrency = active3dCurrency;
    }

    public String getGrossweightCarton() {
        return GrossweightCarton;
    }

    public void setGrossweightCarton(String grossweightCarton) {
        GrossweightCarton = grossweightCarton;
    }

    public String getLength() {
        return Length;
    }

    public void setLength(String length) {
        Length = length;
    }

    public String getAgentContact() {
        return AgentContact;
    }

    public void setAgentContact(String agentContact) {
        AgentContact = agentContact;
    }

    public String getUserToken() {
        return userToken;
    }

    public void setUserToken(String userToken) {
        this.userToken = userToken;
    }

    public String getVolume() {
        return Volume;
    }

    public void setVolume(String volume) {
        Volume = volume;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String width) {
        Width = width;
    }

    public String getTransilateTo() {
        return TransilateTo;
    }

    public void setTransilateTo(String transilateTo) {
        TransilateTo = transilateTo;
    }

    public String getTransilateFrom() {
        return TransilateFrom;
    }

    public void setTransilateFrom(String transilateFrom) {
        TransilateFrom = transilateFrom;
    }

    public String getExchangeRate3dCurrency() {
        return ExchangeRate3dCurrency;
    }

    public void setExchangeRate3dCurrency(String exchangeRate3dCurrency) {
        ExchangeRate3dCurrency = exchangeRate3dCurrency;
    }

    public String getThirdCurrency() {
        return ThirdCurrency;
    }

    public void setThirdCurrency(String thirdCurrency) {
        ThirdCurrency = thirdCurrency;
    }

    public String getExchangeRate() {
        return ExchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        ExchangeRate = exchangeRate;
    }

    public String getOrderEmail() {
        return OrderEmail;
    }

    public void setOrderEmail(String orderEmail) {
        OrderEmail = orderEmail;
    }

    public String getQqID() {
        return QqID;
    }

    public void setQqID(String qqID) {
        QqID = qqID;
    }

    public String getSkype() {
        return Skype;
    }

    public void setSkype(String skype) {
        Skype = skype;
    }

    public String getFax() {
        return Fax;
    }

    public void setFax(String fax) {
        Fax = fax;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getAddressType() {
        return AddressType;
    }

    public void setAddressType(String addressType) {
        AddressType = addressType;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getZipCode() {
        return ZipCode;
    }

    public void setZipCode(String zipCode) {
        ZipCode = zipCode;
    }

    public int getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(int companyID) {
        CompanyID = companyID;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getCountry() {
        return Country;
    }

    public void setCountry(String country) {
        Country = country;
    }


}
