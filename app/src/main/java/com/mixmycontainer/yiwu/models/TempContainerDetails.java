package com.mixmycontainer.yiwu.models;

import io.realm.RealmObject;

/**
 * Created by ADMIN on 18-04-2017.
 */

public class TempContainerDetails extends RealmObject {

    private String ContainerId;
    private String ContainerType;
    private String ContainerName;
    private String MaximumVolume;
    private String MaximumWeight;
    private String FilledWeight;
    private String FilledVolume;
    private String Volumepercentage;
    private String Weightpercentage;
    private String Status;

    public String getMaximumWeight() {
        return MaximumWeight;
    }

    public void setMaximumWeight(String maximumWeight) {
        MaximumWeight = maximumWeight;
    }

    public String getContainerName() {
        return ContainerName;
    }

    public void setContainerName(String containerName) {
        ContainerName = containerName;
    }

    public String getMaximumVolume() {
        return MaximumVolume;
    }

    public void setMaximumVolume(String maximumVolume) {
        MaximumVolume = maximumVolume;
    }

    public String getContainerId() {
        return ContainerId;
    }

    public void setContainerId(String containerId) {
        ContainerId = containerId;
    }

    public String getContainerType() {
        return ContainerType;
    }

    public void setContainerType(String containerType) {
        ContainerType = containerType;
    }

    public String getFilledWeight() {
        return FilledWeight;
    }

    public void setFilledWeight(String filledWeight) {
        FilledWeight = filledWeight;
    }

    public String getFilledVolume() {
        return FilledVolume;
    }

    public void setFilledVolume(String filledVolume) {
        FilledVolume = filledVolume;
    }

    public String getVolumepercentage() {
        return Volumepercentage;
    }

    public void setVolumepercentage(String volumepercentage) {
        Volumepercentage = volumepercentage;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getWeightpercentage() {
        return Weightpercentage;
    }

    public void setWeightpercentage(String weightpercentage) {
        Weightpercentage = weightpercentage;
    }

}
