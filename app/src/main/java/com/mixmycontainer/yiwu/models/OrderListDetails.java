package com.mixmycontainer.yiwu.models;

import io.realm.RealmList;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ADMIN on 30-03-2017.
 */

public class OrderListDetails extends RealmObject {

    private int CompanyID;
    @PrimaryKey
    private String OrderId;
    private String OrderNumber;
    private String Trade;

    private String TradeName;
    private String ProductType;

    private String TotalOrderUsValue;
    private String TotalOrderRmbValue;
    private String TotalOrderZarValue;
    private String Total3rdCurrencyValue;

    private String TotalCartons;
    private String TotalBooth;
    private String TotalContainer;
    private String TotalOrderWeight;

    private String TotalOrderCBM;
    private String OrderStatus;
    private String LastUpdate;
    private String Synch;
    private RealmList<BoothDetails> boothDetailses;
    private RealmList<ContainerDetails> containerDetailses;
    private RealmList<ItemDetails> itemDetailses;


    public String getSynch() {
        return Synch;
    }

    public void setSynch(String synch) {
        Synch = synch;
    }

    public String getTotal3rdCurrencyValue() {
        return Total3rdCurrencyValue;
    }

    public void setTotal3rdCurrencyValue(String total3rdCurrencyValue) {
        Total3rdCurrencyValue = total3rdCurrencyValue;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getTrade() {
        return Trade;
    }

    public void setTrade(String trade) {
        Trade = trade;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getTotalOrderRmbValue() {
        return TotalOrderRmbValue;
    }

    public void setTotalOrderRmbValue(String totalOrderRmbValue) {
        TotalOrderRmbValue = totalOrderRmbValue;
    }

    public String getTotalOrderUsValue() {
        return TotalOrderUsValue;
    }

    public void setTotalOrderUsValue(String totalOrderUsValue) {
        TotalOrderUsValue = totalOrderUsValue;
    }

    public String getTotalOrderZarValue() {
        return TotalOrderZarValue;
    }

    public void setTotalOrderZarValue(String totalOrderZarValue) {
        TotalOrderZarValue = totalOrderZarValue;
    }

    public String getTotalCartons() {
        return TotalCartons;
    }

    public void setTotalCartons(String totalCartons) {
        TotalCartons = totalCartons;
    }

    public String getTotalBooth() {
        return TotalBooth;
    }

    public void setTotalBooth(String totalBooth) {
        TotalBooth = totalBooth;
    }

    public String getTotalContainer() {
        return TotalContainer;
    }

    public void setTotalContainer(String totalContainer) {
        TotalContainer = totalContainer;
    }

    public String getTotalOrderWeight() {
        return TotalOrderWeight;
    }

    public void setTotalOrderWeight(String totalOrderWeight) {
        TotalOrderWeight = totalOrderWeight;
    }

    public String getTotalOrderCBM() {
        return TotalOrderCBM;
    }

    public void setTotalOrderCBM(String totalOrderCBM) {
        TotalOrderCBM = totalOrderCBM;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getLastUpdate() {
        return LastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        LastUpdate = lastUpdate;
    }

    public RealmList<BoothDetails> getBoothDetailses() {
        return boothDetailses;
    }

    public void setBoothDetailses(RealmList<BoothDetails> boothDetailses) {
        this.boothDetailses = boothDetailses;
    }

    public RealmList<ContainerDetails> getContainerDetailses() {
        return containerDetailses;
    }

    public void setContainerDetailses(RealmList<ContainerDetails> containerDetailses) {
        this.containerDetailses = containerDetailses;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public int getCompanyID() {

        return CompanyID;
    }

    public void setCompanyID(int companyID) {
        CompanyID = companyID;
    }

    public String getTradeName() {
        return TradeName;
    }

    public void setTradeName(String tradeName) {
        TradeName = tradeName;
    }

    public RealmList<ItemDetails> getItemDetailses() {
        return itemDetailses;
    }

    public void setItemDetailses(RealmList<ItemDetails> itemDetailses) {
        this.itemDetailses = itemDetailses;
    }

}
