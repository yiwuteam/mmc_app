package com.mixmycontainer.yiwu.models;

/**
 * Created by ADMIN on 29-12-2016.
 */

public class SummeryInfo {

    private String OrderNumber;
    private String TotalOrderUsValue;
    private String TotalOrderRmbValue;
    private String ValuePurchase;
    private String ValuePurchaseRMB;
    private String TotalOrderCBM;
    private String TotalOrderWeight;
    private String Volumepercentage;
    private String Weightpercentage;


    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getTotalOrderUsValue() {
        return TotalOrderUsValue;
    }

    public void setTotalOrderUsValue(String totalOrderUsValue) {
        TotalOrderUsValue = totalOrderUsValue;
    }

    public String getTotalOrderRmbValue() {
        return TotalOrderRmbValue;
    }

    public void setTotalOrderRmbValue(String totalOrderRmbValue) {
        TotalOrderRmbValue = totalOrderRmbValue;
    }

    public String getValuePurchase() {
        return ValuePurchase;
    }

    public void setValuePurchase(String valuePurchase) {
        ValuePurchase = valuePurchase;
    }

    public String getValuePurchaseRMB() {
        return ValuePurchaseRMB;
    }

    public void setValuePurchaseRMB(String valuePurchaseRMB) {
        ValuePurchaseRMB = valuePurchaseRMB;
    }

    public String getTotalOrderCBM() {
        return TotalOrderCBM;
    }

    public void setTotalOrderCBM(String totalOrderCBM) {
        TotalOrderCBM = totalOrderCBM;
    }

    public String getTotalOrderWeight() {
        return TotalOrderWeight;
    }

    public void setTotalOrderWeight(String totalOrderWeight) {
        TotalOrderWeight = totalOrderWeight;
    }

    public String getVolumepercentage() {
        return Volumepercentage;
    }

    public void setVolumepercentage(String volumepercentage) {
        Volumepercentage = volumepercentage;
    }

    public String getWeightpercentage() {
        return Weightpercentage;
    }

    public void setWeightpercentage(String weightpercentage) {
        Weightpercentage = weightpercentage;
    }
}
