package com.mixmycontainer.yiwu.models;

import io.realm.RealmObject;

/**
 * Created by ADMIN on 13-04-2017.
 */

public class BoothDetailsInfo extends RealmObject {
    private String BoothNumber;
    private String BoothName;
    private String Phone;
    private String BusinessScope;
    private String BusinessCard;


    public String getBoothNumber() {
        return BoothNumber;
    }

    public void setBoothNumber(String boothNumber) {
        BoothNumber = boothNumber;
    }

    public String getBoothName() {
        return BoothName;
    }

    public void setBoothName(String boothName) {
        BoothName = boothName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getBusinessScope() {
        return BusinessScope;
    }

    public void setBusinessScope(String businessScope) {
        BusinessScope = businessScope;
    }

    public String getBusinessCard() {
        return BusinessCard;
    }

    public void setBusinessCard(String businessCard) {
        BusinessCard = businessCard;
    }
}
