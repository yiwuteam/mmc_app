package com.mixmycontainer.yiwu.models;

import io.realm.RealmObject;

/**
 * Created by minusbug on 12/22/2016.
 */
public class ContactInfo extends RealmObject {
    private String Id;
    private String GoldAgentId;
    private String Name;
    private String Email;
    private String Mobile;
    private String Skype;
    private String ImageName;

    public String getImageName() {
        return ImageName;
    }

    public void setImageName(String imageName) {
        ImageName = imageName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getGoldAgentId() {
        return GoldAgentId;
    }

    public void setGoldAgentId(String goldAgentId) {
        GoldAgentId = goldAgentId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getMobile() {
        return Mobile;
    }

    public void setMobile(String mobile) {
        Mobile = mobile;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getSkype() {
        return Skype;
    }

    public void setSkype(String skype) {
        Skype = skype;
    }



}
