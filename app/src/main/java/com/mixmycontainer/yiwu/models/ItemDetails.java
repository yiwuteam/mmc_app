package com.mixmycontainer.yiwu.models;

import io.realm.RealmObject;

/**
 * Created by ADMIN on 30-03-2017.
 */

public class ItemDetails extends RealmObject {
    private String orderId;
    private String BoothId;
    private String ContainerId;
    private String ItemId;
    private String ItemNo;
    private String DescriptionFrom;
    private String DescriptionTo;
    private String UnitperCarton;
    private String WeightofCarton;
    private String Length;
    private String Width;
    private String Height;
    private String CBM;
    private String Picture;
    private String PriceInUs;
    private String PriceInRmb;
    private String ThirdCurrencyType;
    private String PriceInThirdCurrency;
    private String UnitQuantity;
    private String CartonQuantity;
    private String TotalPriceInUS;
    private String TotalPriceInRMB;
    private String TotalPriceIn3rdCurrency;
    private String IsEditFinished;
    private String NoOfCartonFilled;
    private String Date;

    public String getContainerId() {
        return ContainerId;
    }

    public void setContainerId(String containerId) {
        ContainerId = containerId;
    }

    public String getBoothId() {
        return BoothId;
    }

    public void setBoothId(String boothId) {
        BoothId = boothId;
    }

    public String getOrderId() {
        return orderId;
    }

    public String getIsEditFinished() {
        return IsEditFinished;
    }

    public void setIsEditFinished(String isEditFinished) {
        IsEditFinished = isEditFinished;
    }

    public String getNoOfCartonFilled() {
        return NoOfCartonFilled;
    }

    public void setNoOfCartonFilled(String noOfCartonFilled) {
        NoOfCartonFilled = noOfCartonFilled;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getUnitperCarton() {
        return UnitperCarton;
    }

    public void setUnitperCarton(String unitperCarton) {
        UnitperCarton = unitperCarton;
    }

    public String getWeightofCarton() {
        return WeightofCarton;
    }

    public void setWeightofCarton(String weightofCarton) {
        WeightofCarton = weightofCarton;
    }

    public String getLength() {
        return Length;
    }

    public void setLength(String length) {
        Length = length;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String width) {
        Width = width;
    }

    public String getCBM() {
        return CBM;
    }

    public void setCBM(String CBM) {
        this.CBM = CBM;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }

    public String getPriceInUs() {
        return PriceInUs;
    }

    public void setPriceInUs(String priceInUs) {
        PriceInUs = priceInUs;
    }

    public String getPriceInRmb() {
        return PriceInRmb;
    }

    public void setPriceInRmb(String priceInRmb) {
        PriceInRmb = priceInRmb;
    }

    public String getPriceInThirdCurrency() {
        return PriceInThirdCurrency;
    }

    public void setPriceInThirdCurrency(String priceInThirdCurrency) {
        PriceInThirdCurrency = priceInThirdCurrency;
    }

    public String getTotalPriceInUS() {
        return TotalPriceInUS;
    }

    public void setTotalPriceInUS(String totalPriceInUS) {
        TotalPriceInUS = totalPriceInUS;
    }

    public String getTotalPriceIn3rdCurrency() {
        return TotalPriceIn3rdCurrency;
    }

    public void setTotalPriceIn3rdCurrency(String totalPriceIn3rdCurrency) {
        TotalPriceIn3rdCurrency = totalPriceIn3rdCurrency;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getTotalPriceInRMB() {
        return TotalPriceInRMB;
    }

    public void setTotalPriceInRMB(String totalPriceInRMB) {
        TotalPriceInRMB = totalPriceInRMB;
    }

    public String getUnitQuantity() {
        return UnitQuantity;
    }

    public void setUnitQuantity(String unitQuantity) {
        UnitQuantity = unitQuantity;
    }

    public String getCartonQuantity() {
        return CartonQuantity;
    }

    public void setCartonQuantity(String cartonQuantity) {
        CartonQuantity = cartonQuantity;
    }

    public String getThirdCurrencyType() {
        return ThirdCurrencyType;
    }

    public void setThirdCurrencyType(String thirdCurrencyType) {
        ThirdCurrencyType = thirdCurrencyType;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getDescriptionTo() {
        return DescriptionTo;
    }

    public void setDescriptionTo(String descriptionTo) {
        DescriptionTo = descriptionTo;
    }

    public String getDescriptionFrom() {
        return DescriptionFrom;
    }

    public void setDescriptionFrom(String descriptionFrom) {
        DescriptionFrom = descriptionFrom;
    }

    public String getItemNo() {
        return ItemNo;
    }

    public void setItemNo(String itemNo) {
        ItemNo = itemNo;
    }

    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }

}
