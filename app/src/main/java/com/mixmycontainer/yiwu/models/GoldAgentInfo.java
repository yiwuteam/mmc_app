package com.mixmycontainer.yiwu.models;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by minusbug on 12/22/2016.
 */
public class GoldAgentInfo extends RealmObject {
    private String Id;
    private String CompanyName;
    private String OfficeAddress;
    private String PhoneNumber;
    private String BusinessYear;
    private String GoldAgentYears;
    private String FaxNumber;
    private RealmList<ContactInfo> contact;


    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getFaxNumber() {
        return FaxNumber;
    }

    public void setFaxNumber(String faxNumber) {
        FaxNumber = faxNumber;
    }

    public RealmList<ContactInfo> getContact() {
        return contact;
    }

    public void setContact(RealmList<ContactInfo> contact) {
        this.contact = contact;
    }

    public String getGoldAgentYears() {
        return GoldAgentYears;
    }

    public void setGoldAgentYears(String goldAgentYears) {
        GoldAgentYears = goldAgentYears;
    }

    public String getBusinessYear() {
        return BusinessYear;
    }

    public void setBusinessYear(String businessYear) {
        BusinessYear = businessYear;
    }

    public String getPhoneNumber() {
        return PhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        PhoneNumber = phoneNumber;
    }

    public String getOfficeAddress() {
        return OfficeAddress;
    }

    public void setOfficeAddress(String officeAddress) {
        OfficeAddress = officeAddress;
    }

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }
}
