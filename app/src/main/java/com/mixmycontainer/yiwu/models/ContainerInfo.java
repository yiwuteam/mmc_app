package com.mixmycontainer.yiwu.models;

import io.realm.RealmObject;

/**
 * Created by AMRITHA on 12/22/2016.
 */
public class ContainerInfo extends RealmObject {
    private String ID;
    private String Container;
    private String MaximumVolume;
    private String MaximumWeight;

    public String getMaximumWeight() {
        return MaximumWeight;
    }

    public void setMaximumWeight(String maximumWeight) {
        MaximumWeight = maximumWeight;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getContainer() {
        return Container;
    }

    public void setContainer(String container) {
        Container = container;
    }

    public String getMaximumVolume() {
        return MaximumVolume;
    }

    public void setMaximumVolume(String maximumVolume) {
        MaximumVolume = maximumVolume;
    }

}
