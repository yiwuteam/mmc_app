package com.mixmycontainer.yiwu.models;

import io.realm.RealmObject;

/**
 * Created by ADMIN on 30-03-2017.
 */

public class ContainerDetails extends RealmObject {
    private String orderId;
    private String ContainerId;
    private String ContainerType;
    private String containerName;
    private String FilledWeight;
    private String FilledVolume;
    private String Volumepercentage;
    private String Weightpercentage;
    private String MaximumVolume;
    private String MaximumWeight;
    private String Status;

    public String getContainerName() {
        return containerName;
    }

    public void setContainerName(String containerName) {
        this.containerName = containerName;
    }

    public String getMaximumVolume() {
        return MaximumVolume;
    }

    public void setMaximumVolume(String maximumVolume) {
        MaximumVolume = maximumVolume;
    }

    public String getMaximumWeight() {
        return MaximumWeight;
    }

    public void setMaximumWeight(String maximumWeight) {
        MaximumWeight = maximumWeight;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getStatus() {

        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getWeightpercentage() {

        return Weightpercentage;
    }

    public void setWeightpercentage(String weightpercentage) {
        Weightpercentage = weightpercentage;
    }

    public String getVolumepercentage() {

        return Volumepercentage;
    }

    public void setVolumepercentage(String volumepercentage) {
        Volumepercentage = volumepercentage;
    }

    public String getFilledVolume() {

        return FilledVolume;
    }

    public void setFilledVolume(String filledVolume) {
        FilledVolume = filledVolume;
    }

    public String getFilledWeight() {

        return FilledWeight;
    }

    public void setFilledWeight(String filledWeight) {
        FilledWeight = filledWeight;
    }

    public String getContainerType() {

        return ContainerType;
    }

    public void setContainerType(String containerType) {
        ContainerType = containerType;
    }

    public String getContainerId() {

        return ContainerId;
    }

    public void setContainerId(String containerId) {
        ContainerId = containerId;
    }
}
