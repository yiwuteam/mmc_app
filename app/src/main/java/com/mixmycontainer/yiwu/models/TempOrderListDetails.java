package com.mixmycontainer.yiwu.models;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by ADMIN on 18-04-2017.
 */

public class TempOrderListDetails extends RealmObject {
    private String OrderId;
    private int CompanyID;
    private RealmList<TempContainerDetails> containerDetailsesTemp;


    public RealmList<TempContainerDetails> getContainerDetailsesTemp() {
        return containerDetailsesTemp;
    }

    public void setContainerDetailsesTemp(RealmList<TempContainerDetails> containerDetailsesTemp) {
        this.containerDetailsesTemp = containerDetailsesTemp;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public int getCompanyID() {
        return CompanyID;
    }

    public void setCompanyID(int companyID) {
        CompanyID = companyID;
    }
}
