package com.mixmycontainer.yiwu.utiles;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;

import com.mixmycontainer.yiwu.R;


/**
 * Created by Sanif on 13-03-2016.
 */
public class PaginationUtil {
    private final LinearLayoutManager mLayoutManager;
    public int previousTotal = 0;
    private boolean loading = true;
    private int visibleThreshold = 1;
    int firstVisibleItem, visibleItemCount, totalItemCount;


    RecyclerView.OnScrollListener scrollListener = new RecyclerView.OnScrollListener() {
        @Override
        public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
            super.onScrolled(recyclerView, dx, dy);

            visibleItemCount = recyclerView.getChildCount();
            totalItemCount = mLayoutManager.getItemCount();
            firstVisibleItem = mLayoutManager.findFirstVisibleItemPosition();

            if (loading) {
                if (totalItemCount > previousTotal) {
                    loading = false;
                    previousTotal = totalItemCount;
                }
            }

            Log.d("PaginationUtil", "visibleItemCount:" + visibleItemCount);
            Log.d("PaginationUtil", "totalItemCount:" + totalItemCount);
            Log.d("PaginationUtil", "firstVisibleItem:" + firstVisibleItem);
            Log.d("PaginationUtil", "previousTotal:" + previousTotal);
            if (!loading && (totalItemCount - visibleItemCount)
                    <= (firstVisibleItem + visibleThreshold)) {

                Log.d("PaginationUtil", "Condition Success");
                if (paginationListener != null) {
                    paginationListener.onEndReached();
                }

                loading = true;
            }
        }
    };

    public static void setPreviousTotal(RecyclerView recyclerView, int total) {
        PaginationUtil paginationUtil = (PaginationUtil) recyclerView.getTag(R.id.pagination_util);
        if (paginationUtil != null) {
            paginationUtil.previousTotal = total;
        }
    }

    public interface PaginationListener {
        void onEndReached();
    }

    private RecyclerView recyclerView;
    private PaginationListener paginationListener;

    private PaginationUtil(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
        this.recyclerView.setTag(R.id.pagination_util, this);
        try {
            this.mLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        } catch (ClassCastException e) {
            throw new RuntimeException("RecyclerView should use LinearLayoutManager");
        }
        initRecyclerView();
    }

    private void initRecyclerView() {
        recyclerView.addOnScrollListener(scrollListener);
    }

    public static PaginationUtil addTo(RecyclerView recyclerView) {
        PaginationUtil paginationUtil = (PaginationUtil) recyclerView.getTag(R.id.pagination_util);
        PaginationUtil recyclerPagination = null;
        if (paginationUtil == null) {
            paginationUtil = new PaginationUtil(recyclerView);
        }
        return paginationUtil;
    }

    public void setPaginationListener(PaginationListener paginationListener) {
        this.paginationListener = paginationListener;
    }

    public static void removeFrom(RecyclerView recyclerView) {
        PaginationUtil paginationUtil = (PaginationUtil) recyclerView.getTag(R.id.pagination_util);
        if (paginationUtil != null) {
            paginationUtil.detach();
        }
    }

    private void detach() {
        Log.d("DETACHS", "detach() called with: " + "");
        recyclerView.removeOnScrollListener(scrollListener);
        recyclerView.setTag(R.id.pagination_util, null);
    }
}
