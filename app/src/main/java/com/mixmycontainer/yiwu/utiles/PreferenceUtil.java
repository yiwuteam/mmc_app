package com.mixmycontainer.yiwu.utiles;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by Sanif on 11-02-2016.
 * Usage:
 * PreferenceUtil.edit(getActivity()).putString(PreferenceUtil.EMAIL, "sanifss@gmail.com").commit();
 * OR
 * SharedPreferences.Editor editor = PreferenceUtil.edit(getActivity());
 * editor.putString(PreferenceUtil.EMAIL, "sanifss@gmail.com");
 * editor.putBoolean(PreferenceUtil.IS_LOGGED_IN, true);
 * editor.commit();
 */
public class PreferenceUtil {
    //PREFERENCES
    public static final String PREF_FILE = "MIX_MY_CONTAINER";
    ///Permision
    public static final String CAMERA_PERMISSION = "CAMERA_PERMISSION";


    public static final String USER_AUTH_TOKEN = "AUTH_TOKEN";
    public static final String FLAG = "FLAG";
    public static final String USER_NAME = "USER_NAME";
    public static final String USER_PIC = "USER_PIC";
    public static final String USER_PASSWORD = "USER_PASSWORD";
    public static final String USER_CBM = "USER_CBM";
    public static final String USER_COMPANY_SETUP_STATUS = "USER_COMPANY_SETUP_STATUS";
    public static final String USER_COMPANY_ID = "USER_COMPANY_ID";
    public static final String USER_AGENT_ID = "USER_AGENT_ID";
    public static final String USER_AGENT_NAME = "USER_AGENT_NAME";
    public static final String ORDER_ADD = "ORDER_ADD";

    public static final String USER_OTP = "USER_OTP";
    public static final String USER_OTP_email = "USER_OTP_email";

    public static final String USER_ORDER_ID = "USER_ORDER_ID";
    public static final String USER_CONTAINER_ID = "USER_CONTAINER_ID";
    public static final String USER_BOOTH_ID = "USER_BOOTH_ID";
    public static final String USER_ITEMDETAIL_ID = "USER_ITEMDETAIL_ID";
    public static final String USER_ORDER_NUMBER = "USER_ORDER_NUMBER";
    public static final String USER_TOTAL_CBM = "USER_TOTAL_CBM";
    public static final String TOTAL_USD = "TOTAL_USD";
    public static final String TOTAL_RMB = "TOTAL_RMB";
    public static final String TRANSLATE_TO = "TRANSLATE_TO";
    public static final String TRANSLATE_FROM = "TRANSLATE_FROM";
    public static final String CONTAINER_TYPE = "CONTAINER_TYPE";

    public static final String GrossWeight = "GrossWeight";
    public static final String ContinueItemAdd = "ContinueItemAdd";

    public static final String EMAIL_SETTINGS = "EMAIL_SETTINGS";
    public static final String LANGUAGE = "LANGUAGE";
    public static final String LOCALE = "LOCALE";


    public static final String VALUE_OF_PURCHASE = "VALUE_OF_PURCHASE";
    public static final String VALUE_OF_PURCHASE_RMB = "VALUE_OF_PURCHASE_RMB";
    public static final String TOTAL_ORDER_CBM = "TOTAL_ORDER_CBM";
    public static final String TOTAL_ORDER_WEIGHT = "TOTAL_ORDER_WEIGHT";
    public static final String TOTAL_VOLUME_PERCENTAGE = "TOTAL_VOLYM_PERCENTAGE";
    public static final String TOTAL_WEIGHT_PERCENTAGE = "TOTAL_WEIGHT_PERCENTAGE";
    public static final String USEDVOLUME = "USEDVOLUME";
    public static final String USEDWEIGHT = "USEDWEIGHT";

    // local database

    public static final String ORDER_ID = "ORDER_ID";
    public static final String BOOTH_ID = "BOOTH_ID";
    public static final String ITEM_ID = "ITEM_ID";
    public static final String CONTAINER_ID = "CONTAINER_ID";
    public static final String CONTAINER_type = "CONTAINER_type";

    public static final String CONTAINER_COUNT = "0";

    public static final String BAL_WEIGHT="BAL_WEIGHT";
    public static final String BAL_VOLUME="BAL_VOLUME";
    public static final String USED_VOLUME="USED_VOLUME";
    public static final String USED_WEIGHT="USED_WEIGHT";

    public static final String MX_WEIGHT="MX_WEIGHT";
    public static final String MX_VOLUME="MX_VOLUME";
    public static final String CONTAINER_NAME="CONTAINER_NAME";

    public static final String CONTAINER_ID_NEW="CONTAINER_ID_NEW";
    //Preference variable used for edit
    public static final String EDIT_ITEM_ID = "EDIT_ITEM_ID";
    public static  final String EDIT_BOOTH_ID = "EDIT_BOOTH_ID";

    public static  final String DELETE_ITEM = "DELETE_ITEM";





    public static SharedPreferences.Editor edit(Context context) {
        return get(context).edit();
    }

    public static SharedPreferences get(Context context) {
        return context.getSharedPreferences(PREF_FILE, Context.MODE_PRIVATE);
    }


}
