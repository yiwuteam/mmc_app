package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.fragment.OtpConfirmFragment;

/**
 * Created by minusbug on 12/28/2016.
 */

public class OtpConfirmActivity extends BaseActivity {

    public static Intent getnewIntent(Context context) {
        Intent intent = new Intent(context, OtpConfirmActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_otp_confirm);
        if (savedInstanceState == null) {
            addFragment(OtpConfirmFragment.newInstnce(), false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);
    }
}
