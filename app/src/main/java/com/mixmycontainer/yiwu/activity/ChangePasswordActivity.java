package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.fragment.ChangePasswordFragment;

/**
 * Created by ADMIN on 14-11-2016.
 */
public class ChangePasswordActivity extends BaseActivity {

    public static Intent getnewIntent(Context context) {
        Intent intent = new Intent(context, ChangePasswordActivity.class);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changepassword);
        if (savedInstanceState == null) {
            addFragment(ChangePasswordFragment.newInstsnce(), false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);

    }

}
