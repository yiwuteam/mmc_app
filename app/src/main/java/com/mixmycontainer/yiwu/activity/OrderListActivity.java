package com.mixmycontainer.yiwu.activity;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.JsonObject;
import com.iceteck.silicompressorr.SiliCompressor;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.fragment.OrderListFragment;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import java.io.ByteArrayOutputStream;
import java.io.File;

public class OrderListActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    ActionBarDrawerToggle toggle;
    DrawerLayout drawer;
    Toolbar toolbar;
    FrameLayout container;
    Bitmap bitmap;
    String filePath, fileNameToSend = "0";
    private View headerView;
    private TextView tvUsername, tvEmailid, iveditimage;
    private ImageView ivProfileImage;

    public static Intent getnewInstance(Context context) {
        Intent intent = new Intent(context, OrderListActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_orderlist);

        if (ContextCompat.checkSelfPermission(OrderListActivity.this,
                Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(OrderListActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                // we can request the permission.
                ActivityCompat.requestPermissions(OrderListActivity.this,
                        new String[]{Manifest.permission.INTERNET, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_NETWORK_STATE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.CAMERA},
                        11);
            }
        }

            if (savedInstanceState == null) {
                addFragment(OrderListFragment.newInstence(), false);
            }
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            container = (FrameLayout) findViewById(R.id.container);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            toggle = new ActionBarDrawerToggle(
                    this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
                @Override
                public void onDrawerSlide(View drawerView, float slideOffset) {
                    super.onDrawerSlide(drawerView, slideOffset);
                    container.setTranslationX(slideOffset * 200);
                    drawer.bringChildToFront(drawerView);
                    drawer.requestLayout();
                }
            };
            drawer.setDrawerListener(toggle);
            NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
            headerView = navigationView.getHeaderView(0);
            tvUsername = (TextView) headerView.findViewById(R.id.tvUsername);
            tvEmailid = (TextView) headerView.findViewById(R.id.tvEmailid);
            iveditimage = (TextView) headerView.findViewById(R.id.iveditimage);
            ivProfileImage = (ImageView) headerView.findViewById(R.id.ivProfileImage);
            tvUsername.setText(PreferenceUtil.get(OrderListActivity.this).getString(PreferenceUtil.USER_NAME, null));
            iveditimage
                    .setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                                if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                                    if (ActivityCompat.shouldShowRequestPermissionRationale(OrderListActivity.this,
                                            Manifest.permission.CAMERA)) {
                                        ActivityCompat.requestPermissions(OrderListActivity.this,
                                                new String[]{Manifest.permission.CAMERA},
                                                11);
                                    } else {
                                        // we can request the permission.
//                        Toast.makeText(getContext(), "Requesting permission", Toast.LENGTH_SHORT).show();
                                        ActivityCompat.requestPermissions(OrderListActivity.this,
                                                new String[]{Manifest.permission.CAMERA},
                                                11);
                                    }
                                } else {

                                    CaptureImageDialog();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                            }

                        }
                    });
            String picurl = PreferenceUtil.get(OrderListActivity.this).getString(PreferenceUtil.USER_PIC, "0");
            if (!picurl.equals("0")) {
                Glide.with(OrderListActivity.this).
                        load(PreferenceUtil.get(OrderListActivity.this).getString(PreferenceUtil.USER_PIC, null))
                        .into(ivProfileImage);
            }



            navigationView.setNavigationItemSelectedListener(this);



    }

    public void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) {
        super.onActivityResult(requestCode, resultCode, imageReturnedIntent);
        switch (requestCode) {
            case 0:
                if (resultCode == RESULT_OK) {
                    bitmap = (Bitmap) imageReturnedIntent.getExtras().get("data");
                    ivProfileImage.setImageBitmap(bitmap);
                    Uri tempUri = getImageUri(OrderListActivity.this, bitmap);
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    filePath = SiliCompressor.with(OrderListActivity.this).compress(String.valueOf(tempUri));
                    File filtoSend = new File(filePath);
                    if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                        uploadimage(filtoSend);
                    } else {
                        Toast.makeText(getApplicationContext(), "Check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                    }
                }

                break;
            case 1:
                if (resultCode == RESULT_OK) {
                    Uri selectedImage = imageReturnedIntent.getData();
                    ivProfileImage.setImageURI(selectedImage);
                    // CALL THIS METHOD TO GET THE ACTUAL PATH
                    filePath = SiliCompressor.with(OrderListActivity.this).compress(String.valueOf(selectedImage));
                    File filtoSend = new File(filePath);

                    if (AppUtils.isNetworkAvailable(getApplicationContext())) {
                        uploadimage(filtoSend);
                    } else {
                        Toast.makeText(getApplicationContext(), "Check your internet connection and try again.", Toast.LENGTH_SHORT).show();
                    }
                }
                break;
        }

    }

    public void uploadimage(final File attachment) {
        Ion.with(OrderListActivity.this)
                .load(URL.UploadItemImage)
                .uploadProgressHandler(new ProgressCallback() {
                    @Override
                    public void onProgress(long downloaded, long total) {

                    }
                })
                .setMultipartFile("image", attachment)
                .setMultipartParameter("type", "2")
                .setMultipartParameter("token", PreferenceUtil.get(OrderListActivity.this).getString(PreferenceUtil.USER_AUTH_TOKEN, null))
                .asJsonObject()
                .setCallback(new FutureCallback<JsonObject>() {
                    @Override
                    public void onCompleted(Exception e, JsonObject result) {
                        if (result.get("resultCode").getAsString().equals("0")) {
                            Toast.makeText(OrderListActivity.this, result.get("message").getAsString() + "", Toast.LENGTH_SHORT).show();


                        } else {
                            fileNameToSend = result.get("filename").getAsString();
                            SharedPreferences.Editor pref = PreferenceUtil.edit(OrderListActivity.this);
                            pref.putString(PreferenceUtil.USER_PIC, fileNameToSend);
                            pref.apply();
                            Toast.makeText(OrderListActivity.this,"Profile image updated successfully" + "", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    private void CaptureImageDialog() {
        final Dialog dialog = new Dialog(OrderListActivity.this);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
//        dialog.getWindow().getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        DisplayMetrics display = getResources().getDisplayMetrics();
        int width = display.widthPixels;
        int height = display.heightPixels;
        dialog.setContentView(R.layout.dialog_image_capture);
        TextView btn_takePhoto = (TextView) dialog.findViewById(R.id.btn_takePhoto);
        TextView btn_captureImage = (TextView) dialog.findViewById(R.id.btn_captureImage);

        btn_takePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent takePicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(takePicture, 0);
                dialog.cancel();
            }
        });
        btn_captureImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                        android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(pickPhoto, 1);
                dialog.cancel();
            }
        });
        dialog.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 11: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    SharedPreferences.Editor pref = PreferenceUtil.edit(OrderListActivity.this);
                    pref.putInt(PreferenceUtil.CAMERA_PERMISSION, 1);
                     pref.apply();
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                    SharedPreferences.Editor pref = PreferenceUtil.edit(OrderListActivity.this);
                    pref.putInt(PreferenceUtil.CAMERA_PERMISSION, 0);
                    pref.apply();
                }
                return;
            }


            // other 'case' lines to check for other
            // permissions this app might request
        }
    }


    @Override
    public void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        toggle.syncState();
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);

            getSupportActionBar().setHomeAsUpIndicator(R.mipmap.ic_hamburger_white);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);
            finish();
        }

    }
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
//        return true;
//    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (toggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_orderlist) {

        } else if (id == R.id.nav_companysetup) {
            startActivity(CompanySetupActcivity.getnewIntent(OrderListActivity.this));
            overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        } else if (id == R.id.nav_language) {
            startActivity(LanguageTimeActivity.getnewIntent(OrderListActivity.this));
            overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        } else if (id == R.id.nav_changepassword) {
            startActivity(ChangePasswordActivity.getnewIntent(OrderListActivity.this));
            overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
        } else if (id == R.id.nav_logout) {
            SharedPreferences.Editor PREF = PreferenceUtil.edit(getApplicationContext());
            PREF.clear();
            PREF.commit();
            startActivity(LogInActivity.getnewIntent(OrderListActivity.this));
            finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return false;
    }

}
