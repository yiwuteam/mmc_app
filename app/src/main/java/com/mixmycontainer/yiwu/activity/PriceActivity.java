package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.bus.events.SummeryPageClickEvent;
import com.mixmycontainer.yiwu.fragment.PriceDetailesFragment;
import com.squareup.otto.Subscribe;

/**
 * Created by ADMIN on 03-12-2016.
 */
public class PriceActivity extends BaseActivity {

    public static Intent getnewIntent(Context context) {
        Intent intent = new Intent(context, PriceActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_price_deatils);
        if (savedInstanceState == null) {
            addFragment(PriceDetailesFragment.getnewInstance(), false);
        }
    }
    @Override
    public void onBackPressed() {


    }

    @Subscribe
    public void summeryClickEvent(SummeryPageClickEvent event) {
        finish();
    }
}
