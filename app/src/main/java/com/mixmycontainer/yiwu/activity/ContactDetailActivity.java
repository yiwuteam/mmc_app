package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.fragment.ContactDetailFragment;

/**
 * Created by ADMIN on 14-11-2016.
 */
public class ContactDetailActivity extends BaseActivity {

    public static Intent getnewIntent(Context context) {
        Intent intent = new Intent(context, ContactDetailActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contcatdetail);
        if (savedInstanceState == null) {
            addFragment(ContactDetailFragment.newInstance(), false);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);

    }
}
