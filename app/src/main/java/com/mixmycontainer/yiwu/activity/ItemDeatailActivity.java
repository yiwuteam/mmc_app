package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.bus.events.SummeryPageClickEvent;
import com.mixmycontainer.yiwu.fragment.ItemDetailFragment;
import com.squareup.otto.Subscribe;

/**
 * Created by ADMIN on 15-11-2016.
 */
public class ItemDeatailActivity extends BaseActivity {

    private static final String ARG_EXTRA_BOOTH_ID = "ARG_EXTRA_BOOTH_ID";
    private static int booth_id;

    public static Intent getnewIntent(Context context) {
        Intent intent = new Intent(context, ItemDeatailActivity.class);
//        intent.putExtra(ARG_EXTRA_BOOTH_ID, booth_id);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_number);
//        booth_id = getIntent().getIntExtra(ARG_EXTRA_BOOTH_ID, 0);
        if (savedInstanceState == null) {
            addFragment(ItemDetailFragment.newinstance(), false);
        }
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);

    }

    @Subscribe
    public void summeryClickEvent(SummeryPageClickEvent event) {
        finish();
    }
}
