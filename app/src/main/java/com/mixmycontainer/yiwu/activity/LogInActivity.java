package com.mixmycontainer.yiwu.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.widget.Toast;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.fragment.LogInFragment;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

/**
 * Created by ADMIN on 09-11-2016.
 */
public class LogInActivity extends BaseActivity {

    public static Intent getnewIntent(Context context) {
        Intent intent = new Intent(context, LogInActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);




       checkPermission();

        if (!TextUtils.isEmpty(PreferenceUtil.get(this).getString(PreferenceUtil.USER_AUTH_TOKEN, null))) {
            startActivity(OrderListActivity.getnewInstance(LogInActivity.this));
            overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);
            finish();
        } else {
            if (savedInstanceState == null) {
                addFragment(LogInFragment.newInstsnce(), false);
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
//        overridePendingTransition(R.anim.scale_center_out, R.anim.nothing_anim);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 11: {
                // If request is cancelled, the result arrays are empty.
                if (ContextCompat.checkSelfPermission(LogInActivity.this,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED )
                {

                    Toast.makeText(this, "This app requires storage permission to continue", Toast.LENGTH_SHORT).show();

                }
                else {

                }

                return;
            }


            // other 'case' lines to check for other
            // permissions this app might request
        }
    }
    public void checkPermission()
    {
        if (ContextCompat.checkSelfPermission(LogInActivity.this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED )
        {
            if (ActivityCompat.shouldShowRequestPermissionRationale(LogInActivity.this,
                    Manifest.permission.READ_EXTERNAL_STORAGE)) {
            } else {
                // we can request the permission.
                ActivityCompat.requestPermissions(LogInActivity.this,
                        new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        11);
            }
        }
    }
}
