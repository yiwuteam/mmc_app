package com.mixmycontainer.yiwu.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

/**
 * Created by ADMIN on 07-12-2016.
 */
public class SplashScreenActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isTaskRoot()
             && getIntent().hasCategory(Intent.CATEGORY_LAUNCHER)
             && getIntent().getAction() != null
             && getIntent().getAction().equals(Intent.ACTION_MAIN)) {

            finish();
            return;
        } else {
            setContentView(R.layout.activity_spalsh_screen);
            if (!TextUtils.isEmpty(PreferenceUtil.get(this).getString(PreferenceUtil.USER_AUTH_TOKEN, null))) {

                startActivity(OrderListActivity.getnewInstance(SplashScreenActivity.this));
                overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_right_out_activity);
                finish();


            } else {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(SplashScreenActivity.this, LogInActivity.class);
                        startActivity(i);
                        finish();
                    }
                }, 1100);
            }
        }
    }
}
