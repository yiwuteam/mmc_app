package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.bus.events.SummeryPageClickEvent;
import com.mixmycontainer.yiwu.fragment.BoothDetailFragment;
import com.squareup.otto.Subscribe;

/**
 * Created by ADMIN on 15-11-2016.
 */
public class Boothdetailactivity extends BaseActivity {

    public static String ARG_EXTRA = "ARG_EXTRA";
    private String flage;

    public static Intent getnewIntent(Context context, String flag) {
        Intent intent = new Intent(context, Boothdetailactivity.class);
        intent.putExtra(ARG_EXTRA, flag);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_boothdetails);


        if (savedInstanceState == null) {
            addFragment(BoothDetailFragment.newinstance(flage), false);
        }
    }

    @Subscribe
    public void summeryClickEvent(SummeryPageClickEvent event) {
        finish();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Toast.makeText(this, "you may continue the order from order list", Toast.LENGTH_SHORT).show();
        finish();
        overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);

    }
}
