package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.bus.events.SummeryPageClickEvent;
import com.mixmycontainer.yiwu.fragment.CartonDetailFragment;
import com.squareup.otto.Subscribe;

/**
 * Created by ADMIN on 29-11-2016.
 */
public class CartonDetailActivity extends BaseActivity {

    public static Intent getnewIntent(Context context) {
        Intent intent = new Intent(context, CartonDetailActivity.class);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_carton_detail);
        if (savedInstanceState == null) {
            addFragment(CartonDetailFragment.newInstance(), false);
        }
    }
    @Subscribe
    public void summeryClickEvent(SummeryPageClickEvent event) {
        finish();
    }
    @Override
    public void onBackPressed() {


    }
}
