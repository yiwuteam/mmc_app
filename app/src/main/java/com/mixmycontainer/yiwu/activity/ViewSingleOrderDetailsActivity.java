package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.fragment.NewOrderFragment;
import com.mixmycontainer.yiwu.fragment.ViewSingleOrderDetailsFragment;

/**
 * Created by minusbug on 12/04/2016.
 */
public class ViewSingleOrderDetailsActivity extends BaseActivity {

    public static String ARG_EXTRA = "ARG_EXTRA";
    private String orderStatuss;

    public static Intent getnewIntent(Context context, String orderStatus) {
        Intent intent = new Intent(context, ViewSingleOrderDetailsActivity.class);
        intent.putExtra(ARG_EXTRA, orderStatus);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_single_order_details);
        orderStatuss = getIntent().getStringExtra(ARG_EXTRA);

        if (savedInstanceState == null) {
            addFragment(ViewSingleOrderDetailsFragment.newInstence(orderStatuss), false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}
