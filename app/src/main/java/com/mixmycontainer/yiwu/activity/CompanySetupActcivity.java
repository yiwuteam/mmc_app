package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.fragment.ComapanySetupFragment;

/**
 * Created by ADMIN on 11-11-2016.
 */
public class CompanySetupActcivity extends BaseActivity {



    public static Intent getnewIntent(Context context) {
        Intent intent = new Intent(context, CompanySetupActcivity.class);
        return intent;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_companysetup);
        if (savedInstanceState == null) {
            addFragment(ComapanySetupFragment.newInstance(), false);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.move_left_in_activity,R.anim.move_right_out_activity);

    }
}
