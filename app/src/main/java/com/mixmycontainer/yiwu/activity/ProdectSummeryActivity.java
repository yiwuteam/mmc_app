package com.mixmycontainer.yiwu.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.fragment.ProdectSummeryFragment;

/**
 * Created by ADMIN on 03-12-2016.
 */
public class ProdectSummeryActivity extends BaseActivity {

    public static Intent getnewIntent(Context context) {
        Intent intent = new Intent(context, ProdectSummeryActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_prodect_summery);
        if (savedInstanceState == null) {
            addFragment(ProdectSummeryFragment.getnewInStance(), false);
        }
    }

    @Override
    public void onBackPressed() {
    }
}
