package com.mixmycontainer.yiwu.activity;

import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.utiles.AppUtils;

/**
 * Created by hp on 16-08-2017.
 */

public class ImageViewActivity extends BaseActivity {
    ImageView imgItem,imgClose;
    String imgUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_view);
        imgItem = (ImageView) findViewById(R.id.img_SingleItem);
        imgClose =(ImageView)findViewById(R.id.img_close);

        imgItem.setImageResource(R.drawable.ic_default);
        imgClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        imgUrl = null;




        Bundle bu = getIntent().getExtras();
        imgUrl = bu.getString("imgValue", " ");
        if (bu == null) {
            Toast.makeText(this, "No image found", Toast.LENGTH_SHORT).show();
        } else {

            if (AppUtils.isNetworkAvailable(getApplicationContext())) {

                Glide.with(getApplicationContext()).
                        load(Uri.parse(imgUrl))
                        .placeholder(R.drawable.ic_default)
                        .skipMemoryCache(true)
                        .into(imgItem);

            } else {
                Toast.makeText(this, "No Internet connection", Toast.LENGTH_SHORT).show();
            }
        }




    }
}
