package com.mixmycontainer.yiwu.adapter;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.Boothdetailactivity;
import com.mixmycontainer.yiwu.activity.ViewSingleOrderDetailsActivity;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.apis.models.OrderListModel;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Handler;


/**
 * Created by ADMIN on 10-11-2016.
 */
public class OrderListAdapter extends RecyclerView.Adapter<OrderListAdapter.OrderListViewHolder> {
    List<HashMap<String, String>> orderList;
    List<OrderListModel> filteredList;
    List<OrderListModel> originalList;

    private Context context;

    public OrderListAdapter(Context context, List<OrderListModel> orderListModels) {
        this.filteredList = orderListModels;
        this.originalList = new ArrayList<>(orderListModels);
        this.context = context;
    }

    public void filter(String charText) {
        String harText = charText;
        filteredList.clear();

        if (TextUtils.isEmpty(charText)) {
            filteredList.addAll(originalList);
        } else {
            for (OrderListModel orderListModel : originalList) {
                if (orderListModel.getOrderNumber().toLowerCase().contains(harText.toLowerCase()) ||
                     orderListModel.getLastUpdate().toLowerCase().contains(harText.toLowerCase())) {
                    filteredList.add(orderListModel);
                }
            }
            if (filteredList.size() == 0) {
                Toast.makeText(context, "No match found.", Toast.LENGTH_SHORT).show();
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public OrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_orderlist, parent, false);

        return new OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderListViewHolder holder, final int position) {
        holder.tvOrdernumber.setText(filteredList.get(position).getOrderNumber());
        holder.tvNoOfBooth.setText(filteredList.get(position).getTotalBooth());
        holder.tvNoOfCatrons.setText(filteredList.get(position).getTotalCartons());
        holder.tvTotalCBM.setText(filteredList.get(position).getTotalOrderCBM());
        holder.tvorderValueUs.setText(filteredList.get(position).getTotalOrderUsValue());
        holder.tvDate.setText(filteredList.get(position).getLastUpdate());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(context);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                DisplayMetrics display = context.getResources().getDisplayMetrics();
                int width = display.widthPixels;
                int height = display.heightPixels;
                dialog.setContentView(R.layout.dialog_full_order_details);
                final TextView btnAdd = (TextView) dialog.findViewById(R.id.btnAdd);
                TextView btnView = (TextView) dialog.findViewById(R.id.btnView);
                TextView btnSendMail = (TextView) dialog.findViewById(R.id.btnSendMail);

                if (filteredList.get(position).getOrderStatus().equals("0")) {
                    btnAdd.setVisibility(View.VISIBLE);
                    btnView.setVisibility(View.VISIBLE);
                } else {
                    btnAdd.setVisibility(View.GONE);
                    btnView.setVisibility(View.VISIBLE);
                }
                btnAdd.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        SharedPreferences.Editor PREF = PreferenceUtil.edit(context);
                        String token = PreferenceUtil.get(context).getString(PreferenceUtil.USER_AUTH_TOKEN, "0");
                        System.out.println("tokoen:-"+token);
                        PREF.putString(PreferenceUtil.ORDER_ADD, "continueOrder");
                        String orderId = String.valueOf(filteredList.get(position).getOrderId());
                        PREF.putString(PreferenceUtil.ORDER_ID, orderId);
                        String containerId = String.valueOf(filteredList.get(position).getContainerId());
                        PREF.putString(PreferenceUtil.CONTAINER_ID,containerId);
                        PREF.putString(PreferenceUtil.EDIT_ITEM_ID,"0");
                        PREF.putString(PreferenceUtil.EDIT_BOOTH_ID,"0");
                        PREF.apply();
                        context.startActivity(Boothdetailactivity.getnewIntent(context, "add"));
                        dialog.dismiss();
                    }
                });
                btnSendMail.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        String Token = PreferenceUtil.get(context).getString(PreferenceUtil.USER_AUTH_TOKEN, null);
                        String orderId = filteredList.get(position).getOrderId();
                        if (AppUtils.isNetworkAvailable(context)) {
                            sendMail(Token, orderId, dialog);
                        } else {
                            dialog.dismiss();
                            Toast.makeText(context, context.getResources().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                        }

                    }
                });
                btnView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                        SharedPreferences.Editor PREF = PreferenceUtil.edit(context);
                        String orderId = filteredList.get(position).getOrderId();
                        PREF.putString(PreferenceUtil.USER_ORDER_ID, orderId);
                        PREF.putString(PreferenceUtil.ORDER_ID, orderId);
                        String containerId = String.valueOf(filteredList.get(position).getContainerId());
                        PREF.putString(PreferenceUtil.CONTAINER_ID,containerId);
                        PREF.putString(PreferenceUtil.EDIT_ITEM_ID,"0");
                        PREF.apply();
                        context.startActivity(ViewSingleOrderDetailsActivity.getnewIntent(context, filteredList.get(position).getOrderStatus()));
                    }
                });
                dialog.getWindow().setLayout((6 * width) / 7, LinearLayout.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.show();
            }
        });

    }

    private void sendMail(String token, String orderId, final Dialog dialog) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        Ion.with(context)
             .load(URL.Send_Mail_Order + token + "&orderid=" + orderId)
             .asJsonObject()
             .setCallback(new FutureCallback<JsonObject>() {
                 @Override

                 public void onCompleted(Exception e, JsonObject result) {
                     progressDialog.dismiss();
                     try {
                         String requestCode = result.get("resultCode").getAsString();
                         Log.d("RESPONSE", new Gson().toJson(result.toString()));

                         if (requestCode.equals("1")) {
                             dialog.dismiss();
                             Toast.makeText(context,result.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                         } else {
                             dialog.dismiss();
                             Toast.makeText(context, result.get("message").getAsString(), Toast.LENGTH_SHORT).show();
                         }
                     } catch (JsonParseException c) {
                         dialog.dismiss();
                         Toast.makeText(context, c.getMessage().toString(), Toast.LENGTH_SHORT).show();
                         c.printStackTrace();
                     } catch (Exception e1) {
                         dialog.dismiss();
                         Toast.makeText(context, e1.getMessage().toString(), Toast.LENGTH_SHORT).show();
                         e1.printStackTrace();
                     }
                 }

             });
    }

    @Override
    public int getItemCount() {
        return filteredList.size();
    }

    class OrderListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvOrdernumber, tvNoOfBooth, tvNoOfCatrons, tvTotalCBM, tvorderValueUs, tvDate;
        LinearLayout lnr_noData;

        public OrderListViewHolder(View itemView) {
            super(itemView);
            tvOrdernumber = (TextView) itemView.findViewById(R.id.tvOrdernumber);
            tvNoOfBooth = (TextView) itemView.findViewById(R.id.tvNoOfBooth);
            tvNoOfCatrons = (TextView) itemView.findViewById(R.id.tvNoOfCatrons);
            tvorderValueUs = (TextView) itemView.findViewById(R.id.tvorderValueUs);
            tvTotalCBM = (TextView) itemView.findViewById(R.id.tvTotalCBM);
            tvDate = (TextView) itemView.findViewById(R.id.tvDate);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
