package com.mixmycontainer.yiwu.adapter;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.SystemClock;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.activity.Boothdetailactivity;
import com.mixmycontainer.yiwu.activity.CartonDetailActivity;
import com.mixmycontainer.yiwu.activity.ImageViewActivity;
import com.mixmycontainer.yiwu.apis.models.OrderListItemsModel;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

import java.util.List;

/**
 * Created by minusbug on 12/04/2016.
 */
public class SingleOrderAdapter extends RecyclerView.Adapter<SingleOrderAdapter.OrderListViewHolder> {
    String imgName;
    Uri imgUri;
    List<OrderListItemsModel> orderListItemsModels;
    private Context context;
    String orderIdToAdapter;
    private long mLastClickTime = 0;
    public SingleOrderAdapter(Context context, List<OrderListItemsModel> orderListItemsModels,String orderIdToAdapter) {
        this.orderListItemsModels = orderListItemsModels;
        this.context = context;
        this.orderIdToAdapter = orderIdToAdapter;
    }



    @Override
    public OrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_single_oreder_details, parent, false);

        return new OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final OrderListViewHolder holder, final int position) {
        holder.btn_img_Delete.setEnabled(true);
        holder.btn_img_edit.setEnabled(true);

        holder.tvBoothNumber.setText(orderListItemsModels.get(position).getBoothNumber());
        holder.tvItemNumber.setText(orderListItemsModels.get(position).getItemNo());
        holder.tvDescription.setText(orderListItemsModels.get(position).getDescriptionFrom()+"/"+orderListItemsModels.get(position).getDescriptionTo());

        holder.tvContainer.setText(orderListItemsModels.get(position).getContainerType());

        holder.tvUnitQuantity.setText(orderListItemsModels.get(position).getUnitQuantity());
        holder.tvCartonQuantity.setText(orderListItemsModels.get(position).getCartonQuantity());
        holder.tvUsd.setText("$" + orderListItemsModels.get(position).getPriceInUs());
        holder.tvRmb.setText("¥" + orderListItemsModels.get(position).getPriceInRmb());
        holder.tvTotalUsd.setText("$" + orderListItemsModels.get(position).getTotalPriceInUS());
        holder.tvTotalRmb.setText("¥" + orderListItemsModels.get(position).getTotalPriceInRMB());

        imgName = orderListItemsModels.get(position).getPicture();
        imgUri = Uri.parse(imgName);
        Glide.with(context).
                load(imgUri)
                .placeholder(R.drawable.ic_default)
                .into(holder.img_Item);
        holder.img_Item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                imgName = orderListItemsModels.get(position).getPicture();
                Intent in = new Intent(context, ImageViewActivity.class);
                in.putExtra("imgValue",imgName);
                context.startActivity(in);

            }
        });

        holder.btn_img_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                SharedPreferences.Editor PREF = PreferenceUtil.edit(context);
                PREF.putString(PreferenceUtil.EDIT_ITEM_ID, orderListItemsModels.get(position).getItemId());
                String editBoothID = orderListItemsModels.get(position).getBoothId();
                PREF.putString(PreferenceUtil.EDIT_BOOTH_ID,editBoothID);
                PREF.putString(PreferenceUtil.ORDER_ADD, "continueOrder");
                PREF.putString(PreferenceUtil.DELETE_ITEM,"0");
                PREF.apply();
                context.startActivity(Boothdetailactivity.getnewIntent(context, "add"));
                holder.btn_img_edit.setEnabled(true);

            }
        });

        holder.btn_img_Delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (SystemClock.elapsedRealtime() - mLastClickTime < 1000) {
                    return;
                }
                mLastClickTime = SystemClock.elapsedRealtime();
                SharedPreferences.Editor PREF = PreferenceUtil.edit(context);
                PREF.putString(PreferenceUtil.EDIT_ITEM_ID, orderListItemsModels.get(position).getItemId());
                String editBoothID = orderListItemsModels.get(position).getBoothId();
                PREF.putString(PreferenceUtil.EDIT_BOOTH_ID,editBoothID);
                PREF.putString(PreferenceUtil.ORDER_ADD, "continueOrder");
                PREF.putString(PreferenceUtil.DELETE_ITEM,"Delete");
                PREF.apply();
                context.startActivity(CartonDetailActivity.getnewIntent(context));

            }
        });
    }

    @Override
    public int getItemCount() {
        return orderListItemsModels.size();
    }

    class OrderListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvBoothNumber, tvItemNumber, tvDescription, tvContainer, tvUnitQuantity, tvCartonQuantity, tvUsd, tvRmb, tvTotalUsd, tvTotalRmb;
        ImageView img_Item;
        ImageButton btn_img_Delete, btn_img_edit;

        public OrderListViewHolder(View itemView) {
            super(itemView);

            tvTotalRmb = (TextView) itemView.findViewById(R.id.tvTotalRmb);
            tvTotalUsd = (TextView) itemView.findViewById(R.id.tvTotalUsd);
            tvRmb = (TextView) itemView.findViewById(R.id.tvRmb);
            tvUsd = (TextView) itemView.findViewById(R.id.tvUsd);
            tvCartonQuantity = (TextView) itemView.findViewById(R.id.tvCartonQuantity);
            tvUnitQuantity = (TextView) itemView.findViewById(R.id.tvUnitQuantity);
            tvContainer = (TextView) itemView.findViewById(R.id.tvContainer);
            tvDescription = (TextView) itemView.findViewById(R.id.tvDescription);
            tvItemNumber = (TextView) itemView.findViewById(R.id.tvItemNumber);
            tvBoothNumber = (TextView) itemView.findViewById(R.id.tvBoothNumber);
            img_Item = (ImageView)itemView.findViewById(R.id.img_item);
            btn_img_edit =(ImageButton)itemView.findViewById(R.id.img_edit);
            btn_img_Delete = (ImageButton) itemView.findViewById(R.id.img_delete);
        }

        @Override
        public void onClick(View view) {

        }
    }


}
