package com.mixmycontainer.yiwu.adapter;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.models.GoldAgentInfo;

import java.util.ArrayList;
import java.util.HashMap;

import io.realm.RealmList;

/**
 * Created by minusbug on 12/22/2016.
 */
public class GoldAgentListAddapter extends RecyclerView.Adapter<GoldAgentListAddapter.OrderListViewHolder> {

    RealmList<GoldAgentInfo> goldAgentDetailList;
    private Context context;
    private itemclick itemclick;

    public GoldAgentListAddapter(Context context, RealmList<GoldAgentInfo> goldAgentDetailList, itemclick itemclick) {
        this.goldAgentDetailList = goldAgentDetailList;
        this.context = context;
        this.itemclick = itemclick;
    }


    @Override
    public OrderListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.adapter_agent_gold, parent, false);
        return new OrderListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(OrderListViewHolder holder, final int position) {
        holder.tvGoldAgentAdress.setText(goldAgentDetailList.get(position).getOfficeAddress());
        holder.tvGoldAgentName.setText(goldAgentDetailList.get(position).getCompanyName());
        holder.tvGoldAgentPhoneNumber.setText(goldAgentDetailList.get(position).getPhoneNumber());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                itemclick.click(position);
            }
        });
        holder.imgMoreInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                final Dialog dialog = new Dialog(context);
                dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
                DisplayMetrics display = context.getResources().getDisplayMetrics();
                int width = display.widthPixels;
                int height = display.heightPixels;
                dialog.setContentView(R.layout.dialog_full_agent_details);
                dialog.setCanceledOnTouchOutside(false);
                TextView tvAgentName = (TextView) dialog.findViewById(R.id.tvAgentName);
                TextView tvBusinessYear = (TextView) dialog.findViewById(R.id.tvBusinessYear);
                TextView tvFaxNumber = (TextView) dialog.findViewById(R.id.tvFaxNumber);
                TextView tvGoldAgentYear = (TextView) dialog.findViewById(R.id.tvGoldAgentYear);
                TextView tvgentAdress = (TextView) dialog.findViewById(R.id.tvgentAdress);
                TextView tvAgentPhoneNumber = (TextView) dialog.findViewById(R.id.tvAgentPhoneNumber);
                ImageView imgClose = (ImageView) dialog.findViewById(R.id.imgClose);
                ListView rvagentdetailslist = (ListView) dialog.findViewById(R.id.simpleListView);
                tvAgentName.setText(goldAgentDetailList.get(position).getCompanyName());
                tvBusinessYear.setText(goldAgentDetailList.get(position).getBusinessYear());
                tvFaxNumber.setText(goldAgentDetailList.get(position).getFaxNumber());
                tvGoldAgentYear.setText(goldAgentDetailList.get(position).getGoldAgentYears());
                tvgentAdress.setText(goldAgentDetailList.get(position).getOfficeAddress());
                tvAgentPhoneNumber.setText(goldAgentDetailList.get(position).getPhoneNumber());
                imgClose.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialog.dismiss();
                    }
                });

                ArrayList<HashMap<String, String>> arrayList = new ArrayList<>();
                for (int i = 0; i < goldAgentDetailList.get(position).getContact().size(); i++) {

                    HashMap<String, String> hashMap = new HashMap<>();//create a hashmap to store the data in key value pair
                    hashMap.put("name", goldAgentDetailList.get(position).getContact().get(i).getName());
                    hashMap.put("email", goldAgentDetailList.get(position).getContact().get(i).getEmail());
                    hashMap.put("phone", goldAgentDetailList.get(position).getContact().get(i).getMobile());
                    hashMap.put("skype", goldAgentDetailList.get(position).getContact().get(i).getSkype());
                    arrayList.add(hashMap);//add the hashmap into arrayList
                }
                String[] from = {"name", "email", "phone", "skype"};//string array
                int[] to = {R.id.tvName, R.id.tvEmail, R.id.tvMobileNumber, R.id.tvSkype};//int array of views id's
                SimpleAdapter simpleAdapter = new SimpleAdapter(context, arrayList, R.layout.list_item_agent_details, from, to);//Create object and set the parameters for simpleAdapter
                rvagentdetailslist.setAdapter(simpleAdapter);//sets the adapter for listView
                rvagentdetailslist.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
//                        Toast.makeText(context, i + "", Toast.LENGTH_SHORT).show();
                    }
                });
                dialog.getWindow().setLayout((4 * width) / 4, LinearLayout.LayoutParams.WRAP_CONTENT);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.WHITE));
                dialog.show();
            }
        });

    }

    @Override
    public int getItemCount() {
        return goldAgentDetailList.size();
    }

    public interface itemclick {
        void click(int position);
    }

    class OrderListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView tvGoldAgentAdress, tvGoldAgentName, tvGoldAgentPhoneNumber;
        ImageView imgMoreInfo;

        public OrderListViewHolder(View itemView) {
            super(itemView);
            tvGoldAgentAdress = (TextView) itemView.findViewById(R.id.tvGoldAgentAdress);
            tvGoldAgentName = (TextView) itemView.findViewById(R.id.tvGoldAgentName);
            tvGoldAgentPhoneNumber = (TextView) itemView.findViewById(R.id.tvGoldAgentPhoneNumber);
            imgMoreInfo = (ImageView) itemView.findViewById(R.id.imgMoreInfo);
        }

        @Override
        public void onClick(View view) {

        }
    }
}
