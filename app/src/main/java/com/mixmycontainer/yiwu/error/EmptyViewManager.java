package com.mixmycontainer.yiwu.error;

import android.content.Context;
import android.support.annotation.DrawableRes;
import android.support.annotation.LayoutRes;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.mixmycontainer.yiwu.R;
import com.mixmycontainer.yiwu.apis.core.ResultCode;


/**
 * Created by Sanif on 13-03-2016.
 */
public class
EmptyViewManager {

    private Context context;
    private View dataView;
    private ViewGroup containerView;
    private View emptyView;


    public EmptyViewManager(Context context, ViewGroup containerView) {
        this(context, null, containerView);
    }

    /**
     * @param context       - The context.
     * @param dataView      - The view in which the data is shown.
     * @param containerView - The view in which empty view should be added.
     */
    public EmptyViewManager(Context context, View dataView, ViewGroup containerView) {
        this.context = context;
        this.dataView = dataView;
        this.containerView = containerView;
    }


    public View showEmptyView(ResultCode resultCode, EmptyParams emptyParams) {


        if (dataView != null) {
            dataView.setVisibility(View.GONE);
        }
        int imageRes = 0;
        int titleRes = 0;
        int actionTextRes = 0;
        try {
            containerView.setVisibility(View.VISIBLE);
            containerView.removeAllViews();
            imageRes = emptyParams.getImgRes();
            titleRes = emptyParams.getTitleRes();
            actionTextRes = emptyParams.getActionTextRes();
        } catch (Exception e) {
            e.printStackTrace();
        }

        View.OnClickListener primaryCallback = emptyParams.getOnClickListener();

        Builder mBuilder = null;
        if (resultCode != null) {//errorcode == 0 means show loading screen
            mBuilder = new Builder(context, containerView);

            switch (resultCode) {
                case NO_INTERNET:
                    emptyView = mBuilder.setImage(imageRes < 1 ? R.mipmap.ic_error_no_internet : imageRes)
                         .setTitle(titleRes < 1 ? R.string.error_no_internet : titleRes)
                         .setPrimaryAction(context.getResources().getString(actionTextRes < 1 ? R.string.error_try_again_action : actionTextRes), primaryCallback)
                         .build();
                    break;
                case ERROR:
                    emptyView = mBuilder
//                            .setImage(imageRes < 1 ? R.drawable.ic_error : imageRes)
                         .setTitle(titleRes < 1 ? R.string.error_un_expected : titleRes)
                         .setPrimaryAction(context.getResources().getString(actionTextRes < 1 ? R.string.error_try_again_action : actionTextRes), primaryCallback)
                         .build();
                    break;
                case IS_EMPTY:
                    emptyView = mBuilder
                         .setImage(imageRes < 1 ? R.mipmap.ic_nodata_available : imageRes)
                         .setTitle(titleRes < 1 ? R.string.txt_no_data_available : titleRes)
                         .setPrimaryAction(context.getResources().getString(actionTextRes < 1 ? R.string.error_try_again_action : actionTextRes), primaryCallback)
                         .build();
                    break;

            }
        }
//        else {//loading view
//            mBuilder = new Builder(context, containerView, R.layout.loading_view);
//            emptyView = mBuilder
//                    .build();
//        }

        return emptyView;
    }

    /**
     * Use the following EmptyView method like below :
     * <p/>
     * EmptyViewManager.showEmptyView(errorCode, context, placeHolder, onClickListener);
     * <p/>
     * errorCode - one among the seven error codes declared above (like ERROR_CODE_NO_INTERNET_CONNECTION, ERROR_CODE_SERVER_DOWN, etc.,)
     * <p/>
     * onClickListener - the listener which should be set for the primary text
     */
    public View showEmptyView(ResultCode errorCode, View.OnClickListener onClickListener) {
        EmptyParams emptyParams = new EmptyParams();
        emptyParams.setOnClickListener(onClickListener);
        return showEmptyView(errorCode, emptyParams);
    }

    public View showEmptyView(ResultCode errorCode) {
        EmptyParams params = new EmptyParams();
        return showEmptyView(errorCode, params);
    }

    /**
     * Hides the empty view and show the dataView
     */
    public void hideEmptyView() {
        if (emptyView != null && emptyView.getParent() == containerView) {
            containerView.removeView(emptyView);
        }
        emptyView = null;
        try {
            containerView.setVisibility(View.GONE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (dataView != null)
            dataView.setVisibility(View.VISIBLE);
    }

    /**
     * show loading screen in the emptyViewHolder
     */
    public void showLoading() {
        showEmptyView(null, new EmptyParams());
    }

    public static class Builder {

        public final int IMAGE_VIEW_ID = R.id.ivErrorImage;
        public final int TEXT_VIEW_ID = R.id.tvErrorTitle;
        public final int BUTTON_ID = R.id.btErrorAction;


        private Context context;
        private View mView;

        public Builder(Context context, ViewGroup parent) {
            this(context, parent, R.layout.layout_empty_view);
        }

        public Builder(Context context, ViewGroup parent, @LayoutRes int res) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mView = inflater.inflate(res, parent, false);
            FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, Gravity.CENTER);
            parent.removeAllViews();
            parent.addView(mView, params);
            this.context = context;
        }

        private static Animation getRotateAnimation() {
            final RotateAnimation rotateAnimation = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
            rotateAnimation.setDuration(1500);
            rotateAnimation.setInterpolator(new LinearInterpolator());
            rotateAnimation.setRepeatCount(Animation.INFINITE);
            return rotateAnimation;
        }

        public Builder setTitle(String title) {
            TextView titleView = (TextView) mView.findViewById(TEXT_VIEW_ID);
            titleView.setText(title);
            if (title.isEmpty()) {
                titleView.setVisibility(View.GONE);
            }
            return this;
        }

        public Builder setTitle(int title) {
            return setTitle(context.getString(title));
        }

        public Builder setImage(@DrawableRes int resId) {
            ImageView imageView = (ImageView) mView.findViewById(IMAGE_VIEW_ID);
            imageView.setImageResource(resId);
            imageView.clearAnimation();//removing animation
            return this;
        }

        public Builder hideImage(@DrawableRes int resId) {
            ImageView imageView = (ImageView) mView.findViewById(IMAGE_VIEW_ID);
            imageView.setVisibility(View.GONE);
            return this;
        }

        public Builder setPrimaryAction(String primaryAction, final View.OnClickListener onClickListener) {
            Button primaryTextView = (Button) mView.findViewById(BUTTON_ID);
            primaryTextView.setText(primaryAction);
            if (onClickListener != null) {
                primaryTextView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        onClickListener.onClick(v);
                    }
                });
                primaryTextView.setVisibility(View.VISIBLE);
            } else {
                primaryTextView.setVisibility(View.GONE);
            }
            return this;
        }

        public View build() {
            return mView;
        }
    }
}
