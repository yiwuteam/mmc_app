package com.mixmycontainer.yiwu.error;

import android.view.View;

/**
 * Created by Sanif on 13-03-2016.
 */
public class EmptyParams {

    private int imgRes;
    private int titleRes;
    private int actionTextRes;
    private View.OnClickListener onClickListener;

    public int getImgRes() {
        return imgRes;
    }

    public void setImgRes(int imgRes) {
        this.imgRes = imgRes;
    }

    public int getTitleRes() {
        return titleRes;
    }

    public void setTitleRes(int titleRes) {
        this.titleRes = titleRes;
    }

    public int getActionTextRes() {
        return actionTextRes;
    }

    public void setActionTextRes(int actionTextRes) {
        this.actionTextRes = actionTextRes;
    }

    public View.OnClickListener getOnClickListener() {
        return onClickListener;
    }

    public void setOnClickListener(View.OnClickListener onClickListener) {
        this.onClickListener = onClickListener;
    }
}
