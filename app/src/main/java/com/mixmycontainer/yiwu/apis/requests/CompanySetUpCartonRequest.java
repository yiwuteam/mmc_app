package com.mixmycontainer.yiwu.apis.requests;

/**
 * Created by minusbug on 11/25/2016.
 */
public class CompanySetUpCartonRequest extends BaseRequest {
    private String token;
    private String grossWeight;
    private String width;
    private String height;
    private String length;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getGrossWeight() {
        return grossWeight;
    }

    public void setGrossWeight(String grossWeight) {
        this.grossWeight = grossWeight;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }


}
