package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpCartonRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpCartonResponse;

/**
 * Created by minusbug on 11/25/2016.
 */
public class CompanySetUpCartonService extends BaseService<CompanySetUpCartonResponse> {
    public CompanySetUpCartonService(Context context) {
        super(context);

    }

    public String URL = com.mixmycontainer.yiwu.apis.core.URL.UpdateCompanySetUpCarton;

    public void post(CompanySetUpCartonRequest args, ApiResponse<CompanySetUpCartonResponse> apiCallback) {
        super.post(URL, args, new TypeToken<CompanySetUpCartonResponse>() {
        }, apiCallback);
    }
}
