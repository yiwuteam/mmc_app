package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.requests.SaveCartonRequest;
import com.mixmycontainer.yiwu.apis.responses.SaveCartonResponse;

/**
 * Created by ADMIN on 04-12-2016.
 */
public class SaveCartonService extends BaseService<SaveCartonResponse> {
    public static final String URL = com.mixmycontainer.yiwu.apis.core.URL.SaveCartonDetail;


    public SaveCartonService(Context context) {
        super(context);
    }

    public void post(SaveCartonRequest args, ApiResponse<SaveCartonResponse> apiCallback) {
        super.post(URL, args, new TypeToken<SaveCartonResponse>() {
        }, apiCallback);
    }
}
