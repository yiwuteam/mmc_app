package com.mixmycontainer.yiwu.apis.requests;

/**
 * Created by minusbug on 11/25/2016.
 */
public class CompanySetUpContactRequest extends BaseRequest {
    private String token;
    private String fax;
    private String msnid;
    private String qqid;
    private String orderemail;
    private String agentemail;
    private String skype;
    private String agentid;
    private String agentname;

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public String getAgentname() {
        return agentname;
    }

    public void setAgentname(String agentname) {
        this.agentname = agentname;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getMsnid() {
        return msnid;
    }

    public void setMsnid(String msnid) {
        this.msnid = msnid;
    }

    public String getQqid() {
        return qqid;
    }

    public void setQqid(String qqid) {
        this.qqid = qqid;
    }

    public String getOrderemail() {
        return orderemail;
    }

    public void setOrderemail(String orderemail) {
        this.orderemail = orderemail;
    }

    public String getAgentemail() {
        return agentemail;
    }

    public void setAgentemail(String agentemail) {
        this.agentemail = agentemail;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }


}
