package com.mixmycontainer.yiwu.apis.models;

import java.util.List;

/**
 * Created by ADMIN on 17-05-2017.
 */

public class BoothDetailOnlineModel {
    private String BoothId;
    private String OrderId;
    private String CompanyId;
    private String BoothNumber;
    private String BoothName;
    private String Phone;
    private String BusinessScope;
    private String ValuePurchase;
    private String ValuePurchaseRMB;
    private String BusinessCard;
    private String BoothStatus;
    private String TradeId;
    private String HallId;
    private List<ItemDetailOnlineModel> itemdetailsbyboothorderid;

    public List<ItemDetailOnlineModel> getItemdetailsbyboothorderid() {
        return itemdetailsbyboothorderid;
    }

    public void setItemdetailsbyboothorderid(List<ItemDetailOnlineModel> itemdetailsbyboothorderid) {
        this.itemdetailsbyboothorderid = itemdetailsbyboothorderid;
    }

    public String getBoothId() {
        return BoothId;
    }

    public void setBoothId(String boothId) {
        BoothId = boothId;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getBoothName() {
        return BoothName;
    }

    public void setBoothName(String boothName) {
        BoothName = boothName;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getBusinessScope() {
        return BusinessScope;
    }

    public void setBusinessScope(String businessScope) {
        BusinessScope = businessScope;
    }

    public String getValuePurchase() {
        return ValuePurchase;
    }

    public void setValuePurchase(String valuePurchase) {
        ValuePurchase = valuePurchase;
    }

    public String getValuePurchaseRMB() {
        return ValuePurchaseRMB;
    }

    public void setValuePurchaseRMB(String valuePurchaseRMB) {
        ValuePurchaseRMB = valuePurchaseRMB;
    }

    public String getBusinessCard() {
        return BusinessCard;
    }

    public void setBusinessCard(String businessCard) {
        BusinessCard = businessCard;
    }

    public String getBoothStatus() {
        return BoothStatus;
    }

    public void setBoothStatus(String boothStatus) {
        BoothStatus = boothStatus;
    }

    public String getHallId() {
        return HallId;
    }

    public void setHallId(String hallId) {
        HallId = hallId;
    }

    public String getTradeId() {
        return TradeId;
    }

    public void setTradeId(String tradeId) {
        TradeId = tradeId;
    }

    public String getBoothNumber() {
        return BoothNumber;
    }

    public void setBoothNumber(String boothNumber) {
        BoothNumber = boothNumber;
    }
}
