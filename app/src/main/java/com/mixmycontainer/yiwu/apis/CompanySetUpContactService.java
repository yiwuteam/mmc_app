package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpAdressRequest;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpContactRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpAdressResponse;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpContactResponse;

/**
 * Created by minusbug on 11/25/2016.
 */
public class CompanySetUpContactService extends BaseService<CompanySetUpContactResponse> {
    public CompanySetUpContactService(Context context) {
        super(context);

    }

    public String URL = com.mixmycontainer.yiwu.apis.core.URL.UpdayteCompanyContact;

    public void post(CompanySetUpContactRequest args, ApiResponse<CompanySetUpContactResponse> apiCallback) {
        super.post(URL, args, new TypeToken<CompanySetUpContactResponse>() {
        }, apiCallback);
    }
}
