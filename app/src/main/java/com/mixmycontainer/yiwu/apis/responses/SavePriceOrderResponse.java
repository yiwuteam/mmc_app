package com.mixmycontainer.yiwu.apis.responses;

import com.mixmycontainer.yiwu.models.SummeryInfo;

/**
 * Created by ADMIN on 04-12-2016.
 */
public class SavePriceOrderResponse extends BaseResponse {

    private SummeryInfo summary;

    public SummeryInfo getSummary() {
        return summary;
    }

    public void setSummary(SummeryInfo summary) {
        this.summary = summary;
    }
}
