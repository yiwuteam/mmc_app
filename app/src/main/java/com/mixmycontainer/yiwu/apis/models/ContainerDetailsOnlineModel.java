package com.mixmycontainer.yiwu.apis.models;

/**
 * Created by ADMIN on 17-05-2017.
 */

public class ContainerDetailsOnlineModel {

    private String ContainerId;
    private String ContainerType;
    private String OrderId;
    private String CompanyId;
    private String FilledWeight;
    private String FilledVolume;
    private String Volumepercentage;
    private String Weightpercentage;
    private String Status;
    private String ItemSaveStatus;

    public String getContainerId() {
        return ContainerId;
    }

    public void setContainerId(String containerId) {
        ContainerId = containerId;
    }

    public String getContainerType() {
        return ContainerType;
    }

    public void setContainerType(String containerType) {
        ContainerType = containerType;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getFilledWeight() {
        return FilledWeight;
    }

    public void setFilledWeight(String filledWeight) {
        FilledWeight = filledWeight;
    }

    public String getVolumepercentage() {
        return Volumepercentage;
    }

    public void setVolumepercentage(String volumepercentage) {
        Volumepercentage = volumepercentage;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getItemSaveStatus() {
        return ItemSaveStatus;
    }

    public void setItemSaveStatus(String itemSaveStatus) {
        ItemSaveStatus = itemSaveStatus;
    }

    public String getWeightpercentage() {
        return Weightpercentage;
    }

    public void setWeightpercentage(String weightpercentage) {
        Weightpercentage = weightpercentage;
    }

    public String getFilledVolume() {
        return FilledVolume;
    }

    public void setFilledVolume(String filledVolume) {
        FilledVolume = filledVolume;
    }
}
