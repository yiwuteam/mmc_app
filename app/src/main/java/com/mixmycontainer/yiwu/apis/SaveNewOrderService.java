package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.requests.SaveNewOrderRequest;
import com.mixmycontainer.yiwu.apis.responses.SaveNewOrderResponse;

/**
 * Created by AMRITHA on 11/29/2016.
 */
public class SaveNewOrderService extends BaseService<SaveNewOrderResponse> {

    public static final String URL = com.mixmycontainer.yiwu.apis.core.URL.SaveNewOrder;

    public SaveNewOrderService(Context context) {
        super(context);

    }

    public void post(SaveNewOrderRequest args, ApiResponse<SaveNewOrderResponse> apiCallback) {
        super.post(URL, args, new TypeToken<SaveNewOrderResponse>() {
        }, apiCallback);
    }
}
