package com.mixmycontainer.yiwu.apis.responses;

import com.mixmycontainer.yiwu.models.SummeryInfo;

/**
 * Created by minusbug on 01/12/2017.
 */

public class SubSaveCartonResponse extends BaseResponse {
    private String balancevolume;
    private String balanceweight;
    private String usedvolume;
    private String usedweight;
    private String balancecartons;
    private String itemId;
    private String totalVolumePercentage;
    private String totalWeightPercentage;


    public String getBalancevolume() {
        return balancevolume;
    }

    public void setBalancevolume(String balancevolume) {
        this.balancevolume = balancevolume;
    }

    public String getBalanceweight() {
        return balanceweight;
    }

    public void setBalanceweight(String balanceweight) {
        this.balanceweight = balanceweight;
    }

    public String getUsedvolume() {
        return usedvolume;
    }

    public void setUsedvolume(String usedvolume) {
        this.usedvolume = usedvolume;
    }

    public String getUsedweight() {
        return usedweight;
    }

    public void setUsedweight(String usedweight) {
        this.usedweight = usedweight;
    }

    public String getBalancecartons() {
        return balancecartons;
    }

    public void setBalancecartons(String balancecartons) {
        this.balancecartons = balancecartons;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getTotalVolumePercentage() {
        return totalVolumePercentage;
    }

    public void setTotalVolumePercentage(String totalVolumePercentage) {
        this.totalVolumePercentage = totalVolumePercentage;
    }

    public String getTotalWeightPercentage() {
        return totalWeightPercentage;
    }

    public void setTotalWeightPercentage(String totalWeightPercentage) {
        this.totalWeightPercentage = totalWeightPercentage;
    }
}
