package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.requests.ChangePasswordRequest;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpTranslateRequest;
import com.mixmycontainer.yiwu.apis.responses.ChangePasswordResponse;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpTranslateResponse;

/**
 * Created by AMRITHA on 11/28/2016.
 */
public class ChangePasswordService extends BaseService<ChangePasswordResponse> {
    public ChangePasswordService(Context context) {
        super(context);

    }

    public String URL = com.mixmycontainer.yiwu.apis.core.URL.ChangePassword;

    public void post(ChangePasswordRequest args, ApiResponse<ChangePasswordResponse> apiCallback) {
        super.post(URL, args, new TypeToken<ChangePasswordResponse>() {
        }, apiCallback);
    }
}
