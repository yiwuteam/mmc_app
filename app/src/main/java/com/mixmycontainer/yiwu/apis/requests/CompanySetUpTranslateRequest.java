package com.mixmycontainer.yiwu.apis.requests;

/**
 * Created by minusbug on 11/25/2016.
 */
public class CompanySetUpTranslateRequest extends BaseRequest {
    private String translateFrom;
    private String translateTo;
    private String token;

    public String getTranslateFrom() {
        return translateFrom;
    }

    public void setTranslateFrom(String translateFrom) {
        this.translateFrom = translateFrom;
    }

    public String getTranslateTo() {
        return translateTo;
    }

    public void setTranslateTo(String translateTo) {
        this.translateTo = translateTo;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
