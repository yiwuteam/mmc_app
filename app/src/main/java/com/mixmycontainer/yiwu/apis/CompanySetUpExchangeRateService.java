package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpExchangeRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpExchangeRateResponse;

/**
 * Created by minusbug on 11/25/2016.
 */
public class CompanySetUpExchangeRateService extends BaseService<CompanySetUpExchangeRateResponse> {
    public CompanySetUpExchangeRateService(Context context) {
        super(context);

    }

    public String URL = com.mixmycontainer.yiwu.apis.core.URL.UpdateCompanySetUpExchangeRate;

    public void post(CompanySetUpExchangeRequest args, ApiResponse<CompanySetUpExchangeRateResponse> apiCallback) {
        super.post(URL, args, new TypeToken<CompanySetUpExchangeRateResponse>() {
        }, apiCallback);
    }
}
