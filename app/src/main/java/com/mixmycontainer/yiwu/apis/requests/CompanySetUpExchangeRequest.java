package com.mixmycontainer.yiwu.apis.requests;

/**
 * Created by minusbug on 11/25/2016.
 */
public class CompanySetUpExchangeRequest  extends BaseRequest {
    private String token;
    private String exchangeRate;
    private String activate3dCurrency;
    private String thirdCurrency;

    public String getActivate3dCurrency() {
        return activate3dCurrency;
    }

    public void setActivate3dCurrency(String activate3dCurrency) {
        this.activate3dCurrency = activate3dCurrency;
    }
    public String getExchangeRate3d() {
        return exchangeRate3d;
    }

    public void setExchangeRate3d(String exchangeRate3d) {
        this.exchangeRate3d = exchangeRate3d;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getExchangeRate() {
        return exchangeRate;
    }

    public void setExchangeRate(String exchangeRate) {
        this.exchangeRate = exchangeRate;
    }



    public String getThirdCurrency() {
        return thirdCurrency;
    }

    public void setThirdCurrency(String thirdCurrency) {
        this.thirdCurrency = thirdCurrency;
    }

    private String exchangeRate3d;
}
