package com.mixmycontainer.yiwu.apis.requests;

/**
 * Created by AMRITHA on 11/29/2016.
 */
public class SaveNewOrderRequest extends BaseRequest {
    private String token;


    private String ordernumber;

    private int containertype;


    public String getOrdernumber() {
        return ordernumber;
    }

    public void setOrdernumber(String ordernumber) {
        this.ordernumber = ordernumber;
    }

    public int getContainertype() {
        return containertype;
    }

    public void setContainertype(int containertype) {
        this.containertype = containertype;
    }


    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
