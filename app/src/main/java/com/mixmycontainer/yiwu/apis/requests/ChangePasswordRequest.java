package com.mixmycontainer.yiwu.apis.requests;

/**
 * Created by AMRITHA on 11/28/2016.
 */
public class ChangePasswordRequest extends BaseRequest {
    private String token;
    private String newPassword;
    private String currentPassword;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    public String getCurrentPassword() {
        return currentPassword;
    }

    public void setCurrentPassword(String currentPassword) {
        this.currentPassword = currentPassword;
    }


}
