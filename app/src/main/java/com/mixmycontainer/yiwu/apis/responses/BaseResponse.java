package com.mixmycontainer.yiwu.apis.responses;


import com.mixmycontainer.yiwu.apis.core.ResultCode;

/**
 * Created by bugs on 2/6/2016.
 */
public class BaseResponse {

    private String message;
    private ResultCode resultCode;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResultCode getResultCode() {
        return resultCode;
    }

    public void setResultCode(ResultCode resultCode) {
        this.resultCode = resultCode;
    }

}
