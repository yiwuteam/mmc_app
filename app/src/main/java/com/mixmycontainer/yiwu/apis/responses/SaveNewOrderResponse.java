package com.mixmycontainer.yiwu.apis.responses;

/**
 * Created by AMRITHA on 11/29/2016.
 */
public class SaveNewOrderResponse extends BaseResponse {
    private int containerId;
    private String orderId;
    private String newOrderNo;

    public ContianerDetails getContianerDetails() {
        return contianerDetails;
    }

    public void setContianerDetails(ContianerDetails contianerDetails) {
        this.contianerDetails = contianerDetails;
    }

    private ContianerDetails contianerDetails;

    public String getNewOrderNo() {
        return newOrderNo;
    }

    public void setNewOrderNo(String newOrderNo) {
        this.newOrderNo = newOrderNo;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public int getContainerId() {
        return containerId;
    }

    public void setContainerId(int containerId) {
        this.containerId = containerId;
    }


}
