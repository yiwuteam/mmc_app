package com.mixmycontainer.yiwu.apis.requests;

/**
 * Created by ADMIN on 04-12-2016.
 */
public class SavePriceOrderRequest extends BaseRequest {

    private String token;
    private String orderId;
    private String itemId;
    private String priceInUs;
    private String priceInRmb;
    private String PriceInZar;
    private String boothid;

    public String getBoothid() {
        return boothid;
    }

    public void setBoothid(String boothid) {
        this.boothid = boothid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getPriceInUs() {
        return priceInUs;
    }

    public void setPriceInUs(String priceInUs) {
        this.priceInUs = priceInUs;
    }

    public String getPriceInRmb() {
        return priceInRmb;
    }

    public void setPriceInRmb(String priceInRmb) {
        this.priceInRmb = priceInRmb;
    }

    public String getPriceInZar() {
        return PriceInZar;
    }

    public void setPriceInZar(String priceInZar) {
        PriceInZar = priceInZar;
    }
}
