package com.mixmycontainer.yiwu.apis.core;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;
import com.mixmycontainer.yiwu.BuildConfig;
import com.mixmycontainer.yiwu.apis.requests.BaseRequest;
import com.mixmycontainer.yiwu.apis.responses.BaseResponse;
import com.mixmycontainer.yiwu.utiles.AppUtils;
import com.mixmycontainer.yiwu.utiles.PreferenceUtil;

/**
 * Created by Sanif on 03-01-2016.
 */
public abstract class BaseService<T extends BaseResponse> {

    final SharedPreferences preferences;
    private Context context;
    private String GET = "GET";
    private String POST = "POST";
    private FutureCallback<?> callback;

    public BaseService(Context context) {
        this.context = context;
        preferences = PreferenceUtil.get(context);
    }

    public static void rawResponse(Context context, String url, Object args) {
        Ion.with(context).load("POST", url).setJsonPojoBody(args).asString().setCallback(new FutureCallback<String>() {
            @Override
            public void onCompleted(Exception e, String result) {
                Log.d("RAW", result);
            }
        });
    }

    protected void post(String url, BaseRequest args, TypeToken<T> returnType, final ApiResponse<T> apiCallback) {
        execute(POST, url, args, returnType, apiCallback);

    }

    protected void get(String url, BaseRequest args, TypeToken<T> returnType, final ApiResponse<T> apiCallback) {
        execute(GET, url, args, returnType, apiCallback);

    }

    protected synchronized void execute(String method, final String url, final BaseRequest args, final TypeToken<T> returnObjectType,
                                        final ApiResponse<T> apiCallback) {

        if (AppUtils.isNetworkAvailable(context)) {
            if (args != null) {
                args.setAppVersion(BuildConfig.VERSION_NAME);
                args.setApiVersion("1.0");
                if (BuildConfig.DEBUG) Log.d("REQUEST", new Gson().toJson(args));
            }

            Ion.with(context).load(method, url).setLogging("WebService", Log.DEBUG)
                 .addHeader("Content-Type", "application/json")
                 .setTimeout(60 * 1000)
                 .setJsonPojoBody(args)
                 .as(returnObjectType)
                 .withResponse()
                 .setCallback(new FutureCallback<Response<T>>() {
                     @Override
                     public void onCompleted(Exception e, Response<T> result) {
                         if (e != null) {
                             execute(POST, url, args, returnObjectType, apiCallback);

                             Log.d("Exeption::", e + "");
                             e.printStackTrace();
                         }
                         if (result != null) {
                             if (BuildConfig.DEBUG) {
                                 try {
                                     String s = result.getException().toString();
                                     Log.d("ERROR", s);
                                 } catch (Exception e1) {
                                     e1.printStackTrace();
                                 }
                                 Log.d("RESPONSE", new Gson().toJson(result.getResult()));
                             }
                             if (result.getResult() != null && result.getResult().getResultCode() == ResultCode.SUCCESS) {
                                 apiCallback.onSuccess(result.getResult().getResultCode(), result.getResult());
                             } else if (result.getResult() != null && result.getResult().getResultCode() == ResultCode.ERROR) {
                                 if (result.getResult() != null) {
                                     apiCallback.onError(result.getResult().getResultCode(), result.getResult());
                                 } else {
                                     apiCallback.onError(ResultCode.ERROR, result.getResult());
                                 }
                             }
                             // Refesh token calling method
                             else if (result.getResult() != null && result.getResult().getResultCode() == ResultCode.NO_AOUTHERISED) {

                             }
                         } else {
                             try {
                                 apiCallback.onError(ResultCode.ERROR, null);
                             } catch (NullPointerException e1) {
                                 e1.printStackTrace();
                             }
                         }
                     }
                 });
        } else {
            apiCallback.onError(ResultCode.NO_INTERNET, null);
        }

    }


}