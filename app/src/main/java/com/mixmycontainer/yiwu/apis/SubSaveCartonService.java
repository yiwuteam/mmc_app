package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.core.URL;
import com.mixmycontainer.yiwu.apis.requests.SavePriceOrderRequest;
import com.mixmycontainer.yiwu.apis.requests.SubSaveCartonRequest;
import com.mixmycontainer.yiwu.apis.responses.SavePriceOrderResponse;
import com.mixmycontainer.yiwu.apis.responses.SubSaveCartonResponse;

/**
 * Created by minusbug on 01/12/2017.
 */

public class SubSaveCartonService extends BaseService<SubSaveCartonResponse> {
    public static final String URL = com.mixmycontainer.yiwu.apis.core.URL.SubsaveCarton;


    public SubSaveCartonService(Context context) {
        super(context);
    }

    public void post(SubSaveCartonRequest args, ApiResponse<SubSaveCartonResponse> apiCallback) {
        super.post(URL, args, new TypeToken<SubSaveCartonResponse>() {
        }, apiCallback);
    }

}