package com.mixmycontainer.yiwu.apis.models;

/**
 * Created by minusbug on 12/04/2016.
 */
public class OrderListItemsModel {
    private String ItemId;
    private String CompanyId;
    private String OrderId;
    private String BoothId;
    private String ContainerId;

    private String ItemNo;
    private String DescriptionFrom;
    private String WeightofCarton;
    private String Length;
    private String Width;

    private String Height;
    private String CBM;
    private String Picture;
    private String PriceInUs;
    private String PriceInRmb;

    private String ThirdCurrencyType;
    private String PriceInThirdCurrency;
    private String UnitQuantity;
    private String CartonQuantity;
    private String TotalPriceInUS;

    private String TotalPriceInRMB;
    private String TotalPriceIn3rdCurrency;
    private String Date;
    private String ContainerType;
    private String FilledWeight;

    private String FilledVolume;
    private String Volumepercentage;
    private String Weightpercentage;
    private String Status;
    private String BoothNumber;

    private String BoothName;
    private String Phone;
    private String BusinessScope;
    private String ValuePurchase;
    private String ValuePurchaseRMB;

    private String BusinessCard;
    private String BoothStatus;
    private String OrderNumber;
    private String Trade;
    private String ProductType;

    private String TotalOrderUsValue;
    private String TotalOrderRmbValue;
    private String TotalOrderZarValue;
    private String TotalCartons;
    private String TotalBooth;

    private String TotalContainer;
    private String TotalOrderWeight;
    private String TotalOrderCBM;
    private String OrderStatus;
    private String LastUpdate;
    private String DescriptionTo;
    private String UnitperCarton;

    public String getDescriptionTo() {
        return DescriptionTo;
    }

    public void setDescriptionTo(String descriptionTo) {
        DescriptionTo = descriptionTo;
    }

    public String getTotalOrderWeight() {
        return TotalOrderWeight;
    }

    public void setTotalOrderWeight(String totalOrderWeight) {
        TotalOrderWeight = totalOrderWeight;
    }

    public String getLastUpdate() {
        return LastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        LastUpdate = lastUpdate;
    }

    public String getTotalOrderRmbValue() {
        return TotalOrderRmbValue;
    }

    public void setTotalOrderRmbValue(String totalOrderRmbValue) {
        TotalOrderRmbValue = totalOrderRmbValue;
    }

    public String getTotalBooth() {
        return TotalBooth;
    }

    public void setTotalBooth(String totalBooth) {
        TotalBooth = totalBooth;
    }

    public String getValuePurchase() {
        return ValuePurchase;
    }

    public void setValuePurchase(String valuePurchase) {
        ValuePurchase = valuePurchase;
    }

    public String getContainerType() {
        return ContainerType;
    }

    public void setContainerType(String containerType) {
        ContainerType = containerType;
    }

    public String getTotalPriceInUS() {
        return TotalPriceInUS;
    }

    public void setTotalPriceInUS(String totalPriceInUS) {
        TotalPriceInUS = totalPriceInUS;
    }

    public String getCBM() {
        return CBM;
    }

    public void setCBM(String CBM) {
        this.CBM = CBM;
    }

    public String getBoothNumber() {
        return BoothNumber;
    }

    public void setBoothNumber(String boothNumber) {
        BoothNumber = boothNumber;
    }

    public String getValuePurchaseRMB() {
        return ValuePurchaseRMB;
    }

    public void setValuePurchaseRMB(String valuePurchaseRMB) {
        ValuePurchaseRMB = valuePurchaseRMB;
    }

    public String getTrade() {
        return Trade;
    }

    public void setTrade(String trade) {
        Trade = trade;
    }

    public String getWeightofCarton() {
        return WeightofCarton;
    }

    public void setWeightofCarton(String weightofCarton) {
        WeightofCarton = weightofCarton;
    }

    public String getPriceInUs() {
        return PriceInUs;
    }

    public void setPriceInUs(String priceInUs) {
        PriceInUs = priceInUs;
    }

    public String getTotalPriceIn3rdCurrency() {
        return TotalPriceIn3rdCurrency;
    }

    public void setTotalPriceIn3rdCurrency(String totalPriceIn3rdCurrency) {
        TotalPriceIn3rdCurrency = totalPriceIn3rdCurrency;
    }

    public String getBoothName() {
        return BoothName;
    }

    public void setBoothName(String boothName) {
        BoothName = boothName;
    }

    public String getBoothStatus() {
        return BoothStatus;
    }

    public void setBoothStatus(String boothStatus) {
        BoothStatus = boothStatus;
    }

    public String getTotalCartons() {
        return TotalCartons;
    }

    public void setTotalCartons(String totalCartons) {
        TotalCartons = totalCartons;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getFilledWeight() {
        return FilledWeight;
    }

    public void setFilledWeight(String filledWeight) {
        FilledWeight = filledWeight;
    }

    public String getVolumepercentage() {
        return Volumepercentage;
    }

    public void setVolumepercentage(String volumepercentage) {
        Volumepercentage = volumepercentage;
    }

    public String getBusinessScope() {
        return BusinessScope;
    }

    public void setBusinessScope(String businessScope) {
        BusinessScope = businessScope;
    }

    public String getItemNo() {
        return ItemNo;
    }

    public void setItemNo(String itemNo) {
        ItemNo = itemNo;
    }

    public String getLength() {
        return Length;
    }

    public void setLength(String length) {
        Length = length;
    }

    public String getCartonQuantity() {
        return CartonQuantity;
    }

    public void setCartonQuantity(String cartonQuantity) {
        CartonQuantity = cartonQuantity;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getFilledVolume() {
        return FilledVolume;
    }

    public void setFilledVolume(String filledVolume) {
        FilledVolume = filledVolume;
    }

    public String getWeightpercentage() {
        return Weightpercentage;
    }

    public void setWeightpercentage(String weightpercentage) {
        Weightpercentage = weightpercentage;
    }

    public String getTotalPriceInRMB() {
        return TotalPriceInRMB;
    }

    public void setTotalPriceInRMB(String totalPriceInRMB) {
        TotalPriceInRMB = totalPriceInRMB;
    }

    public String getPriceInThirdCurrency() {
        return PriceInThirdCurrency;
    }

    public void setPriceInThirdCurrency(String priceInThirdCurrency) {
        PriceInThirdCurrency = priceInThirdCurrency;
    }

    public String getHeight() {
        return Height;
    }

    public void setHeight(String height) {
        Height = height;
    }

    public String getDescriptionFrom() {
        return DescriptionFrom;
    }

    public void setDescriptionFrom(String descriptionFrom) {
        DescriptionFrom = descriptionFrom;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getBoothId() {
        return BoothId;
    }

    public void setBoothId(String boothId) {
        BoothId = boothId;
    }


    public String getItemId() {
        return ItemId;
    }

    public void setItemId(String itemId) {
        ItemId = itemId;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getContainerId() {
        return ContainerId;
    }

    public void setContainerId(String containerId) {
        ContainerId = containerId;
    }

    public String getUnitperCarton() {
        return UnitperCarton;
    }

    public void setUnitperCarton(String unitperCarton) {
        UnitperCarton = unitperCarton;
    }

    public String getWidth() {
        return Width;
    }

    public void setWidth(String width) {
        Width = width;
    }

    public String getUnitQuantity() {
        return UnitQuantity;
    }

    public void setUnitQuantity(String unitQuantity) {
        UnitQuantity = unitQuantity;
    }

    public String getPicture() {
        return Picture;
    }

    public void setPicture(String picture) {
        Picture = picture;
    }

    public String getPriceInRmb() {
        return PriceInRmb;
    }

    public void setPriceInRmb(String priceInRmb) {
        PriceInRmb = priceInRmb;
    }

    public String getThirdCurrencyType() {
        return ThirdCurrencyType;
    }

    public void setThirdCurrencyType(String thirdCurrencyType) {
        ThirdCurrencyType = thirdCurrencyType;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getBusinessCard() {
        return BusinessCard;
    }

    public void setBusinessCard(String businessCard) {
        BusinessCard = businessCard;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getTotalOrderUsValue() {
        return TotalOrderUsValue;
    }

    public void setTotalOrderUsValue(String totalOrderUsValue) {
        TotalOrderUsValue = totalOrderUsValue;
    }

    public String getTotalOrderZarValue() {
        return TotalOrderZarValue;
    }

    public void setTotalOrderZarValue(String totalOrderZarValue) {
        TotalOrderZarValue = totalOrderZarValue;
    }

    public String getTotalContainer() {
        return TotalContainer;
    }

    public void setTotalContainer(String totalContainer) {
        TotalContainer = totalContainer;
    }

    public String getTotalOrderCBM() {
        return TotalOrderCBM;
    }

    public void setTotalOrderCBM(String totalOrderCBM) {
        TotalOrderCBM = totalOrderCBM;
    }

}
