package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpTranslateRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpTranslateResponse;

/**
 * Created by minusbug on 11/25/2016.
 */
public class CompanySetUpTranslationService extends BaseService<CompanySetUpTranslateResponse> {
    public CompanySetUpTranslationService(Context context) {
        super(context);

    }

    public String URL = com.mixmycontainer.yiwu.apis.core.URL.UpdayteCompanyTranslate;

    public void post(CompanySetUpTranslateRequest args, ApiResponse<CompanySetUpTranslateResponse> apiCallback) {
        super.post(URL, args, new TypeToken<CompanySetUpTranslateResponse>() {
        }, apiCallback);
    }
}
