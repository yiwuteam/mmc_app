package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.requests.CompanySetUpAdressRequest;
import com.mixmycontainer.yiwu.apis.responses.CompanySetUpAdressResponse;

/**
 * Created by minusbug on 11/25/2016.
 */
public class CompanySetUpAdressService extends BaseService<CompanySetUpAdressResponse> {
    public CompanySetUpAdressService(Context context) {
        super(context);

    }

    public String URL = com.mixmycontainer.yiwu.apis.core.URL.UpdateCompanySetUpAdrress;

    public void post(CompanySetUpAdressRequest args, ApiResponse<CompanySetUpAdressResponse> apiCallback) {
        super.post(URL, args, new TypeToken<CompanySetUpAdressResponse>() {
        }, apiCallback);
    }
}
