package com.mixmycontainer.yiwu.apis.requests;

/**
 * Created by ADMIN on 04-12-2016.
 */
public class SaveCartonRequest extends BaseRequest {

    private String token;
    private String orderid;
    private String itemid;
    private String unitpercarton;
    private String grossweight;
    private String length;
    private String width;
    private String height;
    private String unitquantity;
    private String cbm;
    private String cartonquanity;


    public String getCbm() {
        return cbm;
    }

    public void setCbm(String cbm) {
        this.cbm = cbm;
    }

    public String getCartonquanity() {
        return cartonquanity;
    }

    public void setCartonquanity(String cartonquanity) {
        this.cartonquanity = cartonquanity;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getItemid() {
        return itemid;
    }

    public void setItemid(String itemid) {
        this.itemid = itemid;
    }

    public String getUnitpercarton() {
        return unitpercarton;
    }

    public void setUnitpercarton(String unitpercarton) {
        this.unitpercarton = unitpercarton;
    }

    public String getGrossweight() {
        return grossweight;
    }

    public void setGrossweight(String grossweight) {
        this.grossweight = grossweight;
    }

    public String getLength() {
        return length;
    }

    public void setLength(String length) {
        this.length = length;
    }

    public String getWidth() {
        return width;
    }

    public void setWidth(String width) {
        this.width = width;
    }

    public String getHeight() {
        return height;
    }

    public void setHeight(String height) {
        this.height = height;
    }

    public String getUnitquantity() {
        return unitquantity;
    }

    public void setUnitquantity(String unitquantity) {
        this.unitquantity = unitquantity;
    }
}
