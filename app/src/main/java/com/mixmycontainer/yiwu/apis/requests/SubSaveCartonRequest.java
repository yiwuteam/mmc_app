package com.mixmycontainer.yiwu.apis.requests;

import com.mixmycontainer.yiwu.apis.requests.BaseRequest;

/**
 * Created by minusbug on 01/12/2017.
 */

public class SubSaveCartonRequest extends BaseRequest {
    private String token;
    private String orderid;

    public String getOrderid() {
        return orderid;
    }

    public void setOrderid(String orderid) {
        this.orderid = orderid;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }


}
