package com.mixmycontainer.yiwu.apis.models;

/**
 * Created by minusbug on 11/30/2016.
 */
public class OrderListModel {

    private String OrderNumber;
    private String OrderId;
    private String CompanyId;
    private String ContainerId;
    private String Trade;
    private String ProductType;
    private String TotalOrderUsValue;
    private String TotalOrderRmbValue;
    private String TotalOrderZarValue;
    private String Total3rdCurrencyValue;
    private String TotalCartons;
    private String TotalBooth;
    private String TotalContainer;
    private String TotalOrderWeight;
    private String TotalOrderCBM;
    private String OrderStatus;
    private String LastUpdate;

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }
    public String getLastUpdate() {
        return LastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        LastUpdate = lastUpdate;
    }

    public String getContainerId() {
        return ContainerId;
    }

    public void setContainerId(String containerId) {
        ContainerId = containerId;
    }


    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }

    public String getTrade() {
        return Trade;
    }

    public void setTrade(String trade) {
        Trade = trade;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getTotalOrderUsValue() {
        return TotalOrderUsValue;
    }

    public void setTotalOrderUsValue(String totalOrderUsValue) {
        TotalOrderUsValue = totalOrderUsValue;
    }

    public String getTotalOrderRmbValue() {
        return TotalOrderRmbValue;
    }

    public void setTotalOrderRmbValue(String totalOrderRmbValue) {
        TotalOrderRmbValue = totalOrderRmbValue;
    }

    public String getTotalBooth() {
        return TotalBooth;
    }

    public void setTotalBooth(String totalBooth) {
        TotalBooth = totalBooth;
    }

    public String getTotalOrderWeight() {
        return TotalOrderWeight;
    }

    public void setTotalOrderWeight(String totalOrderWeight) {
        TotalOrderWeight = totalOrderWeight;
    }

    public String getTotalOrderCBM() {
        return TotalOrderCBM;
    }

    public void setTotalOrderCBM(String totalOrderCBM) {
        TotalOrderCBM = totalOrderCBM;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getTotalContainer() {
        return TotalContainer;
    }

    public void setTotalContainer(String totalContainer) {
        TotalContainer = totalContainer;
    }

    public String getTotalCartons() {
        return TotalCartons;
    }

    public void setTotalCartons(String totalCartons) {
        TotalCartons = totalCartons;
    }

    public String getTotal3rdCurrencyValue() {
        return Total3rdCurrencyValue;
    }

    public void setTotal3rdCurrencyValue(String total3rdCurrencyValue) {
        Total3rdCurrencyValue = total3rdCurrencyValue;
    }

    public String getTotalOrderZarValue() {
        return TotalOrderZarValue;
    }

    public void setTotalOrderZarValue(String totalOrderZarValue) {
        TotalOrderZarValue = totalOrderZarValue;
    }



}
