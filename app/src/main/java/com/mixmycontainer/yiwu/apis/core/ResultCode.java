package com.mixmycontainer.yiwu.apis.core;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Sanif on 03-01-2016.
 */
public enum ResultCode {
    @SerializedName("2")
    NO_AOUTHERISED,
    @SerializedName("1")
    SUCCESS,
    @SerializedName("0")
    ERROR,
    NO_INTERNET,
    IS_EMPTY,
}
