package com.mixmycontainer.yiwu.apis;

import android.content.Context;

import com.google.gson.reflect.TypeToken;
import com.mixmycontainer.yiwu.apis.core.ApiResponse;
import com.mixmycontainer.yiwu.apis.core.BaseService;
import com.mixmycontainer.yiwu.apis.requests.SavePriceOrderRequest;
import com.mixmycontainer.yiwu.apis.responses.SavePriceOrderResponse;

/**
 * Created by ADMIN on 04-12-2016.
 */
public class SavePriceOrederService extends BaseService<SavePriceOrderResponse> {
    public static final String URL = com.mixmycontainer.yiwu.apis.core.URL.SavePriceOrder;


    public SavePriceOrederService(Context context) {
        super(context);
    }

    public void post(SavePriceOrderRequest args, ApiResponse<SavePriceOrderResponse> apiCallback) {
        super.post(URL, args, new TypeToken<SavePriceOrderResponse>() {
        }, apiCallback);
    }

}
