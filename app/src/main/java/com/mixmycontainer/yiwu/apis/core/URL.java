package com.mixmycontainer.yiwu.apis.core;

/**
 * Created by bugs on 2/6/2016.
 */
public class URL {
    public static final String translate_text = "https://translation.googleapis.com/language/translate/v2?key=AIzaSyCFpxcV8vAiJzdOnAKC4pddady4DW2ajQA";

    public static final String BaseURL = "https://yiwutrader.com/Api_controller/";
    public static final String Login = BaseURL + "login?password=";
//    public static final String WorldCountryNames = BaseURL + "worldcountrynames";
    public static final String UpdateCompanySetUpAdrress = BaseURL + "updatecompanyaddress";
    public static final String UpdateCompanySetUpCarton = BaseURL + "updatecompanyaveragecarton";
//    public static final String CurrecnyNames = BaseURL + "worldcurrency";
    public static final String UpdateCompanySetUpExchangeRate = BaseURL + "updatecompanyexchangerate";
    public static final String UpdayteCompanyTranslate = BaseURL + "updatecompanytransilationsetup";
    public static final String UpdayteCompanyContact = BaseURL + "updatecompanycontact";
    public static final String ChangePassword = BaseURL + "changepassword";
//    public static final String CompanySetUpDetails = BaseURL + "companydetails?";
    public static final String CompanyDetails = BaseURL + "dashboard?";
    public static final String SaveNewOrder = BaseURL + "saveneworder";
    public static final String OrderList = BaseURL+"offlineuserorderdetails?token=";
//    public static final String getOrderDetails = BaseURL + "ordersummary?";
//    public static final String SaveBooth = BaseURL + "savebooth";
//    public static final String SaveItemDetail = BaseURL + "savenewitem";
    public static final String SubsaveCarton = BaseURL + "subsavecarton";
    public static final String SaveNewPassword = BaseURL + "changepasswordwithmail";
    public static final String SaveLanguageDetail = BaseURL + "accountoperation";
    public static final String SaveCartonDetail = BaseURL + "savecarton";
//    public static final String AddnewContainer = BaseURL + "addnewcontainer?";
    public static final String SavePriceOrder = BaseURL + "saveitemprice";
//    public static final String EndOrder = BaseURL + "endorder?";
//    public static final String checkBuyerCompanyAdress = BaseURL + "checkvalidcompany?";
    public static final String continue_order = BaseURL + "continueorder?";
//    public static final String New_Trade_and_Hall = BaseURL + "insertnewtradeandhall";
    public static final String setlanguage = BaseURL + "getaccount?";

    public static final String Send_Mail_Order = BaseURL + "sendorderemail?token=";
    public static final String UploadItemImage = BaseURL + "uploaditemimage";
//    public static final String GetOrderDetail = BaseURL + "ordersummary?orderid=";
//    public static final String AddNewContainer = BaseURL + "addnewcontainer?orderid=";
//    public static final String UpgradeContainer = BaseURL + "upgradecontainer?orderid=";
//    public static final String CancelProcess = BaseURL + "ignoreupgrade?orderid=";
    public static final String PhotView = "http://yiwutrader.com/assets/images/";
    public static final String CheckIfBoothExists = BaseURL + "getbooth?boothno=";
//    public static final String NewBooth = BaseURL + "endcurrentbooth?boothid=";
//    public static final String OrderSummary = BaseURL + "ordersummary?boothid=";
    public static final String ForgotPassword = BaseURL + "forgotpassword?emailid=";
    public static final String SignUp = BaseURL + "signup?password=";
//    public static final String ViewOrderItems = BaseURL + "vieworderitems?token=";
}
