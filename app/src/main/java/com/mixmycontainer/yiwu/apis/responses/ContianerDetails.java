package com.mixmycontainer.yiwu.apis.responses;

/**
 * Created by ADMIN on 12-04-2017.
 */

public class ContianerDetails {
    private String Container;
    private String MaximumVolume;
    private String MaximumWeight;

    public String getContainer() {
        return Container;
    }

    public void setContainer(String container) {
        Container = container;
    }

    public String getMaximumVolume() {
        return MaximumVolume;
    }

    public void setMaximumVolume(String maximumVolume) {
        MaximumVolume = maximumVolume;
    }

    public String getMaximumWeight() {
        return MaximumWeight;
    }

    public void setMaximumWeight(String maximumWeight) {
        MaximumWeight = maximumWeight;
    }
}
