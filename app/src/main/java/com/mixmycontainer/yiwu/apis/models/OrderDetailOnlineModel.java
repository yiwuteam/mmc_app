package com.mixmycontainer.yiwu.apis.models;

import java.util.List;

/**
 * Created by ADMIN on 17-05-2017.
 */

public class OrderDetailOnlineModel {
    private String OrderId;
    private String OrderNumber;
    private String CompanyId;
    private String Trade;
    private String ProductType;
    private String TotalOrderUsValue;
    private String TotalOrderRmbValue;
    private String TotalOrderZarValue;
    private String Total3rdCurrencyValue;
    private String TotalCartons;
    private String TotalBooth;
    private String TotalContainer;
    private String TotalOrderWeight;
    private String TotalOrderCBM;
    private String OrderStatus;
    private String LastUpdate;
    private List<BoothDetailOnlineModel> boothdetailsbyorderid;
    private List<ContainerDetailsOnlineModel> containerdetailsbyorderid;

    public List<BoothDetailOnlineModel> getBoothdetailsbyorderid() {
        return boothdetailsbyorderid;
    }

    public void setBoothdetailsbyorderid(List<BoothDetailOnlineModel> boothdetailsbyorderid) {
        this.boothdetailsbyorderid = boothdetailsbyorderid;
    }

    public List<ContainerDetailsOnlineModel> getContainerdetailsbyorderid() {
        return containerdetailsbyorderid;
    }

    public void setContainerdetailsbyorderid(List<ContainerDetailsOnlineModel> containerdetailsbyorderid) {
        this.containerdetailsbyorderid = containerdetailsbyorderid;
    }

    public String getOrderId() {
        return OrderId;
    }

    public void setOrderId(String orderId) {
        OrderId = orderId;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

    public String getTrade() {
        return Trade;
    }

    public void setTrade(String trade) {
        Trade = trade;
    }

    public String getTotalOrderUsValue() {
        return TotalOrderUsValue;
    }

    public void setTotalOrderUsValue(String totalOrderUsValue) {
        TotalOrderUsValue = totalOrderUsValue;
    }

    public String getTotalOrderZarValue() {
        return TotalOrderZarValue;
    }

    public void setTotalOrderZarValue(String totalOrderZarValue) {
        TotalOrderZarValue = totalOrderZarValue;
    }

    public String getTotalCartons() {
        return TotalCartons;
    }

    public void setTotalCartons(String totalCartons) {
        TotalCartons = totalCartons;
    }

    public String getTotalBooth() {
        return TotalBooth;
    }

    public void setTotalBooth(String totalBooth) {
        TotalBooth = totalBooth;
    }

    public String getTotalOrderWeight() {
        return TotalOrderWeight;
    }

    public void setTotalOrderWeight(String totalOrderWeight) {
        TotalOrderWeight = totalOrderWeight;
    }

    public String getOrderStatus() {
        return OrderStatus;
    }

    public void setOrderStatus(String orderStatus) {
        OrderStatus = orderStatus;
    }

    public String getLastUpdate() {
        return LastUpdate;
    }

    public void setLastUpdate(String lastUpdate) {
        LastUpdate = lastUpdate;
    }

    public String getTotalOrderCBM() {
        return TotalOrderCBM;
    }

    public void setTotalOrderCBM(String totalOrderCBM) {
        TotalOrderCBM = totalOrderCBM;
    }

    public String getTotalContainer() {
        return TotalContainer;
    }

    public void setTotalContainer(String totalContainer) {
        TotalContainer = totalContainer;
    }

    public String getTotal3rdCurrencyValue() {
        return Total3rdCurrencyValue;
    }

    public void setTotal3rdCurrencyValue(String total3rdCurrencyValue) {
        Total3rdCurrencyValue = total3rdCurrencyValue;
    }

    public String getTotalOrderRmbValue() {
        return TotalOrderRmbValue;
    }

    public void setTotalOrderRmbValue(String totalOrderRmbValue) {
        TotalOrderRmbValue = totalOrderRmbValue;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getCompanyId() {
        return CompanyId;
    }

    public void setCompanyId(String companyId) {
        CompanyId = companyId;
    }
}
